<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/public/partials
 */
?>
<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//$req_headers = apache_request_headers();
//echo '<pre>';print_r($req_headers);echo '</pre>';
global $MWQS_OF;
$is_valid_user = false;
if(is_user_logged_in() && (current_user_can('editor') || current_user_can('administrator'))){	
	$is_valid_user = true;
}
$go_back_txt = '<a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync').'">Go back to plugin admin</a>';
if(!$is_valid_user){	
	die($MWQS_OF->get_html_msg(__('Not Authorized','mw_wc_qbo_sync'),'<h1>'.__('Not Authorized','mw_wc_qbo_sync').'</h1>'));
}


global $MSQS_QL;
global $wpdb;

if($MSQS_QL->is_connected()){
	$count_msg = '';
	$customer_count = (int) $MSQS_QL->quick_refresh_qbo_customers();
	$product_count = (int) $MSQS_QL->quick_refresh_qbo_products();
	
	$count_msg.=__('<p>Total Customers Recognized: '.$customer_count.'</p>','mw_wc_qbo_sync');
	$count_msg.=__('<p>Total Products Recognized: '.$product_count.'</p>','mw_wc_qbo_sync');
	
	echo $MWQS_OF->get_html_msg(__('Myworks Quickbooks Sync Quick Refresh','mw_wc_qbo_sync'),'<h2>'.__('Quickbooks Online Data Successfully Recognized','mw_wc_qbo_sync').'</h2>'.$count_msg);
}else{
	echo $MWQS_OF->get_html_msg(__('Myworks Quickbooks Sync Quick Refresh','mw_wc_qbo_sync'),'<h2>'.__('Quickbooks Not Connected','mw_wc_qbo_sync').'</h2>');
}
echo $go_back_txt;

