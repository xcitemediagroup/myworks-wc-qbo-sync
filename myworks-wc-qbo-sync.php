<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://myworks.design/software/woocommerce-quickbooks-online-automatic-sync/
 * @since             1.0.0
 * @package           MyWorks_WC_QBO_Sync
 *
 * @wordpress-plugin
 * Plugin Name:       MyWorks Woo Sync for QuickBooks Online
 * Plugin URI:        http://myworks.design/software/woocommerce-quickbooks-online-automatic-sync/
 * Description:       The only WooCommerce plugin to automatically sync your WooCommerce store to QuickBooks Online, all in real-time! Easily sync your orders, customers, inventory and more from your WooCommerce store to QuickBooks Online. Your complete solution to streamline your accounting workflow - with no limits.
 * Version:           1.0.30
 * Author:            MyWorks Design
 * Author URI:        http://myworks.design/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mw_wc_qbo_sync
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'MW_QBO_SYNC_LOG' ) ) {
	define('MW_QBO_SYNC_LOG_DIR', plugin_dir_path(__FILE__) . 'log/');
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_myworks_wc_qbo_sync() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myworks-wc-qbo-sync-activator.php';
	MyWorks_WC_QBO_Sync_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_myworks_wc_qbo_sync() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myworks-wc-qbo-sync-deactivator.php';
	MyWorks_WC_QBO_Sync_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_myworks_wc_qbo_sync' );
register_deactivation_hook( __FILE__, 'deactivate_myworks_wc_qbo_sync' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-myworks-wc-qbo-sync.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_myworks_wc_qbo_sync() {
	$myworks_wc_qbo_sync = new MyWorks_WC_QBO_Sync();	
	$myworks_wc_qbo_sync->run();
}

run_myworks_wc_qbo_sync();