=== MyWorks Woo Sync for QuickBooks Online ===
Contributors: myworksdesign
Donate link: http://myworks.design
Tags: woocommerce, quickbooks, realtime, manual, sync, crm, pull, push, multilingual, multicurrency, multisite, product, tax, payment, customer, coupon, shipping
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 4.8.
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

The only WooCommerce plugin to automatically sync your WooCommerce store to QuickBooks Online, all in real-time! Easily sync your orders, customers, inventory and more from your WooCommerce store to QuickBooks Online. Your complete solution to streamline your accounting workflow - with no limits.

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Follow the step by step wizard to finish your installation and go live.

== Screenshots ==
 
1. This screen shot description corresponds to screenshot-1.(png|jpg). This describes plugin's dashboard

== Changelog ==

V 1.0.0
- First Launch

V1.0.1
- License / connection process update for allow multiple connection of same user.
- Bug fixing and performance improvements.

V1.0.2
- Activate/Deactivate/Upgrade Functions
- Manual Customer Update
- Changes in initial setup alert/notice (after activation)
- Bug fixing and performance improvements.

V1.0.3
- Inventory Pull
- Design improvements
- Bug fixing and performance improvements.

V1.0.4
- Sync WooCommerce Order as QuickBooks Sales Receipt
- Design improvements
- Bug fixing and performance improvements.

V1.0.5
- Product Pull
- Design improvements
- Bug fixing and performance improvements.

V1.0.6
- Update issue resolved
- UI improvement
- Uninstall issue resolved
- Bug fixing and performance improvements.

V1.0.7
- Settings page upgrade
- Error on live server for public refresh issue resolved
- System status page developed
- Bug fixing and performance improvements

V1.0.8
- Webhooks settings added and implemented
- Pull Product description settings added and implemented
- Changes in invoice shipping add
- Bug fixing and performance improvements
- Added loggly api for quickbooks respponse errors
- Make dashboard status dynamic
- Worked in variation mapping page and applied in invoice push

V1.0.9
- Variation support for inventory(qty) update
- Changes in webhooks
- Help functioanlity developed
- Save notification for settings
- Red default admin notice has been removed
- Setup section styling issue has been fixed for all pages
- UI upgradation done
- Bug fixing and code optimization

V1.0.10
- Setup message restriction on other page
- Setup section refresh and map issue resolved
- Invoice / SalesReceipt Update (Push)
- Guest Order Support
- Salect 2 Dropdown Changes
- Bug fixing and performance improvements

V1.0.11
- Auto Reconnect functionality developed
- Changes in Product Automap function
- Added Realtime Sync Tab in settingds
- Bug fixing and performance improvements

V1.0.12
- Module Debug Option for print qbo item object has been added
- Changes inside quick refresh function (allow customer with blank email)
- Bug fixing and performance improvements

V1.0.13
- Dual update functionality added (beta & stable version)
- User preference settings for beta update has been added
- UI improvements
- Bug fixing and performance improvements

V1.0.14
- Changes in inventory update instoxk issue
- Added bootstrap switch instead of jquery switch
- Replace multiselect with item checkboxes in Setting->Realtime Sync
- Changes regarding realtime push on/off
- Changes for guest order payment push
- Bug fixing and performance improvements

V1.0.15
- Activate and Implemented Push All button in push pages (customer,product,payment,order)
- Implemented Sync Status in Push/Pull pages
- Implemented Ajax Select 2 Dropdown on Hover
- Guest Payment Sync Issue resolved
- Duplicate client list in push page resolved
- Ajax issue in sync window has been resolved
- Added cloumn for woocommerce stock in inventory pull page
- Webhook product/inventory update issues resolved
- Trial license functionality improvements done
- Email/Quick refresh cron added
- Plugin debug option (print qbo object in log file in case of qbo response error) added
- Invoice add/update string lenght issue resolved
- Added direct checking customer in qbo if not mapped
- Bug Fixing and Performance Improvement
- Bug fixing and performance improvements

V1.0.16
- Quickbooks ststus column in backend order page
- Quickbooks ststus widget for order details page
- Bug Fixing

V1.0.17
- Realtime Order sync from admin
- Bug Fixing

V1.0.18
- Realtime Product create sync from admin
- Realtime and manual category sync for products (Push and pull)
- Bug Fixing

V1.0.20
- Updated licensing URL to http
- Improved payment syncing for gateways with no actual payment

V1.0.21
- Updated licensing URL to http
- Improved payment syncing for gateways with no actual payment


V1.0.22
- Added Transaction Fee Support (Stripe/WooCommerce Default Currency) 
- Bug Fixes (VAT Error Issue) 
- Improved Sync Status Label in Order Edit Page
- Added Search by Status to Order push page 
- Added Sort icons in list tables 


V1.0.23
- Added Update Product Functionality 
- Changes in billing_phone Custom Field Map Option 
- Coupon/Discount recorded as positive number issue (When added in line item) 

V1.0.25
- Modified real-time payment sync hook for stability
- Changes in mapping WooCommerce Variable Products > QB Bundle Products
- Updates in fixing earlier upgraded plugin database tables


V1.0.26
- Minor Bug Fixes
- Simplified Map > Payment Method Screen
- Split off Compatibility tab to standalone plugin

V1.0.27
- Minor Bug Fixes
- Fixed Pull > Category functionality to pull categories with "&" in the name
- Resolved minor issues with product update from Woo>QB (description/price/tax overwrite)
- Resolved minor issues with shipping & coupon values in QB not taking on the tax rate of the WooCommerce order

V1.0.28
- Minor Bug Fixes
- Added compatibility with USA e-Pay
- Resolved order_id error in WooCommerce 3.0

V1.0.29
- Minor Bug Fixes

V1.0.30
- Minor Bug Fixes
- Added compatibility with multiple connections (up to 5)





== Upgrade Notice ==
 
= 1.0.0 =
Continuous improvements & enhanced user experience
 
= 1.0.1 =
Continuous improvements & enhanced user experience

= 1.0.2 =
Continuous improvements & enhanced user experience

= 1.0.3 =
Continuous improvements & enhanced user experience

= 1.0.4 =
Continuous improvements & enhanced user experience

= 1.0.5 =
Continuous improvements & enhanced user experience

= 1.0.6 =
Continuous improvements & enhanced user experience

= 1.0.7 =
Continuous improvements & enhanced user experience

= 1.0.8 =
Continuous improvements & enhanced user experience

= 1.0.9 =
Continuous improvements & enhanced user experience

= 1.0.10 =
Continuous improvements & enhanced user experience

= 1.0.11 =
Continuous improvements & enhanced user experience

= 1.0.12 =
Continuous improvements & enhanced user experience

= 1.0.13 =
Continuous improvements & enhanced user experience

= 1.0.14 =
Continuous improvements & enhanced user experience

= 1.0.15 =
Continuous improvements & enhanced user experience
