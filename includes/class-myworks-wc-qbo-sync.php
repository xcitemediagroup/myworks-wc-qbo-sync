<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 * @author     My Works <support@myworks.design>
 */
class MyWorks_WC_QBO_Sync {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      MyWorks_WC_QBO_Sync_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'myworks_wc_qbo_sync';
		$this->version = '1.0.30';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - MyWorks_WC_QBO_Sync_Loader. Orchestrates the hooks of the plugin.
	 * - MyWorks_WC_QBO_Sync_i18n. Defines internationalization functionality.
	 * - MyWorks_WC_QBO_Sync_Admin. Defines all hooks for the admin area.
	 * - MyWorks_WC_QBO_Sync_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		
		/**
		 * The class responsible for defining all qb lib functions.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-myworks-wc-qbo-sync-qbo-lib.php';
		
		
		/**
		 * The class responsible for defining all other functions.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-myworks-wc-qbo-sync-oth-funcs.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-myworks-wc-qbo-sync-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-myworks-wc-qbo-sync-i18n.php';		

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-myworks-wc-qbo-sync-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-myworks-wc-qbo-sync-public.php';

		$this->loader = new MyWorks_WC_QBO_Sync_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the MyWorks_WC_QBO_Sync_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new MyWorks_WC_QBO_Sync_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new MyWorks_WC_QBO_Sync_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'mw_wc_qbo_admin_init' );
		$this->loader->add_action( 'init', $plugin_admin, 'mw_wc_qbo_init' );
		
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );		
		
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'create_admin_menus' );		
		
		//woocommerce hooks for realtime synchronization		
		$this->loader->add_action( 'before_delete_post', $plugin_admin, 'mw_qbo_wc_product_delete' );
		$this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'mw_qbo_wc_product_save', 999, 2 );
		$this->loader->add_action( 'woocommerce_payment_complete', $plugin_admin, 'mw_qbo_wc_order_payment' );	
		$this->loader->add_action( 'user_register', $plugin_admin, 'myworks_wc_qbo_sync_registration_realtime' );
		$this->loader->add_action( 'woocommerce_thankyou', $plugin_admin, 'myworks_wc_qbo_sync_order_realtime' );
		//$this->loader->add_action( 'woocommerce_thankyou', $plugin_admin, 'mw_qbo_wc_order_payment' );
		$this->loader->add_action( 'woocommerce_order_refunded', $plugin_admin, 'mw_wc_qbo_sync_woocommerce_order_refunded' );
		// ACTION FOR ADMIN (BACK END) ORDER
		$this->loader->add_action( 'save_post_shop_order', $plugin_admin, 'myworks_wc_qbo_sync_order_realtime' );
		$this->loader->add_action( 'create_product_cat', $plugin_admin, 'myworks_wc_qbo_sync_product_category_realtime' );
		$this->loader->add_action( 'woocommerce_product_set_stock', $plugin_admin, 'myworks_wc_qbo_sync_update_stock' );
		
		$this->loader->add_action( 'woocommerce_variation_set_stock', $plugin_admin, 'myworks_wc_qbo_sync_variation_update_stock' );
		
		//Ajax Actions
		add_action( 'wp_ajax_myworks_wc_qbo_sync_check_license', 'myworks_wc_qbo_sync_check_license' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_refresh_log_chart', 'mw_wc_qbo_sync_refresh_log_chart' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_window', 'mw_wc_qbo_sync_window' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_clear_all_mappings', 'mw_wc_qbo_sync_clear_all_mappings' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_automap_customers', 'mw_wc_qbo_sync_automap_customers' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_automap_products', 'mw_wc_qbo_sync_automap_products' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_clear_all_logs', 'mw_wc_qbo_sync_clear_all_logs' );
		add_action( 'wp_ajax_mw_wc_qbo_sync_clear_all_log_errors', 'mw_wc_qbo_sync_clear_all_log_errors' );
		
		add_action( 'wp_ajax_mw_wc_qbo_sync_automap_variations', 'mw_wc_qbo_sync_automap_variations' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new MyWorks_WC_QBO_Sync_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		//
		$this->loader->add_action( 'init', $plugin_public, 'mw_qbo_sync_public_api_init_internal' );
		$this->loader->add_filter( 'query_vars', $plugin_public, 'mw_qbo_sync_public_api_query_vars' );
		$this->loader->add_action( 'parse_request', $plugin_public, 'mw_qbo_sync_public_api_request' );
		
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    MyWorks_WC_QBO_Sync_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}