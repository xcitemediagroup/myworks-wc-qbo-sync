<?php

/**
 * Fired during plugin activation
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 * @author     My Works <support@myworks.design>
 */ 

require_once plugin_dir_path( __FILE__ ) . 'lib/qbo-lib/QuickBooks.php';
require_once plugin_dir_path( __FILE__ ) . 'lib/simple-http-client-master/lib/SimpleHTTPClient.php';

class MyWorks_WC_QBO_Sync_QBO_Lib {
	protected $Context;
	protected $realm;
	
	protected $IPP;

	protected $creds;
	protected $is_connected;
	
	protected $mw_wc_qbo_sync_plugin_options;
	protected $qbo_company_preferences = false;
	protected $qbo_company_info = false;
	var $qbo_query_limit = 1000;
	
	protected $quickbooks_connection_dashboard_url='https://myworks.design/account';
	
	public function __construct(){
		if($this->Context=='' && $this->realm==''){
			$this->creds();
			$this->connect();			
		}
		if(session_id() == '' || !isset($_SESSION)) {
			// session isn't started
			session_start();
		}
		if(!$this->mw_wc_qbo_sync_plugin_options || !count($this->mw_wc_qbo_sync_plugin_options)){
			$this->set_plugin_options();
		}
		
		if($this->is_connected() && !$this->qbo_company_preferences){
			$this->get_qbo_company_preferences();
		}
		
		if($this->is_connected() && !$this->qbo_company_info){
			$this->set_qbo_company_info();
		}
		
	}
	
	function remove_scheme_from_url($url) {
	   $disallowed = array('http://', 'https://');
	   foreach($disallowed as $d) {
		  if(strpos($url, $d) === 0) {
			 return str_replace($d, '//', $url);
		  }
	   }
	   return $url;
	}
	
	public function get_dashboard_domain(){
		$url = $this->quickbooks_connection_dashboard_url;
		$url = parse_url($url, PHP_URL_HOST);
		return $url;
	}
	
	public function get_quickbooks_connection_dashboard_url($remove_scheme=false){
		if($remove_scheme){
			return $this->remove_scheme_from_url($this->quickbooks_connection_dashboard_url);
		}
		return $this->quickbooks_connection_dashboard_url;
	}
	
	public function set_plugin_options(){
		global $wpdb;
		$option_arr = array();
		$option_data = $this->get_data("SELECT * FROM ".$wpdb->options." WHERE `option_name` LIKE 'mw_wc_qbo_sync%' ");
		if(is_array($option_data) && count($option_data)){
			foreach($option_data as $Option){
				$option_arr[$Option['option_name']] = $Option['option_value'];
			}
		}
		$this->mw_wc_qbo_sync_plugin_options = $option_arr;
	}
	
	public function qbo_clear_braces($resp){
		preg_match("/\d+/i", $resp, $match);
		return (isset($match[0]))?$match[0]:$resp;
	}
	
	//Quickbooks Dropdowns
	//Product Dropdown
	public function get_product_list_array($realtime=false){
		$options = '';		
		if($this->is_connected() && $realtime){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $ItemService->query($Context, $realm, "SELECT COUNT(*)  FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$items = $ItemService->query($Context, $realm, "SELECT Id , Name FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ORDER BY Name ASC STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($items && count($items)>0){
					foreach($items as $Item){						
						$item_id = $this->qbo_clear_braces($Item->getId());												
						$options[$item_id] = $Item->getName();
					}
				}
			}
		}else{
			global $wpdb;
			$whr='';
			$product_list = $this->get_data("SELECT `itemid` , `name` FROM `".$wpdb->prefix.'mw_wc_qbo_sync_qbo_items'."` WHERE `ID` >0 ".$whr." ORDER BY `name` ASC");
			if(is_array($product_list) && count($product_list)){
				foreach($product_list as $product){
					$options[$product['itemid']] = $product['name'];
				}
			}
		}
		return $options;
	}
	
	public function get_product_dropdown_list($s_val='',$realtime=false){
		$options = '';		
		if($this->is_connected() && $realtime){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $ItemService->query($Context, $realm, "SELECT COUNT(*)  FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$items = $ItemService->query($Context, $realm, "SELECT Id , Name FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ORDER BY Name ASC STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($items && count($items)>0){
					foreach($items as $Item){
						if($Item->countUnitPrice()){
							$item_id = $this->qbo_clear_braces($Item->getId());
							$selected = '';
							if($s_val==$item_id){
								$selected = ' selected="selected" ';
							}						
							$options.= '<option '.$selected.' value="'.$item_id.'">'.$Item->getName().'</option>';
						}						
					}
				}
			}
		}else{
			global $wpdb;			
			$options.= $this->option_html($s_val, $wpdb->prefix.'mw_wc_qbo_sync_qbo_items','itemid','name','','name ASC','',true);
			
		}
		return $options;
	}
	
	//Term Dropdown
	public function get_term_dropdown_list($s_val=''){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$TermService = new QuickBooks_IPP_Service_Term();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $TermService->query($Context, $realm, "SELECT COUNT(*)  FROM Term");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$terms = $TermService->query($Context, $realm, "SELECT Id , Name FROM Term STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($terms && count($terms)>0){
					foreach($terms as $Term){						
						$t_id = $this->qbo_clear_braces($Term->getId());
						$selected = '';
						if($s_val==$t_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$t_id.'">'.$Term->getName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Vendor Dropdown
	public function get_vendor_dropdown_list($s_val='',$return_obj_data=false){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$VendorService = new QuickBooks_IPP_Service_Vendor();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $VendorService->query($Context, $realm, "SELECT COUNT(*)  FROM Vendor");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$vendors = $VendorService->query($Context, $realm, "SELECT Id , DisplayName FROM Vendor STARTPOSITION $startPos MaxResults $qboMaxLimit ");
				if($return_obj_data){
					return $vendors;
				}
				if($vendors && count($vendors)>0){
					foreach($vendors as $Vendor){						
						$v_id = $this->qbo_clear_braces($Vendor->getId());
						$selected = '';
						if($s_val==$v_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$v_id.'">'.$Vendor->getDisplayName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Tax Code Dropdown
	public function get_tax_code_dropdown_list($s_val=''){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$TaxCodeService = new QuickBooks_IPP_Service_TaxCode();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $TaxCodeService->query($Context, $realm, "SELECT COUNT(*)  FROM TaxCode");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$taxcodes = $TaxCodeService->query($Context, $realm, "SELECT Id , Name FROM TaxCode STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($taxcodes && count($taxcodes)>0){
					foreach($taxcodes as $TaxCode){						
						$tc_id = $this->qbo_clear_braces($TaxCode->getId());
						$selected = '';
						if($s_val==$tc_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$tc_id.'">'.$TaxCode->getName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Department Dropdown
	public function get_department_dropdown_list($s_val=''){
		$options = '';
		if($this->is_connected() && $this->get_qbo_company_setting('TrackDepartments')){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$DepartmentService = new QuickBooks_IPP_Service_Department();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $DepartmentService->query($Context, $realm, "SELECT COUNT(*)  FROM Department");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$Departments = $DepartmentService->query($Context, $realm, "SELECT Id , Name FROM Department STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($Departments && count($Departments)>0){
					foreach($Departments as $Department){						
						$class_id = $this->qbo_clear_braces($Department->getId());
						$selected = '';
						if($s_val==$class_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$class_id.'">'.$Department->getName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Class Dropdown
	public function get_class_dropdown_list($s_val=''){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ClassService = new QuickBooks_IPP_Service_Class();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $ClassService->query($Context, $realm, "SELECT COUNT(*)  FROM Class");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$classes = $ClassService->query($Context, $realm, "SELECT Id , Name FROM Class STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($classes && count($classes)>0){
					foreach($classes as $Class){						
						$class_id = $this->qbo_clear_braces($Class->getId());
						$selected = '';
						if($s_val==$class_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$class_id.'">'.$Class->getName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Payment Method Dropdown
	public function get_payment_method_dropdown_list($s_val=''){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$PaymentMethodService = new QuickBooks_IPP_Service_PaymentMethod();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $PaymentMethodService->query($Context, $realm, "SELECT COUNT(*)  FROM PaymentMethod");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$pmethods = $PaymentMethodService->query($Context, $realm, "SELECT Id , Name FROM PaymentMethod STARTPOSITION $startPos MaxResults $qboMaxLimit ");		
				if($pmethods && count($pmethods)>0){
					foreach($pmethods as $PaymentMethod){						
						$pm_id = $this->qbo_clear_braces($PaymentMethod->getId());
						$selected = '';
						if($s_val==$pm_id){
							$selected = ' selected="selected" ';
						}						
						$options.= '<option '.$selected.' value="'.$pm_id.'">'.$PaymentMethod->getName().'</option>';
					}
				}
			}
		}
		return $options;
	}
	
	//Account Dropdown
	public function get_account_list_array($show_ac_type=false){
		$options = array();
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$AccountService = new QuickBooks_IPP_Service_Account();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $AccountService->query($Context, $realm, "SELECT COUNT(*)  FROM Account");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$accounts = $AccountService->query($Context, $realm, "SELECT * FROM Account STARTPOSITION $startPos MaxResults $qboMaxLimit ");				
				if($accounts && count($accounts)>0){
					foreach($accounts as $Account){
						$ac_type = $Account->getAccountType();
						$a_id = $this->qbo_clear_braces($Account->getId());
												
						if($show_ac_type){
							$options[$a_id] =  $Account->getFullyQualifiedName().' ('.$ac_type.')';
						}else{
							$options[$a_id] = $Account->getFullyQualifiedName();
						}
												
					}
				}
			}
		}
	}
	
	public function get_account_dropdown_list($s_val='',$show_ac_type=false){
		$options = '';
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$AccountService = new QuickBooks_IPP_Service_Account();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $AccountService->query($Context, $realm, "SELECT COUNT(*)  FROM Account");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$accounts = $AccountService->query($Context, $realm, "SELECT * FROM Account STARTPOSITION $startPos MaxResults $qboMaxLimit ");				
				if($accounts && count($accounts)>0){
					foreach($accounts as $Account){
						$ac_type = $Account->getAccountType();
						$a_id = $this->qbo_clear_braces($Account->getId());
						$selected = '';
						if($s_val==$a_id){
							$selected = ' selected="selected" ';
						}						
						if($show_ac_type){
							$options.= '<option '.$selected.' value="'.$a_id.'">'.$Account->getFullyQualifiedName().' ('.$ac_type.')'.'</option>';
						}else{
							$options.= '<option '.$selected.' value="'.$a_id.'">'.$Account->getFullyQualifiedName().'</option>';
						}
												
					}
				}
			}
		}
		return $options;
	}
	
	public function get_array_isset($data,$keyword,$default='',$decode=true,$trim=0,$addslash=false,$replace_array=array()){
		$return = $default;
		if(is_array($data) && count($data)){
			if(isset($data[$keyword])){
				$return = $data[$keyword];
				$return = trim($return);
				if($decode){
					$return = htmlspecialchars_decode($return,ENT_QUOTES);
				}				
				if($trim){
					if(strlen($return) > $trim){
						$return = substr($return,0,$trim);
					}
				}
				if($addslash){
					$return = addslashes($return);
				}
				if(is_array($replace_array) && count($replace_array)){
					$return = str_replace($replace_array,'',$return);
				}
			}
		}
		return $return;
	}
	
	/*Restrictions*/
	public function if_sync_customer($wc_cus_id){
		return true;
	}
	
	//14-03-2017
	public function check_save_get_qbo_guest_id($customer_data){
		if($qbo_customerid = $this->if_qbo_guest_exists($customer_data,true)){		
			return $qbo_customerid;
		}
		return $this->AddGuest($customer_data);
	}
	
	public function if_qbo_guest_exists($customer_data,$return_qbo_customert_id=false){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$name_replace_chars = array(':','\t','\n');
			$billing_email = $this->get_array_isset($customer_data,'billing_email','');
			$shipping_company = $this->get_array_isset($customer_data,'shipping_company','',true,100,false,$name_replace_chars);
			
			if($billing_email!=''){
				$CustomerService = new QuickBooks_IPP_Service_Customer();
				if($shipping_company!='' && $this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
					$customers = $CustomerService->query($Context,$realm ,"SELECT * FROM Customer WHERE DisplayName = '{$shipping_company}' ");
				}else{
					$customers = $CustomerService->query($Context,$realm ,"SELECT * FROM Customer WHERE PrimaryEmailAddr = '{$billing_email}' ");
				}
				
				if($customers && count($customers)){
					$customer = $customers[0];					
					
					if($return_qbo_customert_id){
						return $this->qbo_clear_braces($customer->getId());
					}else{
						return $customer;
					}
				}
			}			
			
		}
		return false;	
	}
	
	public function if_sync_guest($email){
		return true;
	}
	
	public function UpdateGuest($customer_data,$qbo_customer_obj=false){
		$manual = $this->get_array_isset($customer_data,'manual',false);
		if($manual){
			$this->set_session_val('sync_window_push_manual_update',true);
		}
		
		if(!$this->is_connected()){
			return false;
		}
		
		if(is_array($customer_data) && count($customer_data)){
			$billing_email = $this->get_array_isset($customer_data,'billing_email','');
			if($billing_email==''){
				return false;
			}
			
			$wc_inv_id = $this->get_array_isset($customer_data,'wc_inv_id',0);
			
			if($this->if_sync_guest($wc_customerid)){
				if($qbo_customer_obj && count($qbo_customer_obj)){
					$customer = $qbo_customer_obj;
				}else{
					$customer = $this->if_qbo_guest_exists($customer_data);
				}				
				
				if(!$customer){
					$this->save_log("Update Customer/Guest Error \n"."Email:{$billing_email}",'QuickBooks Customer Not Found.','Customer',0);
					return false;
				}
				
				$customerService = new QuickBooks_IPP_Service_Customer();
				$Context = $this->Context;
				$realm = $this->realm;
				
				$name_replace_chars = array(':','\t','\n');
					
				$firstname = $this->get_array_isset($customer_data,'billing_first_name','',true,25,false,$name_replace_chars);
				$lastname = $this->get_array_isset($customer_data,'billing_last_name','',true,25,false,$name_replace_chars);
				$company = $this->get_array_isset($customer_data,'billing_company','',true,50,false,$name_replace_chars);				
				$display_name = $this->get_array_isset($customer_data,'display_name','',true,100,false,$name_replace_chars);
				
				if($wc_inv_id && $this->check_qbo_customer_by_display_name($display_name)){
					if(!$this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
						$display_name.=' -'.$wc_inv_id;
					}					
				}
				
				//
				$middlename = '';
				
				$phone = $this->get_array_isset($customer_data,'billing_phone','',true,21);
				$email = $this->get_array_isset($customer_data,'billing_email','',true);
				
				
				$currency = $this->get_array_isset($customer_data,'currency','',true);
				
				$note = $this->get_array_isset($customer_data,'note','',true);
				
				$customer->setGivenName($firstname);
				$customer->setFamilyName($lastname);
				
				$customer->setCompanyName($company);
				$customer->setDisplayName($display_name);
				
				/*
				$primaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
				$primaryEmailAddr->setAddress($email);
				$customer->setPrimaryEmailAddr($primaryEmailAddr);
				*/

				if($phone!=''){
					$PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
					$PrimaryPhone->setFreeFormNumber($phone);
					$customer->setPrimaryPhone($PrimaryPhone);
				}
				if($note!=''){
					$customer->setNotes($note);
				}
				
				if($currency!='' && $this->get_qbo_company_setting('is_m_currency')){
					 $customer->setCurrencyRef("{-$currency}");
				}
				
				$address = $this->get_array_isset($customer_data,'billing_address_1','',true);
				if($address!=''){						
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($address);

					$BillAddr->setLine2($this->get_array_isset($customer_data,'billing_address_2','',true));

					$BillAddr->setCity($this->get_array_isset($customer_data,'billing_city','',true));
					
					$country = $this->get_array_isset($customer_data,'billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'billing_state','',true));
					
					$BillAddr->setPostalCode($this->get_array_isset($customer_data,'billing_postcode','',true));
					$customer->setBillAddr($BillAddr);
				}
				
				//
				$shipping_address = $this->get_array_isset($customer_data,'shipping_address_1','',true);
				if($shipping_address!=''){						
					$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
					$ShipAddr->setLine1($shipping_address);
					
					$ShipAddr->setLine2($this->get_array_isset($customer_data,'shipping_address_2','',true));

					$ShipAddr->setCity($this->get_array_isset($customer_data,'shipping_city','',true));
					
					$country = $this->get_array_isset($customer_data,'shipping_country','',true);
					$country = $this->get_country_name_from_code($country);
					$ShipAddr->setCountry($country);
					
					$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'shipping_state','',true));
					
					$ShipAddr->setPostalCode($this->get_array_isset($customer_data,'shipping_postcode','',true));
					$customer->setShipAddr($ShipAddr);
				}
				
				$log_title = "";
				$log_details = "";
				$log_status = 0;
				
				if ($resp = $customerService->update($Context, $realm, $customer->getId(), $customer)){
					$qbo_customerid = $this->qbo_clear_braces($customer->getId());
					$log_title.="Update Customer/Guest\n";
					$log_title.="Email: {$billing_email}";
					$log_details.="Customer #$wc_customerid has been updated, Quickbooks Customer ID is #$qbo_customerid";
					$log_status = 1;
					$this->save_log($log_title,$log_details,'Customer',$log_status,true);
					$this->add_qbo_item_obj_into_log_file('Customer/Guest Update',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
					$this->save_qbo_customer_local($qbo_customerid,$firstname,$lastname,$middlename,$company,$display_name,$email);
					
					return $qbo_customerid;
				}else{
					$res_err = $customerService->lastError($Context);
					$log_title.="Update Customer/Guest Error\n";
					$log_title.="Email: {$billing_email}";
					$log_details.="Error:$res_err";
					$this->save_log($log_title,$log_details,'Customer',$log_status,true,true);
					$this->add_qbo_item_obj_into_log_file('Customer/Guest Update',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
					return false;
				}				
			}
		}
	}
	
	public function AddGuest($customer_data){
		if(!$this->is_connected()){
			return false;
		}
		if(is_array($customer_data) && count($customer_data)){
			$billing_email = $this->get_array_isset($customer_data,'billing_email','');
			if($billing_email==''){
				return false;
			}
			$wc_inv_id = $this->get_array_isset($customer_data,'wc_inv_id',0);
			
			if($this->if_sync_guest($billing_email)){								
				if(!$this->if_qbo_guest_exists($customer_data)){
					$name_replace_chars = array(':','\t','\n');
					
					$firstname = $this->get_array_isset($customer_data,'billing_first_name','',true,25,false,$name_replace_chars);
					$lastname = $this->get_array_isset($customer_data,'billing_last_name','',true,25,false,$name_replace_chars);
					$company = $this->get_array_isset($customer_data,'billing_company','',true,50,false,$name_replace_chars);
					
					$display_name = $this->get_array_isset($customer_data,'display_name','',true,100,false,$name_replace_chars);
					
					if($wc_inv_id && $this->check_qbo_customer_by_display_name($display_name)){
						if(!$this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
							$display_name.=' -'.$wc_inv_id;
						}						
					}
					
					//
					$middlename = '';
					
					$phone = $this->get_array_isset($customer_data,'billing_phone','',true,21);
					$email = $this->get_array_isset($customer_data,'billing_email','',true);
					
					
					$currency = $this->get_array_isset($customer_data,'currency','',true);
					
					$note = $this->get_array_isset($customer_data,'note','',true);
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$customerService = new QuickBooks_IPP_Service_Customer();
					$customer = new QuickBooks_IPP_Object_Customer();
					
					$customer->setGivenName($firstname);
					$customer->setFamilyName($lastname);
					
					$customer->setCompanyName($company);
					$customer->setDisplayName($display_name);
					
					$primaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
					$primaryEmailAddr->setAddress($email);
					$customer->setPrimaryEmailAddr($primaryEmailAddr);

					if($phone!=''){
						$PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
						$PrimaryPhone->setFreeFormNumber($phone);
						$customer->setPrimaryPhone($PrimaryPhone);
					}
					if($note!=''){
						$customer->setNotes($note);
					}
					
					if($currency!='' && $this->get_qbo_company_setting('is_m_currency')){
						 $customer->setCurrencyRef("{-$currency}");
					}
					
					$address = $this->get_array_isset($customer_data,'billing_address_1','',true);
					if($address!=''){						
						$BillAddr = new QuickBooks_IPP_Object_BillAddr();
						$BillAddr->setLine1($address);

						$BillAddr->setLine2($this->get_array_isset($customer_data,'billing_address_2','',true));

						$BillAddr->setCity($this->get_array_isset($customer_data,'billing_city','',true));
						
						$country = $this->get_array_isset($customer_data,'billing_country','',true);
						$country = $this->get_country_name_from_code($country);
						
						$BillAddr->setCountry($country);
						
						$BillAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'billing_state','',true));
						
						$BillAddr->setPostalCode($this->get_array_isset($customer_data,'billing_postcode','',true));
						$customer->setBillAddr($BillAddr);
					}
					
					//
					$shipping_address = $this->get_array_isset($customer_data,'shipping_address_1','',true);
					if($shipping_address!=''){						
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($shipping_address);
						
						$ShipAddr->setLine2($this->get_array_isset($customer_data,'shipping_address_2','',true));

						$ShipAddr->setCity($this->get_array_isset($customer_data,'shipping_city','',true));
						
						$country = $this->get_array_isset($customer_data,'shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'shipping_state','',true));
						
						$ShipAddr->setPostalCode($this->get_array_isset($customer_data,'shipping_postcode','',true));
						$customer->setShipAddr($ShipAddr);
					}
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					//$this->_p($customer);
					if ($resp = $customerService->add($Context, $realm, $customer)){
						$qbo_customerid = $this->qbo_clear_braces($resp);
						$log_title.="Export Customer/Guest\n";
						$log_title.="Email: {$billing_email}";
						$log_details.="Customer has been exported, Quickbooks Customer ID is #$qbo_customerid";
						$log_status = 1;
						$this->save_log($log_title,$log_details,'Customer',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Customer/Guest Add',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						$this->save_qbo_customer_local($qbo_customerid,$firstname,$lastname,$middlename,$company,$display_name,$email);
						
						return $qbo_customerid;
						
					}else{
						$res_err = $customerService->lastError($Context);
						$log_title.="Export Customer/Guest Error\n";
						$log_title.="Email: {$billing_email}";
						$log_details.="Error:$res_err";
						$this->save_log($log_title,$log_details,'Customer',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Customer/Guest Add',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}			
		}
	}
	
	public function check_save_get_qbo_customer_id($customer_data){		
		$wc_customerid = $this->get_array_isset($customer_data,'wc_customerid',0);
		if($qbo_customerid = $this->if_qbo_customer_exists($customer_data,true)){
			return $qbo_customerid;
		}
		return $this->AddCustomer($customer_data);
	}
	
	public function qbo_real_time_customer_check_get_object($qbo_customerid){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$customerService = new QuickBooks_IPP_Service_Customer();
			$customerData = $customerService->query($Context, $realm, "SELECT * FROM Customer WHERE Id = '".$qbo_customerid."' ");
			//$this->_p($customerData,true);
			if($customerData && count($customerData)){
				return $customerData[0];
			}
			return false;
		}
	}
	
	public function if_qbo_customer_exists($customer_data,$return_qbo_customert_id=false,$realtime_check_get_obj=false){		
		global $wpdb;
		$name_replace_chars = array(':','\t','\n');
		
		$wc_customerid = (int) $this->get_array_isset($customer_data,'wc_customerid','',true);
		$display_name = $this->get_array_isset($customer_data,'display_name','',true,100,false,$name_replace_chars);
		$email = $this->get_array_isset($customer_data,'email','',true);
		
		$shipping_company = $this->get_array_isset($customer_data,'shipping_company','',true,100,false,$name_replace_chars);
		
		//31-05-2017
		if($shipping_company!='' && $this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
			$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_customers';
			$query = $wpdb->prepare("SELECT `qbo_customerid` FROM `$table` WHERE `dname` = %s AND `dname` !='' ",$shipping_company);
		}else{
			//Map table
			$table = $wpdb->prefix.'mw_wc_qbo_sync_customer_pairs';
			$query = $wpdb->prepare("SELECT `qbo_customerid` FROM `$table` WHERE `wc_customerid` = %d AND `qbo_customerid` >0 AND `wc_customerid` > 0 ",$wc_customerid);
			
			//Qbo customer table
			if(!count($this->get_data($query))){
				$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_customers';
				$query = $wpdb->prepare("SELECT `qbo_customerid` FROM `$table` WHERE `email` = %s AND `email` !='' ",$email);			
			}
		}
		
		$query_customer = $this->get_row($query);
		//$this->_p($query_customer,true);die;
		
		//31-03-2017
		if(empty($query_customer) && $this->option_checked('mw_wc_qbo_sync_customer_qbo_check')){
			if($email!=''){
				$Context = $this->Context;
				$realm = $this->realm;
				
				$CustomerService = new QuickBooks_IPP_Service_Customer();
				
				if($shipping_company!='' && $this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
					$customers = $CustomerService->query($Context,$realm ,"SELECT * FROM Customer WHERE DisplayName = '{$shipping_company}' ");
				}else{
					$customers = $CustomerService->query($Context,$realm ,"SELECT * FROM Customer WHERE PrimaryEmailAddr = '{$email}' ");
				}
				
				//$this->_p($customer);die;
				if($customers && count($customers)){
					$customer = $customers[0];					
					
					if($return_qbo_customert_id){
						if($realtime_check_get_obj){
							return $customer;
						}
						return $this->qbo_clear_braces($customer->getId());
					}
					return true;
				}
			}
			return false;
		}
		
		if($return_qbo_customert_id){
			if($realtime_check_get_obj){
				//$this->_p($query_customer,true);
				if(is_array($query_customer) && count($query_customer)){
					return $this->qbo_real_time_customer_check_get_object($query_customer['qbo_customerid']);
				}
				return false;
			}
			return (is_array($query_customer) && count($query_customer))?$query_customer['qbo_customerid']:0;
		}
		return (is_array($query_customer) && count($query_customer))?true:false;
		
	}
	
	//01-02-2017
	public function check_save_automap_customer_data($w_cus,$all_qbo_customers){
		global $wpdb;
		$map_tbl = $wpdb->prefix."mw_wc_qbo_sync_customer_pairs";
		$user_email = $w_cus['user_email'];
		$user_email = $this->sanitize($user_email);
		
		foreach($all_qbo_customers as $q_cus){
			if($user_email!='' && $user_email==$q_cus['email']){
				$save_data = array();
				$save_data['wc_customerid'] = $w_cus['ID'];
				$save_data['qbo_customerid'] = $q_cus['qbo_customerid'];
				$wpdb->insert($map_tbl,$save_data);
				break;
			}
		}
	}
	public function AutoMapCustomer(){
		global $wpdb;
		//21-02-2017
		$roles = 'customer'; // we can use multiple role comma separeted
		if ( ! is_array( $roles ) ){
			//$roles = array_walk( explode( ",", $roles ), 'trim' );
			$roles = array_map('trim',explode( ",", $roles ));
		}
		
		$sql = '
			SELECT  ' . $wpdb->users . '.ID, ' . $wpdb->users . '.display_name, ' . $wpdb->users . '.user_email
			FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
			ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id			
			WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
			AND     (
		';
		$i = 1;
		foreach ( $roles as $role ) {
			$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%%"' . $role . '"%%\' ';
			if ( $i < count( $roles ) ) $sql .= ' OR ';
			$i++;
		}
		$sql .= ' ) ';		
		
		//$sql = "SELECT `ID` , `user_email` , `display_name` FROM ".$wpdb->users."";
		
		$all_wc_customers = $this->get_data($sql);
		$all_qbo_customers = $this->get_data("SELECT `qbo_customerid`, `email` , `dname` FROM ".$wpdb->prefix."mw_wc_qbo_sync_qbo_customers");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` ");
		
		if(is_array($all_wc_customers) && count($all_wc_customers) && is_array($all_qbo_customers) && count($all_qbo_customers)){
			foreach($all_wc_customers as $w_cus){
				$this->check_save_automap_customer_data($w_cus,$all_qbo_customers);
			}
		}
		unset($all_wc_customers);
		unset($all_qbo_customers);
	}
	
	//24-03-2017
	public function AutoMapCustomerNew(){		
		global $wpdb;
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` ");
		
		$roles = 'customer'; // we can use multiple role comma separeted
		if ( ! is_array( $roles ) ){
			//$roles = array_walk( explode( ",", $roles ), 'trim' );
			$roles = array_map('trim',explode( ",", $roles ));
		}
		
		$sql_count = '
			SELECT  COUNT(DISTINCT(' . $wpdb->users . '.ID))
			FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
			ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id,
			'.$wpdb->prefix.'mw_wc_qbo_sync_qbo_customers qc
			WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'			
			AND     (
		';
		$i = 1;
		foreach ( $roles as $role ) {
			$sql_count .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%%"' . $role . '"%%\' ';
			if ( $i < count( $roles ) ) $sql_count .= ' OR ';
			$i++;
		}
		$sql_count .= ' ) ';
		
		$sql_count.=' AND '. $wpdb->users . '.user_email !=\'\' ';
		$sql_count.=' AND '. $wpdb->users . '.user_email = qc.email';		
		
		$max_limit = 1000;
		
		$count = (int) $wpdb->get_var($sql_count);
		if(!$count){return false;}
		
		$batchCount =  ($max_limit >= $count) ? 1 : ceil($count / $max_limit);
		
		for ($i=0; $i<$batchCount; $i++) {
			$startPos = $i*$max_limit;			
			$sql = '
				SELECT  DISTINCT(' . $wpdb->users . '.ID),qc.qbo_customerid
				FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
				ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id,
				'.$wpdb->prefix.'mw_wc_qbo_sync_qbo_customers qc
				WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'			
				AND     (
			';
			$j = 1;
			foreach ( $roles as $role ) {
				$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%%"' . $role . '"%%\' ';
				if ( $j < count( $roles ) ) $sql .= ' OR ';
				$j++;
			}
			$sql .= ' ) ';
			
			$sql.=' AND '. $wpdb->users . '.user_email !=\'\' ';
			$sql.=' AND '. $wpdb->users . '.user_email = qc.email ';
			$sql.=' GROUP BY '. $wpdb->users . '.ID';
			$sql.=" LIMIT {$startPos},{$max_limit}";
			
			$match_data = $this->get_data($sql);
			//$this->_p($match_data);continue;
			$c_map_ivs = '';
			if(is_array($match_data) && count($match_data)){
				foreach($match_data as $md){
					$c_map_ivs.='('.$md['ID'].','.$md['qbo_customerid'].'),';
				}
			}
			if($c_map_ivs!=''){
				$c_map_ivs = substr($c_map_ivs,0,-1);
				$c_map_insert_q = "INSERT INTO {$wpdb->prefix}mw_wc_qbo_sync_customer_pairs (wc_customerid,qbo_customerid) VALUES {$c_map_ivs}";
				$wpdb->query($c_map_insert_q);
			}
		}
		
		return $count;
	}
	
	//21-02-2017
	public function check_save_automap_product_data($w_pro,$all_qbo_products){
		global $wpdb;
		$map_tbl = $wpdb->prefix."mw_wc_qbo_sync_product_pairs";
		$sku = $this->sanitize($w_pro['sku']);
		$name = $this->sanitize($w_pro['name']);
		
		foreach($all_qbo_products as $q_pro){
			//15-03-2017
			$is_match_map_product = false;
			
			if($sku!=''){
				if($sku==$q_pro['sku']){
					$is_match_map_product = true;
				}
				if($q_pro['sku']=='' && $sku==$q_pro['name']){
					$is_match_map_product = true;
				}
			}
			
			if($is_match_map_product){
				$save_data = array();
				$save_data['wc_product_id'] = $w_pro['ID'];
				$save_data['quickbook_product_id'] = $q_pro['itemid'];
				$wpdb->insert($map_tbl,$save_data);
				break;
			}
		}
	}
	
	public function AutoMapProduct(){
		global $wpdb;
		$status = 'publish';
		$sql = "
			SELECT DISTINCT(p.ID), p.post_title AS name, pm1.meta_value AS sku
			FROM ".$wpdb->posts." p
			LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
			AND pm1.meta_key =  '_sku' )			
			WHERE p.post_type =  'product'
			AND p.post_status = '".$status."'
			AND pm1.meta_value!=''		
		";
		$all_wc_products = $this->get_data($sql);
		$all_qbo_products = $this->get_data("SELECT `itemid`, `sku` , `name` FROM ".$wpdb->prefix."mw_wc_qbo_sync_qbo_items");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` ");
		
		if(is_array($all_wc_products) && count($all_wc_products) && is_array($all_qbo_products) && count($all_qbo_products)){
			foreach($all_wc_products as $w_pro){
				$this->check_save_automap_product_data($w_pro,$all_qbo_products);
			}
		}
		unset($all_wc_products);
		unset($all_qbo_products);
	}
	
	//26-04-2017
	public function AutoMapVariation(){
		global $wpdb;
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` ");
		
		$status = 'publish';
		$sql_count = "
			SELECT COUNT(DISTINCT(p.ID))
			FROM ".$wpdb->posts." p
			LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
			AND pm1.meta_key =  '_sku' ),
			".$wpdb->prefix."mw_wc_qbo_sync_qbo_items qp
			WHERE p.post_type =  'product_variation'
			AND p.post_status NOT IN('trash','auto-draft','inherit')
			AND pm1.meta_value!=''
			AND qp.itemid > 0
			AND (qp.sku=pm1.meta_value OR (qp.sku='' AND pm1.meta_value=qp.name)) 
		";
		//AND p.post_status = '".$status."'		
		
		$max_limit = 1000;
		
		$count = (int) $wpdb->get_var($sql_count);
		if(!$count){return false;}
		
		$batchCount =  ($max_limit >= $count) ? 1 : ceil($count / $max_limit);
		
		for ($i=0; $i<$batchCount; $i++) {
			$startPos = $i*$max_limit;
			
			$sql = "
				SELECT DISTINCT(p.ID), qp.itemid
				FROM ".$wpdb->posts." p
				LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
				AND pm1.meta_key =  '_sku' ),
				".$wpdb->prefix."mw_wc_qbo_sync_qbo_items qp
				WHERE p.post_type =  'product_variation'
				AND p.post_status NOT IN('trash','auto-draft','inherit')
				AND pm1.meta_value!=''
				AND qp.itemid > 0
				AND (qp.sku=pm1.meta_value OR (qp.sku='' AND pm1.meta_value=qp.name))
				GROUP BY p.ID
				LIMIT {$startPos},{$max_limit}
			";
			//AND p.post_status = '".$status."'
			//echo $sql;
			$match_data = $this->get_data($sql);
			$p_map_ivs = '';
			if(is_array($match_data) && count($match_data)){
				foreach($match_data as $md){
					$p_map_ivs.='('.$md['ID'].','.$md['itemid'].'),';
				}
			}
			if($p_map_ivs!=''){
				$p_map_ivs = substr($p_map_ivs,0,-1);
				$p_map_insert_q = "INSERT INTO {$wpdb->prefix}mw_wc_qbo_sync_variation_pairs (wc_variation_id,quickbook_product_id) VALUES {$p_map_ivs}";
				$wpdb->query($p_map_insert_q);
			}
		}
		return $count;
	}
	
	//24-03-2017
	public function AutoMapProductNew(){
		global $wpdb;
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` ");
		
		$status = 'publish';
		$sql_count = "
			SELECT COUNT(DISTINCT(p.ID))
			FROM ".$wpdb->posts." p
			LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
			AND pm1.meta_key =  '_sku' ),
			".$wpdb->prefix."mw_wc_qbo_sync_qbo_items qp
			WHERE p.post_type =  'product'
			AND p.post_status NOT IN('trash','auto-draft','inherit')
			AND pm1.meta_value!=''
			AND qp.itemid > 0
			AND (qp.sku=pm1.meta_value OR (qp.sku='' AND pm1.meta_value=qp.name)) 
		";
		//AND p.post_status = '".$status."'		
		
		$max_limit = 1000;
		
		$count = (int) $wpdb->get_var($sql_count);
		if(!$count){return false;}
		
		$batchCount =  ($max_limit >= $count) ? 1 : ceil($count / $max_limit);
		
		for ($i=0; $i<$batchCount; $i++) {
			$startPos = $i*$max_limit;
			
			$sql = "
				SELECT DISTINCT(p.ID), qp.itemid
				FROM ".$wpdb->posts." p
				LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
				AND pm1.meta_key =  '_sku' ),
				".$wpdb->prefix."mw_wc_qbo_sync_qbo_items qp
				WHERE p.post_type =  'product'
				AND p.post_status NOT IN('trash','auto-draft','inherit')
				AND pm1.meta_value!=''
				AND qp.itemid > 0
				AND (qp.sku=pm1.meta_value OR (qp.sku='' AND pm1.meta_value=qp.name))
				GROUP BY p.ID
				LIMIT {$startPos},{$max_limit}
			";
			//AND p.post_status = '".$status."'
			//echo $sql;
			$match_data = $this->get_data($sql);
			$p_map_ivs = '';
			if(is_array($match_data) && count($match_data)){
				foreach($match_data as $md){
					$p_map_ivs.='('.$md['ID'].','.$md['itemid'].'),';
				}
			}
			if($p_map_ivs!=''){
				$p_map_ivs = substr($p_map_ivs,0,-1);
				$p_map_insert_q = "INSERT INTO {$wpdb->prefix}mw_wc_qbo_sync_product_pairs (wc_product_id,quickbook_product_id) VALUES {$p_map_ivs}";
				$wpdb->query($p_map_insert_q);
			}
		}
		return $count;
	}
	
	/**
	 * Update Customer Into Quickbooks Online.
	 *
	 * @since    1.0.1
	 * Last Updated: 2017-02-20
	 */
	
	public function UpdateCustomer($customer_data){
		$manual = $this->get_array_isset($customer_data,'manual',false);
		if($manual){
			$this->set_session_val('sync_window_push_manual_update',true);
		}
		
		if(!$this->is_connected()){
			return false;
		}
		if(is_array($customer_data) && count($customer_data)){
			$wc_customerid = $this->get_array_isset($customer_data,'wc_customerid',0);
			//$qbo_customerid = $this->get_array_isset($customer_data,'qbo_customerid',0);
			
			if($this->if_sync_customer($wc_customerid)){
				$customer = $this->if_qbo_customer_exists($customer_data,true,true);
				
				if(!$customer){
					$this->save_log('Update Customer Error #'.$wc_customerid,'QuickBooks Customer Not Found.','Customer',0);
					return false;
				}
				
				$customerService = new QuickBooks_IPP_Service_Customer();
				$Context = $this->Context;
				$realm = $this->realm;
				
				$name_replace_chars = array(':','\t','\n');
					
				$firstname = $this->get_array_isset($customer_data,'firstname','',true,25,false,$name_replace_chars);
				$lastname = $this->get_array_isset($customer_data,'lastname','',true,25,false,$name_replace_chars);
				$company = $this->get_array_isset($customer_data,'company','',true,50,false,$name_replace_chars);				
				$display_name = $this->get_array_isset($customer_data,'display_name','',true,100,false,$name_replace_chars);
				
				//28-03-2017
				if($this->option_checked('mw_wc_qbo_sync_append_client')){
					if($this->check_qbo_customer_by_display_name($display_name) && !$this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
						$display_name.=' -'.$wc_customerid;
					}
				}
					
				//
				$middlename = '';
				
				$phone = $this->get_array_isset($customer_data,'billing_phone','',true,21);
				$email = $this->get_array_isset($customer_data,'email','',true);
				
				
				$currency = $this->get_array_isset($customer_data,'currency','',true);
				
				$note = $this->get_array_isset($customer_data,'note','',true);
				
				$customer->setGivenName($firstname);
				$customer->setFamilyName($lastname);
				
				$customer->setCompanyName($company);
				$customer->setDisplayName($display_name);
				
				/*
				$primaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
				$primaryEmailAddr->setAddress($email);
				$customer->setPrimaryEmailAddr($primaryEmailAddr);
				*/

				if($phone!=''){
					$PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
					$PrimaryPhone->setFreeFormNumber($phone);
					$customer->setPrimaryPhone($PrimaryPhone);
				}
				if($note!=''){
					$customer->setNotes($note);
				}
				
				if($currency!='' && $this->get_qbo_company_setting('is_m_currency')){
					 $customer->setCurrencyRef("{-$currency}");
				}
				
				$address = $this->get_array_isset($customer_data,'billing_address_1','',true);
				if($address!=''){						
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($address);

					$BillAddr->setLine2($this->get_array_isset($customer_data,'billing_address_2','',true));

					$BillAddr->setCity($this->get_array_isset($customer_data,'billing_city','',true));
					
					$country = $this->get_array_isset($customer_data,'billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'billing_state','',true));
					
					$BillAddr->setPostalCode($this->get_array_isset($customer_data,'billing_postcode','',true));
					$customer->setBillAddr($BillAddr);
				}
				
				//
				$shipping_address = $this->get_array_isset($customer_data,'shipping_address_1','',true);
				if($shipping_address!=''){						
					$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
					$ShipAddr->setLine1($shipping_address);
					
					$ShipAddr->setLine2($this->get_array_isset($customer_data,'shipping_address_2','',true));

					$ShipAddr->setCity($this->get_array_isset($customer_data,'shipping_city','',true));
					
					$country = $this->get_array_isset($customer_data,'shipping_country','',true);
					$country = $this->get_country_name_from_code($country);
					$ShipAddr->setCountry($country);
					
					$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'shipping_state','',true));
					
					$ShipAddr->setPostalCode($this->get_array_isset($customer_data,'shipping_postcode','',true));
					$customer->setShipAddr($ShipAddr);
				}
				
				$log_title = "";
				$log_details = "";
				$log_status = 0;
				
				if ($resp = $customerService->update($Context, $realm, $customer->getId(), $customer)){
					$qbo_customerid = $this->qbo_clear_braces($customer->getId());
					$log_title.="Update Customer #$wc_customerid\n";
					$log_details.="Customer #$wc_customerid has been updated, Quickbooks Customer ID is #$qbo_customerid";
					$log_status = 1;
					$this->save_log($log_title,$log_details,'Customer',$log_status,true);
					$this->add_qbo_item_obj_into_log_file('Customer Update',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
					$this->save_qbo_customer_local($qbo_customerid,$firstname,$lastname,$middlename,$company,$display_name,$email);
					
					return $qbo_customerid;
				}else{
					$res_err = $customerService->lastError($Context);
					$log_title.="Update Customer Error #$wc_customerid\n";
					$log_details.="Error:$res_err";
					$this->save_log($log_title,$log_details,'Customer',$log_status,true,true);
					$this->add_qbo_item_obj_into_log_file('Customer Update',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
					return false;
				}				
			}
		}
		
	}
	public function get_admin_user_dropdown_list($selected=''){
		$user_list = get_users(array('role'=>'Administrator'));
		$u_arr = array();
		if(is_array($user_list) && count($user_list)){
			foreach($user_list as $ul){
				if(isset($ul->data->user_email) && $ul->data->user_email!=''){					
					$u_arr[$ul->ID] = $ul->display_name.' ('.$ul->data->user_email.')';
				}				
			}
			$this->only_option($selected,$u_arr);			
		}
	}
	
	public function get_admin_email_by_id($id){
		$id = (int) $id;		
		if(!$id){return '';}
		
		$user_list = get_users(array('role'=>'Administrator'));
		$u_arr = array();
		if(is_array($user_list) && count($user_list)){
			foreach($user_list as $ul){
				if(isset($ul->data->user_email) && $ul->data->user_email!=''){					
					if($id && $ul->ID==$id){
						return $ul->data->user_email;
					}
				}				
			}			
		}
	}
	
	
	
	public function check_qbo_customer_by_display_name($display_name){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$customerService = new QuickBooks_IPP_Service_Customer();
			$customerCheck = $customerService->query($Context,$realm, "SELECT Id FROM Customer WHERE DisplayName = '{$display_name}' ");
			if($customerCheck && count($customerCheck)){
				return true;
			}
		}
		return false;
	}
	
	public function get_wc_customer_currency($wc_cus_id){
		$wc_cus_id = (int) $wc_cus_id;
		if($wc_cus_id){
			global $wpdb;
			$om = $this->get_row("SELECT `post_id` FROM `{$wpdb->postmeta}` WHERE `meta_key` = '_customer_user' AND `meta_value` = '{$wc_cus_id}' LIMIT 0,1 ");
			if(is_array($om) && count($om)){
				$order_id = (int) $om['post_id'];
				if($order_id){
					$om = $this->get_row("SELECT `meta_value` FROM `{$wpdb->postmeta}` WHERE `meta_key` = '_order_currency' AND `post_id` = {$order_id} LIMIT 0,1 ");
					if(is_array($om) && count($om)){
						return $om['meta_value'];
					}
				}
			}
		}
	}
	
	/**
	 * Add Customer Into Quickbooks Online.
	 *
	 * @since    1.0.0
	 * Last Updated: 2017-01-04
	 */
	
	public function AddCustomer($customer_data){
		if(!$this->is_connected()){
			return false;
		}
		if(is_array($customer_data) && count($customer_data)){
			//$this->_p($customer_data);return false;
			$wc_customerid = $this->get_array_isset($customer_data,'wc_customerid',0);
			if($this->if_sync_customer($wc_customerid)){								
				if(!$this->if_qbo_customer_exists($customer_data)){
					$name_replace_chars = array(':','\t','\n');
					
					$firstname = $this->get_array_isset($customer_data,'firstname','',true,25,false,$name_replace_chars);
					$lastname = $this->get_array_isset($customer_data,'lastname','',true,25,false,$name_replace_chars);
					$company = $this->get_array_isset($customer_data,'company','',true,50,false,$name_replace_chars);				
					$display_name = $this->get_array_isset($customer_data,'display_name','',true,100,false,$name_replace_chars);
					
					//28-03-2017
					if($this->option_checked('mw_wc_qbo_sync_append_client')){
						if($this->check_qbo_customer_by_display_name($display_name) && !$this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
							$display_name.=' -'.$wc_customerid;
						}
					}
					//
					$middlename = '';
					
					$phone = $this->get_array_isset($customer_data,'billing_phone','',true,21);
					$email = $this->get_array_isset($customer_data,'email','',true);
					
					
					$currency = $this->get_array_isset($customer_data,'currency','',true);
					
					$note = $this->get_array_isset($customer_data,'note','',true);
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$customerService = new QuickBooks_IPP_Service_Customer();
					$customer = new QuickBooks_IPP_Object_Customer();
					
					$customer->setGivenName($firstname);
					$customer->setFamilyName($lastname);
					
					$customer->setCompanyName($company);
					$customer->setDisplayName($display_name);
					
					$primaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
					$primaryEmailAddr->setAddress($email);
					$customer->setPrimaryEmailAddr($primaryEmailAddr);

					if($phone!=''){
						$PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
						$PrimaryPhone->setFreeFormNumber($phone);
						$customer->setPrimaryPhone($PrimaryPhone);
					}
					if($note!=''){
						$customer->setNotes($note);
					}
					
					if($currency!='' && $this->get_qbo_company_setting('is_m_currency')){
						 $customer->setCurrencyRef("{-$currency}");
					}
					
					$address = $this->get_array_isset($customer_data,'billing_address_1','',true);
					if($address!=''){						
						$BillAddr = new QuickBooks_IPP_Object_BillAddr();
						$BillAddr->setLine1($address);

						$BillAddr->setLine2($this->get_array_isset($customer_data,'billing_address_2','',true));

						$BillAddr->setCity($this->get_array_isset($customer_data,'billing_city','',true));
						
						$country = $this->get_array_isset($customer_data,'billing_country','',true);
						$country = $this->get_country_name_from_code($country);
						
						$BillAddr->setCountry($country);
						
						$BillAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'billing_state','',true));
						
						$BillAddr->setPostalCode($this->get_array_isset($customer_data,'billing_postcode','',true));
						$customer->setBillAddr($BillAddr);
					}
					
					//
					$shipping_address = $this->get_array_isset($customer_data,'shipping_address_1','',true);
					if($shipping_address!=''){						
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($shipping_address);
						
						$ShipAddr->setLine2($this->get_array_isset($customer_data,'shipping_address_2','',true));

						$ShipAddr->setCity($this->get_array_isset($customer_data,'shipping_city','',true));
						
						$country = $this->get_array_isset($customer_data,'shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($customer_data,'shipping_state','',true));
						
						$ShipAddr->setPostalCode($this->get_array_isset($customer_data,'shipping_postcode','',true));
						$customer->setShipAddr($ShipAddr);
					}
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					//$this->_p($customer);
					if ($resp = $customerService->add($Context, $realm, $customer)){
						$qbo_customerid = $this->qbo_clear_braces($resp);
						$log_title.="Export Customer #$wc_customerid\n";
						$log_details.="Customer #$wc_customerid has been exported, Quickbooks Customer ID is #$qbo_customerid";
						$log_status = 1;
						$this->save_log($log_title,$log_details,'Customer',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Customer Add',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						$this->save_qbo_customer_local($qbo_customerid,$firstname,$lastname,$middlename,$company,$display_name,$email);
						$this->save_customer_map($wc_customerid,$qbo_customerid);
						
						return $qbo_customerid;
						
					}else{
						$res_err = $customerService->lastError($Context);
						$log_title.="Export Customer Error #$wc_customerid\n";
						$log_details.="Error:$res_err";
						$this->save_log($log_title,$log_details,'Customer',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Customer Add',$customer_data,$customer,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}			
		}
	}
	public function save_customer_map($wc_customerid,$qbo_customerid){
		$wc_customerid = intval($wc_customerid);
		$qbo_customerid = intval($qbo_customerid);
		if($wc_customerid && $qbo_customerid){
			global $wpdb;
			$save_data = array();			
			$save_data['qbo_customerid'] = $qbo_customerid;
			$table = $wpdb->prefix.'mw_wc_qbo_sync_customer_pairs';
			
			if($this->get_field_by_val($table,'id','wc_customerid',$wc_customerid)){
				$wpdb->update($table,$save_data,array('wc_customerid'=>$wc_customerid),'',array('%d'));
			}else{
				$save_data['wc_customerid'] = $wc_customerid;
				$wpdb->insert($table, $save_data);
			}
		}
	}
	
	
	//
	public function save_item_map($wc_product_id,$quickbook_product_id,$pull=false,$is_variation=false){
		$wc_product_id = intval($wc_product_id);
		$quickbook_product_id = intval($quickbook_product_id);
		if($wc_product_id && $quickbook_product_id){
			global $wpdb;
			$save_data = array();
			$table = $wpdb->prefix.'mw_wc_qbo_sync_product_pairs';
			$w_p_f = 'wc_product_id';
			if($is_variation){
				$table = $wpdb->prefix.'mw_wc_qbo_sync_variation_pairs';
				$w_p_f = 'wc_variation_id';
			}
			
			if(!$pull){
				$save_data['quickbook_product_id'] = $quickbook_product_id;			
				if($this->get_field_by_val($table,'id',$w_p_f,$wc_product_id)){
					$wpdb->update($table,$save_data,array($w_p_f=>$wc_product_id),'',array('%d'));
				}else{
					$save_data[$w_p_f] = $wc_product_id;
					$wpdb->insert($table, $save_data);
				}
			}else{
				$save_data[$w_p_f] = $wc_product_id;			
				if($this->get_field_by_val($table,'id','quickbook_product_id',$quickbook_product_id)){
					$wpdb->update($table,$save_data,array('quickbook_product_id'=>$quickbook_product_id),'',array('%d'));
				}else{
					$save_data['quickbook_product_id'] = $quickbook_product_id;
					$wpdb->insert($table, $save_data);
				}
			}			
		}
	}
	
	public function save_qbo_customer_local($qbo_customerid,$first,$last,$middle,$company,$dname,$email){
		$qbo_customerid = intval($qbo_customerid);
		if($qbo_customerid){
			global $wpdb;
			$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_customers';
			$save_data = array();
			
			$save_data['first'] = $first;
			$save_data['last'] = $last;
			$save_data['middle'] = $middle;
			
			$save_data['company'] = $company;
			$save_data['dname'] = $dname;
			$save_data['email'] = $email;
			
			$save_data = array_map(array($this, 'trim_add_slash'), $save_data);
			
			if($this->get_field_by_val($table,'id','qbo_customerid',$qbo_customerid)){
				$wpdb->update($table,$save_data,array('qbo_customerid'=>$qbo_customerid),'',array('%d'));
				return $qbo_customerid;
			}else{
				$save_data['qbo_customerid'] = $qbo_customerid;
				$wpdb->insert($table, $save_data);
				$insert_id = $wpdb->insert_id;
				return $insert_id;
			}
			
		}
	}
	
	//
	public function save_qbo_item_local($itemid,$name,$sku,$product_type){
		$itemid = intval($itemid);
		if($itemid){
			global $wpdb;
			$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_items';
			$save_data = array();
			
			$save_data['name'] = $name;
			$save_data['sku'] = $sku;
			$save_data['product_type'] = $product_type;
			
			$save_data = array_map(array($this, 'trim_add_slash'), $save_data);
			
			if($this->get_field_by_val($table,'ID','itemid',$itemid)){
				$wpdb->update($table,$save_data,array('itemid'=>$itemid),'',array('%d'));
				return $itemid;
			}else{
				$save_data['itemid'] = $itemid;
				$wpdb->insert($table, $save_data);
				$insert_id = $wpdb->insert_id;
				return $insert_id;
			}
			
		}
	}
	
	public function trim_add_slash($str){
		return addslashes(trim($str));
	}
	
	//27-04-2017
	public function set_qbo_company_info(){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;

			$CompanyInfoService = new QuickBooks_IPP_Service_CompanyInfo();
			$Info = $CompanyInfoService->get($Context, $realm);
			
			$this->qbo_company_info = $Info;
		}		
	}
	public function get_qbo_company_info($key='country',$debug=false){		
		$return = '';
		if($key!='' && $this->is_connected()){
			
			$Info = $this->qbo_company_info;
			//$this->_p($key);
			if($debug){
				$this->_p($Info);
			}
			
			if(!$Info){return;}

			switch ($key) {
				case 'name':
					if($Info->countCompanyName()){
						$return = $Info->getCompanyName();
					}
					break;

				case 'type':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="CompanyType"]/Value');
					break;

				case 'country':
					if($Info->countCountry()){
						$return = $Info->getCountry();
					}                        
					break;
					
				case 'is_sku_enabled':
					$OfferingSku = $Info->getXPath('//CompanyInfo/NameValue[Name="OfferingSku"]/Value');
					if($OfferingSku=='QuickBooks Online Plus'){
						$return = true;
					}else{
						$return = false;
					}
					break;
					
				case 'is_category_enabled':
					$ItemCategoriesFeature = $Info->getXPath('//CompanyInfo/NameValue[Name="ItemCategoriesFeature"]/Value');					
					if($ItemCategoriesFeature=='true'){
						$return = true;
					}else{
						$return = false;
					}
					break;
					
				case "OfferingSku":				
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="OfferingSku"]/Value');
					break;
				
				case 'AssignedTime':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="AssignedTime"]/Value');
					break;
					
				case 'SubscriptionStatus':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="SubscriptionStatus"]/Value');
					break;
					
				case 'FirstTxnDate':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="FirstTxnDate"]/Value');
					break;
					
				case 'PayrollFeature':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="PayrollFeature"]/Value');
					break;
					
				case 'AccountantFeature':
					$return = $Info->getXPath('//CompanyInfo/NameValue[Name="AccountantFeature"]/Value');
					break;
				
				default:
					# code...
					break;
			}


		}
		return $return;
	}
	
	
	public function get_qbo_company_preferences(){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$q_prf = new QuickBooks_IPP_Service_Preferences();
			$prf = $q_prf->get($Context, $realm);
			$this->qbo_company_preferences = $prf;
		}
	}
	
	public function get_qbo_company_setting($setting=''){		
		$return = false;

		if($setting!='' && $this->is_connected()){
			/*
			$Context = $this->Context;
			$realm = $this->realm;

			$q_prf = new QuickBooks_IPP_Service_Preferences();
			$prf = $q_prf->get($Context, $realm);
			*/
			
			$prf = $this->qbo_company_preferences;			
			//$this->_p($prf);
			
			if(!$prf){
				return $return;
			}
			
			switch ($setting) {
				case 'is_shipping_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countAllowShipping()){
						$return = $prf->getSalesFormsPrefs()->getAllowShipping();
					}
					
					break;
					
				case 'is_discount_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countAllowDiscount()){
						$return = $prf->getSalesFormsPrefs()->getAllowDiscount();
					}
					
					break;
					
				case 'is_deposit_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countAllowDeposit()){
						$return = $prf->getSalesFormsPrefs()->getAllowDeposit();
					}
					
					break;
					
				case 'is_service_date_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countAllowServiceDate()){
						$return = $prf->getSalesFormsPrefs()->getAllowServiceDate();
					}
					
					break;
					
				case 'is_estimate_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countAllowEstimates()){
						$return = $prf->getSalesFormsPrefs()->getAllowEstimates();
					}
					
					break;
					
				case 'is_custom_txn_num_allowed':
					if($prf->countSalesFormsPrefs() && $prf->getSalesFormsPrefs()->countCustomTxnNumbers()){
						$return = $prf->getSalesFormsPrefs()->getCustomTxnNumbers();
					}
					
					break;
				
				case 'is_m_currency':
					if($prf->countCurrencyPrefs()){
						$return = $prf->getCurrencyPrefs()->getMultiCurrencyEnabled();
					}
					
					break;

				case 'h_currency':
					if($prf->countCurrencyPrefs()){
						$return = $prf->getCurrencyPrefs()->getHomeCurrency();
					}
					
					break;

				case 'is_sales_tax':
					if($prf->countTaxPrefs()){
						$return = $prf->getTaxPrefs()->getUsingSalesTax();
					}
					break;
					
				case 'TrackDepartments':
					if($prf->countAccountingInfoPrefs()){
						$return = $prf->getAccountingInfoPrefs()->getTrackDepartments();
					}
					break;
					
				case 'DepartmentTerminology':
					if($prf->countAccountingInfoPrefs()){
						$return = $prf->countAccountingInfoPrefs()->getDepartmentTerminology();
					}
					break;
					
				case 'ClassTrackingPerTxn':
					if($prf->countAccountingInfoPrefs()){
						$return = $prf->getAccountingInfoPrefs()->getClassTrackingPerTxn();
					}
					break;
					
				case 'ClassTrackingPerTxnLine':
					if($prf->countAccountingInfoPrefs()){
						$return = $prf->countAccountingInfoPrefs()->getClassTrackingPerTxnLine();
					}
					break;
				
				default:
					# code...
					break;
			}
		}
		//21-04-2017		
		if($return=='true'){
			$return = true;
		}elseif($return=='false'){
			$return = false;
		}	
		
		return $return;
	}
	
	public function mod_qbo_get_tx_dtls($qbo_tax_code=''){
		$tx_dtls = array();		
		if($qbo_tax_code!='' && $this->is_connected()){
			$qbo_tax_code_w = $this->qbo_clear_braces($qbo_tax_code);
		
			$Context = $this->Context;
			$realm = $this->realm;
			
			$TaxCodeService = new QuickBooks_IPP_Service_TaxCode();
			$taxcodes = $TaxCodeService->query($Context, $realm, "SELECT * FROM TaxCode WHERE Id = '$qbo_tax_code_w' ");
			
			if($taxcodes && count($taxcodes)){
				$TaxCode = $taxcodes[0];
				$tx_dtls['Name'] = $TaxCode->getName();
				$tx_dtls['Active'] = $TaxCode->getActive();
				$tx_dtls['Taxable'] = $TaxCode->getTaxable();
				$tx_dtls['TaxGroup'] = $TaxCode->getTaxGroup();
				$tx_dtls['TaxRateDetail'] = array();
				
				if($TaxCode->countSalesTaxRateList()){
					if($TaxCode->getSalesTaxRateList()->countTaxRateDetail()){							
						for($i=0;$i<$TaxCode->getSalesTaxRateList()->countTaxRateDetail();$i++){
							$TaxRateDetail = $TaxCode->getSalesTaxRateList()->getTaxRateDetail($i);
							$s_rate_details = array();
							$s_rate_details['TaxRateRef'] = $TaxRateDetail->getTaxRateRef();
							$s_rate_details['TaxRateRef_name'] = $TaxRateDetail->getTaxRateRef_name();
							$s_rate_details['TaxTypeApplicable'] = $TaxRateDetail->getTaxTypeApplicable();
							$s_rate_details['TaxOrder'] = $TaxRateDetail->getTaxOrder();
							$s_rate_details['TaxOnTaxOrder'] = $TaxRateDetail->getTaxOnTaxOrder();
							
							$tx_dtls['TaxRateDetail'][] = $s_rate_details;
						}
					}
					
				}
				
			}
		}
		
		return $tx_dtls;
	}
	
	public function get_qbo_tax_code_value_by_key($qbo_tax_code='{-TAX}',$key="TaxRateRef"){		
		if(!$this->is_connected()){
			return;
		}		
		$qbo_tax_code_w = $this->qbo_clear_braces($qbo_tax_code);
		
		$Context = $this->Context;
		$realm = $this->realm;

		$TaxCodeService = new QuickBooks_IPP_Service_TaxCode();
		//$taxcodes = $TaxCodeService->query($Context, $realm, "SELECT * FROM TaxCode");
		$taxcodes = $TaxCodeService->query($Context, $realm, "SELECT * FROM TaxCode WHERE Id = '$qbo_tax_code_w' ");
		$return = '';
		
		if(count($taxcodes)){
			foreach ($taxcodes as $TaxCode){                    
				if($TaxCode->getId()==$qbo_tax_code){

					if($TaxCode->countSalesTaxRateList()){
					   if($TaxCode->getSalesTaxRateList()->countTaxRateDetail()){

						 if($key=='TaxRateRef'){$return = $TaxCode->getSalesTaxRateList()->getTaxRateDetail()->getTaxRateRef();}
						 if($key=='TaxTypeApplicable'){$return = $TaxCode->getSalesTaxRateList()->getTaxRateDetail()->getTaxTypeApplicable();}
						 if($key=='TaxOrder'){$return = $TaxCode->getSalesTaxRateList()->getTaxRateDetail()->getTaxOrder();} 

					   } 
					}

					//if need other data
					if($TaxCode->countActive()){
						if($key == 'Active'){$return = $TaxCode->getActive();}
					}

					if($TaxCode->countTaxable() && $key == 'Taxable'){$return = $TaxCode->getTaxable();}
					if($TaxCode->countName() && $key == 'Name'){$return = $TaxCode->getName();}
					if($TaxCode->countTaxGroup() && $key == 'TaxGroup'){$return = $TaxCode->getTaxGroup();}
					
					break;
				}
			}
		}

		return $return;
	}
	
	public function get_qbo_tax_rate_value_by_key($qbo_tax_rate_code='',$key="RateValue"){
		
		if(!$this->is_connected()){
			return;
		}
		
		$qbo_tax_rate_code_w = $this->qbo_clear_braces($qbo_tax_rate_code);

		$return =($key=="RateValue")?0:'';

		if($qbo_tax_rate_code!=''){

			$Context = $this->Context;
			$realm = $this->realm;

			$TaxRateService = new QuickBooks_IPP_Service_TaxRate();
			//$taxrates = $TaxRateService->query($Context, $realm, "SELECT * FROM TaxRate");
			$taxrates = $TaxRateService->query($Context, $realm, "SELECT * FROM TaxRate WHERE Id = '$qbo_tax_rate_code_w' ");
			foreach ($taxrates as $TaxRate){                    

				if($TaxRate->getId()==$qbo_tax_rate_code){                        

					if($TaxRate->countRateValue()){
						if($key=='RateValue'){
						   $return =  $TaxRate->getRateValue();                              

						}
					}
					//
					
					if($key=='Name'){
						$return =  $TaxRate->getName();
					}
					
					if($key=='Active'){
						$return =  $TaxRate->getActive();
					}
					
					break;

				}
			}
			
		}            
		return $return;
	}
	
	public function get_option($key='',$default=''){
		$option = $default;
		if($key!=''){
			//$this->_p($this->mw_wc_qbo_sync_plugin_options);
			if(is_array($this->mw_wc_qbo_sync_plugin_options) && count($this->mw_wc_qbo_sync_plugin_options) && isset($this->mw_wc_qbo_sync_plugin_options[$key])){				
				$option = $this->mw_wc_qbo_sync_plugin_options[$key];
			}else{
				$option = get_option($key);
			}			
		}
		$option = trim($option);
		return $option;
	}
	
	public function get_all_options($keys=array()){
		$option_arr = array();
		if(isset($this->mw_wc_qbo_sync_plugin_options)){
			$option_arr =  $this->mw_wc_qbo_sync_plugin_options;
		}		
		//
		if(is_array($keys) && count($keys)){
			foreach($keys as $val){
				if(!isset($option_arr[$val])){
					$option_arr[$val] = '';
				}
			}
		}		
		return $option_arr;
	}
	
	public function option_checked($option=''){
		if($this->get_option($option)=='true'){
			return true;
		}
		return false;
	}
	
	public function truncate_number( $number, $precision = 2) {
		$value = ( string )$number;
		preg_match( "/(-+)?\d+(\.\d{1,".$precision."})?/" , $value, $matches );
		return (float) $matches[0];		
		//old code
		if($number>0){
		// Are we negative?
		$negative = $number / abs($number);
		// Cast the number to a positive to solve rounding
		$number = abs($number);
		// Calculate precision number for dividing / multiplying
		$precision = pow(10, $precision);
		// Run the math, re-applying the negative value to ensure returns correctly negative / positive
		return floor( $number * $precision ) / $precision * $negative;
		}else{
			return $number;
		}
	}
	
	public function sp_round($num=''){
		$i_amnt = $num;
		if ($num!='' && $num>0 && strpos($num, '.') !== false) {
			list($before_dot, $after_dot) = explode('.', $num);
			if(strlen($after_dot)>2){
				$first_three_digit = substr($after_dot, 0, 3);
				$last_digit = substr($first_three_digit, -1);
				if($last_digit>5){
					$i_amnt = round($num,2,PHP_ROUND_HALF_DOWN);				
				}else{				
					$first_two_digit = substr($after_dot, 0, 2);
					$i_amnt = $before_dot.'.'.$first_two_digit;
					
					
				}
			}
		}
		
		$i_amnt = floatval($i_amnt);
		return $i_amnt;
	}
	
	public function save_log($log_title='',$log_msg='',$type='',$success=0,$add_into_loggly=false,$qbo_response=false){
		if($log_title!=''){
			global $wpdb;
			$table = $wpdb->prefix.'mw_wc_qbo_sync_log';
			
			$max_log_save_day = intval($this->get_option('mw_wc_qbo_sync_save_log_for'));
			$max_log_save_day = ($max_log_save_day<30)?30:$max_log_save_day;
			
			$log_last_date = date('Y-m-d',strtotime("-$max_log_save_day days"));
			$log_last_date = $log_last_date.' 23:59:59';
			
			$wpdb->query( 
			$wpdb->prepare( 
					"
					DELETE FROM $table
					 WHERE `added_date` < %s					 
					",
					$log_last_date
					)
			);
			
			$log_data = array();
			$log_title = addslashes($log_title);
			$log_msg = addslashes($log_msg);
			
			$log_data['log_title'] = $log_title;
			$log_data['details'] = $log_msg;
			$log_data['log_type'] = $type;
			$log_data['success'] = intval($success);	
			$log_data['added_date'] = $this->now();
			
			$wpdb->insert($table, $log_data);
			//$log_id = $wpdb->insert_id;
			if($add_into_loggly){
				
				$loggly_msg = array();
				
				$s_type = (intval($success))?'success':'error';
				$loggly_msg['type'] = $s_type;
				
				$licensekey = $this->get_option('mw_wc_qbo_sync_license');
				$loggly_msg['licensekey'] = $licensekey;
				
				$loggly_msg['url'] = get_site_url();
				$loggly_msg['title'] = $log_title;				
				$loggly_msg['message'] = $log_msg;	
				
				$loggly_msg['log_type'] = $type;
				
				if($this->is_connected()){
					$realm = $this->realm;
					$loggly_msg['qbo_realm'] = $realm;
				}
				
				/*
				$loggly_msg = '';
				$loggly_msg.= "URL: ".get_site_url()."\n";
				$loggly_msg.="Title: ".$log_title;
				$loggly_msg.="Message: ".$log_msg;
				*/
				
				$this->loggly_api_add_log($loggly_msg);
			}
			
		}
	}
	
	public function get_qbo_salesreceipt_id($wc_inv_id,$wc_inv_num=''){
		 return $this->check_quickbooks_salesreceipt_get_obj($wc_inv_id,$wc_inv_num,true);
	}
	
	
	public function get_qbo_invoice_id($wc_inv_id,$wc_inv_num=''){
		 return $this->check_quickbooks_invoice_get_obj($wc_inv_id,$wc_inv_num,true);
	}
	
	//25-04-2017
	public function if_sync_category($wc_category_id){
		if(!$this->get_qbo_company_info('is_category_enabled')){
			return false;
		}
		return true;
	}
	public function check_category_exists($cat_data, $return_id=false, $get_obj=false){
		//$name = $this->get_array_isset($cat_data,'name','',true);
		$name_replace_chars = array(':');
		$name = $this->get_array_isset($cat_data,'name','',true,100,false,$name_replace_chars);
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$ItemService = new QuickBooks_IPP_Service_Term();
			if($item_data = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Type = 'Category' AND Name = '$name'")){
				$item_id = $this->qbo_clear_braces($item_data[0]->getId());
				if($get_obj){
					return $item_data[0];
				}
				return ($return_id)?$item_id:true;
			}
		}
		return false;
	}
	
	//08-05-2017	
	public function UpdateCategory($cat_data){
		if($this->is_connected()){
			$manual = $this->get_array_isset($cat_data,'manual',false);
			if($manual){
				$this->set_session_val('sync_window_push_manual_update',true);
			}
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$wc_category_id = (int) $this->get_array_isset($cat_data,'term_id',0,true);
			if($this->if_sync_category($wc_category_id)){
				if($qbo_cat_obj = $this->check_category_exists($cat_data,false,true)){
					$ItemService = new QuickBooks_IPP_Service_Item();
					$item = $qbo_cat_obj;
					
					//$name = $this->get_array_isset($cat_data,'name','',true);
					$name_replace_chars = array(':');
					$name = $this->get_array_isset($cat_data,'name','',true,100,false,$name_replace_chars);
					
					//$item->setName($name);
					//$item->setType('Category');
					
					$parent_id = (int) $this->get_array_isset($cat_data,'parent',0);
					if($parent_id){
						$parent_data = (array) get_term($parent_id,'product_cat');
						if(is_array($parent_data) && count($parent_data)){
							if($qbo_parent_cat_id = $this->check_category_exists($parent_data,true)){
								$item->setSubItem(true);
								$item->setParentRef($qbo_parent_cat_id);
								//$item->setLevel(1);
								
							}else{
								$this->save_log('Update Category Error','Parent category #'.$parent_id.' not found in QuickBooks.','Category',0);
								return false;
							}
						}
					}else{
						if($item->countSubItem() && $item->getSubItem()=='true'){
							$item->setSubItem(false);
							
							if($item->countParentRef()){
								$item->unsetParentRef();								
							}
							
							if($item->countParentRef_name()){
								$item->unsetParentRef_name();
							}
							
							if($item->countLevel()){
								$item->unsetLevel();
							}
						}
					}
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($cat_data);
					//$this->_p($item);
					//die;
					//return false;
					
					if ($resp = $ItemService->update($Context, $realm, $item->getId(), $item)){
						$qbo_cat_id = $this->qbo_clear_braces($item->getId());
						$log_title.="Update Category #$wc_category_id\n";
						$log_details.="Category #$wc_category_id has been updated, QuickBooks Category ID is #$qbo_cat_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Category',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Category Add',$cat_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_cat_id;
						
					}else{
						$res_err = $ItemService->lastError($Context);
						$log_title.="Update Category Error #$wc_category_id\n";
						$log_details.="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Category',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Category Add',$cat_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}
		}
	}
	
	
	public function AddCategory($cat_data){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$wc_category_id = (int) $this->get_array_isset($cat_data,'term_id',0,true);
			if($this->if_sync_category($wc_category_id)){
				if(!$this->check_category_exists($cat_data)){
					$ItemService = new QuickBooks_IPP_Service_Item();
					$item = new QuickBooks_IPP_Object_Item();
					
					//$name = $this->get_array_isset($cat_data,'name','',true);
					$name_replace_chars = array(':');
					$name = $this->get_array_isset($cat_data,'name','',true,100,false,$name_replace_chars);
					
					$item->setName($name);
					$item->setType('Category');
					
					$parent_id = (int) $this->get_array_isset($cat_data,'parent',0);
					if($parent_id){
						$parent_data = (array) get_term($parent_id,'product_cat');
						if(is_array($parent_data) && count($parent_data)){
							if($qbo_parent_cat_id = $this->check_category_exists($parent_data,true)){
								$item->setSubItem(true);
								$item->setParentRef($qbo_parent_cat_id);
								//$item->setLevel(1);
								
							}else{
								$this->save_log('Export Category Error','Parent category #'.$parent_id.' not found in QuickBooks.','Category',0);
								return false;
							}
						}
					}
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($cat_data);
					//$this->_p($item);
					//die;
					//return false;
					
					if ($resp = $ItemService->add($Context, $realm, $item)){
						$qbo_cat_id = $this->qbo_clear_braces($resp);
						$log_title.="Export Category #$wc_category_id\n";
						$log_details.="Category #$wc_category_id has been exported, QuickBooks Category ID is #$qbo_cat_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Category',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Category Add',$cat_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_cat_id;
						
					}else{
						$res_err = $ItemService->lastError($Context);
						$log_title.="Export Category Error #$wc_category_id\n";
						$log_details.="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Category',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Category Add',$cat_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}
		}
	}
	
	
	/**
	 * Add Product Into Quickbooks Online.
	 *
	 * @since    1.0.0
	 * Last Updated: 2017-01-04
	 */
	
	public function AddProduct($product_data){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$wc_product_id = (int) $this->get_array_isset($product_data,'wc_product_id',0,true);
			if($this->if_sync_product($wc_product_id)){
				if(!$this->check_product_exists($product_data)){
					$ItemService = new QuickBooks_IPP_Service_Item();
					$item = new QuickBooks_IPP_Object_Item();
					
					//08-05-2017
					$name_replace_chars = array(':');					
					$name = $this->get_array_isset($product_data,'name','',true,100,false,$name_replace_chars);
					
					//$name = $this->get_array_isset($product_data,'name','',true);
					$sku = $this->get_array_isset($product_data,'_sku','',true);
					
					$_manage_stock = $this->get_array_isset($product_data,'_manage_stock','no',true);
					$_downloadable = $this->get_array_isset($product_data,'_downloadable','no',true);
					$_virtual = $this->get_array_isset($product_data,'_virtual','no',true);
					
					$_stock = $this->get_array_isset($product_data,'_stock',0,true);
					
					$is_variation = $this->get_array_isset($product_data,'is_variation',false,false);
					$sync_item = ($is_variation)?'Variation':'Product';
					
					if($_manage_stock=='yes'){
						if($this->get_qbo_company_info('is_sku_enabled')){
							$type = 'Inventory';
						}else{
							$type = 'NonInventory';
						}
						
					}elseif($_virtual=='yes'){
						$type = 'Service';
					}else{
						$type = 'NonInventory';
					}
					
					//Group,Service,NonInventory
					
					$_sale_price = $this->get_array_isset($product_data,'_sale_price',0);
					$_min_variation_price = $this->get_array_isset($product_data,'_min_variation_price',0);
					
					$_max_variation_price = $this->get_array_isset($product_data,'_max_variation_price',0);
					
					$_price = $this->get_array_isset($product_data,'_price',0);
					
					$unitPrice = $_price;
					
					$_tax_class = $this->get_array_isset($product_data,'_tax_class','');
					
					$_tax_status = $this->get_array_isset($product_data,'_tax_status','');
					$taxable = ($_tax_status!='' && $_tax_status!='none')?true:false;					
					
					$active = $this->get_array_isset($product_data,'active',true);
					
					//$qty = (int) $this->get_array_isset($product_data,'total_stock',0);
					$qty = $_stock;
					
					$item->setName($name);
					
					$mw_wc_qbo_sync_product_pull_desc_field = $this->get_option('mw_wc_qbo_sync_product_pull_desc_field');
					$desc = '';
					
					if($mw_wc_qbo_sync_product_pull_desc_field=='short_description'){
						$desc = $this->get_array_isset($product_data,'short_description','',true,4000);						
					}else{
						$desc = $this->get_array_isset($product_data,'description','',true,4000);						
					}
					
					$item->setDescription($desc);
				
					$item->setType($type);
					
					$item->setSku($sku);
					
					//$unitPrice = number_format($unitPrice, 2);
					$unitPrice = str_replace(',','',$unitPrice);
					$unitPrice = floatval($unitPrice);
					
					$item->setUnitPrice($unitPrice);
					
					$item->setTaxable($taxable);
					$item->setActive($active);
					
					$qbo_product_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_product_account');
					if(!$qbo_product_account){
						$this->save_log('Export '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product account not selected.','Product',0);
						return false;
					}
					
					$item->setIncomeAccountRef($qbo_product_account);
					
					//
					if($type=='Inventory'){
						
						$item->setQtyOnHand($qty);
						$item->setTrackQtyOnHand(true);
						$item->setInvStartDate(date('Y-m-d'));
					
						$qbo_product_asset_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_asset_account');
						if(!$qbo_product_asset_account){
							$this->save_log('Export '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product inventory asset account not selected.','Product',0);
							return false;
						}
						$item->setAssetAccountRef($qbo_product_asset_account);
						
						$qbo_product_expense_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_expense_account');
						if(!$qbo_product_expense_account){
							$this->save_log('Export '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product expense account not selected.','Product',0);
							return false;
						}
						$item->setExpenseAccountRef($qbo_product_expense_account);
						
					}
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($product_data);
					//$this->_p($item);
					//die;
					//return false;
					
					if ($resp = $ItemService->add($Context, $realm, $item)){
						$qbo_item_id = $this->qbo_clear_braces($resp);
						$log_title.="Export {$sync_item} #$wc_product_id\n";
						$log_details.="{$sync_item} #$wc_product_id has been exported, QuickBooks Product ID is #$qbo_item_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Product',$log_status,true);
						$this->add_qbo_item_obj_into_log_file(''.$sync_item.' Add',$product_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						
						$this->save_qbo_item_local($qbo_item_id,$name,$sku,$type);
						$this->save_item_map($wc_product_id,$qbo_item_id,false,$is_variation);
						
						return $qbo_item_id;
						
					}else{
						$res_err = $ItemService->lastError($Context);
						$log_title.="Export {$sync_item} Error #$wc_product_id\n";
						$log_details.="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Product',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file(''.$sync_item.' Add',$product_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}
		}
	}
	
	//01-06-2017
	public function UpdateProduct($product_data){
		$manual = $this->get_array_isset($product_data,'manual',false);
		if($manual){
			$this->set_session_val('sync_window_push_manual_update',true);
		}
		
		if($this->is_connected()){
			global $wpdb;
			
			$wc_product_id = (int) $this->get_array_isset($product_data,'wc_product_id',0,true);
			if($wc_product_id && $this->if_sync_product($wc_product_id)){
				$is_variation = $this->get_array_isset($product_data,'is_variation',false,false);				
				$sync_item = ($is_variation)?'Variation':'Product';
				
				$map_tbl = ($is_variation)?'mw_wc_qbo_sync_variation_pairs':'mw_wc_qbo_sync_product_pairs';
				$w_p_f = ($is_variation)?'wc_variation_id':'wc_product_id';
				
				$map_data = $this->get_row("SELECT `quickbook_product_id` FROM `".$wpdb->prefix."{$map_tbl}` WHERE `{$w_p_f}` = $wc_product_id AND `quickbook_product_id` > 0 ");
				$quickbook_product_id = 0;
				if(is_array($map_data) && count($map_data)){
					$quickbook_product_id = (int) $map_data['quickbook_product_id'];
				}
				if(!$quickbook_product_id){
					if($manual){
						$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,$sync_item.' not mapped.','Product',0);
					}
					return false;
				}
				
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ItemService = new QuickBooks_IPP_Service_Item();
				$sql = "SELECT * FROM Item WHERE Type IN('Inventory','Service','NonInventory') AND Id = '$quickbook_product_id' ";
				$items = $ItemService->query($Context, $realm, $sql);
				
				if(!$items || !count($items)){
					if($manual){
						$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,'Invalid QuickBooks product. ','Product',0);
					}
					return false;
				}
				
				$item = $items[0];
				
				/*
				if($item->getType()=='Group'){
					$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,'Invalid QuickBooks product (Bundle). ','Product',0);
					return false;
				}
				*/
				
				$name_replace_chars = array(':');					
				$name = $this->get_array_isset($product_data,'name','',true,100,false,$name_replace_chars);
				$sku = $this->get_array_isset($product_data,'_sku','',true);
				
				$_manage_stock = $this->get_array_isset($product_data,'_manage_stock','no',true);
				$_downloadable = $this->get_array_isset($product_data,'_downloadable','no',true);
				$_virtual = $this->get_array_isset($product_data,'_virtual','no',true);
				
				$_stock = $this->get_array_isset($product_data,'_stock',0,true);
				
				
				if($_manage_stock=='yes'){
					if($this->get_qbo_company_info('is_sku_enabled')){
						$type = 'Inventory';
					}else{
						$type = 'NonInventory';
					}
					
				}elseif($_virtual=='yes'){
					$type = 'Service';
				}else{
					$type = 'NonInventory';
				}
				
				$_sale_price = $this->get_array_isset($product_data,'_sale_price',0);
				$_min_variation_price = $this->get_array_isset($product_data,'_min_variation_price',0);
				
				$_max_variation_price = $this->get_array_isset($product_data,'_max_variation_price',0);
				
				$_price = $this->get_array_isset($product_data,'_price',0);
				
				$unitPrice = $_price;
				
				$_tax_class = $this->get_array_isset($product_data,'_tax_class','');
				
				$_tax_status = $this->get_array_isset($product_data,'_tax_status','');
				$taxable = ($_tax_status!='' && $_tax_status!='none')?true:false;
				
				$active = $this->get_array_isset($product_data,'active',true);
				
				//$qty = (int) $this->get_array_isset($product_data,'total_stock',0);
				$qty = $_stock;
				
				$item->setName($name);				
				
				$mw_wc_qbo_sync_product_pull_desc_field = $this->get_option('mw_wc_qbo_sync_product_pull_desc_field');
				$desc = '';
				
				if($mw_wc_qbo_sync_product_pull_desc_field=='short_description'){
					$desc = $this->get_array_isset($product_data,'short_description','',true,4000);						
				}else{
					$desc = $this->get_array_isset($product_data,'description','',true,4000);				
				}
				
				$item->setDescription($desc);
				
				$item->setSku($sku);
				
				//$unitPrice = number_format($unitPrice, 2);
				$unitPrice = str_replace(',','',$unitPrice);
				$unitPrice = floatval($unitPrice);
					
				$item->setUnitPrice($unitPrice);
				
				$item->setTaxable($taxable);
				$item->setActive($active);
				
				$qbo_product_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_product_account');
				if(!$qbo_product_account){
					$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product account not selected.','Product',0);
					return false;
				}
				
				$item->setIncomeAccountRef($qbo_product_account);
				
				//
				if($manual){
					if($item->getType()=='Inventory'){
						$item->setQtyOnHand($qty);
					}
				}				
				
				/*
				$update_p_type = false;
				if($type=='Inventory' && $update_p_type){
					
					//$item->setQtyOnHand($qty);
					if($item->getType()!='Inventory'){
						$item->setType($type);
						$item->setTrackQtyOnHand(true);
						$item->setInvStartDate(date('Y-m-d'));
					}					
					
					$qbo_product_asset_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_asset_account');
					if(!$qbo_product_asset_account){
						$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product inventory asset account not selected.','Product',0);
						return false;
					}
					$item->setAssetAccountRef($qbo_product_asset_account);
					
					$qbo_product_expense_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_expense_account');
					if(!$qbo_product_expense_account){
						$this->save_log('Update '.$sync_item.' Error #'.$wc_product_id,'QuickBooks product expense account not selected.','Product',0);
						return false;
					}
					$item->setExpenseAccountRef($qbo_product_expense_account);
					
				}else{
					//$item->setType($type);
				}
				*/
				
				$log_title = "";
				$log_details = "";
				$log_status = 0;
				
				//$this->_p($product_data);
				//$this->_p($item);
				//die;
				//return false;
				
				if ($resp = $ItemService->update($Context, $realm, $item->getId(), $item)){
					$qbo_item_id = $this->qbo_clear_braces($item->getId());
					$log_title.="Update {$sync_item} #$wc_product_id\n";
					$log_details.="{$sync_item} #$wc_product_id has been updated, QuickBooks Product ID is #$qbo_item_id";
					$log_status = 1;						
					$this->save_log($log_title,$log_details,'Product',$log_status,true);
					$this->add_qbo_item_obj_into_log_file(''.$sync_item.' Update',$product_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
					
					$this->save_qbo_item_local($qbo_item_id,$name,$sku,$type);					
					return $qbo_item_id;
					
				}else{
					$res_err = $ItemService->lastError($Context);
					$log_title.="Update {$sync_item} Error #$wc_product_id\n";
					$log_details.="Error:$res_err";					
					$this->save_log($log_title,$log_details,'Product',$log_status,true,true);
					$this->add_qbo_item_obj_into_log_file(''.$sync_item.' Update',$product_data,$item,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
					return false;
				}
				
			}
		}		
	}
	
	public function if_sync_product($wc_product_id){
		return true;
	}
	
	public function check_product_exists($product_data,$realtime=true,$return_id=false){		
		global $wpdb;
		$is_exists = false;
		$wc_product_id = (int) $this->get_array_isset($product_data,'wc_product_id',0,true);
		
		$name_replace_chars = array(':');					
		$name = $this->get_array_isset($product_data,'name','',true,100,false,$name_replace_chars);
		
		//$name = $this->get_array_isset($product_data,'name','',true);
		
		$sku = $this->get_array_isset($product_data,'_sku','',true);
		//$type = $this->get_array_isset($product_data,'type','',true);
		
		$is_variation = $this->get_array_isset($product_data,'is_variation',false,false);
		
		if($name!=''){
			if($item_id = $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','ID','name',$name)){
				$is_exists = true;
			}
		}
		
		//10-04-2017		
		if(!$is_exists){
			if($sku!=''){
				if($item_id = $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','ID','sku',$sku)){
					$is_exists = true;
				}
			}
		}
		
		//map table
		
		if(!$is_exists){
			$map_tbl = ($is_variation)?'mw_wc_qbo_sync_variation_pairs':'mw_wc_qbo_sync_product_pairs';
			$w_p_f = ($is_variation)?'wc_variation_id':'wc_product_id';
			if($item_id = $this->get_field_by_val($wpdb->prefix.$map_tbl,'quickbook_product_id',$w_p_f,$wc_product_id)){
				$is_exists = true;
			}
		}
		
		
		if(!$is_exists && $realtime){
			$item_id = (int) $this->check_product_realtime($product_data);
			if($item_id){
				$is_exists = true;
			}
		}
		
		return ($return_id)?$item_id:$is_exists;
	}
	
	public function check_product_realtime($product_data,$get_obj=false){
		$name = $this->get_array_isset($product_data,'name','',true);
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$ItemService = new QuickBooks_IPP_Service_Term();
			if($item_data = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Name = '$name' AND Type IN ('Inventory','Service','NonInventory','Group') ")){
				$item_id = $this->qbo_clear_braces($item_data[0]->getId());
				return ($get_obj)?$item_data[0]:$item_id;
			}
		}
		return false;
	}
	
	public function check_quickbooks_invoice_get_obj($wc_inv_id,$wc_inv_num='',$get_only_id=false){		
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$DocNumber = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			$invoiceService = new QuickBooks_IPP_Service_Invoice();
			$invoices_data = $invoiceService->query($Context, $realm, "SELECT * FROM Invoice WHERE DocNumber = '$DocNumber'");
			//$this->_p($invoices_data,true);
			if($invoices_data && count($invoices_data)){
				$invoices_data = $invoices_data[0];
				if($get_only_id){
					return $this->qbo_clear_braces($invoices_data->getId());
				}
				return $invoices_data;
			}
			return false;
		}
	}
	
	public function check_quickbooks_salesreceipt_get_obj($wc_inv_id,$wc_inv_num='',$get_only_id=false){	
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$DocNumber = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			$SalesReceiptService = new QuickBooks_IPP_Service_SalesReceipt();
			$salesreceipt_data = $SalesReceiptService->query($Context, $realm, "SELECT * FROM SalesReceipt WHERE DocNumber = '$DocNumber'");
			//$this->_p($salesreceipt_data,true);
			if($salesreceipt_data && count($salesreceipt_data)){
				$salesreceipt_data = $salesreceipt_data[0];
				if($get_only_id){
					return $this->qbo_clear_braces($salesreceipt_data->getId());
				}
				return $salesreceipt_data;
			}
			return false;
		}
	}
	
	public function if_sync_invoice($wc_inv_id,$wc_cus_id=0,$wc_inv_no=''){
		$ord_id_num = ($wc_inv_no!='')?$wc_inv_no:$wc_inv_id;
		if($wc_inv_id < (int) $this->get_option('mw_wc_qbo_sync_invoice_min_id')){
			$this->save_log('Export Order #'.$ord_id_num,'Order sync not allowed for ID less than #'.(int) $this->get_option('mw_wc_qbo_sync_invoice_min_id'),'Invoice',2);
			return false;
		}
		return true;
	}
	
	//19-06-2017
	public function get_wc_order_id_from_qbo_inv_sr_doc_no($qbo_inv_sr_doc_no=0){
		$qbo_inv_sr_doc_no = $this->sanitize($qbo_inv_sr_doc_no);		
		$wc_inv_id = 0;
		if($qbo_inv_sr_doc_no!=''){
			global $wpdb;			
			if($this->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $this->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
				$sql = "SELECT p.ID FROM `{$wpdb->posts}` p, `{$wpdb->postmeta}` pm WHERE pm.meta_key = '_order_number_formatted' AND pm.meta_value = %s AND pm.post_id = p.ID AND p.post_type = 'shop_order' ";
			}else{
				$qbo_inv_sr_doc_no = (int) $qbo_inv_sr_doc_no;
				$sql = "SELECT `ID` FROM `{$wpdb->posts}` WHERE `ID` = %d AND `post_type` = 'shop_order' ";				
			}
			$sql = $wpdb->prepare($sql,$qbo_inv_sr_doc_no);			
			$wc_ord_data = $this->get_row($sql);
			$wc_inv_id = (int) $wc_ord_data['ID'];
		}
		return $wc_inv_id;
	}
	
	public function Qbo_Pull_Payment($payment_info){
		if($this->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
			return false;
		}
		$qbo_payment_id = (int) $this->get_array_isset($payment_info,'qbo_payment_id',0);
		$manual = $this->get_array_isset($payment_info,'manual',false);
		
		$webhook_log_txt = '';
		$webhook = $this->get_array_isset($payment_info,'webhook',false);
		if($webhook){
			$webhook_log_txt = 'Webhook ';
			//$manual = true;
		}
		
		global $wpdb;
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$PaymentService = new QuickBooks_IPP_Service_Payment();
			$sql = "SELECT * FROM Payment WHERE Id = '$qbo_payment_id' ";
			$items = $PaymentService->query($Context, $realm, $sql);
			if(!$items || !count($items)){
				$this->save_log($webhook_log_txt.'Import Payment Error #'.$qbo_payment_id,'Invalid QuickBooks Payment.','Payment',0);
				return false;
			}
			
			$payment = $items[0];
			$CustomerRef = $payment->getCustomerRef();
			$TotalAmt = $payment->getTotalAmt();
			
			$TxnDate = $payment->getTxnDate();
			
			$qbo_inv_id = 0;
			$wc_inv_id = 0;
			
			if($payment->countLine()){
				if($payment->getLine()->countLinkedTxn()){
					if($payment->getLine()->getLinkedTxn()->getTxnType()=='Invoice'){
						$qbo_inv_id = $this->qbo_clear_braces($payment->getLine()->getLinkedTxn()->getTxnId());
					}
				}
			}
			$qbo_inv_doc_no = '';
			if($qbo_inv_id){
				$invoiceService = new QuickBooks_IPP_Service_Invoice();
				$invoices_data = $invoiceService->query($Context, $realm, "SELECT DocNumber FROM Invoice WHERE Id = '$qbo_inv_id'");				
				if($invoices_data && count($invoices_data)){
					$qbo_inv_doc_no = $invoices_data[0]->getDocNumber();
					$wc_inv_id = $this->get_wc_order_id_from_qbo_inv_sr_doc_no($qbo_inv_doc_no);
				}				
			}
			
			if($wc_inv_id){
				$order = get_post($wc_inv_id);
				//$invoice_data = $this->get_wc_order_details_from_order($wc_inv_id,$order);
				if(is_object($order) && count($order)){
					$order_status = $order->post_status;
					
					$op_invalid_status_static = array('auto-draft','draft','trash');
					$prevent_statues = $this->get_option('mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses');
					if($prevent_statues!=''){
						$prevent_statues = explode(',',$prevent_statues);
					}
					
					$is_valid_payment_pull = true;
					$payment_post_status = $this->get_option('mw_wc_qbo_sync_pmnt_pull_order_status');
					$payment_post_status = trim($payment_post_status);
					if($payment_post_status==''){
						$payment_post_status = 'wc-completed';
					}
					
					if(is_array($prevent_statues) && in_array($order_status,$prevent_statues)){
						$is_valid_payment_pull = false;
					}
					
					if(in_array($order_status,$op_invalid_status_static)){
						$is_valid_payment_pull = false;
					}
					
					if($order_status==$payment_post_status){
						$is_valid_payment_pull = false;
					}
					
					if($order_status!='' && $payment_post_status!='' && $is_valid_payment_pull){
						$post_data = array();
						$post_data['ID'] = $wc_inv_id;
						
						$post_data['post_status'] = $payment_post_status;
						$post_data['wp_error'] = true;
						
						$post_meta_arr = array();
						
						$log_title = '';
						$log_details = '';
						$log_status = 0;
						
						$is_wp_error = false;
						$wp_err_txt = '';
						$return = $this->save_wp_post('shop_order',$post_data,$post_meta_arr);
						//
						if ( is_wp_error( $return ) ) {
							$is_wp_error = true;
							$wp_err_txt.= (string) $return->get_error_message();
							$wp_err_txt.= (string) $return->get_error_data();
						}
						
						if(!$is_wp_error && (int) $return){
							$post_id = (int) $return;
							
							/*Order Note*/
							$order_statuses = wc_get_order_statuses();
							$old_status =(is_array($order_statuses) && isset($order_statuses[$order_status]))?$order_statuses[$order_status]:$order_status;
							$new_status =(is_array($order_statuses) && isset($order_statuses[$payment_post_status]))?$order_statuses[$payment_post_status]:$payment_post_status;
							$order = new WC_Order( $post_id );
							$order_note = __('Order status changed from '.$old_status.' to '.$new_status,'mw_wc_qbo_sync');
							$order_note.=PHP_EOL;
							$order_note.='Payment Pull - MyWorks WooCommerce Sync for QuickBooks Online';
							$order->add_order_note($order_note);
							
							$log_title.=$webhook_log_txt."Import Payment #$qbo_payment_id\n";
							$log_details.="Payment #$qbo_payment_id has been imported, WooCommerce Order #{$qbo_inv_doc_no}";
							
							$log_status = 1;
							$this->save_log($log_title,$log_details,'Payment',$log_status,true);						
							$this->save_payment_id_map($post_id,$qbo_payment_id,1);
							
							return $post_id;
						}else{
							$log_title.=$webhook_log_txt."Import Payment Error #$qbo_payment_id\n";
							$log_details = "WooCommerce Order #{$qbo_inv_doc_no}\n";
							if(isset($post_data['wp_error'])){
								$log_details.="Error:$wp_err_txt";
							}else{
								$log_details.="Error:Wordpress save post error";
							}
							$this->save_log($log_title,$log_details,'Payment',$log_status,true);
							return false;
						}
					}
				}
			}
		}
	}
	
	//02-05-2017
	public function Qbo_Pull_Category($category_info){
		if(!$this->get_qbo_company_info('is_category_enabled')){
			return false;
		}
		
		$qbo_category_id = (int) $this->get_array_isset($category_info,'qbo_category_id',0);
		$manual = $this->get_array_isset($category_info,'manual',false);
		
		$webhook_log_txt = '';
		$webhook = $this->get_array_isset($category_info,'webhook',false);
		if($webhook){
			$webhook_log_txt = 'Webhook ';
			//$manual = true;
		}
		
		global $wpdb;
		if($this->is_connected()){			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Item();
			$sql = "SELECT * FROM Item WHERE Type = 'Category' AND Id = '$qbo_category_id' ";
			$items = $ItemService->query($Context, $realm, $sql);
			if(!$items || !count($items)){
				$this->save_log($webhook_log_txt.'Import Category Error','Invalid QuickBooks Category.','Category',0);
				return false;
			}
			
			$item = $items[0];
			$cat_name = $item->getName();
			//$cat_name = $this->sanitize($cat_name);
			
			//$name_replace_chars = array(':');					
			//$cat_name = $this->get_array_isset(array('cat_name'=>$cat_name),'cat_name','',true,100,false,$name_replace_chars);
			
			$cat_name = esc_sql($cat_name);
			$cat_name = htmlspecialchars($cat_name);
			
			
			$wc_cat_check_sql = "
			SELECT t.term_id AS id, t.name
			FROM   {$wpdb->terms} t
			LEFT JOIN {$wpdb->term_taxonomy} tt
			ON t.term_id = tt.term_id				
			WHERE  tt.taxonomy = 'product_cat'
			AND (t.name = %s OR REPLACE(t.name,':','') = %s)
			LIMIT 0,1
			";
			$wc_cat_check_sql = $wpdb->prepare($wc_cat_check_sql,$cat_name,$cat_name);
			$wc_cat_check_data = $this->get_row($wc_cat_check_sql);
			
			
			//$wc_cat_check_data = term_exists( $cat_name, 'product_cat' );
			
			//08-05-2017
			$up_term_id = 0;
			
			if(is_array($wc_cat_check_data) && count($wc_cat_check_data)){
				$up_term_id = $wc_cat_check_data['id'];
				if($manual){
					//$this->save_log($webhook_log_txt.'Import Category Error','WooCommerce category already exists with same name.','Category',0);
				}				
				//return false;
			}
			
			$up_term_id = (int) $up_term_id;
			
			//15-06-2017
			if($up_term_id && $manual){
				$this->set_session_val('sync_window_pull_manual_update',true);
			}
			
			$wc_parent_cat_id = 0;
			if($item->countSubItem() && $item->getSubItem()=='true'){
				$ParentRef = $item->getParentRef();
				$ParentRef = $this->qbo_clear_braces($ParentRef);
				
				/*
				$sql_p = "SELECT Name FROM Item WHERE Type = 'Category' AND Id = '$ParentRef' ";
				$items_p = $ItemService->query($Context, $realm, $sql_p);
				if($items_p && count($items_p)){
					$items_p = $items_p[0];
					$ParentRef_name = $items_p->getName();
				}
				*/
				
				$ParentRef_name = $item->getParentRef_name();
				if(strpos( $ParentRef_name, ':' ) !== false){
					$ParentRef_name_arr = explode(':',$ParentRef_name);
					if(is_array($ParentRef_name_arr) && count($ParentRef_name_arr)){
						$ParentRef_name = end($ParentRef_name_arr);
					}				
				}
				
				//$ParentRef_name = $this->get_array_isset(array('ParentRef_name'=>$ParentRef_name),'ParentRef_name','',true,100,false,$name_replace_chars);
				$ParentRef_name = esc_sql($ParentRef_name);
				
				$Level = $item->getLevel();
				
				
				$wc_cat_check_sql = "
				SELECT t.term_id AS id, t.name
				FROM   {$wpdb->terms} t
				LEFT JOIN {$wpdb->term_taxonomy} tt
				ON t.term_id = tt.term_id				
				WHERE  tt.taxonomy = 'product_cat'
				AND (t.name = %s OR REPLACE(t.name,':','') = %s)
				LIMIT 0,1
				";
				$wc_cat_check_sql = $wpdb->prepare($wc_cat_check_sql,$ParentRef_name,$ParentRef_name);
				$wc_cat_check_data = $this->get_row($wc_cat_check_sql);
							
				//$wc_cat_check_data = term_exists( $ParentRef_name, 'product_cat' );
				if(is_array($wc_cat_check_data) && count($wc_cat_check_data)){
					$wc_parent_cat_id = $wc_cat_check_data['id'];
					//$wc_parent_cat_id = $wc_cat_check_data['term_id'];
				}else{
					if($up_term_id){
						$this->save_log($webhook_log_txt.'Import Update Category Error','Parent category '.$ParentRef_name.' (#'.$ParentRef.') not found in WooCommerce.','Category',0);
					}else{
						$this->save_log($webhook_log_txt.'Import Category Error','Parent category '.$ParentRef_name.' (#'.$ParentRef.') not found in WooCommerce.','Category',0);
					}
					
					return false;
				}			
			}
			$wc_term_arg = array();
			$wc_term_arg['description'] = '';
			if($wc_parent_cat_id){
				$wc_term_arg['parent'] = (int) $wc_parent_cat_id;
			}
			
			//$this->_p($up_term_id);die;
			if($up_term_id){
				$term_insert_data = wp_update_term(
				  $up_term_id, // the term 
				  'product_cat', // the taxonomy
				  $wc_term_arg
				);
			}else{
				$term_insert_data = wp_insert_term(
				  $cat_name, // the term 
				  'product_cat', // the taxonomy
				  $wc_term_arg
				);
			}
			
			
			
			$log_title = '';
			$log_details = '';
			$log_status = 0;
			
			if(is_array($term_insert_data) && count($term_insert_data) && isset($term_insert_data['term_id']) && (int) $term_insert_data['term_id']){
				$wc_cat_id = $term_insert_data['term_id'];
				
				if($up_term_id){
					$log_title.=$webhook_log_txt."Import Update Category #$qbo_category_id\n";
					$log_details.="Category #$qbo_category_id has been updated, WooCommerce Product Category ID is #$wc_cat_id";
				}else{
					$log_title.=$webhook_log_txt."Import Category #$qbo_category_id\n";
					$log_details.="Category #$qbo_category_id has been imported, WooCommerce Product Category ID is #$wc_cat_id";
				}
				
				
				$log_status = 1;
				$this->save_log($log_title,$log_details,'Category',$log_status,true);
				return $wc_cat_id;
			}else{
				if($up_term_id){
					$log_title.=$webhook_log_txt."Import Update Category Error #$qbo_category_id\n";
				}else{
					$log_title.=$webhook_log_txt."Import Category Error #$qbo_category_id\n";
				}
				
				$log_details.="Error:Wordpress save term error";
				$this->save_log($log_title,$log_details,'Category',$log_status,true);
				return false;
			}
		}
	}
	
	//23-02-2017
	
	public function Qbo_Pull_Product($product_info){
		if(!$this->is_connected()){return false;}
		
		global $wpdb;
		$qbo_product_id = (int) $this->get_array_isset($product_info,'qbo_product_id',0);
		$manual = $this->get_array_isset($product_info,'manual',false);
		
		$webhook_log_txt = '';
		$webhook = $this->get_array_isset($product_info,'webhook',false);
		if($webhook){
			$webhook_log_txt = 'Webhook ';
			//$manual = true;
		}
		
		if($Item = $this->check_is_valid_qbo_product_get_obj($qbo_product_id)){
			
			$type = $Item->getType();
			//17-05-2017
			if($type=='Group'){
				$this->save_log($webhook_log_txt.'Import Product Error #'.$qbo_product_id,'Bundle item not supported.'.$p_map_log_txt.'. ','Product',0);
				return false;
			}
			
			$name =  $Item->getName();
			$sku = ($Item->countSku())?$Item->getSku():'';
			
			$name =  wp_strip_all_tags($name);
			
			if($name==''){
				return false;
			}
			//
			$is_variation = false;
			$is_update = false;
			if($wc_product_id = (int) $this->check_wc_product_exists($qbo_product_id,$name,$sku,true)){
				$item_id = (int) $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_product_pairs','wc_product_id','quickbook_product_id',$qbo_product_id);
				
				//18-05-2017				
				if(!$item_id){
					$item_id = (int) $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_variation_pairs','wc_variation_id','quickbook_product_id',$qbo_product_id);
					$is_variation = true;
				}
				
				$p_map_log_txt = '';
				if($item_id){
					$is_update = true;
				}else{
					$p_map_log_txt = ' (Sku or name matched but not mapped)';
				}
				
				if($manual && !$item_id){
					$this->save_log($webhook_log_txt.'Import Product Error #'.$qbo_product_id,'Product already exists'.$p_map_log_txt.'. ','Product',0);
				}
				
				//
				if($item_id && $is_variation){
					$this->save_log($webhook_log_txt.'Import Update Variation Error #'.$qbo_product_id,'Not supported by plugin.','Product',0);
					return false;
				}
				
				if(!$item_id){
					return false;
				}				
			}
			$wc_product_data = array();
			//ID for update
			if($is_update){
				$wc_product_data['ID'] = $wc_product_id;
				if($manual){
					$this->set_session_val('sync_window_pull_manual_update',true);
				}
			}
			
			//23-06-2017
			$post_excerpt = '';
			$post_content = '';			
			
			$mw_wc_qbo_sync_product_pull_desc_field = $this->get_option('mw_wc_qbo_sync_product_pull_desc_field');
			if($mw_wc_qbo_sync_product_pull_desc_field=='short_description'){
				$post_excerpt = (string) $Item->getDescription();
			}else{
				$post_content = (string) $Item->getDescription();
			}
			
			$wc_product_data['post_excerpt'] = $post_excerpt;
			$wc_product_data['post_content'] = $post_content;
			
			$wc_product_data['post_title'] = $name;
						
			$wc_product_data['wp_error'] = true;
			
			if(!$is_update){
				$post_status = $this->get_option('mw_wc_qbo_sync_product_pull_wc_status');
				$wc_product_data['post_status'] = $post_status;
			}
			
			$wc_product_meta = array();
			
			$_tax_status = ($Item->getTaxable())?'taxable':'';
			$wc_product_meta['_tax_status'] = $_tax_status;
			
			$wc_product_meta['_sku'] = $sku;
			
			$_price = ($Item->countUnitPrice())?$Item->getUnitPrice():'';
			$wc_product_meta['_price'] = $_price;
			$wc_product_meta['_regular_price'] = $_price;
			
			$_manage_stock = ($Item->getTrackQtyOnHand())?'yes':'no';
			$wc_product_meta['_manage_stock'] = $_manage_stock;
			
			if($_manage_stock=='yes'){
				$_stock = ($Item->countQtyOnHand())?$Item->getQtyOnHand():'';
				if(!$is_update){
					$wc_product_meta['_stock'] = $_stock;
					//21-03-2017
					if($_stock && $_stock>0){
						$wc_product_meta['_stock_status'] = 'instock';
					}else{
						$wc_product_meta['_stock_status'] = 'outofstock';
					}
				}				
			}
			
			$wc_product_meta['total_sales'] = '0';
			$wc_product_meta['_downloadable'] = 'no';
			$wc_product_meta['_visibility'] = 'visible';
			$wc_product_meta['_virtual'] = 'no';
			
			$wc_product_meta['_purchase_note'] = '';
			
			//_regular_price,_sale_price,_featured,_weight,_length,_width,_height,_product_attributes,_sale_price_dates_from,_sale_price_dates_to
			//_sold_individually,_backorders
			
			
			$tax_input = array();
			
			if($this->get_qbo_company_info('is_category_enabled')){
				$wc_p_cat_id_arr = array();
				if($Item->countSubItem() && $Item->getSubItem()=='true'){
					$ParentRef = $Item->getParentRef();
					$ParentRef = $this->qbo_clear_braces($ParentRef);
					$Level = $Item->getLevel();
					
					$ParentRef_name = $Item->getParentRef_name();
					$ParentRef_name_arr = array();
					
					if(strpos( $ParentRef_name, ':' ) !== false){
						$ParentRef_name_arr = explode(':',$ParentRef_name);											
					}else{
						$ParentRef_name_arr[] = $ParentRef_name;
					}
					
					if(is_array($ParentRef_name_arr) && count($ParentRef_name_arr)){						
						foreach($ParentRef_name_arr as $ParentRef_name){								
							$ParentRef_name = esc_sql($ParentRef_name);
					
							$wc_cat_check_sql = "
							SELECT t.term_id AS id, t.name
							FROM   {$wpdb->terms} t
							LEFT JOIN {$wpdb->term_taxonomy} tt
							ON t.term_id = tt.term_id				
							WHERE  tt.taxonomy = 'product_cat'
							AND (t.name = %s OR REPLACE(t.name,':','') = %s)
							LIMIT 0,1
							";
							$wc_cat_check_sql = $wpdb->prepare($wc_cat_check_sql,$ParentRef_name,$ParentRef_name);
							$wc_cat_check_data = $this->get_row($wc_cat_check_sql);									
							
							if(is_array($wc_cat_check_data) && count($wc_cat_check_data)){
								$wc_p_cat_id = $wc_cat_check_data['id'];
								$wc_p_cat_id_arr[] = (int) $wc_p_cat_id;
							}
						}
					}		
				}
				
				if(is_array($wc_p_cat_id_arr) && count($wc_p_cat_id_arr)){
					$wc_p_cat_id_arr = array_map('intval',$wc_p_cat_id_arr);
					$wc_p_cat_id_arr = array_unique( $wc_p_cat_id_arr );
					$tax_input = array('product_cat'=>$wc_p_cat_id_arr);
				}
			}			
			
			//$this->_p($wc_product_data);
			//$this->_p($wc_product_meta);
			//return false;
			
			$log_title = '';
			$log_details = '';
			$log_status = 0;
			
			$is_wp_error = false;
			$wp_err_txt = '';
			$return = $this->save_wp_post('product',$wc_product_data,$wc_product_meta,$tax_input);
			//$this->_p($return);
			
			if ( is_wp_error( $return ) ) {
				$is_wp_error = true;
				$wp_err_txt.= (string) $return->get_error_message();
				$wp_err_txt.= (string) $return->get_error_data();
			}
			//return false;
			
			if(!$is_wp_error && (int) $return){
				$post_id = (int) $return;
				if($is_update){
					$log_title.=$webhook_log_txt."Import Product Update #$qbo_product_id\n";
					$log_details.="Product #$qbo_product_id has been updated, WooCommerce Product ID is #$post_id";
				}else{
					$log_title.=$webhook_log_txt."Import Product #$qbo_product_id\n";
					$log_details.="Product #$qbo_product_id has been imported, WooCommerce Product ID is #$post_id";
				}
				
				$log_status = 1;
				$this->save_log($log_title,$log_details,'Product',$log_status,true);
				$this->save_qbo_item_local($qbo_product_id,$name,$sku,$type);
				$this->save_item_map($post_id,$qbo_product_id,true);
				
				return $post_id;
			}else{
				if($is_update){
					$log_title.=$webhook_log_txt."Import Product Update Error #$qbo_product_id\n";
				}else{
					$log_title.=$webhook_log_txt."Import Product Error #$qbo_product_id\n";
				}
				
				if(isset($wc_product_data['wp_error'])){
					$log_details.="Error:$wp_err_txt";
				}else{
					$log_details.="Error:Wordpress save post error";
				}
				$this->save_log($log_title,$log_details,'Product',$log_status,true);
				return false;
			}
		}
	}
	
	public function save_wp_post($post_type,$post_data,$post_meta_arr=array(),$tax_input=array()){
		if($post_type!='' && is_array($post_data) && count($post_data)){
			$wp_error = $this->get_array_isset($post_data,'wp_error',false);
			$post_data['post_type'] = $post_type;
			//03-05-2017
			if(is_array($tax_input) && count($tax_input)){
				$post_data['tax_input'] = $tax_input;
			}
			
			if(isset($post_data['ID']) && (int) $post_data['ID']){
				$return = wp_update_post( $post_data ,$wp_error );
			}else{
				$return = wp_insert_post( $post_data ,$wp_error );
			}
			
			if((int) $return && is_array($post_meta_arr) && count($post_meta_arr)){
				$post_id = (int) $return;
				foreach($post_meta_arr as $key => $val){
					update_post_meta($post_id, $key, $val);
				}
			}
			return $return;
		}		
	}
	
	public function check_wc_product_exists($qbo_product_id,$name,$sku='',$get_wc_product_id=false){
		global $wpdb;
		$is_exists = false;
		
		//15-06-2017
		$name = htmlspecialchars($name);
		
		//
		$wc_product_id = 0;
		
		//map table		
		if(!$is_exists){
			if($item_id = $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_product_pairs','wc_product_id','quickbook_product_id',$qbo_product_id)){
				$is_exists = true;
				$wc_product_id = $item_id;
			}
		}
		
		//18-05-2017
		if(!$is_exists){
			if($item_id = $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_variation_pairs','wc_variation_id','quickbook_product_id',$qbo_product_id)){
				$is_exists = true;
				$wc_product_id = $item_id;
			}
		}
		
		$sku = (string) $sku;
		if(!$is_exists && $sku!=''){
			$sql = $wpdb->prepare("SELECT `meta_id` , `post_id` FROM {$wpdb->postmeta} WHERE `meta_key` = '_sku' AND `meta_value` = %s AND `meta_value`  !='' ",$sku);
			$check_product = $this->get_row($sql);
			if(is_array($check_product) && count($check_product)){
				$is_exists = true;
				$wc_product_id = $check_product['post_id'];
			}
		}
		
		$p_tbl_chk = false;
		if(!$is_exists){
			//$check_product = $this->get_row($wpdb->prepare("SELECT `ID` FROM {$wpdb->posts} WHERE `post_title` = %s ",$name));
			$check_product = $this->get_row($wpdb->prepare("SELECT `ID` FROM {$wpdb->posts} WHERE REPLACE(post_title,':','') = %s AND `post_type` = 'product' AND post_status NOT IN('auto-draft','trash') ",$name));
			
			$p_tbl_chk = true;
			if(is_array($check_product) && count($check_product)){
				$is_exists = true;
				$wc_product_id = $check_product['ID'];
			}
		}
		
		//New
		if(!$is_exists){			
			$check_product = $this->get_row($wpdb->prepare("SELECT `ID` FROM {$wpdb->posts} WHERE REPLACE(post_title,':','') = %s AND `post_type` = 'product_variation' AND post_status NOT IN('auto-draft','trash') ",$name));
			
			$p_tbl_chk = true;
			if(is_array($check_product) && count($check_product)){
				$is_exists = true;
				$wc_product_id = $check_product['ID'];
			}
		}
		
		//23-06-2017
		if(!$is_exists && !$p_tbl_chk){
			$wc_product_id = (int) $wc_product_id;
			if($wc_product_id){
				$check_product = $this->get_row($wpdb->prepare("SELECT `ID` FROM {$wpdb->posts} WHERE ID = %d AND `post_type` IN('product','product_variation') AND post_status NOT IN('auto-draft','trash') ",$wc_product_id));
				if(!is_array($check_product) || !count($check_product)){
					$is_exists = false;
				}
			}			
		}		
		
		//16-03-2017
		if($get_wc_product_id){
			if($is_exists){
				return $wc_product_id;
			}else{
				return 0;
			}			
		}
		return $is_exists;
	}
	
	//16-03-2017
	public function check_if_real_time_push_enable_for_item($item=''){
		if($item!=''){
			$mw_wc_qbo_sync_rt_push_enable = $this->option_checked('mw_wc_qbo_sync_rt_push_enable');
			if(!$mw_wc_qbo_sync_rt_push_enable){
				return false;
			}
			$mw_wc_qbo_sync_rt_push_items = (string) $this->get_option('mw_wc_qbo_sync_rt_push_items');
			if($mw_wc_qbo_sync_rt_push_items!=''){
				$mw_wc_qbo_sync_rt_push_items = explode(',',$mw_wc_qbo_sync_rt_push_items);
				if(is_array($mw_wc_qbo_sync_rt_push_items) && count($mw_wc_qbo_sync_rt_push_items)){
					if(in_array($item,$mw_wc_qbo_sync_rt_push_items)){
						return true;
					}
				}
			}else{
				//return true;
			}
		}
		return false;
	}
	
	public function Process_QuickBooks_WebHooks_Request($entities){
		//https://developer.intuit.com/docs/0100_quickbooks_online/0300_references/0000_programming_guide/0020_webhooks
		if(!$this->is_connected()){
			return false;
		}
		
		if(is_array($entities) && count($entities)){
			$is_sku_enabled = $this->get_qbo_company_info('is_sku_enabled');
			$is_category_enabled = $this->get_qbo_company_info('is_category_enabled');
			
			foreach($entities as $Entity){
				
				$name = $Entity->name;
				$id = (int) $Entity->id;
				$operation = $Entity->operation;
				$lastUpdated = $Entity->lastUpdated;
				
				$mw_wc_qbo_sync_webhook_items = $this->get_option('mw_wc_qbo_sync_webhook_items');
				if($mw_wc_qbo_sync_webhook_items!=''){
					$mw_wc_qbo_sync_webhook_items = explode(',',$mw_wc_qbo_sync_webhook_items);
				}
				
				if(is_array($mw_wc_qbo_sync_webhook_items) && count($mw_wc_qbo_sync_webhook_items)){
					$is_rt_item_import = false;
					
					if(in_array('Product',$mw_wc_qbo_sync_webhook_items) || in_array('Inventory',$mw_wc_qbo_sync_webhook_items) || in_array('Category',$mw_wc_qbo_sync_webhook_items)){
						$is_rt_item_import = true;
					}
					
					if($name == 'Item' && $is_rt_item_import){
						
						$Context = $this->Context;
						$realm = $this->realm;
						
						$ItemService = new QuickBooks_IPP_Service_Term();
						$sql = "SELECT * FROM Item WHERE Id = '$id' ";
						$items = $ItemService->query($Context, $realm, $sql);
						$items = ($items && count($items))?$items[0]:'';
						
						//Product Add/Update
						if(in_array('Product',$mw_wc_qbo_sync_webhook_items)){
							if(($operation == 'Create' || $operation=='Update') && $this->check_is_valid_qbo_product($id,$items)){
								$return_id = $this->Qbo_Pull_Product(array('qbo_product_id'=>$id,'webhook'=>true));
							}
						}				
						
						//Inventory Update
						if(in_array('Inventory',$mw_wc_qbo_sync_webhook_items) && $is_sku_enabled){				
							if($operation == 'Update' && $name == 'Item' && $this->check_is_valid_qbo_inventory($id,$items)){
								$return_id = $this->UpdateWooCommerceInventory(array('qbo_inventory_id'=>$id,'webhook'=>true));
							}				
						}
						
						//Category Add
						if(in_array('Category',$mw_wc_qbo_sync_webhook_items) && $is_category_enabled){
							if(($operation == 'Create') && $this->check_is_valid_qbo_category($id,$items)){
								$return_id = $this->Qbo_Pull_Category(array('qbo_category_id'=>$id,'webhook'=>true));
							}
						}
					}
					
					if($name == 'Customer' && in_array('Customer',$mw_wc_qbo_sync_webhook_items)){
						//
					}
					
					if($name == 'Invoice' && in_array('Invoice',$mw_wc_qbo_sync_webhook_items)){
						//
					}
					
					//Payment Add
					if($name == 'Payment' && in_array('Payment',$mw_wc_qbo_sync_webhook_items)){					
						if(($operation == 'Create')){
							$return_id = $this->Qbo_Pull_Payment(array('qbo_payment_id'=>$id,'webhook'=>true));
						}
					}
					
				}				
			}
		}
	}
	
	public function check_is_valid_qbo_product_get_obj($qbo_product_id,$get_only_id=false){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$sql = "SELECT * FROM Item WHERE Id = '$qbo_product_id' AND Type IN ('Inventory','Service','NonInventory','Group') ";
			$items = $ItemService->query($Context, $realm, $sql);
			if($items && count($items)){
				if($get_only_id){
					return $this->qbo_clear_braces($items[0]->getId());
				}
				return $items[0];
			}
		}
		return false;
	}
	
	public function check_is_valid_qbo_inventory($qbo_inventory_id,$item_obj=''){
		if($this->is_connected()){
			if(is_object($item_obj) && !empty($item_obj)){
				if($item_obj->countType() && $item_obj->getType()=='Inventory'){
					return true;
				}
				return false;
			}
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$sql = "SELECT * FROM Item WHERE Type = 'Inventory' AND Id = '$qbo_inventory_id' ";
			$items = $ItemService->query($Context, $realm, $sql);
			if($items && count($items)){
				return true;
			}
		}
		return false;
	}
	
	//03-05-2017
	public function check_is_valid_qbo_category($qbo_inventory_id,$item_obj=''){
		if($this->is_connected()){
			if(is_object($item_obj) && !empty($item_obj)){
				if($item_obj->countType() && $item_obj->getType()=='Category'){
					return true;
				}
				return false;
			}
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$sql = "SELECT * FROM Item WHERE Type = 'Category' AND Id = '$qbo_inventory_id' ";
			$items = $ItemService->query($Context, $realm, $sql);
			if($items && count($items)){
				return true;
			}
		}
		return false;
	}
	
	public function check_is_valid_qbo_product($qbo_product_id,$item_obj=''){
		if($this->is_connected()){
			if(is_object($item_obj) && !empty($item_obj)){
				//
				if($item_obj->countType() && ($item_obj->getType()=='NonInventory' || $item_obj->getType()=='Inventory' || $item_obj->getType()=='Service' || $item_obj->getType()=='Group')){
					return true;
				}
				return false;
			}
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$sql = "SELECT * FROM Item WHERE Id = '$qbo_product_id' AND Type IN ('Inventory','Service','NonInventory','Group') ";
			$items = $ItemService->query($Context, $realm, $sql);
			if($items && count($items)){
				return true;
			}
		}
		return false;
	}
	
	//06-06-2017
	public function VariationUpdateQboInventory($inventory_data){
		if($this->is_connected()){
			if(!$this->get_qbo_company_info('is_sku_enabled')){
				return false;
			}
			global $wpdb;
			$wc_inventory_id = (int) $this->get_array_isset($inventory_data,'wc_inventory_id',0);
			$manual = $this->get_array_isset($inventory_data,'manual',false);
			if($wc_inventory_id){
				$ext_log = '';
				$variation = get_post($wc_inventory_id);
				
				if(!is_object($variation) || !count($variation)){
					if($manual){
						$this->save_log('Export Variation Inventory Error #'.$wc_inventory_id,'Woocommerce variation not found!','Inventory',0);
					}
					return false;
				}
				
				if($variation->post_type!='product_variation'){
					if($manual){
						$this->save_log('Export Variation Inventory Error #'.$wc_inventory_id,'Woocommerce variation is not valid.','Inventory',0);
					}
					return false;
				}
				
				$variation_meta = get_post_meta($wc_inventory_id);
				
				if(!$variation_meta){
					if($manual){
						$this->save_log($webhook_log_txt.'Export Variation Inventory Error #'.$wc_inventory_id,'WooCommerce variation information not found. '.$ext_log,'Inventory',0);						
					}
					return false;
				}
				
				$_manage_stock = (isset($variation_meta['_manage_stock'][0]))?$variation_meta['_manage_stock'][0]:'no';
				$_backorders = (isset($variation_meta['_backorders'][0]))?$variation_meta['_backorders'][0]:'no';
				$_stock = (isset($variation_meta['_stock'][0]))?$variation_meta['_stock'][0]:0;
				
				if($_manage_stock!='yes'){
					if($manual){
						$this->save_log('Export Variation Inventory Error #'.$wc_inventory_id,'Invalid Woocommerce inventory. ','Inventory',0);
					}
					return false;
				}
				
				$map_data = $this->get_row("SELECT `quickbook_product_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` WHERE `wc_variation_id` = $wc_inventory_id AND `quickbook_product_id` > 0 ");
				$quickbook_product_id = 0;
				if(is_array($map_data) && count($map_data)){
					$quickbook_product_id = (int) $map_data['quickbook_product_id'];
				}
				if(!$quickbook_product_id){
					if($manual){
						$this->save_log('Export Variation Inventory Error #'.$wc_inventory_id,'QuickBooks inventory not found. ','Inventory',0);
					}
					return false;
				}
				
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ItemService = new QuickBooks_IPP_Service_Item();
				$sql = "SELECT * FROM Item WHERE Type = 'Inventory' AND Id = '$quickbook_product_id' ";
				$items = $ItemService->query($Context, $realm, $sql);
				
				if(!$items || !count($items)){
					if($manual){
						$this->save_log('Export Variation Inventory Error #'.$wc_inventory_id,'Invalid QuickBooks inventory. ','Inventory',0);
					}
					return false;
				}
				
				$Inventory = $items[0];
				$QtyOnHand = $Inventory->getQtyOnHand();
				
				if($QtyOnHand!=$_stock){
					$Inventory->setQtyOnHand($_stock);
					//$this->set_show_all_error();
					if ($resp = $ItemService->update($Context, $realm, $Inventory->getId(), $Inventory)){						
						$qbo_inv_id = $this->qbo_clear_braces($Inventory->getId());
						$log_title ="Update Variation Inventory #$wc_inventory_id\n";
						$log_details ="Variation Inventory #$wc_inventory_id has been updated, QuickBooks Inventory ID is #$qbo_inv_id";
						$this->save_log($log_title,$log_details,'Inventory',1,true);
						$this->add_qbo_item_obj_into_log_file('Variation Inventory Update',$inventory_data,$Inventory,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_inv_id;
						
					}else{
						$res_err = $ItemService->lastError($Context);
						$log_title ="Update Variation Inventory Error #$wc_inventory_id\n";
						$log_details ="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Inventory',0,true,true);
						$this->add_qbo_item_obj_into_log_file('Variation Inventory Update',$inventory_data,$Inventory,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}else{
					$log = "Stocks on both ends are same (".$QtyOnHand.").".$ext_log;
					$this->save_log('Export Variation Inventory #'.$wc_inventory_id,$log,'Inventory',1);
				}
			}
		}
	}
	
	//09-05-2017
	public function UpdateQboInventory($inventory_data){		
		if($this->is_connected()){
			if(!$this->get_qbo_company_info('is_sku_enabled')){
				return false;
			}
			global $wpdb;
			$wc_inventory_id = (int) $this->get_array_isset($inventory_data,'wc_inventory_id',0);
			$manual = $this->get_array_isset($inventory_data,'manual',false);
			if($wc_inventory_id){
				$ext_log = '';
				$_product = wc_get_product( $wc_inventory_id );
				if(!count($_product)){
					$this->save_log('Export Inventory Error #'.$wc_inventory_id,'Woocommerce product not found. ','Inventory',0);
					return false;
				}
				
				$product_meta = get_post_meta($wc_inventory_id);
				
				if(!$product_meta){
					if($manual){
						$this->save_log($webhook_log_txt.'Export Inventory Error #'.$wc_inventory_id,'WooCommerce product information not found. '.$ext_log,'Inventory',0);						
					}
					return false;
				}
				
				$_manage_stock = (isset($product_meta['_manage_stock'][0]))?$product_meta['_manage_stock'][0]:'no';
				$_backorders = (isset($product_meta['_backorders'][0]))?$product_meta['_backorders'][0]:'no';
				$_stock = (isset($product_meta['_stock'][0]))?$product_meta['_stock'][0]:0;
				
				if($_manage_stock!='yes'){
					if($manual){
						$this->save_log('Export Inventory Error #'.$wc_inventory_id,'Invalid Woocommerce inventory. ','Inventory',0);
					}
					return false;
				}
				
				$map_data = $this->get_row("SELECT `quickbook_product_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `wc_product_id` = $wc_inventory_id AND `quickbook_product_id` > 0 ");
				$quickbook_product_id = 0;
				if(is_array($map_data) && count($map_data)){
					$quickbook_product_id = (int) $map_data['quickbook_product_id'];
				}
				if(!$quickbook_product_id){
					if($manual){
						$this->save_log('Export Inventory Error #'.$wc_inventory_id,'QuickBooks inventory not found. ','Inventory',0);
					}
					return false;
				}
				
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ItemService = new QuickBooks_IPP_Service_Item();
				$sql = "SELECT * FROM Item WHERE Type = 'Inventory' AND Id = '$quickbook_product_id' ";
				$items = $ItemService->query($Context, $realm, $sql);
				
				if(!$items || !count($items)){
					if($manual){
						$this->save_log('Export Inventory Error #'.$wc_inventory_id,'Invalid QuickBooks inventory. ','Inventory',0);
					}
					return false;
				}
				
				$Inventory = $items[0];
				$QtyOnHand = $Inventory->getQtyOnHand();
				
				if($QtyOnHand!=$_stock){
					$Inventory->setQtyOnHand($_stock);
					//$this->set_show_all_error();
					if ($resp = $ItemService->update($Context, $realm, $Inventory->getId(), $Inventory)){						
						$qbo_inv_id = $this->qbo_clear_braces($Inventory->getId());
						$log_title ="Update Inventory #$wc_inventory_id\n";
						$log_details ="Inventory #$wc_inventory_id has been updated, QuickBooks Inventory ID is #$qbo_inv_id";								
						$this->save_log($log_title,$log_details,'Inventory',1,true);
						$this->add_qbo_item_obj_into_log_file('Inventory Update',$inventory_data,$Inventory,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_inv_id;
						
					}else{
						$res_err = $ItemService->lastError($Context);
						$log_title ="Update Inventory Error #$wc_inventory_id\n";
						$log_details ="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Inventory',0,true,true);
						$this->add_qbo_item_obj_into_log_file('Inventory Update',$inventory_data,$Inventory,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}else{
					$log = "Stocks on both ends are same (".$QtyOnHand.").".$ext_log;
					$this->save_log('Export Inventory #'.$wc_inventory_id,$log,'Inventory',1);
				}
				
			}
		}
	}
	
	public function set_show_all_error(){
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
	}
	
	//21-02-2017
	public function UpdateWooCommerceInventory($inventory_data){
		if($this->is_connected()){
			global $wpdb;
			$qbo_inventory_id = (int) $this->get_array_isset($inventory_data,'qbo_inventory_id',0);
			$manual = $this->get_array_isset($inventory_data,'manual',false);
			
			//
			$webhook_log_txt = '';
			$webhook = $this->get_array_isset($inventory_data,'webhook',false);
			if($webhook){
				$webhook_log_txt = 'Webhook ';
				//$manual = true;
			}
			
			$cron = $this->get_array_isset($inventory_data,'cron',false);
			if($cron){
				$webhook_log_txt = 'Cron ';
			}
			
			if($qbo_inventory_id){
				$Context = $this->Context;
				$realm = $this->realm;

				$ItemService = new QuickBooks_IPP_Service_Term();
				$sql = "SELECT * FROM Item WHERE Type = 'Inventory' AND Id = '$qbo_inventory_id' ";
				$items = $ItemService->query($Context, $realm, $sql);
				
				if(!$items || !count($items)){
					if($manual){
						$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'Invalid QuickBooks inventory. ','Inventory',0);
					}
					return false;
				}
				
				$Inventory = $items[0];
				$QtyOnHand = $Inventory->getQtyOnHand();
				
				//
				$qbo_product_name =  $Inventory->getName();
				$ext_log = "\n".'Name: '.$qbo_product_name;
				
				$map_data = $this->get_row("SELECT `wc_product_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `quickbook_product_id` = $qbo_inventory_id AND `wc_product_id` > 0 ");
				
				//variation				
				$is_variation = false;
				if(!count($map_data)){
					$map_data = $this->get_row("SELECT `wc_variation_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` WHERE `quickbook_product_id` = $qbo_inventory_id AND `wc_variation_id` > 0 ");
					$is_variation = true;
				}
				
				
				if(!count($map_data)){
					if($manual){
						$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'WooCommerce product not found. '.$ext_log,'Inventory',0);						
					}
					return false;
				}
				
				$wc_product_id = 0;
				$is_variation_parent = false;
				
				if($is_variation){
					$wc_variation_id = $map_data['wc_variation_id'];
					$variation_manage_stock = get_post_meta($wc_variation_id,'_manage_stock',true);
					
					if($variation_manage_stock=='yes'){
						$wc_product_id = $wc_variation_id;
					}else{
						$parent_id = (int) $this->get_field_by_val($wpdb->posts,'post_parent','ID',$wc_variation_id);
						if($parent_id){
							$wc_product_id = $parent_id;
							$is_variation_parent = true;
						}
						
					}
					
				}else{
					$wc_product_id = $map_data['wc_product_id'];
				}
				
				$product_meta = get_post_meta($wc_product_id);
				if(!$product_meta){
					if($manual){
						$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'WooCommerce product information not found. '.$ext_log,'Inventory',0);						
					}
					return false;
				}
				
				$_manage_stock = (isset($product_meta['_manage_stock'][0]))?$product_meta['_manage_stock'][0]:'no';
				$_backorders = (isset($product_meta['_backorders'][0]))?$product_meta['_backorders'][0]:'no';
				$_stock = (isset($product_meta['_stock'][0]))?$product_meta['_stock'][0]:0;
				
				
				$is_valid_wc_inventory = false;
				
				if($_manage_stock=='yes'){
					$is_valid_wc_inventory = true;
				}
				
				if(!$is_valid_wc_inventory){
					if($manual){
						$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'WooCommerce inventory not valid. '.$ext_log,'Inventory',0);						
					}
					return false;
				}
				
				//Parent
				if($is_variation_parent){
					$parent_qbo_inventory_id = (int) $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_product_pairs','quickbook_product_id','wc_product_id',$wc_product_id);
					
					if(!$parent_qbo_inventory_id){
						if($manual){
							$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'Invalid QuickBooks inventory. ','Inventory',0);
						}
						return false;
					}
					
					$sql = "SELECT * FROM Item WHERE Type = 'Inventory' AND Id = '$parent_qbo_inventory_id' ";
					$items = $ItemService->query($Context, $realm, $sql);
					
					if(!$items || !count($items)){
						if($manual){
							$this->save_log($webhook_log_txt.'Import Inventory Error #'.$qbo_inventory_id,'Invalid QuickBooks inventory. ','Inventory',0);	
						}
						return false;
					}
					
					$Inventory = $items[0];
					$QtyOnHand = $Inventory->getQtyOnHand();
				}
				
				if($QtyOnHand!=$_stock){
					$_stock = number_format(floatval($_stock),2);
					$return = update_post_meta($wc_product_id, '_stock', $QtyOnHand);
					//21-03-2017
					if($QtyOnHand && $QtyOnHand>0){
						update_post_meta($wc_product_id, '_stock_status', 'instock');
					}else{
						update_post_meta($wc_product_id, '_stock_status', 'outofstock');
					}
					$log = "WooCommerce Product #$wc_product_id stock updated from $_stock to $QtyOnHand ".$ext_log;
					$this->save_log($webhook_log_txt.'Import Inventory #'.$qbo_inventory_id,$log,'Inventory',1);
				}else{
					$log = "Stocks on both ends are same (".$QtyOnHand.").".$ext_log;
					$this->save_log($webhook_log_txt.'Import Inventory #'.$qbo_inventory_id,$log,'Inventory',1);
				}
				
				return $wc_product_id;
				
			}
		}	
	}
	
	//12-06-2017
	public function get_mapped_acof_qbo_item_from_val($acof_txt=''){
		$qp_id = 0;
		$acof_txt = trim($acof_txt);
		if($acof_txt!=''){
			$acof_map = $this->get_option('mw_wc_qbo_sync_compt_acof_wf_qi_map');
			if($acof_map!=''){
				$acof_map_arr = unserialize($acof_map);
				if(is_array($acof_map_arr) && count($acof_map_arr)){
					foreach($acof_map_arr as $k =>$v){
						$k = base64_decode($k);
						if($acof_txt==$k){
							$qp_id = (int) $v;
							break;
						}
					}
				}
				
			}
		}
		return $qp_id;
	}
	
	public function get_mapped_qbo_items_from_wc_items($wc_items=array(),$real_time_data=false,$acof_txt=''){
		//$this->_p($wc_items);return false;
		$qbo_items = array();
		if(is_array($wc_items) && count($wc_items)){
			global $wpdb;
			$wc_product_id = (int) $wc_items['product_id'];
			//07-03-2017
			$map_data = array();
			//12-06-2017
			$acof_map_product = false;
			if($acof_txt!=''){
				$acof_p_id = (int) $this->get_mapped_acof_qbo_item_from_val($acof_txt);
				if($acof_p_id){
					$map_data['itemid'] = $acof_p_id;
					$acof_map_product = true;
				}
			}
			
			if(!count($map_data)){
				$wc_variation_id = (isset($wc_items['variation_id']))?(int) $wc_items['variation_id']:0;
				if($wc_variation_id){
					$map_data = $this->get_row("SELECT `quickbook_product_id` AS itemid , `class_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` WHERE `wc_variation_id` = $wc_variation_id AND `quickbook_product_id` > 0 ");
				}
			}			
			
			if(!count($map_data)){
				$map_data = $this->get_row("SELECT `quickbook_product_id` AS itemid , `class_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `wc_product_id` = $wc_product_id AND `quickbook_product_id` > 0 ");
			}
			
			//24-05-2017
			if(count($map_data)){
				$qbo_item_id = (int) $map_data['itemid'];
				$product_type = $this->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','product_type','itemid',$qbo_item_id);
				$map_data['product_type'] = $product_type;
			}
			
			if(!count($map_data)){
				$qbo_default_product_id = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
				$map_data = $this->get_row("SELECT `itemid` , `product_type` FROM `".$wpdb->prefix."mw_wc_qbo_sync_qbo_items` WHERE `itemid` = $qbo_default_product_id ");
			}
			
			if(is_array($map_data) && count($map_data)){
				$qbo_items_tmp = array();				
				$qbo_items_tmp['Description'] = $this->get_array_isset($wc_items,'name','');
				$qbo_items_tmp['Qty'] = $wc_items['qty'];
				$qbo_items_tmp['UnitPrice'] = $wc_items['unit_price'];
				$qbo_items_tmp['ItemRef'] = $map_data['itemid'];
				
				$qbo_items_tmp['acof_map_product'] = $acof_map_product;
				//
				$qbo_items_tmp['qbo_product_type'] = $map_data['product_type'];
				
				$qbo_items_tmp['Taxed'] = ($wc_items['line_tax']>0)?1:0;
				$qbo_items_tmp['ClassRef'] = (isset($map_data['class_id']))?$map_data['class_id']:'';
				$qbo_items = $qbo_items_tmp;
				foreach($wc_items as $k => $val){
					if($k!='name' && $k!='qty' && $k!='unit_price'){
						$qbo_items[$k] = $val;
					}
				}				
				
			}
		}
		return $qbo_items;
	}
	
	//08-02-2017
	public function get_mapped_shipping_product($wc_shippingmethod=''){
		global $wpdb;
		$qbo_shipping_product = array();
		$qbo_shipping_product['ItemRef'] = (int) $this->get_option('mw_wc_qbo_sync_default_shipping_product');
		//29-05-2017
		if($wc_shippingmethod=='no_method_found'){
			return $qbo_shipping_product;
		}
		
		$wc_shippingmethod = $this->sanitize($wc_shippingmethod);
		if($wc_shippingmethod!=''){
			$map_data = $this->get_row($wpdb->prepare("SELECT `qbo_product_id` , `class_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_shipping_product_map` WHERE `wc_shippingmethod` = %s AND  `qbo_product_id` > 0 ",$wc_shippingmethod));
			if(is_array($map_data) && count($map_data)){
				$qbo_shipping_product['ItemRef'] = (int) $map_data['qbo_product_id'];
				$qbo_shipping_product['ClassRef'] = $map_data['class_id'];
			}
		}		
		return $qbo_shipping_product;
	}
	
	public function get_mapped_payment_method_data($wc_paymentmethod='',$wc_currency=''){
		global $wpdb;
		$wc_paymentmethod = $this->sanitize($wc_paymentmethod);
		if($wc_paymentmethod!=''){
			$map_data = $this->get_row($wpdb->prepare("SELECT * FROM `".$wpdb->prefix."mw_wc_qbo_sync_paymentmethod_map` WHERE `wc_paymentmethod` = %s AND `currency` = %s ",$wc_paymentmethod,$wc_currency));
			return $map_data;
		}
		return array();
	}
	
	//10-02-2017
	public function get_mapped_coupon_product($wc_couponcode=''){
		global $wpdb;
		$promo_id = 0;
		$description = '';
		
		$qbo_coupon_product = array();
		$qbo_coupon_product['ItemRef'] = (int) $this->get_option('mw_wc_qbo_sync_default_coupon_code');
		$wc_couponcode = $this->sanitize($wc_couponcode);
		
		if($wc_couponcode!=''){
			$promo_data = $this->get_row($wpdb->prepare("SELECT `ID` , `post_excerpt` FROM `".$wpdb->posts."` WHERE `post_type` = 'shop_coupon' AND `post_title` = %s ",$wc_couponcode));
			if(is_array($promo_data) && count($promo_data)){
				$promo_id = (int) $promo_data['ID'];
				$description = $promo_data['post_excerpt'];
			}
			
			$map_data = $this->get_row($wpdb->prepare("SELECT `qbo_product_id` , `class_id` FROM `".$wpdb->prefix."mw_wc_qbo_sync_promo_code_product_map` WHERE `promo_id` = %s AND  `qbo_product_id` > 0 ",$promo_id));
			if(is_array($map_data) && count($map_data)){
				$qbo_coupon_product['ItemRef'] = (int) $map_data['qbo_product_id'];
				$qbo_coupon_product['ClassRef'] = $map_data['class_id'];
			}
		}
		$qbo_coupon_product['Description'] = 'Coupon: '.$wc_couponcode;
		$qbo_coupon_product['Description'] = $this->get_array_isset($qbo_coupon_product,'Description');
		return $qbo_coupon_product;
	}
	
	//11-04-2017
	public function get_wc_order_details_from_order($order_id,$order){
		global $wpdb;
		$order_id = (int) $order_id;
		if($order_id && is_object($order) && count($order)){			
			//$this->_p($order);			
			$order_meta = get_post_meta($order_id);
			//$this->_p($order_meta);			
			$invoice_data = array();
			$invoice_data['wc_inv_id'] = $order_id;
			$invoice_data['wc_inv_num'] = '';
			//19-05-2017
			if($this->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $this->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
				$_order_number_formatted = isset($order_meta['_order_number_formatted'][0])?$order_meta['_order_number_formatted'][0]:'';
				if($_order_number_formatted!=''){
					$_order_number_formatted = trim($_order_number_formatted);
				}				
				$invoice_data['wc_inv_num'] = $_order_number_formatted;
			}		
			
			$invoice_data['order_type'] = '';
			$invoice_data['wc_inv_date'] = $order->post_date;
			$invoice_data['wc_inv_due_date'] = $order->post_date;
			
			//$invoice_data['customer_message'] = $order->post_excerpt;
			$invoice_data['customer_note'] = $order->post_excerpt;			
			$invoice_data['order_status'] = $order->post_status;
			
			$wc_cus_id = isset($order_meta['_customer_user'][0])?(int) $order_meta['_customer_user'][0]:0;
			$invoice_data['wc_cus_id'] = $wc_cus_id;
			$invoice_data['wc_customerid'] = $wc_cus_id;
			
			
			if(is_array($order_meta) && count($order_meta)){
				foreach ($order_meta as $key => $value){
					$invoice_data[$key] = ($value[0])?$value[0]:'';
				}
			}
			
			$wc_oi_table = $wpdb->prefix.'woocommerce_order_items';
			$wc_oi_meta_table = $wpdb->prefix.'woocommerce_order_itemmeta';
			
			$order_items = $this->get_data("SELECT * FROM {$wc_oi_table} WHERE `order_id` = {$order_id} ");
			//$this->_p($order_items);
			$line_items = $used_coupons = $tax_details = $shipping_details = array();
			$dc_gt_fees = array();
			if(is_array($order_items) && count($order_items)){
				foreach($order_items as $oi){
					$order_item_id = (int) $oi['order_item_id'];
					$oi_meta = $this->get_data("SELECT * FROM {$wc_oi_meta_table} WHERE `order_item_id` = {$order_item_id} ");
					//$this->_p($oi_meta);
					$om_arr = array();
					if(is_array($oi_meta) && count($oi_meta)){
						foreach($oi_meta as $om){
							$om_arr[$om['meta_key']] = $om['meta_value'];
						}
					}
					
					$om_arr['name'] = $oi['order_item_name'];
					$om_arr['type'] = $oi['order_item_type'];
					
					if($oi['order_item_type']=='line_item'){
						$line_items[] = $om_arr;
					}
					
					if($oi['order_item_type']=='coupon'){
						$used_coupons[] = $om_arr;
					}
					
					if($oi['order_item_type']=='shipping'){
						if(isset($om_arr['name'])){
							$om_arr['name'] = $this->get_array_isset($om_arr,'name');
						}
						$shipping_details[] = $om_arr;
					}
					
					if($oi['order_item_type']=='tax'){
						if(isset($om_arr['label'])){
							$om_arr['label'] = $this->get_array_isset($om_arr,'label');
						}
						$tax_details[] = $om_arr;
					}
					
					//16-05-2017
					if($oi['order_item_type']=='fee'){
						if(isset($om_arr['name'])){
							$om_arr['name'] = $this->get_array_isset($om_arr,'name');
						}						
						$dc_gt_fees[] = $om_arr;
					}
				}
			}
			
			//12-06-2017
			$acof_txt='';
			if($this->is_plugin_active('woocommerce-admin-custom-order-fields') && $this->option_checked('mw_wc_qbo_sync_compt_p_wacof')){
				$mw_wc_qbo_sync_compt_p_wacof_m_field = (int) $this->get_option('mw_wc_qbo_sync_compt_p_wacof_m_field');
				$mw_wc_qbo_sync_compt_acof_wf_qi_map = $this->get_option('mw_wc_qbo_sync_compt_acof_wf_qi_map');
				if($mw_wc_qbo_sync_compt_acof_wf_qi_map!=''){
					$mw_wc_qbo_sync_compt_acof_wf_qi_map = unserialize($mw_wc_qbo_sync_compt_acof_wf_qi_map);
				}				
				if($mw_wc_qbo_sync_compt_p_wacof_m_field && is_array($mw_wc_qbo_sync_compt_acof_wf_qi_map) && count($mw_wc_qbo_sync_compt_acof_wf_qi_map)){
					if(isset($invoice_data['_wc_acof_'.$mw_wc_qbo_sync_compt_p_wacof_m_field])){
						$acof_txt = $invoice_data['_wc_acof_'.$mw_wc_qbo_sync_compt_p_wacof_m_field];
						$acof_txt = trim($acof_txt);
					}					
				}
			}
			
			$qbo_inv_items = array();
			//$this->_p($line_items);
			if(is_array($line_items) && count($line_items)){
				foreach ( $line_items as $item ) {
					$product_data = array();
					foreach($item as $key=>$val){
						if($this->start_with($key,'_')){
							$key = substr($key,1);							
						}
						$product_data[$key] = $val;
					}
					$product_data['unit_price'] = ($product_data['line_subtotal']/$product_data['qty']);
					$qbo_inv_items[] = $this->get_mapped_qbo_items_from_wc_items($product_data,false,$acof_txt);
				}
			}
			
			$invoice_data['used_coupons'] = $used_coupons;
			
			$order_shipping_total = isset($order_meta['_order_shipping'][0])?$order_meta['_order_shipping'][0]:0;			
			
			$invoice_data['shipping_details'] = $shipping_details;
			$invoice_data['order_shipping_total'] = $order_shipping_total;
			
			$invoice_data['tax_details'] = $tax_details;
			
			$invoice_data['qbo_inv_items'] = $qbo_inv_items;
			
			$invoice_data['dc_gt_fees'] = $dc_gt_fees;
			
			//$this->_p($invoice_data);
			return $invoice_data;
		}
	}
	
	//
	public function get_plugin_db_tbl_list(){
		global $wpdb;
		$tl_q = "SHOW TABLES LIKE '{$wpdb->prefix}mw_wc_qbo_sync\_%'";
		$tbl_list = $this->get_data($tl_q);
		
		$p_tbls = array();
		if(is_array($tbl_list) && count($tbl_list)){
			foreach($tbl_list as $tl){
				if(is_array($tl) && count($tl)){
					$tl_v = current($tl);$tl_v = (string) $tl_v;$tl_v = trim($tl_v);
					if($tl_v!=''){
						$p_tbls[] = $tl_v;
					}
				}
			}
		}
		return $p_tbls;
	}
	public function db_check_get_fields_details($s_tbf_list=array()){
		$tb_f_list = array();
		$tbls = $this->get_plugin_db_tbl_list();
		if(is_array($tbls) && count($tbls)){
			foreach($tbls as $tln){
				$tcq = "SHOW COLUMNS FROM {$tln}";
				$tc_list = $this->get_data($tcq);
				$tc_tmp_arr = array();
				if(is_array($tc_list) && count($tc_list)){
					foreach($tc_list as $tc_l){
						$tc_tmp_arr[$tc_l['Field']] = $tc_l;
					}
				}
				//$this->_p($tc_list);
				$tb_f_list[$tln] = $tc_tmp_arr;				
			}
		}
		//$this->_p($tbls);
		//$this->_p($tb_f_list);
		return $tb_f_list;
	}
	
	public function debug(){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			/*
			$invoiceService = new QuickBooks_IPP_Service_Invoice();
			$invoices = $invoiceService->query($Context,$realm ,"SELECT * FROM Invoice WHERE DocNumber = '1091' ");
			$this->_p($invoices);
			*/
			
			/*
			$ItemService = new QuickBooks_IPP_Service_Item();
			$sql = "SELECT * FROM Item WHERE Type = 'Group' ";
			$items = $ItemService->query($Context, $realm, $sql);
			$this->_p($items);
			*/
			
			//$this->_p($this->is_plugin_active('woocommerce-measurement-price-calculator'),true);
			//$this->_p($this->qbo_company_preferences);
			//$this->_p($this->get_qbo_group_product_details(0));
			//$this->Cron_Deposit(array('stripe'),'0',array('USD'));
			
			//$prf = $this->qbo_company_preferences;
			//$this->_p($prf);
			
			//CDC Test
			
			/*
			$interval_mins = 30;
			$now = new DateTime(null, new DateTimeZone('America/Los_Angeles'));
			$datetime = $now->format('Y-m-d H:i:s');
			$datetime = date('Y-m-d H:i:s',strtotime("-{$interval_mins} minutes",strtotime($datetime)));
			$timestamp = date('Y-m-d', strtotime($datetime)) . 'T' . date('H:i:s', strtotime($datetime));
			
			$cdc_objects = array('Item');
			
			$CDCService = new QuickBooks_IPP_Service_ChangeDataCapture();
			$cdc = $CDCService->cdc($Context, $realm, $cdc_objects,	$timestamp);
			
			$this->_p($cdc);
			$this->_p($CDCService->lastRequest());
			$this->_p($CDCService->lastResponse());
			*/
			//$this->_p($this->db_check_get_fields_details());			
		}
	}
	
	
	/**
	 * Update Invoice Into Quickbooks Online.
	 *
	 * @since    1.0.9
	 * Last Updated: 2017-03-14
	*/
	
	public function UpdateInvoice($invoice_data){
		$manual = $this->get_array_isset($invoice_data,'manual',false);
		if($manual){
			$this->set_session_val('sync_window_push_manual_update',true);
		}		
		
		if($this->is_connected()){
			$wc_inv_id = $this->get_array_isset($invoice_data,'wc_inv_id',0);
			$wc_inv_num = $this->get_array_isset($invoice_data,'wc_inv_num','');
			$wc_cus_id = $this->get_array_isset($invoice_data,'wc_cus_id','');
			
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			//Zero Total Option Check
			$_order_total = $this->get_array_isset($invoice_data,'_order_total',0);
			if($this->option_checked('mw_wc_qbo_sync_null_invoice')){
				if($_order_total==0 || $_order_total<0){
					$this->save_log('Update Invoice Error #'.$ord_id_num,'Invoice amount 0 not allowed in setting ','Invoice',0);
					return false;
				}				
			}
			
			if($this->if_sync_invoice($wc_inv_id,$wc_cus_id,$wc_inv_num)){
				if($invoice = $this->check_quickbooks_invoice_get_obj($wc_inv_id,$wc_inv_num)){
					//$this->_p($invoice);die;
					
					//Remove Line Items
					$line_count = $invoice->countLine();
					for($i=0;$i<=$line_count;$i++){
						$invoice->unsetLine($i);
					}
					
					//26-04-2017
					for($i=0;$i<=$invoice->countTxnTaxDetail();$i++){
						$invoice->unsetTxnTaxDetail($i);
					}
					
					$wc_inv_date = $this->get_array_isset($invoice_data,'wc_inv_date','');
					$wc_inv_date = $this->view_date($wc_inv_date);
					
					$wc_inv_due_date = $this->get_array_isset($invoice_data,'wc_inv_due_date','');					
					$wc_inv_due_date = $this->view_date($wc_inv_due_date);
					
					$qbo_customerid = $this->get_array_isset($invoice_data,'qbo_customerid',0);
					
					if($this->qbo_clear_braces($invoice->getCustomerRef()) != $qbo_customerid){
						$this->save_log('Update Invoice Error #'.$ord_id_num,'Customer Mismatch ','Invoice',0);
						return false;
					}
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$invoiceService = new QuickBooks_IPP_Service_Invoice();
					
					$invoice->setTxnDate($wc_inv_date);
					$invoice->setDueDate($wc_inv_due_date);

					/*Count Total Amounts*/
					$_cart_discount = $this->get_array_isset($invoice_data,'_cart_discount',0);
					$_cart_discount_tax = $this->get_array_isset($invoice_data,'_cart_discount_tax',0);
					
					$_order_tax = $this->get_array_isset($invoice_data,'_order_tax',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					//Shipping Total
					$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					
					$qbo_inv_items = (isset($invoice_data['qbo_inv_items']))?$invoice_data['qbo_inv_items']:array();					
					
					$total_line_subtotal = 0;
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){
						foreach($qbo_inv_items as $qbo_item){
							$total_line_subtotal+=$qbo_item['line_subtotal'];
						}
					}
					
					//Qbo settings
					$qbo_is_sales_tax = $this->get_qbo_company_setting('is_sales_tax');
					$qbo_company_country = $this->get_qbo_company_info('country');
					
					//Tax rates
					$qbo_tax_code = '';
					$apply_tax = false;
					$is_tax_applied = false;
					$is_inclusive = false;
					
					$tax_rate_id = 0;
					$tax_rate_id_2 = 0;
					
					$tax_details = (isset($invoice_data['tax_details']))?$invoice_data['tax_details']:array();
					if(count($tax_details)){
						$tax_rate_id = $tax_details[0]['rate_id'];
					}
					if(count($tax_details)>1){
						$tax_rate_id_2 = $tax_details[1]['rate_id'];
					}
					
					$qbo_tax_code = $this->get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2);
					if($qbo_tax_code!='' || $qbo_tax_code!='NON'){
						$apply_tax = true;
					}
					
					$Tax_Code_Details = $this->mod_qbo_get_tx_dtls($qbo_tax_code);					
					$is_qbo_dual_tax = false;
					
					if(count($Tax_Code_Details)){
						if($Tax_Code_Details['TaxGroup'] && count($Tax_Code_Details['TaxRateDetail'])>1){
							$is_qbo_dual_tax = true;
						}
					}
					
					
					$Tax_Rate_Ref = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']:'';
					$TaxPercent = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref);
					$Tax_Name = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef_name']:'';
					
					if($is_qbo_dual_tax){
						$Tax_Rate_Ref_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']:'';
						$TaxPercent_2 = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref_2);						
						$Tax_Name_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef_name']:'';
					}
					
					$_prices_include_tax = $this->get_array_isset($invoice_data,'_prices_include_tax','no',true);
					if($qbo_is_sales_tax){						
						$tax_type = $this->get_tax_type($_prices_include_tax);
						$is_inclusive = $this->is_tax_inclusive($tax_type);
						$invoice->setGlobalTaxCalculation($tax_type);
						$invoice->setApplyTaxAfterDiscount(true);
					}
					
					//Bundle Support
					$is_bundle_order = false;
					if($this->is_plugin_active('woocommerce-product-bundles') && $this->option_checked('mw_wc_qbo_sync_compt_wpbs')){
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if(isset($qbo_item['bundled_items']) && $qbo_item['qbo_product_type'] == 'Group'){
									$is_bundle_order = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									//$this->_p($qbo_gp_details);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_compt_wpbs_ap_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$invoice->addLine($line);
								}
							}
							
						}
					}
					
					//Map Bundle Support
					$map_bundle_support = false;
					if(!$is_bundle_order){					
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if($qbo_item['qbo_product_type'] == 'Group'){
									$map_bundle_support = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$invoice->addLine($line);								
								}
							}
						}
					}
					
					//Add Invoice items
					$first_line_desc = '';
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){						
						$first_line_desc = $qbo_inv_items[0]['Description'];
						foreach($qbo_inv_items as $qbo_item){
							//Bundle Support
							if($is_bundle_order){
								if(isset($qbo_item['bundled_items']) || isset($qbo_item['bundled_item_id']) || isset($qbo_item['bundle_cart_key'])){
									continue;
								}
							}
							
							if($map_bundle_support && $qbo_item['qbo_product_type'] == 'Group'){
								continue;
							}
							
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							$UnitPrice = $qbo_item["UnitPrice"];
							if($_cart_discount){
								//$UnitPrice = $this->get_discounted_item_price($_cart_discount,$total_line_subtotal,$UnitPrice);
							}
							
							$Amount = $qbo_item['Qty']*$UnitPrice;					
							
							$line->setAmount($Amount);
							$line->setDescription($qbo_item['Description']);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							$tax_class =  $qbo_item["tax_class"];
							
							if($qbo_is_sales_tax){
								if($apply_tax && $qbo_item["Taxed"]){
									$is_tax_applied = true;														
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									
									if($is_inclusive){
										
									}
									
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$salesItemLineDetail->setItemRef($qbo_item["ItemRef"]);
							
							$Qty = $qbo_item["Qty"];
							//10-05-2017
							if($this->is_plugin_active('woocommerce-measurement-price-calculator') && isset($qbo_item['measurement_data']) && $this->option_checked('mw_wc_qbo_sync_measurement_qty')){
								$measurement_data = unserialize($qbo_item['measurement_data']);
								if(is_array($measurement_data) && isset($measurement_data['_measurement_needed'])){
									$_measurement_needed = floatval($measurement_data['_measurement_needed']);
									if($_measurement_needed>0){
										$UnitPrice = $UnitPrice/$_measurement_needed;
										//$UnitPrice = number_format($UnitPrice, 2);
										$_quantity = (isset($measurement_data['_quantity']))?$measurement_data['_quantity']:1;
										$Qty = $_measurement_needed*$_quantity;
									}
								}
							}
							
							$salesItemLineDetail->setQty($Qty);
							$salesItemLineDetail->setUnitPrice($UnitPrice);
							
							if(isset($qbo_item["ClassRef"]) && $qbo_item["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($qbo_item["ClassRef"]);
							}
							
							
							if($this->option_checked('mw_wc_qbo_sync_invoice_date')){
								$salesItemLineDetail->setServiceDate($wc_inv_date);
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);
							$invoice->addLine($line);
						}
					}
					
					//pgdf compatibility
					//multiple fee plugin
					
					if($this->get_wc_fee_plugin_check()){
						$dc_gt_fees = (isset($invoice_data['dc_gt_fees']))?$invoice_data['dc_gt_fees']:array();
						if(is_array($dc_gt_fees) && count($dc_gt_fees)){
							foreach($dc_gt_fees as $df){
								$line = new QuickBooks_IPP_Object_Line();
								$line->setDetailType('SalesItemLineDetail');
								
								$UnitPrice = $df['_line_total'];
								$Qty = 1;
								$Amount = $Qty*$UnitPrice;		
								
								$line->setAmount($Amount);
								$line->setDescription($df['name']);
								
								$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
								$_line_tax = $df['_line_tax'];
								//$df_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($df['_line_tax_data']);
								if($_line_tax && $qbo_is_sales_tax){
									//$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$df_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}
								if(!$_line_tax && $qbo_is_sales_tax){
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
								
								$df_ItemRef = $this->get_wc_fee_qbo_product($df['name']);
								
								$salesItemLineDetail->setItemRef($df_ItemRef);
								$salesItemLineDetail->setQty($Qty);							
								$salesItemLineDetail->setUnitPrice($UnitPrice);
								
								$line->addSalesItemLineDetail($salesItemLineDetail);
								$invoice->addLine($line);
							}
						}
					}
					
					/*Add Invoice Shipping*/
					$shipping_details  = (isset($invoice_data['shipping_details']))?$invoice_data['shipping_details']:array();
					
					$shipping_method = '';
					$shipping_method_name = '';
					
					$shipping_taxes = '';
					if(isset($shipping_details[0])){
						if($this->get_array_isset($shipping_details[0],'type','')=='shipping'){
							$shipping_method_id = $this->get_array_isset($shipping_details[0],'method_id','');
							if($shipping_method_id!=''){
								//$shipping_method = substr($shipping_method_id, 0, strpos($shipping_method_id, ":"));
								$shipping_method = $this->wc_get_sm_data_from_method_id_str($shipping_method_id);
							}
							$shipping_method = ($shipping_method=='')?'no_method_found':$shipping_method;
							
							$shipping_method_name =  $this->get_array_isset($shipping_details[0],'name','',true,30);
							
							//Serialized
							$shipping_taxes = $this->get_array_isset($shipping_details[0],'taxes','');
							//$shipping_taxes = unserialize($shipping_taxes);
							//$this->_p($shipping_taxes);							
						}						
					}
					
					$qbo_is_shipping_allowed = $this->get_qbo_company_setting('is_shipping_allowed');
					
					//$order_shipping_total+=$_order_shipping_tax;
					
					if($shipping_method!=''){
						if($qbo_is_shipping_allowed){
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							$line->setAmount($order_shipping_total);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef('SHIPPING_ITEM_ID');
							
							if($qbo_is_sales_tax){
								if($_order_shipping_tax > 0){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}else{							
							$shipping_product_arr = $this->get_mapped_shipping_product($shipping_method);						
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							//
							$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
							
							$line->setAmount($order_shipping_total);
							
							$shipping_description = ($shipping_method_name!='')?'Shipping ('.$shipping_method_name.')':'Shipping';
							
							$line->setDescription($shipping_description);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							
							$salesItemLineDetail->setItemRef($shipping_product_arr["ItemRef"]);
							
							if(isset($shipping_product_arr["ClassRef"]) && $shipping_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($shipping_product_arr["ClassRef"]);
							}
							
							$salesItemLineDetail->setUnitPrice($order_shipping_total);
							
							
							if($qbo_is_sales_tax){							
								if($_order_shipping_tax > 0){
									//$shipping_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($shipping_taxes,'shipping');
									//$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$shipping_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}
						
					}					
					
					/*Add Invoice Coupons*/
					$used_coupons  = (isset($invoice_data['used_coupons']))?$invoice_data['used_coupons']:array();
					
					$qbo_is_discount_allowed = $this->get_qbo_company_setting('is_discount_allowed');
					
					if(count($used_coupons)){
						foreach($used_coupons as $coupon){
							$coupon_name = $coupon['name'];
							$coupon_discount_amount = $coupon['discount_amount'];							
							$coupon_discount_amount = -1 * abs($coupon_discount_amount);
							
							$coupon_discount_amount_tax = $coupon['discount_amount_tax'];
							
							$coupon_product_arr = $this->get_mapped_coupon_product($coupon_name);
							$line = new QuickBooks_IPP_Object_Line();
							
							$line->setDetailType('SalesItemLineDetail');
							if($qbo_is_discount_allowed){
								$line->setAmount(0);
							}else{
								$line->setAmount($coupon_discount_amount);
							}
							
							
							$line->setDescription($coupon_product_arr['Description']);
						
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef($coupon_product_arr['ItemRef']);
							if(isset($coupon_product_arr["ClassRef"]) && $coupon_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($coupon_product_arr["ClassRef"]);
							}
							if($qbo_is_discount_allowed){
								//$salesItemLineDetail->setUnitPrice(0);
							}else{
								$salesItemLineDetail->setUnitPrice($coupon_discount_amount);
							}
							
							if($qbo_is_sales_tax){
								if($coupon_discount_amount_tax > 0 || $is_tax_applied){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}							
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}
					}					
					
					/*Discount Line*/
					if($_cart_discount && $qbo_is_discount_allowed){
						$qbo_discount_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_discount_account');
						$line = new QuickBooks_IPP_Object_Line();
						$line->setDetailType('DiscountLineDetail');							
						$line->setAmount($_cart_discount);						
						$line->setDescription('Total Discount');
						
						$discountLineDetail = new QuickBooks_IPP_Object_DiscountLineDetail();
						$discountLineDetail->setPercentBased(false);
						$discountLineDetail->setDiscountAccountRef($qbo_discount_account);
						
						if($qbo_is_sales_tax){
							if($is_tax_applied){
								$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
								if($TaxCodeRef!=''){
									$discountLineDetail->setTaxCodeRef($TaxCodeRef);
								}
							}else{
								$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
								$discountLineDetail->setTaxCodeRef($zero_rated_tax_code);
							}							
						}
						
						$line->addDiscountLineDetail($discountLineDetail);						
						$invoice->addLine($line);
						
					}
					
					//15-05-2017					
					if($this->is_plugin_active('woocommerce-order-delivery') && $this->option_checked('mw_wc_qbo_sync_compt_p_wod')){
						if($invoice->countShipDate()){
							$invoice->unsetShipDate();
						}
						$_delivery_date = $this->get_array_isset($invoice_data,'_delivery_date','',true);
						if($_delivery_date!=''){
							$_delivery_date = $this->view_date($_delivery_date);
							$invoice->setShipDate($_delivery_date);
						}
					}					
					//
					$cf_map_data = $this->get_cf_map_data();
					
					//20-04-2017
					$_billing_email = $this->get_array_isset($invoice_data,'_billing_email','',true);
					$BillEmail = new QuickBooks_IPP_Object_BillEmail();
					$BillEmail->setAddress($_billing_email);
					$invoice->setBillEmail($BillEmail);
					
					//BillAddr
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($this->get_array_isset($invoice_data,'_billing_first_name','',true).' '.$this->get_array_isset($invoice_data,'_billing_last_name','',true));
					
					$is_cf_bf_applied = false;
					if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
						$bp_a = explode(',',$cf_map_data['billing_phone']);
						if(is_array($bp_a) && in_array('bill_addr',array_map('trim', $bp_a))){
							$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
							if($_billing_phone!=''){
								$BillAddr->setLine2($_billing_phone);
								$is_cf_bf_applied = true;
							}
						}
					}
					
					if($is_cf_bf_applied){
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine5($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}else{
						$BillAddr->setLine2($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}				
					
					$BillAddr->setCity($this->get_array_isset($invoice_data,'_billing_city','',true));
					
					$country = $this->get_array_isset($invoice_data,'_billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_billing_state','',true));
					$BillAddr->setPostalCode($this->get_array_isset($invoice_data,'_billing_postcode','',true));
					$invoice->setBillAddr($BillAddr);
					
					//ShipAddr
					if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($this->get_array_isset($invoice_data,'_shipping_first_name','',true).' '.$this->get_array_isset($invoice_data,'_shipping_last_name','',true));
						
						$is_cf_bf_applied = false;
						if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
							$bp_a = explode(',',$cf_map_data['billing_phone']);
							if(is_array($bp_a) && in_array('ship_addr',array_map('trim', $bp_a))){
								$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
								if($_billing_phone!=''){
									$ShipAddr->setLine2($_billing_phone);
									$is_cf_bf_applied = true;
								}
							}
						}
						
						if($is_cf_bf_applied){
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine5($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}else{
							$ShipAddr->setLine2($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}
												
						$ShipAddr->setCity($this->get_array_isset($invoice_data,'_shipping_city','',true));
						
						$country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_shipping_state','',true));
						$ShipAddr->setPostalCode($this->get_array_isset($invoice_data,'_shipping_postcode','',true));
						$invoice->setShipAddr($ShipAddr);
					}
					
					/*Add  Invoice Note Start*/
					
					$customer_note = $this->get_array_isset($invoice_data,'customer_note','',true);
					//if($customer_note!=''){}
					
					if($this->option_checked('mw_wc_qbo_sync_invoice_notes')){
						//custom field
						$note_cf_id = (int) $this->get_option('mw_wc_qbo_sync_invoice_note_id');
						$note_cf_name = trim($this->get_option('mw_wc_qbo_sync_invoice_note_id'));
						
						if($note_cf_id && $note_cf_name!=''){
							$Cus_Field = new QuickBooks_IPP_Object_CustomField();
							$Cus_Field->setDefinitionId($note_cf_id);
							$Cus_Field->setName($note_cf_name);
							
							$Cus_Field->setType('StringType');
							$Cus_Field->setStringValue($customer_note);
							$invoice->addCustomField($Cus_Field);
							
						}
						
					}elseif($this->option_checked('mw_wc_qbo_sync_invoice_memo')){
						//29-03-2017
						if(strlen($customer_note) > 4000){
							$customer_note = substr($customer_note,0,4000);
						}
						$invoice->setPrivateNote($customer_note);
					}else{
						if(strlen($customer_note) > 1000){
							$customer_note = substr($customer_note,0,1000);
						}
						$invoice->setCustomerMemo($customer_note);
					}
					
					if($this->option_checked('mw_wc_qbo_sync_invoice_memo_statement')){
						if(strlen($first_line_desc) > 4000){
							$first_line_desc = substr($first_line_desc,0,4000);
						}
						$invoice->setPrivateNote($first_line_desc);
					}
					
					/*Add Invoice Note End*/
					
					//Custom Field Mapiing					
					if(isset($cf_map_data['shipping_details'])){
						if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
							$shipping_details = $this->get_shipping_details_from_order_data($invoice_data);
							if($cf_map_data['shipping_details']=='customer_memo'){
								$invoice->setCustomerMemo($shipping_details);
							}
						}						
					}
					
					if(isset($cf_map_data['shipping_method_name']) && $cf_map_data['shipping_method_name']=='ship_method'){
						$invoice->setShipMethodRef($shipping_method_name);
					}
					
					$is_dpt_added = false;
					if($this->is_plugin_active('woocommerce-hear-about-us') && $this->get_qbo_company_setting('TrackDepartments') && $this->option_checked('mw_wc_qbo_sync_compt_wchau_enable')){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$mdp_id = (int) $this->get_compt_map_dep_item_id($source);
							if($mdp_id){
								$invoice->setDepartmentRef($mdp_id);
								$is_dpt_added = true;
							}
						}
					}
					
					if(!$is_dpt_added && isset($cf_map_data['source']) && $cf_map_data['source']!=''){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						$cf_s = explode(',',$cf_map_data['source']);
						if(is_array($cf_s) && count($cf_s)==2){
							$cf_s = array_map('trim', $cf_s);
							$cf_s_id = (int) $cf_s[0];
							$cf_s_name = $cf_s[1];
							if($cf_s_id && $cf_s_name!=''){
								$Cus_Field = new QuickBooks_IPP_Object_CustomField();
								$Cus_Field->setDefinitionId($cf_s_id);
								$Cus_Field->setName($cf_s_name);
								
								$Cus_Field->setType('StringType');
								$Cus_Field->setStringValue($source);
								$invoice->addCustomField($Cus_Field);
							}
						}						
					}
					
					/*Add Invoice Currency Start*/
					
					$_order_currency = $this->get_array_isset($invoice_data,'_order_currency','',true);
					$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
					if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
						
						$currency_rate_date = $wc_inv_date;
						$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
						
						$invoice->setCurrencyRef($_order_currency);
						$invoice->setExchangeRate($currency_rate);
					}
					
					/*Add Invoice Currency End*/
					
					$_payment_method = $this->get_array_isset($invoice_data,'_payment_method','',true);					
					$payment_method_map_data  = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					$term_id	= (int) $this->get_array_isset($payment_method_map_data,'term_id','',true);
					if($term_id){
						$invoice->setSalesTermRef($term_id);
					}
					
					/*Add Invoice Tax*/
					if($apply_tax && $is_tax_applied && $Tax_Rate_Ref!=''  && $Tax_Name!=''){
						$TxnTaxDetail = new QuickBooks_IPP_Object_TxnTaxDetail();
						$TxnTaxDetail->setTxnTaxCodeRef($qbo_tax_code);
						$TaxLine = new QuickBooks_IPP_Object_TaxLine();
						$TaxLine->setDetailType('TaxLineDetail');
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLine_2 = new QuickBooks_IPP_Object_TaxLine();
							$TaxLine_2->setDetailType('TaxLineDetail');
							
							$TaxLineDetail_2 = new QuickBooks_IPP_Object_TaxLineDetail();
						}
						
						$TaxLineDetail = new QuickBooks_IPP_Object_TaxLineDetail();

						$TaxLineDetail->setTaxRateRef($Tax_Rate_Ref);
						$TaxLineDetail->setPerCentBased('true');
						
						//$NetAmountTaxable = 0;
						//$TaxLineDetail->setNetAmountTaxable($NetAmountTaxable);
						
						$TaxLineDetail->setTaxPercent($TaxPercent);
						
						$TaxLine->addTaxLineDetail($TaxLineDetail);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLineDetail_2->setTaxRateRef($Tax_Rate_Ref_2);
							$TaxLineDetail_2->setPerCentBased('true');
							$TaxLineDetail_2->setTaxPercent($TaxPercent_2);
							
							//$NetAmountTaxable_2 = 0;
							//$TaxLineDetail_2->setNetAmountTaxable($NetAmountTaxable_2);
							
							$TaxLine_2->addTaxLineDetail($TaxLineDetail_2);
						}
						
						$TxnTaxDetail->addTaxLine($TaxLine);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TxnTaxDetail->addTaxLine($TaxLine_2);
						}
						
						$SalesTax = new QuickBooks_IPP_Object_SalesTax();
						$SalesTax->setTaxable('true');
						$SalesTax->setSalesTaxCodeId($Tax_Rate_Ref);

						$SalesTax->setSalesTaxCodeName($Tax_Name);

						$invoice->addSalesTax($SalesTax);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$SalesTax_2 = new QuickBooks_IPP_Object_SalesTax();
							$SalesTax_2->setTaxable('true');
							$SalesTax_2->setSalesTaxCodeId($Tax_Rate_Ref_2);

							$SalesTax_2->setSalesTaxCodeName($Tax_Name_2);

							$invoice->addSalesTax($SalesTax_2);							
						}
						
						$invoice->addTxnTaxDetail($TxnTaxDetail);						
						
					}
					
					//_transaction_id
					
					
					//
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($invoice_data);
					//$this->_p($invoice);
					//return false;
					
					 if ($resp = $invoiceService->update($Context, $realm, $invoice->getId(), $invoice)){
						$qbo_inv_id = $this->qbo_clear_braces($invoice->getId());
						$log_title.="Update Order #$ord_id_num\n";
						$log_details.="Order #$ord_id_num has been updated, QuickBooks Invoice ID is #$qbo_inv_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Invoice Update',$invoice_data,$invoice,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_inv_id;
						
					}else{
						$res_err = $invoiceService->lastError($Context);
						$log_title.="Update Order Error #$ord_id_num\n";
						$log_details.="Error:$res_err";						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Invoice Update',$invoice_data,$invoice,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
					
				}
			}
		}
	}
	
	/**
	 * Update SalesReceipt Into Quickbooks Online.
	 *
	 * @since    1.0.9
	 * Last Updated: 2017-03-14
	*/
	
	public function UpdateSalesReceipt($invoice_data){
		$manual = $this->get_array_isset($invoice_data,'manual',false);
		if($manual){
			$this->set_session_val('sync_window_push_manual_update',true);
		}
		
		if($this->is_connected()){
			$wc_inv_id = $this->get_array_isset($invoice_data,'wc_inv_id',0);
			$wc_inv_num = $this->get_array_isset($invoice_data,'wc_inv_num','');
			$wc_cus_id = $this->get_array_isset($invoice_data,'wc_cus_id','');
			
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			//Zero Total Option Check
			$_order_total = $this->get_array_isset($invoice_data,'_order_total',0);
			if($this->option_checked('mw_wc_qbo_sync_null_invoice')){
				if($_order_total==0 || $_order_total<0){
					$this->save_log('Update Sales Receipt Error #'.$ord_id_num,'Order amount 0 not allowed in setting ','Invoice',0);
					return false;
				}				
			}
			
			if($this->if_sync_invoice($wc_inv_id,$wc_cus_id,$wc_inv_num)){
				if($SalesReceipt = $this->check_quickbooks_salesreceipt_get_obj($wc_inv_id,$wc_inv_num)){
					//$this->_p($SalesReceipt);die;
					
					//Remove Line Items
					$line_count = $SalesReceipt->countLine();
					for($i=0;$i<=$line_count;$i++){
						$SalesReceipt->unsetLine($i);
					}
					
					//26-04-2017
					for($i=0;$i<=$SalesReceipt->countTxnTaxDetail();$i++){
						$SalesReceipt->unsetTxnTaxDetail($i);
					}
					
					$wc_inv_date = $this->get_array_isset($invoice_data,'wc_inv_date','');
					$wc_inv_date = $this->view_date($wc_inv_date);
					
					$wc_inv_due_date = $this->get_array_isset($invoice_data,'wc_inv_due_date','');					
					$wc_inv_due_date = $this->view_date($wc_inv_due_date);
					
					$qbo_customerid = $this->get_array_isset($invoice_data,'qbo_customerid',0);
					
					if($this->qbo_clear_braces($SalesReceipt->getCustomerRef()) != $qbo_customerid){
						$this->save_log('Update Sales Receipt Error #'.$ord_id_num,'Customer Mismatch ','Invoice',0);
						return false;
					}
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$SalesReceiptService = new QuickBooks_IPP_Service_SalesReceipt();
					
					$SalesReceipt->setTxnDate($wc_inv_date);
					
					
					/*Count Total Amounts*/
					$_cart_discount = $this->get_array_isset($invoice_data,'_cart_discount',0);
					$_cart_discount_tax = $this->get_array_isset($invoice_data,'_cart_discount_tax',0);
					
					$_order_tax = $this->get_array_isset($invoice_data,'_order_tax',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					//Shipping Total
					$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					
					$qbo_inv_items = (isset($invoice_data['qbo_inv_items']))?$invoice_data['qbo_inv_items']:array();					
					
					$total_line_subtotal = 0;
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){
						foreach($qbo_inv_items as $qbo_item){
							$total_line_subtotal+=$qbo_item['line_subtotal'];
						}
					}
					
					//Qbo settings
					$qbo_is_sales_tax = $this->get_qbo_company_setting('is_sales_tax');
					$qbo_company_country = $this->get_qbo_company_info('country');
					
					//Tax rates
					$qbo_tax_code = '';
					$apply_tax = false;
					$is_tax_applied = false;
					$is_inclusive = false;
					
					$tax_rate_id = 0;
					$tax_rate_id_2 = 0;
					
					$tax_details = (isset($invoice_data['tax_details']))?$invoice_data['tax_details']:array();
					if(count($tax_details)){
						$tax_rate_id = $tax_details[0]['rate_id'];
					}
					if(count($tax_details)>1){
						$tax_rate_id_2 = $tax_details[1]['rate_id'];
					}
					
					$qbo_tax_code = $this->get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2);
					if($qbo_tax_code!='' || $qbo_tax_code!='NON'){
						$apply_tax = true;
					}
					
					$Tax_Code_Details = $this->mod_qbo_get_tx_dtls($qbo_tax_code);					
					$is_qbo_dual_tax = false;
					
					if(count($Tax_Code_Details)){
						if($Tax_Code_Details['TaxGroup'] && count($Tax_Code_Details['TaxRateDetail'])>1){
							$is_qbo_dual_tax = true;
						}
					}
					
					
					$Tax_Rate_Ref = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']:'';
					$TaxPercent = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref);
					$Tax_Name = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef_name']:'';
					
					if($is_qbo_dual_tax){
						$Tax_Rate_Ref_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']:'';
						$TaxPercent_2 = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref_2);						
						$Tax_Name_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef_name']:'';
					}
					
					$_prices_include_tax = $this->get_array_isset($invoice_data,'_prices_include_tax','no',true);
					if($qbo_is_sales_tax){						
						$tax_type = $this->get_tax_type($_prices_include_tax);
						$is_inclusive = $this->is_tax_inclusive($tax_type);
						$SalesReceipt->setGlobalTaxCalculation($tax_type);
						$SalesReceipt->setApplyTaxAfterDiscount(true);
					}
					
					//Bundle Support
					$is_bundle_order = false;
					if($this->is_plugin_active('woocommerce-product-bundles') && $this->option_checked('mw_wc_qbo_sync_compt_wpbs')){
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if(isset($qbo_item['bundled_items']) && $qbo_item['qbo_product_type'] == 'Group'){
									$is_bundle_order = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									//$this->_p($qbo_gp_details);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_compt_wpbs_ap_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$SalesReceipt->addLine($line);
								}
							}
							
						}
					}
					
					//Map Bundle Support
					$map_bundle_support = false;
					if(!$is_bundle_order){					
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if($qbo_item['qbo_product_type'] == 'Group'){
									$map_bundle_support = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$SalesReceipt->addLine($line);						
								}
							}
						}
					}
					
					//Add SalesReceipt items
					$first_line_desc = '';
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){						
						$first_line_desc = $qbo_inv_items[0]['Description'];
						foreach($qbo_inv_items as $qbo_item){
							//Bundle Support
							if($is_bundle_order){
								if(isset($qbo_item['bundled_items']) || isset($qbo_item['bundled_item_id']) || isset($qbo_item['bundle_cart_key'])){
									continue;
								}
							}
							
							if($map_bundle_support && $qbo_item['qbo_product_type'] == 'Group'){
								continue;
							}
							
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							$UnitPrice = $qbo_item["UnitPrice"];
							if($_cart_discount){
								//$UnitPrice = $this->get_discounted_item_price($_cart_discount,$total_line_subtotal,$UnitPrice);
							}
							
							$Amount = $qbo_item['Qty']*$UnitPrice;					
							
							$line->setAmount($Amount);
							$line->setDescription($qbo_item['Description']);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							$tax_class =  $qbo_item["tax_class"];
							
							if($qbo_is_sales_tax){
								if($apply_tax && $qbo_item["Taxed"]){
									$is_tax_applied = true;														
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									
									if($is_inclusive){
										
									}
									
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$salesItemLineDetail->setItemRef($qbo_item["ItemRef"]);
							
							$Qty = $qbo_item["Qty"];
							//10-05-2017
							if($this->is_plugin_active('woocommerce-measurement-price-calculator') && isset($qbo_item['measurement_data']) && $this->option_checked('mw_wc_qbo_sync_measurement_qty')){
								$measurement_data = unserialize($qbo_item['measurement_data']);
								if(is_array($measurement_data) && isset($measurement_data['_measurement_needed'])){
									$_measurement_needed = floatval($measurement_data['_measurement_needed']);
									if($_measurement_needed>0){
										$UnitPrice = $UnitPrice/$_measurement_needed;
										//$UnitPrice = number_format($UnitPrice, 2);
										$_quantity = (isset($measurement_data['_quantity']))?$measurement_data['_quantity']:1;
										$Qty = $_measurement_needed*$_quantity;
									}
								}
							}
							
							$salesItemLineDetail->setQty($Qty);
							$salesItemLineDetail->setUnitPrice($UnitPrice);
							
							if(isset($qbo_item["ClassRef"]) && $qbo_item["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($qbo_item["ClassRef"]);
							}
							
							
							if($this->option_checked('mw_wc_qbo_sync_invoice_date')){
								$salesItemLineDetail->setServiceDate($wc_inv_date);
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);
							$SalesReceipt->addLine($line);
						}
					}
					
					//pgdf compatibility					
					if($this->get_wc_fee_plugin_check()){
						$dc_gt_fees = (isset($invoice_data['dc_gt_fees']))?$invoice_data['dc_gt_fees']:array();
						if(is_array($dc_gt_fees) && count($dc_gt_fees)){
							foreach($dc_gt_fees as $df){
								$line = new QuickBooks_IPP_Object_Line();
								$line->setDetailType('SalesItemLineDetail');
								
								$UnitPrice = $df['_line_total'];
								$Qty = 1;
								$Amount = $Qty*$UnitPrice;		
								
								$line->setAmount($Amount);
								$line->setDescription($df['name']);
								
								$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
								$_line_tax = $df['_line_tax'];
								//$df_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($df['_line_tax_data']);
								if($_line_tax && $qbo_is_sales_tax){
									//$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$df_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}
								if(!$_line_tax && $qbo_is_sales_tax){
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
								
								/*
								$df_ItemRef = (int) $this->get_option('mw_wc_qbo_sync_compt_gf_qbo_item');
								if(!$df_ItemRef){
									$df_ItemRef = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
								}
								*/
								$df_ItemRef = $this->get_wc_fee_qbo_product($df['name']);
								
								$salesItemLineDetail->setItemRef($df_ItemRef);
								$salesItemLineDetail->setQty($Qty);							
								$salesItemLineDetail->setUnitPrice($UnitPrice);
								
								$line->addSalesItemLineDetail($salesItemLineDetail);
								$SalesReceipt->addLine($line);
							}
						}
					}
					
					/*Add SalesReceipt Shipping*/
					$shipping_details  = (isset($invoice_data['shipping_details']))?$invoice_data['shipping_details']:array();
					
					$shipping_method = '';
					$shipping_method_name = '';
					
					$shipping_taxes = '';
					
					if(isset($shipping_details[0])){
						if($this->get_array_isset($shipping_details[0],'type','')=='shipping'){
							$shipping_method_id = $this->get_array_isset($shipping_details[0],'method_id','');
							if($shipping_method_id!=''){
								//$shipping_method = substr($shipping_method_id, 0, strpos($shipping_method_id, ":"));
								$shipping_method = $this->wc_get_sm_data_from_method_id_str($shipping_method_id);
							}
							$shipping_method = ($shipping_method=='')?'no_method_found':$shipping_method;
							
							$shipping_method_name =  $this->get_array_isset($shipping_details[0],'name','',true,30);
							
							//Serialized
							$shipping_taxes = $this->get_array_isset($shipping_details[0],'taxes','');
							//$shipping_taxes = unserialize($shipping_taxes);
							//$this->_p($shipping_taxes);							
						}						
					}
					
					$qbo_is_shipping_allowed = $this->get_qbo_company_setting('is_shipping_allowed');
					
					//$order_shipping_total+=$_order_shipping_tax;
					
					if($shipping_method!=''){						
						if($qbo_is_shipping_allowed){
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							$line->setAmount($order_shipping_total);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef('SHIPPING_ITEM_ID');
							
							if($qbo_is_sales_tax){
								if($_order_shipping_tax > 0){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}else{
							$shipping_product_arr = $this->get_mapped_shipping_product($shipping_method);						
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							//
							$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
							
							$line->setAmount($order_shipping_total);
							
							$shipping_description = ($shipping_method_name!='')?'Shipping ('.$shipping_method_name.')':'Shipping';
							
							$line->setDescription($shipping_description);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							
							$salesItemLineDetail->setItemRef($shipping_product_arr["ItemRef"]);
							
							if(isset($shipping_product_arr["ClassRef"]) && $shipping_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($shipping_product_arr["ClassRef"]);
							}
							
							$salesItemLineDetail->setUnitPrice($order_shipping_total);
							
							
							if($qbo_is_sales_tax){								
								if($_order_shipping_tax > 0){
									//$shipping_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($shipping_taxes,'shipping');
									//$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$shipping_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}
						
					}
					
					/*Add SalesReceipt Coupons*/
					$used_coupons  = (isset($invoice_data['used_coupons']))?$invoice_data['used_coupons']:array();
					
					$qbo_is_discount_allowed = $this->get_qbo_company_setting('is_discount_allowed');
					
					if(count($used_coupons)){
						foreach($used_coupons as $coupon){
							$coupon_name = $coupon['name'];
							$coupon_discount_amount = $coupon['discount_amount'];							
							$coupon_discount_amount = -1 * abs($coupon_discount_amount);
							
							$coupon_discount_amount_tax = $coupon['discount_amount_tax'];
							
							$coupon_product_arr = $this->get_mapped_coupon_product($coupon_name);
							$line = new QuickBooks_IPP_Object_Line();
							
							$line->setDetailType('SalesItemLineDetail');
							if($qbo_is_discount_allowed){
								$line->setAmount(0);
							}else{
								$line->setAmount($coupon_discount_amount);
							}
							
							
							$line->setDescription($coupon_product_arr['Description']);
						
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef($coupon_product_arr['ItemRef']);
							if(isset($coupon_product_arr["ClassRef"]) && $coupon_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($coupon_product_arr["ClassRef"]);
							}
							if($qbo_is_discount_allowed){
								//$salesItemLineDetail->setUnitPrice(0);
							}else{
								$salesItemLineDetail->setUnitPrice($coupon_discount_amount);
							}
							
							if($qbo_is_sales_tax){
								if($coupon_discount_amount_tax > 0 || $is_tax_applied){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}							
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}
					}					
					
					/*Discount Line*/
					if($_cart_discount && $qbo_is_discount_allowed){
						$qbo_discount_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_discount_account');
						$line = new QuickBooks_IPP_Object_Line();
						$line->setDetailType('DiscountLineDetail');							
						$line->setAmount($_cart_discount);						
						$line->setDescription('Total Discount');
						
						$discountLineDetail = new QuickBooks_IPP_Object_DiscountLineDetail();
						$discountLineDetail->setPercentBased(false);
						$discountLineDetail->setDiscountAccountRef($qbo_discount_account);
						
						if($qbo_is_sales_tax){
							if($is_tax_applied){
								$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
								if($TaxCodeRef!=''){
									$discountLineDetail->setTaxCodeRef($TaxCodeRef);
								}
							}else{
								$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
								$discountLineDetail->setTaxCodeRef($zero_rated_tax_code);
							}							
						}
						
						$line->addDiscountLineDetail($discountLineDetail);						
						$SalesReceipt->addLine($line);
						
					}
					
					//15-05-2017
					if($this->is_plugin_active('woocommerce-order-delivery') && $this->option_checked('mw_wc_qbo_sync_compt_p_wod')){
						if($SalesReceipt->countShipDate()){
							$SalesReceipt->unsetShipDate();
						}
						$_delivery_date = $this->get_array_isset($invoice_data,'_delivery_date','',true);
						if($_delivery_date!=''){
							$_delivery_date = $this->view_date($_delivery_date);
							$SalesReceipt->setShipDate($_delivery_date);
						}
					}

					//
					$cf_map_data = $this->get_cf_map_data();
					
					//20-04-2017
					$_billing_email = $this->get_array_isset($invoice_data,'_billing_email','',true);
					$BillEmail = new QuickBooks_IPP_Object_BillEmail();
					$BillEmail->setAddress($_billing_email);
					$SalesReceipt->setBillEmail($BillEmail);
					
					//BillAddr
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($this->get_array_isset($invoice_data,'_billing_first_name','',true).' '.$this->get_array_isset($invoice_data,'_billing_last_name','',true));
					
					$is_cf_bf_applied = false;
					if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
						$bp_a = explode(',',$cf_map_data['billing_phone']);
						if(is_array($bp_a) && in_array('bill_addr',array_map('trim', $bp_a))){
							$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
							if($_billing_phone!=''){
								$BillAddr->setLine2($_billing_phone);
								$is_cf_bf_applied = true;
							}
						}
					}
					
					if($is_cf_bf_applied){
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine5($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}else{
						$BillAddr->setLine2($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}
					
					$BillAddr->setCity($this->get_array_isset($invoice_data,'_billing_city','',true));
					
					$country = $this->get_array_isset($invoice_data,'_billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_billing_state','',true));
					$BillAddr->setPostalCode($this->get_array_isset($invoice_data,'_billing_postcode','',true));
					$SalesReceipt->setBillAddr($BillAddr);
					
					//ShipAddr
					if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($this->get_array_isset($invoice_data,'_shipping_first_name','',true).' '.$this->get_array_isset($invoice_data,'_shipping_last_name','',true));
						
						$is_cf_bf_applied = false;
						if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
							$bp_a = explode(',',$cf_map_data['billing_phone']);
							if(is_array($bp_a) && in_array('ship_addr',array_map('trim', $bp_a))){
								$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
								if($_billing_phone!=''){
									$ShipAddr->setLine2($_billing_phone);
									$is_cf_bf_applied = true;
								}
							}
						}
						
						if($is_cf_bf_applied){
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine5($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}else{
							$ShipAddr->setLine2($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}
												
						$ShipAddr->setCity($this->get_array_isset($invoice_data,'_shipping_city','',true));
						
						$country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_shipping_state','',true));
						$ShipAddr->setPostalCode($this->get_array_isset($invoice_data,'_shipping_postcode','',true));
						$SalesReceipt->setShipAddr($ShipAddr);
					}
					
					/*Add  SalesReceipt Note Start*/
					
					$customer_note = $this->get_array_isset($invoice_data,'customer_note','',true);
					//if($customer_note!=''){}					
					if($this->option_checked('mw_wc_qbo_sync_invoice_notes')){
						//custom field
						$note_cf_id = (int) $this->get_option('mw_wc_qbo_sync_invoice_note_id');
						$note_cf_name = trim($this->get_option('mw_wc_qbo_sync_invoice_note_id'));
						
						if($note_cf_id && $note_cf_name!=''){
							$Cus_Field = new QuickBooks_IPP_Object_CustomField();
							$Cus_Field->setDefinitionId($note_cf_id);
							$Cus_Field->setName($note_cf_name);
							
							$Cus_Field->setType('StringType');
							$Cus_Field->setStringValue($customer_note);
							$SalesReceipt->addCustomField($Cus_Field);
							
						}
						
					}elseif($this->option_checked('mw_wc_qbo_sync_invoice_memo')){
						if(strlen($customer_note) > 4000){
							$customer_note = substr($customer_note,0,4000);
						}
						$SalesReceipt->setPrivateNote($customer_note);
					}else{
						if(strlen($customer_note) > 1000){
							$customer_note = substr($customer_note,0,1000);
						}
						$SalesReceipt->setCustomerMemo($customer_note);
					}
					if($this->option_checked('mw_wc_qbo_sync_invoice_memo_statement')){
						$SalesReceipt->setPrivateNote($first_line_desc);
					}
					
					/*Add SalesReceipt Note End*/
					
					//Custom Field Mapiing					
					if(isset($cf_map_data['shipping_details'])){
						if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
							$shipping_details = $this->get_shipping_details_from_order_data($invoice_data);
							if($cf_map_data['shipping_details']=='customer_memo'){
								$SalesReceipt->setCustomerMemo($shipping_details);
							}
						}						
					}
					
					if(isset($cf_map_data['shipping_method_name']) && $cf_map_data['shipping_method_name']=='ship_method'){
						$SalesReceipt->setShipMethodRef($shipping_method_name);
					}
					
					$is_dpt_added = false;
					if($this->is_plugin_active('woocommerce-hear-about-us') && $this->get_qbo_company_setting('TrackDepartments') && $this->option_checked('mw_wc_qbo_sync_compt_wchau_enable')){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$mdp_id = (int) $this->get_compt_map_dep_item_id($source);
							if($mdp_id){
								$SalesReceipt->setDepartmentRef($mdp_id);
								$is_dpt_added = true;
							}
						}
					}
					
					if(!$is_dpt_added && isset($cf_map_data['source']) && $cf_map_data['source']!=''){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						$cf_s = explode(',',$cf_map_data['source']);
						if(is_array($cf_s) && count($cf_s)==2){
							$cf_s = array_map('trim', $cf_s);
							$cf_s_id = (int) $cf_s[0];
							$cf_s_name = $cf_s[1];
							if($cf_s_id && $cf_s_name!=''){
								$Cus_Field = new QuickBooks_IPP_Object_CustomField();
								$Cus_Field->setDefinitionId($cf_s_id);
								$Cus_Field->setName($cf_s_name);
								
								$Cus_Field->setType('StringType');
								$Cus_Field->setStringValue($source);
								$SalesReceipt->addCustomField($Cus_Field);
							}
						}
					}
					
					/*Add SalesReceipt Currency Start*/
					
					$_order_currency = $this->get_array_isset($invoice_data,'_order_currency','',true);
					$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
					if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
						
						$currency_rate_date = $wc_inv_date;
						$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
						
						$SalesReceipt->setCurrencyRef($_order_currency);
						$SalesReceipt->setExchangeRate($currency_rate);
					}
					
					/*Add SalesReceipt Currency End*/
					
					
					/*Add SalesReceipt Tax*/
					if($apply_tax && $is_tax_applied && $Tax_Rate_Ref!=''  && $Tax_Name!=''){
						$TxnTaxDetail = new QuickBooks_IPP_Object_TxnTaxDetail();
						$TxnTaxDetail->setTxnTaxCodeRef($qbo_tax_code);
						$TaxLine = new QuickBooks_IPP_Object_TaxLine();
						$TaxLine->setDetailType('TaxLineDetail');
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLine_2 = new QuickBooks_IPP_Object_TaxLine();
							$TaxLine_2->setDetailType('TaxLineDetail');
							
							$TaxLineDetail_2 = new QuickBooks_IPP_Object_TaxLineDetail();
						}
						
						$TaxLineDetail = new QuickBooks_IPP_Object_TaxLineDetail();

						$TaxLineDetail->setTaxRateRef($Tax_Rate_Ref);
						$TaxLineDetail->setPerCentBased('true');
						
						//$NetAmountTaxable = 0;
						//$TaxLineDetail->setNetAmountTaxable($NetAmountTaxable);
						
						$TaxLineDetail->setTaxPercent($TaxPercent);
						
						$TaxLine->addTaxLineDetail($TaxLineDetail);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLineDetail_2->setTaxRateRef($Tax_Rate_Ref_2);
							$TaxLineDetail_2->setPerCentBased('true');
							$TaxLineDetail_2->setTaxPercent($TaxPercent_2);
							
							//$NetAmountTaxable_2 = 0;
							//$TaxLineDetail_2->setNetAmountTaxable($NetAmountTaxable_2);
							
							$TaxLine_2->addTaxLineDetail($TaxLineDetail_2);
						}
						
						$TxnTaxDetail->addTaxLine($TaxLine);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TxnTaxDetail->addTaxLine($TaxLine_2);
						}
						
						$SalesTax = new QuickBooks_IPP_Object_SalesTax();
						$SalesTax->setTaxable('true');
						$SalesTax->setSalesTaxCodeId($Tax_Rate_Ref);

						$SalesTax->setSalesTaxCodeName($Tax_Name);

						$SalesReceipt->addSalesTax($SalesTax);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$SalesTax_2 = new QuickBooks_IPP_Object_SalesTax();
							$SalesTax_2->setTaxable('true');
							$SalesTax_2->setSalesTaxCodeId($Tax_Rate_Ref_2);

							$SalesTax_2->setSalesTaxCodeName($Tax_Name_2);

							$SalesReceipt->addSalesTax($SalesTax_2);							
						}
						
						$SalesReceipt->addTxnTaxDetail($TxnTaxDetail);						
						
					}
					
					//					
					$_transaction_id = $this->get_array_isset($invoice_data,'_transaction_id','',true);
					
					
					$_payment_method = $this->get_array_isset($invoice_data,'_payment_method','',true);
					$payment_method_map_data  = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					
					if(count($payment_method_map_data)){
						if($payment_method_map_data['enable_payment']){
							$payment_data = $this->wc_get_payment_details_by_txn_id($_transaction_id,$wc_inv_id);
							
							if($payment_method_map_data['qb_p_method_id']){
								$SalesReceipt->setPaymentMethodRef($payment_method_map_data['qb_p_method_id']);
							}
							
							//CreditCardPayment
							$SalesReceipt->setDepositToAccountRef($payment_method_map_data['qbo_account_id']);
							
							if(count($payment_data)){								
								$SalesReceipt->setPaymentRefNum($payment_data['payment_id']);
							}
						}	
					}
					
					//
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($invoice_data);
					//$this->_p($SalesReceipt);
					//return false;
					
					if ($resp = $SalesReceiptService->update($Context, $realm, $SalesReceipt->getId(), $SalesReceipt)){
						$qbo_sr_id = $this->qbo_clear_braces($SalesReceipt->getId());
						$log_title.="Update Order #$ord_id_num\n";
						$log_details.="Order #$ord_id_num has been updated, QuickBooks SalesReceipt ID is #$qbo_sr_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Sales Receipt Update',$invoice_data,$SalesReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_sr_id;
						
					}else{
						$res_err = $SalesReceiptService->lastError($Context);
						$log_title.="Update Order Error #$ord_id_num\n";
						$log_details.="Error:$res_err";						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Sales Receipt Update',$invoice_data,$SalesReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}
		}
	}
	
	//27-04-2017
	public function wc_get_sm_data_from_method_id_str($method_id='',$key=''){
		$shipping_method = '';
		if($method_id!=''){
			if(strpos( $method_id, ':' ) !== false){
				$shipping_method = substr($method_id, 0, strpos($method_id, ":"));
			}else{
				$sm_arr = explode('_',$method_id);
				if(is_array($sm_arr) && count($sm_arr)>2){
					$sm_count = count($sm_arr);
					
					$sm_id_index = (int) ($sm_count-2);
					$sm_id = 0;
					if(isset($sm_arr[$sm_id_index]) && is_numeric($sm_arr[$sm_id_index])){
						$sm_id = (int) $sm_arr[$sm_id_index];
						unset($sm_arr[$sm_id_index]);
					}
					
					
					$sm_reg_id_index = (int) ($sm_count-1);
					$sm_reg_id = 0;
					if(isset($sm_arr[$sm_reg_id_index]) && is_numeric($sm_arr[$sm_reg_id_index])){
						$sm_reg_id = (int) $sm_arr[$sm_reg_id_index];
						unset($sm_arr[$sm_reg_id_index]);
					}
					$shipping_method = implode('_',$sm_arr);
				}
			}
		}
		return $shipping_method;
	}
	
	public function get_cf_map_data(){
		$rd = array();
		global $wpdb;
		$cmd = $this->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_wq_cf_map');
		if(is_array($cmd) && count($cmd)){
			foreach($cmd as $row){
				$rd[$row['wc_field']] = $row['qb_field'];
			}
		}
		return $rd;
	}
	
	public function get_shipping_details_from_order_data($invoice_data){
		$sd='';
		if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){	
			$_shipping_first_name = $this->get_array_isset($invoice_data,'_shipping_first_name','',true);
			$_shipping_last_name = $this->get_array_isset($invoice_data,'_shipping_last_name','',true);
			
			$_shipping_company = $this->get_array_isset($invoice_data,'_shipping_company','',true);
			
			$_shipping_address_1 = $this->get_array_isset($invoice_data,'_shipping_address_1','',true);
			$_shipping_address_2 = $this->get_array_isset($invoice_data,'_shipping_address_2','',true);
			
			$_shipping_city = $this->get_array_isset($invoice_data,'_shipping_city','',true);
			$_shipping_country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
			$country = $this->get_country_name_from_code($_shipping_country);
			
			$_shipping_state = $this->get_array_isset($invoice_data,'_shipping_state','',true);
			$_shipping_postcode = $this->get_array_isset($invoice_data,'_shipping_postcode','',true);
			
			$sd = $_shipping_first_name.' '.$_shipping_last_name.PHP_EOL;
			$sd.=$_shipping_company.PHP_EOL;
			
			$sd.=$_shipping_address_1.PHP_EOL;
			$sd.=$_shipping_address_2.PHP_EOL;
			
			$sd.=$_shipping_city.', '.$_shipping_state.' '.$_shipping_postcode.PHP_EOL;
			if($_shipping_country!=$country){
				$sd.=$country.' ('.$_shipping_country.')';
			}else{
				$sd.=$country;
			}			
		}
		return $sd;
	}
	
	/**
	 * Add Invoice Into Quickbooks Online.
	 *
	 * @since    1.0.0
	 * Last Updated: 2017-01-04
	*/
	
	public function AddInvoice($invoice_data){		
		if($this->is_connected()){
			//$this->_p($invoice_data);return false;
			
			$wc_inv_id = $this->get_array_isset($invoice_data,'wc_inv_id',0);
			$wc_inv_num = $this->get_array_isset($invoice_data,'wc_inv_num','');
			$wc_cus_id = $this->get_array_isset($invoice_data,'wc_cus_id','');
			
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			//Zero Total Option Check
			$_order_total = $this->get_array_isset($invoice_data,'_order_total',0);
			if($this->option_checked('mw_wc_qbo_sync_null_invoice')){
				if($_order_total==0 || $_order_total<0){
					$this->save_log('Export Order Error #'.$ord_id_num,'Order amount 0 not allowed in setting ','Invoice',0);
					return false;
				}				
			}
			
			if($this->if_sync_invoice($wc_inv_id,$wc_cus_id,$wc_inv_num)){				
				if(!$this->check_quickbooks_invoice_get_obj($wc_inv_id,$wc_inv_num)){					
					$wc_inv_date = $this->get_array_isset($invoice_data,'wc_inv_date','');
					$wc_inv_date = $this->view_date($wc_inv_date);
					
					$wc_inv_due_date = $this->get_array_isset($invoice_data,'wc_inv_due_date','');					
					$wc_inv_due_date = $this->view_date($wc_inv_due_date);
					
					$qbo_customerid = $this->get_array_isset($invoice_data,'qbo_customerid',0);
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$invoiceService = new QuickBooks_IPP_Service_Invoice();
					$invoice = new QuickBooks_IPP_Object_Invoice();
					
					$DocNumber = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
					$invoice->setDocNumber($DocNumber);
					
					$invoice->setTxnDate($wc_inv_date);
					$invoice->setDueDate($wc_inv_due_date);
					
					$invoice->setCustomerRef($qbo_customerid);
										
					/*Count Total Amounts*/
					$_cart_discount = $this->get_array_isset($invoice_data,'_cart_discount',0);
					$_cart_discount_tax = $this->get_array_isset($invoice_data,'_cart_discount_tax',0);
					
					$_order_tax = $this->get_array_isset($invoice_data,'_order_tax',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					//Shipping Total
					$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					
					$qbo_inv_items = (isset($invoice_data['qbo_inv_items']))?$invoice_data['qbo_inv_items']:array();					
					
					$total_line_subtotal = 0;
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){
						foreach($qbo_inv_items as $qbo_item){
							$total_line_subtotal+=$qbo_item['line_subtotal'];
						}
					}
					
					//Qbo settings
					$qbo_is_sales_tax = $this->get_qbo_company_setting('is_sales_tax');
					$qbo_company_country = $this->get_qbo_company_info('country');
					
					//Tax rates
					$qbo_tax_code = '';
					$apply_tax = false;
					$is_tax_applied = false;
					$is_inclusive = false;
					
					$tax_rate_id = 0;
					$tax_rate_id_2 = 0;
					
					$tax_details = (isset($invoice_data['tax_details']))?$invoice_data['tax_details']:array();
					if(count($tax_details)){
						$tax_rate_id = $tax_details[0]['rate_id'];
					}
					if(count($tax_details)>1){
						$tax_rate_id_2 = $tax_details[1]['rate_id'];
					}
					
					$qbo_tax_code = $this->get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2);
					if($qbo_tax_code!='' || $qbo_tax_code!='NON'){
						$apply_tax = true;
					}
					
					$Tax_Code_Details = $this->mod_qbo_get_tx_dtls($qbo_tax_code);					
					$is_qbo_dual_tax = false;
					
					if(count($Tax_Code_Details)){
						if($Tax_Code_Details['TaxGroup'] && count($Tax_Code_Details['TaxRateDetail'])>1){
							$is_qbo_dual_tax = true;
						}
					}
					
					
					$Tax_Rate_Ref = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']:'';
					$TaxPercent = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref);
					$Tax_Name = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef_name']:'';
					
					if($is_qbo_dual_tax){
						$Tax_Rate_Ref_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']:'';
						$TaxPercent_2 = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref_2);						
						$Tax_Name_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef_name']:'';
					}
					
					$_prices_include_tax = $this->get_array_isset($invoice_data,'_prices_include_tax','no',true);
					if($qbo_is_sales_tax){						
						$tax_type = $this->get_tax_type($_prices_include_tax);
						$is_inclusive = $this->is_tax_inclusive($tax_type);
						$invoice->setGlobalTaxCalculation($tax_type);
						$invoice->setApplyTaxAfterDiscount(true);
					}
					
					//Bundle Support
					$is_bundle_order = false;
					if($this->is_plugin_active('woocommerce-product-bundles') && $this->option_checked('mw_wc_qbo_sync_compt_wpbs')){
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if(isset($qbo_item['bundled_items']) && $qbo_item['qbo_product_type'] == 'Group'){
									$is_bundle_order = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									//$this->_p($qbo_gp_details);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_compt_wpbs_ap_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$invoice->addLine($line);
								}
							}
							
							/*
							if($is_bundle_order){
								$bundle_arr = array();
								foreach($qbo_inv_items as $qbo_item){
									if(isset($qbo_item['bundled_items']) && $qbo_item['qbo_product_type'] == 'Group'){
										$bundled_item_ref = (int) $qbo_item['ItemRef'];
										$bundled_price = $qbo_item['UnitPrice'];
										$bundled_items = $qbo_item['bundled_items'];
										$bundled_items = unserialize($bundled_items);
										
										$stamp = $qbo_item['stamp'];
										$stamp = unserialize($stamp);
										
										$bundle_cart_key = $qbo_item['bundle_cart_key'];
										if(is_array($bundled_items) && count($bundled_items) && is_array($stamp) && count($stamp)){
											foreach($qbo_inv_items as $qbo_item){
												if(isset($qbo_item['bundled_item_id']) && isset($qbo_item['bundled_by']) && isset($qbo_item['bundle_cart_key'])){
													if(in_array($qbo_item['bundle_cart_key'],$bundled_items) && isset($stamp[$qbo_item['bundled_item_id']])){
														if($stamp[$qbo_item['bundled_item_id']]['product_id']==$qbo_item['product_id']){
															if($bundle_cart_key == $qbo_item['bundled_by']){
																//
															}
														}
													}
												}
											}
										}										
									}
								}
							}
							*/
						}
					}
					
					//07-06-2017					
					//Map Bundle Support
					$map_bundle_support = false;
					if(!$is_bundle_order){					
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if($qbo_item['qbo_product_type'] == 'Group'){
									$map_bundle_support = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$invoice->addLine($line);								
								}
							}
						}
					}							
					
					//Add Invoice items
					$first_line_desc = '';
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){						
						$first_line_desc = $qbo_inv_items[0]['Description'];
						foreach($qbo_inv_items as $qbo_item){
							//Bundle Support
							if($is_bundle_order){
								if(isset($qbo_item['bundled_items']) || isset($qbo_item['bundled_item_id']) || isset($qbo_item['bundle_cart_key'])){
									continue;
								}
							}
							
							if($map_bundle_support && $qbo_item['qbo_product_type'] == 'Group'){
								continue;
							}
							
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							$UnitPrice = $qbo_item["UnitPrice"];
							if($_cart_discount){
								//$UnitPrice = $this->get_discounted_item_price($_cart_discount,$total_line_subtotal,$UnitPrice);
							}
							
							$Amount = $qbo_item['Qty']*$UnitPrice;			
							
							$line->setAmount($Amount);
							$line->setDescription($qbo_item['Description']);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							$tax_class =  $qbo_item["tax_class"];
							
							if($qbo_is_sales_tax){
								if($apply_tax && $qbo_item["Taxed"]){
									$is_tax_applied = true;														
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									
									if($is_inclusive){
										
									}
									
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$salesItemLineDetail->setItemRef($qbo_item["ItemRef"]);
							
							$Qty = $qbo_item["Qty"];
							//10-05-2017
							if($this->is_plugin_active('woocommerce-measurement-price-calculator') && isset($qbo_item['measurement_data']) && $this->option_checked('mw_wc_qbo_sync_measurement_qty')){
								$measurement_data = unserialize($qbo_item['measurement_data']);
								if(is_array($measurement_data) && isset($measurement_data['_measurement_needed'])){
									$_measurement_needed = floatval($measurement_data['_measurement_needed']);
									if($_measurement_needed>0){
										$UnitPrice = $UnitPrice/$_measurement_needed;
										//$UnitPrice = number_format($UnitPrice, 2);
										$_quantity = (isset($measurement_data['_quantity']))?$measurement_data['_quantity']:1;
										$Qty = $_measurement_needed*$_quantity;
									}
								}
							}
							
							$salesItemLineDetail->setQty($Qty);							
							$salesItemLineDetail->setUnitPrice($UnitPrice);
							
							if(isset($qbo_item["ClassRef"]) && $qbo_item["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($qbo_item["ClassRef"]);
							}
							
							
							if($this->option_checked('mw_wc_qbo_sync_invoice_date')){
								$salesItemLineDetail->setServiceDate($wc_inv_date);
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);
							$invoice->addLine($line);
						}
					}
					
					//pgdf compatibility					
					if($this->get_wc_fee_plugin_check()){
						$dc_gt_fees = (isset($invoice_data['dc_gt_fees']))?$invoice_data['dc_gt_fees']:array();
						if(is_array($dc_gt_fees) && count($dc_gt_fees)){
							foreach($dc_gt_fees as $df){
								$line = new QuickBooks_IPP_Object_Line();
								$line->setDetailType('SalesItemLineDetail');
								
								$UnitPrice = $df['_line_total'];
								$Qty = 1;
								$Amount = $Qty*$UnitPrice;		
								
								$line->setAmount($Amount);
								$line->setDescription($df['name']);
								
								$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
								$_line_tax = $df['_line_tax'];
								//$df_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($df['_line_tax_data']);
								if($_line_tax && $qbo_is_sales_tax){
									//$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$df_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}
								if(!$_line_tax && $qbo_is_sales_tax){
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
								
								$df_ItemRef = $this->get_wc_fee_qbo_product($df['name']);
								
								$salesItemLineDetail->setItemRef($df_ItemRef);
								$salesItemLineDetail->setQty($Qty);							
								$salesItemLineDetail->setUnitPrice($UnitPrice);
								
								$line->addSalesItemLineDetail($salesItemLineDetail);
								$invoice->addLine($line);
							}
						}
					}					
					
					/*Add Invoice Shipping*/
					$shipping_details  = (isset($invoice_data['shipping_details']))?$invoice_data['shipping_details']:array();
					
					$shipping_method = '';
					$shipping_method_name = '';
					
					$shipping_taxes = '';
					if(isset($shipping_details[0])){
						if($this->get_array_isset($shipping_details[0],'type','')=='shipping'){
							$shipping_method_id = $this->get_array_isset($shipping_details[0],'method_id','');
							if($shipping_method_id!=''){
								//$shipping_method = substr($shipping_method_id, 0, strpos($shipping_method_id, ":"));
								$shipping_method = $this->wc_get_sm_data_from_method_id_str($shipping_method_id);
							}							
							$shipping_method = ($shipping_method=='')?'no_method_found':$shipping_method;
							
							$shipping_method_name =  $this->get_array_isset($shipping_details[0],'name','',true,30);
							
							//Serialized
							$shipping_taxes = $this->get_array_isset($shipping_details[0],'taxes','');
							//$shipping_taxes = unserialize($shipping_taxes);
							//$this->_p($shipping_taxes);							
						}						
					}
					
					$qbo_is_shipping_allowed = $this->get_qbo_company_setting('is_shipping_allowed');
					
					//$order_shipping_total+=$_order_shipping_tax;
					
					if($shipping_method!=''){
						if($qbo_is_shipping_allowed){
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							$line->setAmount($order_shipping_total);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef('SHIPPING_ITEM_ID');
							
							if($qbo_is_sales_tax){
								if($_order_shipping_tax > 0){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}							
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}else{							
							$shipping_product_arr = $this->get_mapped_shipping_product($shipping_method);						
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							//16-05-2017
							$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
							
							$line->setAmount($order_shipping_total);
							
							$shipping_description = ($shipping_method_name!='')?'Shipping ('.$shipping_method_name.')':'Shipping';
							
							$line->setDescription($shipping_description);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							
							$salesItemLineDetail->setItemRef($shipping_product_arr["ItemRef"]);
							
							if(isset($shipping_product_arr["ClassRef"]) && $shipping_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($shipping_product_arr["ClassRef"]);
							}
							
							$salesItemLineDetail->setUnitPrice($order_shipping_total);
							
							
							if($qbo_is_sales_tax){								
								if($_order_shipping_tax > 0){
									//$shipping_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($shipping_taxes,'shipping');
									//$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$shipping_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}
						
					}
					
					/*Add Invoice Coupons*/
					$used_coupons  = (isset($invoice_data['used_coupons']))?$invoice_data['used_coupons']:array();
					
					$qbo_is_discount_allowed = $this->get_qbo_company_setting('is_discount_allowed');
					
					if(count($used_coupons)){
						foreach($used_coupons as $coupon){
							$coupon_name = $coupon['name'];
							$coupon_discount_amount = $coupon['discount_amount'];							
							$coupon_discount_amount = -1 * abs($coupon_discount_amount);
							
							$coupon_discount_amount_tax = $coupon['discount_amount_tax'];
							
							$coupon_product_arr = $this->get_mapped_coupon_product($coupon_name);
							$line = new QuickBooks_IPP_Object_Line();
							
							$line->setDetailType('SalesItemLineDetail');
							if($qbo_is_discount_allowed){
								$line->setAmount(0);
							}else{
								$line->setAmount($coupon_discount_amount);
							}
							
							
							$line->setDescription($coupon_product_arr['Description']);
						
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef($coupon_product_arr['ItemRef']);
							if(isset($coupon_product_arr["ClassRef"]) && $coupon_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($coupon_product_arr["ClassRef"]);
							}
							if($qbo_is_discount_allowed){
								//$salesItemLineDetail->setUnitPrice(0);
							}else{
								$salesItemLineDetail->setUnitPrice($coupon_discount_amount);
							}
							
							if($qbo_is_sales_tax){
								if($coupon_discount_amount_tax > 0 || $is_tax_applied){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}							
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$invoice->addLine($line);
						}
					}					
					
					/*Discount Line*/
					if($_cart_discount && $qbo_is_discount_allowed){
						$qbo_discount_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_discount_account');
						$line = new QuickBooks_IPP_Object_Line();
						$line->setDetailType('DiscountLineDetail');							
						$line->setAmount($_cart_discount);						
						$line->setDescription('Total Discount');
						
						$discountLineDetail = new QuickBooks_IPP_Object_DiscountLineDetail();
						$discountLineDetail->setPercentBased(false);
						$discountLineDetail->setDiscountAccountRef($qbo_discount_account);
						if($qbo_is_sales_tax){
							if($is_tax_applied){
								$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
								if($TaxCodeRef!=''){
									$discountLineDetail->setTaxCodeRef($TaxCodeRef);
								}
							}else{
								$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
								$discountLineDetail->setTaxCodeRef($zero_rated_tax_code);
							}							
						}
						
						$line->addDiscountLineDetail($discountLineDetail);						
						$invoice->addLine($line);
						
					}
					
					//15-05-2017
					if($this->is_plugin_active('woocommerce-order-delivery') && $this->option_checked('mw_wc_qbo_sync_compt_p_wod')){
						$_delivery_date = $this->get_array_isset($invoice_data,'_delivery_date','',true);
						if($_delivery_date!=''){
							$_delivery_date = $this->view_date($_delivery_date);
							$invoice->setShipDate($_delivery_date);
						}
					}					
					//
					$cf_map_data = $this->get_cf_map_data();
					
					//20-04-2017
					$_billing_email = $this->get_array_isset($invoice_data,'_billing_email','',true);
					$BillEmail = new QuickBooks_IPP_Object_BillEmail();
					$BillEmail->setAddress($_billing_email);
					$invoice->setBillEmail($BillEmail);
					
					//BillAddr
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($this->get_array_isset($invoice_data,'_billing_first_name','',true).' '.$this->get_array_isset($invoice_data,'_billing_last_name','',true));
					
					//01-06-2017
					$is_cf_bf_applied = false;			
					if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
						$bp_a = explode(',',$cf_map_data['billing_phone']);
						if(is_array($bp_a) && in_array('bill_addr',array_map('trim', $bp_a))){
							$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
							if($_billing_phone!=''){
								$BillAddr->setLine2($_billing_phone);
								$is_cf_bf_applied = true;
							}
						}
					}
					
					if($is_cf_bf_applied){
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine5($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}else{
						$BillAddr->setLine2($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}
					
					$BillAddr->setCity($this->get_array_isset($invoice_data,'_billing_city','',true));
					
					$country = $this->get_array_isset($invoice_data,'_billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_billing_state','',true));
					$BillAddr->setPostalCode($this->get_array_isset($invoice_data,'_billing_postcode','',true));
					$invoice->setBillAddr($BillAddr);
					
					//ShipAddr
					if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($this->get_array_isset($invoice_data,'_shipping_first_name','',true).' '.$this->get_array_isset($invoice_data,'_shipping_last_name','',true));
						
						$is_cf_bf_applied = false;
						if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
							$bp_a = explode(',',$cf_map_data['billing_phone']);
							if(is_array($bp_a) && in_array('bill_addr',array_map('trim', $bp_a))){
								$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
								if($_billing_phone!=''){
									$ShipAddr->setLine2($_billing_phone);
									$is_cf_bf_applied = true;
								}
							}
						}
						
						if($is_cf_bf_applied){
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine5($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}else{
							$ShipAddr->setLine2($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}						
						
						$ShipAddr->setCity($this->get_array_isset($invoice_data,'_shipping_city','',true));
						
						$country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_shipping_state','',true));
						$ShipAddr->setPostalCode($this->get_array_isset($invoice_data,'_shipping_postcode','',true));
						$invoice->setShipAddr($ShipAddr);
					}
					
					/*Add  Invoice Note Start*/
					
					$customer_note = $this->get_array_isset($invoice_data,'customer_note','',true);
					if($customer_note!=''){
						if($this->option_checked('mw_wc_qbo_sync_invoice_notes')){
							//custom field
							$note_cf_id = (int) $this->get_option('mw_wc_qbo_sync_invoice_note_id');
							$note_cf_name = trim($this->get_option('mw_wc_qbo_sync_invoice_note_id'));
							
							if($note_cf_id && $note_cf_name!=''){
								$Cus_Field = new QuickBooks_IPP_Object_CustomField();
								$Cus_Field->setDefinitionId($note_cf_id);
								$Cus_Field->setName($note_cf_name);
								
								$Cus_Field->setType('StringType');
								$Cus_Field->setStringValue($customer_note);
								$invoice->addCustomField($Cus_Field);
								
							}
							
						}elseif($this->option_checked('mw_wc_qbo_sync_invoice_memo')){
							//29-03-2017
							if(strlen($customer_note) > 4000){
								$customer_note = substr($customer_note,0,4000);
							}
							$invoice->setPrivateNote($customer_note);
						}else{
							if(strlen($customer_note) > 1000){
								$customer_note = substr($customer_note,0,1000);
							}							
							$invoice->setCustomerMemo($customer_note);
						}
					}					
					
					if($this->option_checked('mw_wc_qbo_sync_invoice_memo_statement')){
						if(strlen($first_line_desc) > 4000){
							$first_line_desc = substr($first_line_desc,0,4000);
						}
						$invoice->setPrivateNote($first_line_desc);
					}
					
					/*Add Invoice Note End*/
					
					//Custom Field Mapiing					
					if(isset($cf_map_data['shipping_details'])){
						if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
							$shipping_details = $this->get_shipping_details_from_order_data($invoice_data);
							if($cf_map_data['shipping_details']=='customer_memo'){
								$invoice->setCustomerMemo($shipping_details);
							}
						}						
					}
					
					if(isset($cf_map_data['shipping_method_name']) && $cf_map_data['shipping_method_name']=='ship_method'){
						if($shipping_method_name!=''){
							$invoice->setShipMethodRef($shipping_method_name);
						}
					}
					
					$is_dpt_added = false;
					if($this->is_plugin_active('woocommerce-hear-about-us') && $this->get_qbo_company_setting('TrackDepartments') && $this->option_checked('mw_wc_qbo_sync_compt_wchau_enable')){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$mdp_id = (int) $this->get_compt_map_dep_item_id($source);
							if($mdp_id){
								$invoice->setDepartmentRef($mdp_id);
								$is_dpt_added = true;
							}
						}
					}
					
					//26-05-2017
					if(!$is_dpt_added && isset($cf_map_data['source']) && $cf_map_data['source']!=''){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$cf_s = explode(',',$cf_map_data['source']);
							if(is_array($cf_s) && count($cf_s)==2){
								$cf_s = array_map('trim', $cf_s);
								$cf_s_id = (int) $cf_s[0];
								$cf_s_name = $cf_s[1];
								if($cf_s_id && $cf_s_name!=''){
									$Cus_Field = new QuickBooks_IPP_Object_CustomField();
									$Cus_Field->setDefinitionId($cf_s_id);
									$Cus_Field->setName($cf_s_name);
									
									$Cus_Field->setType('StringType');
									$Cus_Field->setStringValue($source);
									$invoice->addCustomField($Cus_Field);
								}
							}
						}
					}
					
					/*Add Invoice Currency Start*/
					
					$_order_currency = $this->get_array_isset($invoice_data,'_order_currency','',true);
					$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
					if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
						
						$currency_rate_date = $wc_inv_date;
						$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
						//11-04-2017
						$invoice->setCurrencyRef($_order_currency);
						$invoice->setExchangeRate($currency_rate);
					}
					
					/*Add Invoice Currency End*/
					
					$_payment_method = $this->get_array_isset($invoice_data,'_payment_method','',true);					
					$payment_method_map_data  = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					$term_id	= (int) $this->get_array_isset($payment_method_map_data,'term_id','',true);
					if($term_id){
						$invoice->setSalesTermRef($term_id);
					}
					
					/*Add Invoice Tax*/
					if($apply_tax && $is_tax_applied && $Tax_Rate_Ref!=''  && $Tax_Name!=''){
						$TxnTaxDetail = new QuickBooks_IPP_Object_TxnTaxDetail();
						$TxnTaxDetail->setTxnTaxCodeRef($qbo_tax_code);
						$TaxLine = new QuickBooks_IPP_Object_TaxLine();
						$TaxLine->setDetailType('TaxLineDetail');
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLine_2 = new QuickBooks_IPP_Object_TaxLine();
							$TaxLine_2->setDetailType('TaxLineDetail');
							
							$TaxLineDetail_2 = new QuickBooks_IPP_Object_TaxLineDetail();
						}
						
						$TaxLineDetail = new QuickBooks_IPP_Object_TaxLineDetail();

						$TaxLineDetail->setTaxRateRef($Tax_Rate_Ref);
						$TaxLineDetail->setPerCentBased('true');
						
						//$NetAmountTaxable = 0;
						//$TaxLineDetail->setNetAmountTaxable($NetAmountTaxable);
						
						$TaxLineDetail->setTaxPercent($TaxPercent);
						
						$TaxLine->addTaxLineDetail($TaxLineDetail);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLineDetail_2->setTaxRateRef($Tax_Rate_Ref_2);
							$TaxLineDetail_2->setPerCentBased('true');
							$TaxLineDetail_2->setTaxPercent($TaxPercent_2);
							
							//$NetAmountTaxable_2 = 0;
							//$TaxLineDetail_2->setNetAmountTaxable($NetAmountTaxable_2);
							
							$TaxLine_2->addTaxLineDetail($TaxLineDetail_2);
						}
						
						$TxnTaxDetail->addTaxLine($TaxLine);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TxnTaxDetail->addTaxLine($TaxLine_2);
						}
						
						$SalesTax = new QuickBooks_IPP_Object_SalesTax();
						$SalesTax->setTaxable('true');
						$SalesTax->setSalesTaxCodeId($Tax_Rate_Ref);

						$SalesTax->setSalesTaxCodeName($Tax_Name);

						$invoice->addSalesTax($SalesTax);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$SalesTax_2 = new QuickBooks_IPP_Object_SalesTax();
							$SalesTax_2->setTaxable('true');
							$SalesTax_2->setSalesTaxCodeId($Tax_Rate_Ref_2);

							$SalesTax_2->setSalesTaxCodeName($Tax_Name_2);

							$invoice->addSalesTax($SalesTax_2);							
						}
						
						$invoice->addTxnTaxDetail($TxnTaxDetail);						
						
					}
					
					//_transaction_id
					
					
					//
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($invoice_data);
					//$this->_p($invoice);
					//return false;
					
					if ($resp = $invoiceService->add($Context, $realm, $invoice)){
						$qbo_inv_id = $this->qbo_clear_braces($resp);
						$log_title.="Export Order #$ord_id_num\n";
						$log_details.="Order #$ord_id_num has been exported, QuickBooks Invoice ID is #$qbo_inv_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Invoice Add',$invoice_data,$invoice,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_inv_id;
						
					}else{
						$res_err = $invoiceService->lastError($Context);
						$log_title.="Export Order Error #$ord_id_num\n";
						$log_details.="Error:$res_err";					
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Invoice Add',$invoice_data,$invoice,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());					
						return false;
					}
				}
			}		
		}
	}
	
	public function add_txt_to_log_file($txt){
		if($txt!=''){
			$lof_file_path = MW_QBO_SYNC_LOG_DIR."mw-qbo-sync-log.log";
			if(file_exists($lof_file_path)){
				$f_ot = 'a';
				$log_file = fopen($lof_file_path, $f_ot);
				fwrite($log_file, "\n". $txt);
				fclose($log_file);
			}
		}
	}
	
	//20-03-2017
	public function add_qbo_item_obj_into_log_file($type,$wc_data,$item,$request='',$response='',$suc_log=false,$append=true){
		$is_log_allowed = false;
		if($this->option_checked('mw_wc_qbo_sync_err_add_item_obj_into_log_file') && !$suc_log){
			$is_log_allowed = true;
		}
		
		if($this->option_checked('mw_wc_qbo_sync_success_add_item_obj_into_log_file') && $suc_log){
			$is_log_allowed = true;
		}
		
		if($is_log_allowed && $type!='' && !empty($item) && !empty($wc_data)){
			$f_log_txt = '';
			$f_log_txt.=$type.' ('.date('Y-m-d H:i:s').')'.PHP_EOL;
			
			$f_log_txt.="Woocommerce Data:".PHP_EOL;
			$f_log_txt.=print_r($wc_data,true).PHP_EOL;
			
			$f_log_txt.="QuickBooks Object:".PHP_EOL;
			$f_log_txt.=print_r($item,true).PHP_EOL;
			
			$f_log_txt.="Request:".PHP_EOL;
			$f_log_txt.=$request.PHP_EOL;
			
			$f_log_txt.="Response:".PHP_EOL;
			$f_log_txt.=$response.PHP_EOL;
			
			$f_ot = ($append)?'a':'w';
			
			$log_filename = ($suc_log)?'mw-qbo-sync-req-res-log.log':'mw-qbo-sync-log.log';
			//07-04-2017
			if((time()-filemtime(MW_QBO_SYNC_LOG_DIR.$log_filename)) > 86400){
				$f_ot = 'w';
			}		
			
			$log_file = fopen(MW_QBO_SYNC_LOG_DIR.$log_filename, $f_ot);
			fwrite($log_file, "\n". $f_log_txt);
			fclose($log_file);
		}	
	}
	
	//06-06-2017
	public function get_compt_map_dep_item_id($source){
		$mdp_id = 0;
		if($source!=''){
			$dpt_ma = $this->get_option('mw_wc_qbo_sync_compt_wchau_wf_qi_map');
			$wchau_options = get_option('wchau_options');
			if($dpt_ma && $wchau_options!=''){
				$dpt_ma = unserialize($dpt_ma);
				$wchau_options = explode(PHP_EOL,$wchau_options);								
				if(is_array($dpt_ma) && count($dpt_ma) && is_array($wchau_options) && count($wchau_options)){
					$wchau_options = array_map('trim',$wchau_options);
					$dpt_ma = array_map('trim',$dpt_ma);
					
					if(in_array($source,$wchau_options)){
						foreach($dpt_ma as $k => $dp){
							$k = base64_decode($k);
							if($source==$k){
								$mdp_id = (int) $dp;
								break;
							}
						}
					}else{
						foreach($dpt_ma as $k => $dp){
							$k = base64_decode($k);
							if('Other'==$k || 'Others'==$k){
								$mdp_id = (int) $dp;
								break;
							}
						}
					}
				}
			}
		}		
		return $mdp_id;
	}
	
	//22-02-2017
	
	public function AddSalesReceipt($invoice_data){
		if($this->is_connected()){
			if(!$this->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
				return false;
			}
			
			$wc_inv_id = $this->get_array_isset($invoice_data,'wc_inv_id',0);
			$wc_inv_num = $this->get_array_isset($invoice_data,'wc_inv_num','');
			$wc_cus_id = $this->get_array_isset($invoice_data,'wc_cus_id','');
			
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			//Zero Total Option Check
			$_order_total = $this->get_array_isset($invoice_data,'_order_total',0);
			if($this->option_checked('mw_wc_qbo_sync_null_invoice')){
				if($_order_total==0 || $_order_total<0){
					$this->save_log('Export Order Error #'.$ord_id_num,'Order amount 0 not allowed in setting ','Invoice',0);
					return false;
				}				
			}
			//SalesReceipt
			if($this->if_sync_invoice($wc_inv_id,$wc_cus_id,$wc_inv_num)){			
				if(!$this->check_quickbooks_salesreceipt_get_obj($wc_inv_id,$wc_inv_num)){			
					$wc_inv_date = $this->get_array_isset($invoice_data,'wc_inv_date','');
					$wc_inv_date = $this->view_date($wc_inv_date);
					
					$qbo_customerid = $this->get_array_isset($invoice_data,'qbo_customerid',0);
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$SalesReceiptService = new QuickBooks_IPP_Service_SalesReceipt();
					$SalesReceipt = new QuickBooks_IPP_Object_SalesReceipt();
					
					$DocNumber = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
					$SalesReceipt->setDocNumber($DocNumber);
					
					$SalesReceipt->setTxnDate($wc_inv_date);
					
					
					$SalesReceipt->setCustomerRef($qbo_customerid);
					
					/*Count Total Amounts*/
					$_cart_discount = $this->get_array_isset($invoice_data,'_cart_discount',0);
					$_cart_discount_tax = $this->get_array_isset($invoice_data,'_cart_discount_tax',0);
					
					$_order_tax = $this->get_array_isset($invoice_data,'_order_tax',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					//Shipping Total
					$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
					$_order_shipping_tax = $this->get_array_isset($invoice_data,'_order_shipping_tax',0);
					
					
					$qbo_inv_items = (isset($invoice_data['qbo_inv_items']))?$invoice_data['qbo_inv_items']:array();					
					
					$total_line_subtotal = 0;
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){
						foreach($qbo_inv_items as $qbo_item){
							$total_line_subtotal+=$qbo_item['line_subtotal'];
						}
					}
					
					//Qbo settings
					$qbo_is_sales_tax = $this->get_qbo_company_setting('is_sales_tax');
					$qbo_company_country = $this->get_qbo_company_info('country');
					
					//Tax rates
					$qbo_tax_code = '';
					$apply_tax = false;
					$is_tax_applied = false;
					$is_inclusive = false;
					
					$tax_rate_id = 0;
					$tax_rate_id_2 = 0;
					
					$tax_details = (isset($invoice_data['tax_details']))?$invoice_data['tax_details']:array();
					if(count($tax_details)){
						$tax_rate_id = $tax_details[0]['rate_id'];
					}
					if(count($tax_details)>1){
						$tax_rate_id_2 = $tax_details[1]['rate_id'];
					}
					
					$qbo_tax_code = $this->get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2);
					if($qbo_tax_code!='' || $qbo_tax_code!='NON'){
						$apply_tax = true;
					}
					
					$Tax_Code_Details = $this->mod_qbo_get_tx_dtls($qbo_tax_code);					
					$is_qbo_dual_tax = false;
					
					if(count($Tax_Code_Details)){
						if($Tax_Code_Details['TaxGroup'] && count($Tax_Code_Details['TaxRateDetail'])>1){
							$is_qbo_dual_tax = true;
						}
					}
					
					
					$Tax_Rate_Ref = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']:'';
					$TaxPercent = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref);
					$Tax_Name = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef_name']:'';
					
					if($is_qbo_dual_tax){
						$Tax_Rate_Ref_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']:'';
						$TaxPercent_2 = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref_2);						
						$Tax_Name_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef_name']:'';
					}
					
					$_prices_include_tax = $this->get_array_isset($invoice_data,'_prices_include_tax','no',true);
					if($qbo_is_sales_tax){						
						$tax_type = $this->get_tax_type($_prices_include_tax);
						$is_inclusive = $this->is_tax_inclusive($tax_type);
						$SalesReceipt->setGlobalTaxCalculation($tax_type);
						$SalesReceipt->setApplyTaxAfterDiscount(true);
					}
					
					//Bundle Support
					$is_bundle_order = false;
					if($this->is_plugin_active('woocommerce-product-bundles') && $this->option_checked('mw_wc_qbo_sync_compt_wpbs')){
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if(isset($qbo_item['bundled_items']) && $qbo_item['qbo_product_type'] == 'Group'){
									$is_bundle_order = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									//$this->_p($qbo_gp_details);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_compt_wpbs_ap_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$SalesReceipt->addLine($line);
								}
							}
							
						}
					}
					
					//Map Bundle Support
					$map_bundle_support = false;
					if(!$is_bundle_order){					
						if(is_array($qbo_inv_items) && count($qbo_inv_items)){
							foreach($qbo_inv_items as $qbo_item){
								if($qbo_item['qbo_product_type'] == 'Group'){
									$map_bundle_support = true;
									$line = new QuickBooks_IPP_Object_Line();
									$line->setDetailType('GroupLineDetail');
									
									$line->setAmount(0);
									$GroupLineDetail = new QuickBooks_IPP_Object_GroupLineDetail();
									
									$GroupLineDetail->setGroupItemRef($qbo_item['ItemRef']);
									$GroupLineDetail->setQuantity($qbo_item['Qty']);
									
									$qbo_gp_details = $this->get_qbo_group_product_details($qbo_item['ItemRef']);
									if(is_array($qbo_gp_details) && count($qbo_gp_details) && isset($qbo_gp_details['buldle_items'])){
										if(is_array($qbo_gp_details['buldle_items']) && count($qbo_gp_details['buldle_items'])){
											foreach($qbo_gp_details['buldle_items'] as $qbo_gp_item){
												$gp_line = new QuickBooks_IPP_Object_Line();
												
												$gp_line->setDetailType('SalesItemLineDetail');
												$UnitPrice = $qbo_gp_item["UnitPrice"];
												$Amount = $qbo_gp_item['Qty']*$UnitPrice;							
												$gp_line->setAmount($Amount);
												
												$gp_line->setDescription($qbo_gp_item['ItemRef_name']);					
												$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
												
												$tax_class =  $qbo_item["tax_class"];
												
												if($qbo_is_sales_tax){
													if($apply_tax && $qbo_item["Taxed"]){
														$is_tax_applied = true;														
														$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
														
														if($is_inclusive){
															
														}
														
														if($TaxCodeRef!=''){
															$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
														}
													}else{
														$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
														$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
													}
												}
												
												$salesItemLineDetail->setItemRef($qbo_gp_item["ItemRef"]);
												
												$Qty = $qbo_gp_item["Qty"];
												
												$salesItemLineDetail->setQty($Qty);							
												$salesItemLineDetail->setUnitPrice($UnitPrice);
												
												$gp_line->addSalesItemLineDetail($salesItemLineDetail);
												$GroupLineDetail->addLine($gp_line);
											}
										}
										
										$wc_b_price = $qbo_item['UnitPrice'];
										$qbo_b_tp = $qbo_gp_details['b_tp'];
										$gp_p_diff = ($wc_b_price-$qbo_b_tp);
										
										if($gp_p_diff!=0){
											$b_q_ap = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
											$gp_line = new QuickBooks_IPP_Object_Line();
											$gp_line->setDetailType('SalesItemLineDetail');
											
											$UnitPrice = $gp_p_diff;
											$Qty = 1;
											$Amount = $Qty*$UnitPrice;				
											$gp_line->setAmount($Amount);
											
											$gp_line->setDescription('Bundle Product Price Adjustment');			
											$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
											
											$tax_class =  $qbo_item["tax_class"];
											
											if($qbo_is_sales_tax){
												if($apply_tax && $qbo_item["Taxed"]){
													$is_tax_applied = true;														
													$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
													
													if($is_inclusive){
														
													}
													
													if($TaxCodeRef!=''){
														$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
													}
												}else{
													$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
													$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
												}
											}
											
											$salesItemLineDetail->setItemRef($b_q_ap);
											
											$salesItemLineDetail->setQty($Qty);							
											$salesItemLineDetail->setUnitPrice($UnitPrice);
											
											$gp_line->addSalesItemLineDetail($salesItemLineDetail);
											$GroupLineDetail->addLine($gp_line);
										}
									}
									
									$line->addGroupLineDetail($GroupLineDetail);
									$SalesReceipt->addLine($line);						
								}
							}
						}
					}
					
					//Add SalesReceipt items
					$first_line_desc = '';
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){						
						$first_line_desc = $qbo_inv_items[0]['Description'];
						foreach($qbo_inv_items as $qbo_item){
							//Bundle Support
							if($is_bundle_order){
								if(isset($qbo_item['bundled_items']) || isset($qbo_item['bundled_item_id']) || isset($qbo_item['bundle_cart_key'])){
									continue;
								}
							}
							
							if($map_bundle_support && $qbo_item['qbo_product_type'] == 'Group'){
								continue;
							}
							
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							$UnitPrice = $qbo_item["UnitPrice"];
							if($_cart_discount){
								//$UnitPrice = $this->get_discounted_item_price($_cart_discount,$total_line_subtotal,$UnitPrice);
							}
							
							$Amount = $qbo_item['Qty']*$UnitPrice;					
							
							$line->setAmount($Amount);
							$line->setDescription($qbo_item['Description']);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							$tax_class =  $qbo_item["tax_class"];
							
							if($qbo_is_sales_tax){
								if($apply_tax && $qbo_item["Taxed"]){
									$is_tax_applied = true;														
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									
									if($is_inclusive){
										
									}
									
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$salesItemLineDetail->setItemRef($qbo_item["ItemRef"]);
							
							$Qty = $qbo_item["Qty"];
							//10-05-2017
							if($this->is_plugin_active('woocommerce-measurement-price-calculator') && isset($qbo_item['measurement_data']) && $this->option_checked('mw_wc_qbo_sync_measurement_qty')){
								$measurement_data = unserialize($qbo_item['measurement_data']);
								if(is_array($measurement_data) && isset($measurement_data['_measurement_needed'])){
									$_measurement_needed = floatval($measurement_data['_measurement_needed']);
									if($_measurement_needed>0){
										$UnitPrice = $UnitPrice/$_measurement_needed;
										//$UnitPrice = number_format($UnitPrice, 2);
										$_quantity = (isset($measurement_data['_quantity']))?$measurement_data['_quantity']:1;
										$Qty = $_measurement_needed*$_quantity;
									}
								}
							}
							
							$salesItemLineDetail->setQty($Qty);
							$salesItemLineDetail->setUnitPrice($UnitPrice);
							
							if(isset($qbo_item["ClassRef"]) && $qbo_item["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($qbo_item["ClassRef"]);
							}
							
							
							if($this->option_checked('mw_wc_qbo_sync_invoice_date')){
								$salesItemLineDetail->setServiceDate($wc_inv_date);
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);
							$SalesReceipt->addLine($line);
						}
					}
					
					//pgdf compatibility					
					if($this->get_wc_fee_plugin_check()){
						$dc_gt_fees = (isset($invoice_data['dc_gt_fees']))?$invoice_data['dc_gt_fees']:array();
						if(is_array($dc_gt_fees) && count($dc_gt_fees)){
							foreach($dc_gt_fees as $df){
								$line = new QuickBooks_IPP_Object_Line();
								$line->setDetailType('SalesItemLineDetail');
								
								$UnitPrice = $df['_line_total'];
								$Qty = 1;
								$Amount = $Qty*$UnitPrice;		
								
								$line->setAmount($Amount);
								$line->setDescription($df['name']);
								
								$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
								$_line_tax = $df['_line_tax'];
								//$df_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($df['_line_tax_data']);
								if($_line_tax && $qbo_is_sales_tax){
									//$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$df_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}
								if(!$_line_tax && $qbo_is_sales_tax){
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
								
								$df_ItemRef = $this->get_wc_fee_qbo_product($df['name']);
								
								$salesItemLineDetail->setItemRef($df_ItemRef);
								$salesItemLineDetail->setQty($Qty);							
								$salesItemLineDetail->setUnitPrice($UnitPrice);
								
								$line->addSalesItemLineDetail($salesItemLineDetail);
								$SalesReceipt->addLine($line);
							}
						}
					}
					
					/*Add SalesReceipt Shipping*/
					$shipping_details  = (isset($invoice_data['shipping_details']))?$invoice_data['shipping_details']:array();
					
					$shipping_method = '';
					$shipping_method_name = '';
					
					$shipping_taxes = '';
					
					if(isset($shipping_details[0])){
						if($this->get_array_isset($shipping_details[0],'type','')=='shipping'){
							$shipping_method_id = $this->get_array_isset($shipping_details[0],'method_id','');
							if($shipping_method_id!=''){
								//$shipping_method = substr($shipping_method_id, 0, strpos($shipping_method_id, ":"));
								$shipping_method = $this->wc_get_sm_data_from_method_id_str($shipping_method_id);
							}
							$shipping_method = ($shipping_method=='')?'no_method_found':$shipping_method;
							
							$shipping_method_name =  $this->get_array_isset($shipping_details[0],'name','',true,30);
							
							//Serialized
							$shipping_taxes = $this->get_array_isset($shipping_details[0],'taxes','');
							//$shipping_taxes = unserialize($shipping_taxes);
							//$this->_p($shipping_taxes);							
						}						
					}
					
					$qbo_is_shipping_allowed = $this->get_qbo_company_setting('is_shipping_allowed');
					
					//$order_shipping_total+=$_order_shipping_tax;
					
					if($shipping_method!=''){						
						if($qbo_is_shipping_allowed){
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							$line->setAmount($order_shipping_total);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef('SHIPPING_ITEM_ID');
							
							if($qbo_is_sales_tax){
								if($_order_shipping_tax > 0){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}else{
							$shipping_product_arr = $this->get_mapped_shipping_product($shipping_method);						
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							//
							$order_shipping_total = $this->get_array_isset($invoice_data,'order_shipping_total',0);
							
							$line->setAmount($order_shipping_total);
							
							$shipping_description = ($shipping_method_name!='')?'Shipping ('.$shipping_method_name.')':'Shipping';
							
							$line->setDescription($shipping_description);
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							
							$salesItemLineDetail->setItemRef($shipping_product_arr["ItemRef"]);
							
							if(isset($shipping_product_arr["ClassRef"]) && $shipping_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($shipping_product_arr["ClassRef"]);
							}
							
							$salesItemLineDetail->setUnitPrice($order_shipping_total);
							
							
							if($qbo_is_sales_tax){								
								if($_order_shipping_tax > 0){
									//$shipping_tax_code = $this->get_qbo_tax_map_code_from_serl_line_tax_data($shipping_taxes,'shipping');
									//$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$shipping_tax_code;
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}
						
					}
					
					/*Add SalesReceipt Coupons*/
					$used_coupons  = (isset($invoice_data['used_coupons']))?$invoice_data['used_coupons']:array();
					
					$qbo_is_discount_allowed = $this->get_qbo_company_setting('is_discount_allowed');
					
					if(count($used_coupons)){
						foreach($used_coupons as $coupon){
							$coupon_name = $coupon['name'];
							$coupon_discount_amount = $coupon['discount_amount'];							
							$coupon_discount_amount = -1 * abs($coupon_discount_amount);
							
							$coupon_discount_amount_tax = $coupon['discount_amount_tax'];
							
							$coupon_product_arr = $this->get_mapped_coupon_product($coupon_name);
							$line = new QuickBooks_IPP_Object_Line();
							
							$line->setDetailType('SalesItemLineDetail');
							if($qbo_is_discount_allowed){
								$line->setAmount(0);
							}else{
								$line->setAmount($coupon_discount_amount);
							}
							
							
							$line->setDescription($coupon_product_arr['Description']);
						
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							$salesItemLineDetail->setItemRef($coupon_product_arr['ItemRef']);
							if(isset($coupon_product_arr["ClassRef"]) && $coupon_product_arr["ClassRef"]!=''){
								$salesItemLineDetail->setClassRef($coupon_product_arr["ClassRef"]);
							}
							if($qbo_is_discount_allowed){
								//$salesItemLineDetail->setUnitPrice(0);
							}else{
								$salesItemLineDetail->setUnitPrice($coupon_discount_amount);
							}
							
							if($qbo_is_sales_tax){
								if($coupon_discount_amount_tax > 0 || $is_tax_applied){
									$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}							
							}
							
							$line->addSalesItemLineDetail($salesItemLineDetail);						
							$SalesReceipt->addLine($line);
						}
					}					
					
					/*Discount Line*/
					if($_cart_discount && $qbo_is_discount_allowed){
						$qbo_discount_account = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_discount_account');
						$line = new QuickBooks_IPP_Object_Line();
						$line->setDetailType('DiscountLineDetail');							
						$line->setAmount($_cart_discount);						
						$line->setDescription('Total Discount');
						
						$discountLineDetail = new QuickBooks_IPP_Object_DiscountLineDetail();
						$discountLineDetail->setPercentBased(false);
						$discountLineDetail->setDiscountAccountRef($qbo_discount_account);
						
						if($qbo_is_sales_tax){
							if($is_tax_applied){
								$TaxCodeRef = ($qbo_company_country=='US')?'{-TAX}':$qbo_tax_code;
								if($TaxCodeRef!=''){
									$discountLineDetail->setTaxCodeRef($TaxCodeRef);
								}
							}else{
								$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
								$discountLineDetail->setTaxCodeRef($zero_rated_tax_code);
							}							
						}
						
						$line->addDiscountLineDetail($discountLineDetail);						
						$SalesReceipt->addLine($line);
						
					}
					
					//15-05-2017
					if($this->is_plugin_active('woocommerce-order-delivery') && $this->option_checked('mw_wc_qbo_sync_compt_p_wod')){
						$_delivery_date = $this->get_array_isset($invoice_data,'_delivery_date','',true);
						if($_delivery_date!=''){
							$_delivery_date = $this->view_date($_delivery_date);
							$SalesReceipt->setShipDate($_delivery_date);
						}
					}
					//
					$cf_map_data = $this->get_cf_map_data();
					
					//20-04-2017
					$_billing_email = $this->get_array_isset($invoice_data,'_billing_email','',true);
					$BillEmail = new QuickBooks_IPP_Object_BillEmail();
					$BillEmail->setAddress($_billing_email);
					$SalesReceipt->setBillEmail($BillEmail);
					
					//BillAddr
					$BillAddr = new QuickBooks_IPP_Object_BillAddr();
					$BillAddr->setLine1($this->get_array_isset($invoice_data,'_billing_first_name','',true).' '.$this->get_array_isset($invoice_data,'_billing_last_name','',true));
					
					$is_cf_bf_applied = false;
					if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
						$bp_a = explode(',',$cf_map_data['billing_phone']);
						if(is_array($bp_a) && in_array('bill_addr',array_map('trim', $bp_a))){
							$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
							if($_billing_phone!=''){
								$BillAddr->setLine2($_billing_phone);
								$is_cf_bf_applied = true;
							}
						}
					}
					
					if($is_cf_bf_applied){
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine5($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}else{
						$BillAddr->setLine2($this->get_array_isset($invoice_data,'_billing_company','',true));					
						$BillAddr->setLine3($this->get_array_isset($invoice_data,'_billing_address_1','',true));
						$BillAddr->setLine4($this->get_array_isset($invoice_data,'_billing_address_2','',true));
					}
					
					$BillAddr->setCity($this->get_array_isset($invoice_data,'_billing_city','',true));
					
					$country = $this->get_array_isset($invoice_data,'_billing_country','',true);
					$country = $this->get_country_name_from_code($country);
					$BillAddr->setCountry($country);
					
					$BillAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_billing_state','',true));
					$BillAddr->setPostalCode($this->get_array_isset($invoice_data,'_billing_postcode','',true));
					$SalesReceipt->setBillAddr($BillAddr);
					
					//ShipAddr
					if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
						$ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
						$ShipAddr->setLine1($this->get_array_isset($invoice_data,'_shipping_first_name','',true).' '.$this->get_array_isset($invoice_data,'_shipping_last_name','',true));
						
						$is_cf_bf_applied = false;
						if(isset($cf_map_data['billing_phone']) && $cf_map_data['billing_phone']!=''){
							$bp_a = explode(',',$cf_map_data['billing_phone']);
							if(is_array($bp_a) && in_array('ship_addr',array_map('trim', $bp_a))){
								$_billing_phone = $this->get_array_isset($invoice_data,'_billing_phone','',true);
								if($_billing_phone!=''){
									$ShipAddr->setLine2($_billing_phone);
									$is_cf_bf_applied = true;
								}
							}
						}
						
						if($is_cf_bf_applied){
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine5($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}else{
							$ShipAddr->setLine2($this->get_array_isset($invoice_data,'_shipping_company','',true));						
							$ShipAddr->setLine3($this->get_array_isset($invoice_data,'_shipping_address_1','',true));
							$ShipAddr->setLine4($this->get_array_isset($invoice_data,'_shipping_address_2','',true));
						}
						
						$ShipAddr->setCity($this->get_array_isset($invoice_data,'_shipping_city','',true));
						
						$country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
						$country = $this->get_country_name_from_code($country);
						$ShipAddr->setCountry($country);
						
						$ShipAddr->setCountrySubDivisionCode($this->get_array_isset($invoice_data,'_shipping_state','',true));
						$ShipAddr->setPostalCode($this->get_array_isset($invoice_data,'_shipping_postcode','',true));
						$SalesReceipt->setShipAddr($ShipAddr);
					}
					
					/*Add  SalesReceipt Note Start*/
					
					$customer_note = $this->get_array_isset($invoice_data,'customer_note','',true);
					if($customer_note!=''){
						if($this->option_checked('mw_wc_qbo_sync_invoice_notes')){
							//custom field
							$note_cf_id = (int) $this->get_option('mw_wc_qbo_sync_invoice_note_id');
							$note_cf_name = trim($this->get_option('mw_wc_qbo_sync_invoice_note_id'));
							
							if($note_cf_id && $note_cf_name!=''){
								$Cus_Field = new QuickBooks_IPP_Object_CustomField();
								$Cus_Field->setDefinitionId($note_cf_id);
								$Cus_Field->setName($note_cf_name);
								
								$Cus_Field->setType('StringType');
								$Cus_Field->setStringValue($customer_note);
								$SalesReceipt->addCustomField($Cus_Field);
								
							}
							
						}elseif($this->option_checked('mw_wc_qbo_sync_invoice_memo')){
							if(strlen($customer_note) > 4000){
								$customer_note = substr($customer_note,0,4000);
							}
							$SalesReceipt->setPrivateNote($customer_note);
						}else{
							if(strlen($customer_note) > 1000){
								$customer_note = substr($customer_note,0,1000);
							}
							$SalesReceipt->setCustomerMemo($customer_note);
						}
					}					
					
					if($this->option_checked('mw_wc_qbo_sync_invoice_memo_statement')){
						$SalesReceipt->setPrivateNote($first_line_desc);
					}
					
					/*Add SalesReceipt Note End*/
					
					//Custom Field Mapiing					
					if(isset($cf_map_data['shipping_details'])){
						if($this->get_array_isset($invoice_data,'_shipping_first_name','',true)!=''){
							$shipping_details = $this->get_shipping_details_from_order_data($invoice_data);
							if($cf_map_data['shipping_details']=='customer_memo'){
								$SalesReceipt->setCustomerMemo($shipping_details);
							}
						}						
					}
					
					if(isset($cf_map_data['shipping_method_name']) && $cf_map_data['shipping_method_name']=='ship_method'){
						if($shipping_method_name!=''){
							$SalesReceipt->setShipMethodRef($shipping_method_name);
						}
					}
					
					//02-06-2017
					$is_dpt_added = false;
					if($this->is_plugin_active('woocommerce-hear-about-us') && $this->get_qbo_company_setting('TrackDepartments') && $this->option_checked('mw_wc_qbo_sync_compt_wchau_enable')){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$mdp_id = (int) $this->get_compt_map_dep_item_id($source);
							if($mdp_id){
								$SalesReceipt->setDepartmentRef($mdp_id);
								$is_dpt_added = true;
							}
						}
					}
					
					if(!$is_dpt_added && isset($cf_map_data['source']) && $cf_map_data['source']!=''){
						$source = $this->get_array_isset($invoice_data,'source','',true);
						if($source!=''){
							$cf_s = explode(',',$cf_map_data['source']);
							if(is_array($cf_s) && count($cf_s)==2){
								$cf_s = array_map('trim', $cf_s);
								$cf_s_id = (int) $cf_s[0];
								$cf_s_name = $cf_s[1];
								if($cf_s_id && $cf_s_name!=''){
									$Cus_Field = new QuickBooks_IPP_Object_CustomField();
									$Cus_Field->setDefinitionId($cf_s_id);
									$Cus_Field->setName($cf_s_name);
									
									$Cus_Field->setType('StringType');
									$Cus_Field->setStringValue($source);
									$SalesReceipt->addCustomField($Cus_Field);
								}
							}
						}
					}
					
					/*Add SalesReceipt Currency Start*/
					
					$_order_currency = $this->get_array_isset($invoice_data,'_order_currency','',true);
					$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
					if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
						
						$currency_rate_date = $wc_inv_date;
						$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
						
						$SalesReceipt->setCurrencyRef($_order_currency);
						$SalesReceipt->setExchangeRate($currency_rate);
					}
					
					/*Add SalesReceipt Currency End*/
					
					
					/*Add SalesReceipt Tax*/
					if($apply_tax && $is_tax_applied && $Tax_Rate_Ref!=''  && $Tax_Name!=''){
						$TxnTaxDetail = new QuickBooks_IPP_Object_TxnTaxDetail();
						$TxnTaxDetail->setTxnTaxCodeRef($qbo_tax_code);
						$TaxLine = new QuickBooks_IPP_Object_TaxLine();
						$TaxLine->setDetailType('TaxLineDetail');
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLine_2 = new QuickBooks_IPP_Object_TaxLine();
							$TaxLine_2->setDetailType('TaxLineDetail');
							
							$TaxLineDetail_2 = new QuickBooks_IPP_Object_TaxLineDetail();
						}
						
						$TaxLineDetail = new QuickBooks_IPP_Object_TaxLineDetail();

						$TaxLineDetail->setTaxRateRef($Tax_Rate_Ref);
						$TaxLineDetail->setPerCentBased('true');
						
						//$NetAmountTaxable = 0;
						//$TaxLineDetail->setNetAmountTaxable($NetAmountTaxable);
						
						$TaxLineDetail->setTaxPercent($TaxPercent);
						
						$TaxLine->addTaxLineDetail($TaxLineDetail);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLineDetail_2->setTaxRateRef($Tax_Rate_Ref_2);
							$TaxLineDetail_2->setPerCentBased('true');
							$TaxLineDetail_2->setTaxPercent($TaxPercent_2);
							
							//$NetAmountTaxable_2 = 0;
							//$TaxLineDetail_2->setNetAmountTaxable($NetAmountTaxable_2);
							
							$TaxLine_2->addTaxLineDetail($TaxLineDetail_2);
						}
						
						$TxnTaxDetail->addTaxLine($TaxLine);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TxnTaxDetail->addTaxLine($TaxLine_2);
						}
						
						$SalesTax = new QuickBooks_IPP_Object_SalesTax();
						$SalesTax->setTaxable('true');
						$SalesTax->setSalesTaxCodeId($Tax_Rate_Ref);

						$SalesTax->setSalesTaxCodeName($Tax_Name);

						$SalesReceipt->addSalesTax($SalesTax);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$SalesTax_2 = new QuickBooks_IPP_Object_SalesTax();
							$SalesTax_2->setTaxable('true');
							$SalesTax_2->setSalesTaxCodeId($Tax_Rate_Ref_2);

							$SalesTax_2->setSalesTaxCodeName($Tax_Name_2);

							$SalesReceipt->addSalesTax($SalesTax_2);							
						}
						
						$SalesReceipt->addTxnTaxDetail($TxnTaxDetail);						
						
					}
					
					//					
					$_transaction_id = $this->get_array_isset($invoice_data,'_transaction_id','',true);
					
					
					$_payment_method = $this->get_array_isset($invoice_data,'_payment_method','',true);
					$payment_method_map_data  = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					
					if(count($payment_method_map_data)){
						if($payment_method_map_data['enable_payment']){
							$payment_data = $this->wc_get_payment_details_by_txn_id($_transaction_id,$wc_inv_id);
							
							if($payment_method_map_data['qb_p_method_id']){
								$SalesReceipt->setPaymentMethodRef($payment_method_map_data['qb_p_method_id']);
							}
							
							//CreditCardPayment
							$SalesReceipt->setDepositToAccountRef($payment_method_map_data['qbo_account_id']);
							
							if(count($payment_data)){								
								$SalesReceipt->setPaymentRefNum($payment_data['payment_id']);
							}
						}	
					}
					
					//
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					//$this->_p($invoice_data);
					//$this->_p($SalesReceipt);
					//return false;
					
					if ($resp = $SalesReceiptService->add($Context, $realm, $SalesReceipt)){
						$qbo_sr_id = $this->qbo_clear_braces($resp);
						$log_title.="Export Order #$ord_id_num\n";
						$log_details.="Order #$ord_id_num has been exported, QuickBooks SalesReceipt ID is #$qbo_sr_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Sales Receipt Add',$invoice_data,$SalesReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_sr_id;
						
					}else{
						$res_err = $SalesReceiptService->lastError($Context);
						$log_title.="Export Order Error #$ord_id_num\n";
						$log_details.="Error:$res_err";						
						$this->save_log($log_title,$log_details,'Invoice',$log_status,true,true);
						
						$this->add_qbo_item_obj_into_log_file('Sales Receipt Add',$invoice_data,$SalesReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
						return false;
					}
				}
			}			
		}
	}
	
	public function get_item_tax_rate($line_tax,$line_total,$item_id=0,$tax_class=''){	
		$item_rate = round(ceil(($line_tax / $line_total)*100),2);
		//$item_rate = number_format(round((float)$item_rate,4),4);
		$item_rate = number_format((float)$item_rate, 4, '.', '');
		return $item_rate;
	}
	
	//16-05-2017
	public function get_qbo_tax_map_code_from_serl_line_tax_data($ltd,$l_type=''){
		$tr1_id = 0;
		$tr2_id = 0;
		if($ltd!=''){
			$ltd = unserialize($ltd);
			if(is_array($ltd) && count($ltd)){
				$ltd_arr = array();
				if($l_type=='shipping'){
					//22-05-2017
					if(isset($ltd['total']) && is_array($ltd['total']) && count($ltd['total'])){
						$ltd_arr = $ltd['total'];
					}else{
						$ltd_arr = $ltd;
					}					
				}else{
					if(isset($ltd['total']) && is_array($ltd['total']) && count($ltd['total'])){
						$ltd_arr = $ltd['total'];
					}
					if(isset($ltd['subtotal']) && is_array($ltd['subtotal']) && count($ltd['subtotal'])){
						//$ltd_arr = $ltd['subtotal'];
					}
				}
				if(is_array($ltd_arr) && count($ltd_arr)){
					$i=1;
					foreach($ltd_arr as $k=>$v){
						if($i==1){
							$tr1_id = (int) $k;
						}
						if($i==2){
							$tr2_id = (int) $k;
						}
						$i++;
					}
				}				
			}
		}
		if($tr1_id>0 || $tr2_id>0){
			return $this->get_qbo_mapped_tax_code($tr1_id,$tr2_id);
		}
		return '';
	}
	
	public function get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2,$tax_details=array(),$invoice_data=array()){
		
		$qbo_tax_code = '';
		//$woocommerce_tax_based_on = $this->get_option('woocommerce_tax_based_on');
		
		/*
		if($woocommerce_tax_based_on=='billing'){
			$state = $this->get_array_isset($invoice_data,'_billing_state','',true);
			$country = $this->get_array_isset($invoice_data,'_billing_country','',true);
		}elseif($woocommerce_tax_based_on=='shipping'){
			$state = $this->get_array_isset($invoice_data,'_shipping_state','',true);
			$country = $this->get_array_isset($invoice_data,'_shipping_country','',true);
		}else{
			$base_location = wc_get_base_location();
			$state = $this->get_array_isset($base_location,'state','',true);
			$country = $this->get_array_isset($base_location,'country','',true);
		}
		*/
		
		global $wpdb;
		$tax_map_table = $wpdb->prefix.'mw_wc_qbo_sync_tax_map';
		
		$tax_map_data = $this->get_row($wpdb->prepare("SELECT `qbo_tax_code` FROM ".$tax_map_table." WHERE `wc_tax_id` = %d AND `wc_tax_id_2` = %d ",$tax_rate_id,$tax_rate_id_2));
		//$this->_p($tax_map_data);
		if(is_array($tax_map_data) && count($tax_map_data)){
			$qbo_tax_code = $tax_map_data['qbo_tax_code'];
		}
		return $qbo_tax_code;
	}
	
	
	public function get_discounted_item_price($discount,$subtotal,$amount){		
		$item_amount = $amount-($discount/($subtotal)*$amount);
		$item_amount = number_format((float)$item_amount, 4, '.', '');
		return $item_amount;
	}
	
	public function get_qbo_zero_rated_tax_code($country=''){
		if($country==''){
			$country = $this->get_qbo_company_info('country');
		}
		$qbo_tax_code = '';
		if($this->is_connected()){
			if($country=='US'){
				$qbo_tax_code = 'NON';
			}else{
				$qbo_tax_code = $this->get_option('mw_wc_qbo_sync_tax_rule');
			}
		}
		return $qbo_tax_code;
	}
	
	public function get_tax_type($prices_include_tax='no'){
		if($prices_include_tax=='yes'){
			//return 'TaxInclusive';
		}
		return $this->get_option('mw_wc_qbo_sync_tax_format');
	}
	public function is_tax_inclusive($tax_type=''){
		$tax_type = ($tax_type=='')?$this->get_tax_type():$tax_type;
		return ($tax_type=='TaxInclusive')?true:false;
	}
	
	/*Get qbo currency rate by date*/
	public function get_qbo_cur_rate($source_cur,$date='',$target_car=''){
		$rate = 1;
		if($this->is_connected()){
			if($date==''){
			$date = date('Y-m-d');
			}
			
			if($source_cur!=''){
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ExchangeRateService = new QuickBooks_IPP_Service_ExchangeRate();
				$exchangerates = $ExchangeRateService->query($Context, $realm, "SELECT * FROM ExchangeRate WHERE SourceCurrencyCode = '$source_cur' AND AsOfDate = '$date' ");
				if(!$exchangerates){
					$yesterday = date('Y-m-d',strtotime("-1 days"));
					$exchangerates = $ExchangeRateService->query($Context, $realm, "SELECT * FROM ExchangeRate WHERE SourceCurrencyCode = '$source_cur' AND AsOfDate = '$yesterday' ");
				}
				if($exchangerates && count($exchangerates)){
					$ExchangeRate = $exchangerates[0];
					if($ExchangeRate->countRate()){
						$rate = $ExchangeRate->getRate();
					}
				}else{
					//get qbo home currency rate from woocommerce
					if($target_car!=''){
						
					}
				}
			}
		}		
		return $rate;
	}
	
	//07-04-2017
	public function AddRefund($refund_data){
		if($this->is_connected()){
			$manual = $this->get_array_isset($refund_data,'manual',false);
			$wc_inv_id = (int) $this->get_array_isset($refund_data,'wc_inv_id',0);
			$wc_rfnd_id = (int) $this->get_array_isset($refund_data,'refund_id',0);
			$wc_cus_id = (int) $this->get_array_isset($refund_data,'customer_user',0);
			
			$wc_inv_num = $this->get_array_isset($refund_data,'wc_inv_num','');
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$wc_inv_id;
			
			if($this->if_sync_refund($refund_data)){
				//$this->add_txt_to_log_file('Refund Test');
				$qbo_customer_id = (int) $this->get_array_isset($refund_data,'qbo_customerid',0);
				if(!$this->if_refund_exists($refund_data)){
					
					$Context = $this->Context;
					$realm = $this->realm;
					
					$RefundReceiptService = new QuickBooks_IPP_Service_RefundReceipt();
					$RefundReceipt = new QuickBooks_IPP_Object_RefundReceipt();
					
					$wc_inv_date = $this->get_array_isset($refund_data,'wc_inv_date','');
					$wc_inv_date = $this->view_date($wc_inv_date);
					
					$wc_rfnd_date = $this->get_array_isset($refund_data,'refund_date','');
					$wc_rfnd_date = $this->view_date($wc_rfnd_date);
					
					$RefundReceipt->setDocNumber($wc_inv_id.'-'.$wc_rfnd_id);
					$RefundReceipt->setCustomerRef("{-$qbo_customer_id}");
					$RefundReceipt->setTxnDate($wc_rfnd_date);
					
					$_order_currency = $this->get_array_isset($refund_data,'_order_currency','',true);
					$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
					if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
						
						$currency_rate_date = $wc_inv_date;
						$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
						
						$RefundReceipt->setCurrencyRef($_order_currency);
						$RefundReceipt->setExchangeRate($currency_rate);
					}
					
					$qbo_inv_items = (isset($refund_data['qbo_inv_items']))?$refund_data['qbo_inv_items']:array();
					
					$_refund_amount = $this->get_array_isset($refund_data,'_refund_amount',0);
					$_order_total = $this->get_array_isset($refund_data,'_order_total',0);
					$is_partial = false;
					
					if($_order_total!=$_refund_amount){
						$is_partial = true;
					}
					//
					$is_partial = true;
					
					//Qbo settings
					$qbo_is_sales_tax = $this->get_qbo_company_setting('is_sales_tax');
					$qbo_company_country = $this->get_qbo_company_info('country');
					
					//Tax rates
					$qbo_tax_code = '';
					$apply_tax = false;
					$is_tax_applied = false;
					$is_inclusive = false;
					
					$tax_rate_id = 0;
					$tax_rate_id_2 = 0;
					
					$tax_details = (isset($refund_data['tax_details']))?$refund_data['tax_details']:array();
					if(count($tax_details)){
						$tax_rate_id = $tax_details[0]['rate_id'];
					}
					if(count($tax_details)>1){
						$tax_rate_id_2 = $tax_details[1]['rate_id'];
					}
					
					$qbo_tax_code = $this->get_qbo_mapped_tax_code($tax_rate_id,$tax_rate_id_2);
					if($qbo_tax_code!='' || $qbo_tax_code!='NON'){
						$apply_tax = true;
					}
					
					$Tax_Code_Details = $this->mod_qbo_get_tx_dtls($qbo_tax_code);					
					$is_qbo_dual_tax = false;
					
					if(count($Tax_Code_Details)){
						if($Tax_Code_Details['TaxGroup'] && count($Tax_Code_Details['TaxRateDetail'])>1){
							$is_qbo_dual_tax = true;
						}
					}
					
					
					$Tax_Rate_Ref = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']:'';
					$TaxPercent = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref);
					$Tax_Name = (isset($Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][0]['TaxRateRef_name']:'';
					
					if($is_qbo_dual_tax){
						$Tax_Rate_Ref_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']:'';
						$TaxPercent_2 = $this->get_qbo_tax_rate_value_by_key($Tax_Rate_Ref_2);						
						$Tax_Name_2 = (isset($Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef']))?$Tax_Code_Details['TaxRateDetail'][1]['TaxRateRef_name']:'';
					}
					
					$_prices_include_tax = $this->get_array_isset($refund_data,'_prices_include_tax','no',true);
					if($qbo_is_sales_tax){						
						$tax_type = $this->get_tax_type($_prices_include_tax);
						$is_inclusive = $this->is_tax_inclusive($tax_type);
						$RefundReceipt->setGlobalTaxCalculation($tax_type);
						$RefundReceipt->setApplyTaxAfterDiscount(true);
					}
					
					//
					if($is_partial){
						//$apply_tax = false;
						$RefundReceipt->setTotalAmt($_refund_amount);
					}
					
					$refund_note = $this->get_array_isset($refund_data,'refund_note','',true,4000);
					$RefundReceipt->setPrivateNote($refund_note);
					
					//Add Refund items
					$first_line_desc = '';
					if(is_array($qbo_inv_items) && count($qbo_inv_items)){						
						$first_line_desc = $qbo_inv_items[0]['Description'];
						foreach($qbo_inv_items as $qbo_item){
							
							$line = new QuickBooks_IPP_Object_Line();
							$line->setDetailType('SalesItemLineDetail');
							
							$salesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
							
							$UnitPrice = $qbo_item["UnitPrice"];
							if($_cart_discount){
								//$UnitPrice = $this->get_discounted_item_price($_cart_discount,$total_line_subtotal,$UnitPrice);
							}
							
							//16-06-2017
							if($is_partial){
								$UnitPrice = $_refund_amount;
							}
							
							if($apply_tax && $TaxPercent > 0){
								/*
								$UnitPrice = round($UnitPrice / (($TaxPercent/100) + 1),2);
								if($is_qbo_dual_tax && $TaxPercent_2 > 0){
									$UnitPrice = round($UnitPrice / (($TaxPercent_2/100) + 1),2);
								}
								*/
								$comb_tp = ($is_qbo_dual_tax && $TaxPercent_2 > 0)?$TaxPercent+$TaxPercent_2:$TaxPercent;								
								$UnitPrice = round($UnitPrice / (($comb_tp/100) + 1),2);
								
								$is_tax_applied = true;
							}
							
							if($qbo_is_sales_tax){
								if($is_tax_applied){
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}								
							}
							
							$Amount = $qbo_item['Qty']*$UnitPrice;
							$line->setDescription('Refund - '.$qbo_item['Description']);
							$tax_class =  $qbo_item["tax_class"];
							
							/*
							if($qbo_is_sales_tax){
								if($apply_tax && $qbo_item["Taxed"]){
									$is_tax_applied = true;														
									$TaxCodeRef = ($qbo_company_country=='US')?'TAX':$qbo_tax_code;
									
									if($is_inclusive){
										
									}
									
									if($TaxCodeRef!=''){
										$salesItemLineDetail->setTaxCodeRef($TaxCodeRef);
									}
								}else{
									$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
									$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
								}
							}
							*/
							
							//23-05-2017
							
							/*
							if($qbo_is_sales_tax){
								$zero_rated_tax_code = $this->get_qbo_zero_rated_tax_code($qbo_company_country);
								$salesItemLineDetail->setTaxCodeRef($zero_rated_tax_code);
							}
							*/
							
							if($qbo_item["qbo_product_type"]=='Group'){
								$qdp = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
								$salesItemLineDetail->setItemRef($qdp);
							}else{
								$salesItemLineDetail->setItemRef($qbo_item["ItemRef"]);
							}
							
							if(isset($qbo_item["ClassRef"]) && $qbo_item["ClassRef"]!=''){
								if($qbo_item["qbo_product_type"]!='Group'){
									$salesItemLineDetail->setClassRef($qbo_item["ClassRef"]);
								}								
							}							
							
							if($this->option_checked('mw_wc_qbo_sync_invoice_date')){
								$salesItemLineDetail->setServiceDate($wc_inv_date);
							}
							
							$salesItemLineDetail->setUnitPrice($UnitPrice);
							
							if($is_partial){
								$line->setAmount($UnitPrice);								
								$salesItemLineDetail->setQty(1);
							}else{
								$line->setAmount($Amount);								
								$salesItemLineDetail->setQty($qbo_item["Qty"]);
							}
							
							
							$line->addSalesItemLineDetail($salesItemLineDetail);
							$RefundReceipt->addLine($line);
							if($is_partial){break;}
						}
					}
					
					$_payment_method = $this->get_array_isset($refund_data,'_payment_method','',true);					
					$pm_map_data = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
			
					$enable_batch = (int) $this->get_array_isset($pm_map_data,'enable_batch',0);
					if($enable_batch){
						$r_acc_id = (int) $this->get_array_isset($pm_map_data,'udf_account_id',0);						
					}else{
						$r_acc_id = (int) $this->get_array_isset($pm_map_data,'qbo_account_id',0);
					}					
					$RefundReceipt->setDepositToAccountRef("{-$r_acc_id}");
					
					$qb_p_method_id = (int) $this->get_array_isset($pm_map_data,'qb_p_method_id',0);					
					if($qb_p_method_id){
						$RefundReceipt->setPaymentMethodRef("{-$qb_p_method_id}");
					}
					
					/*Add Refund Tax*/
					if($apply_tax && $is_tax_applied && $Tax_Rate_Ref!=''  && $Tax_Name!=''){
						$TxnTaxDetail = new QuickBooks_IPP_Object_TxnTaxDetail();
						$TxnTaxDetail->setTxnTaxCodeRef($qbo_tax_code);
						$TaxLine = new QuickBooks_IPP_Object_TaxLine();
						$TaxLine->setDetailType('TaxLineDetail');
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLine_2 = new QuickBooks_IPP_Object_TaxLine();
							$TaxLine_2->setDetailType('TaxLineDetail');
							
							$TaxLineDetail_2 = new QuickBooks_IPP_Object_TaxLineDetail();
						}
						
						$TaxLineDetail = new QuickBooks_IPP_Object_TaxLineDetail();

						$TaxLineDetail->setTaxRateRef($Tax_Rate_Ref);
						$TaxLineDetail->setPerCentBased('true');
						
						//$NetAmountTaxable = 0;
						//$TaxLineDetail->setNetAmountTaxable($NetAmountTaxable);
						
						$TaxLineDetail->setTaxPercent($TaxPercent);
						
						$TaxLine->addTaxLineDetail($TaxLineDetail);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TaxLineDetail_2->setTaxRateRef($Tax_Rate_Ref_2);
							$TaxLineDetail_2->setPerCentBased('true');
							$TaxLineDetail_2->setTaxPercent($TaxPercent_2);
							
							//$NetAmountTaxable_2 = 0;
							//$TaxLineDetail_2->setNetAmountTaxable($NetAmountTaxable_2);
							
							$TaxLine_2->addTaxLineDetail($TaxLineDetail_2);
						}
						
						$TxnTaxDetail->addTaxLine($TaxLine);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$TxnTaxDetail->addTaxLine($TaxLine_2);
						}
						
						$SalesTax = new QuickBooks_IPP_Object_SalesTax();
						$SalesTax->setTaxable('true');
						$SalesTax->setSalesTaxCodeId($Tax_Rate_Ref);

						$SalesTax->setSalesTaxCodeName($Tax_Name);

						$RefundReceipt->addSalesTax($SalesTax);
						
						if($is_qbo_dual_tax && $TaxPercent_2>0){
							$SalesTax_2 = new QuickBooks_IPP_Object_SalesTax();
							$SalesTax_2->setTaxable('true');
							$SalesTax_2->setSalesTaxCodeId($Tax_Rate_Ref_2);

							$SalesTax_2->setSalesTaxCodeName($Tax_Name_2);

							$RefundReceipt->addSalesTax($SalesTax_2);							
						}
						
						$RefundReceipt->addTxnTaxDetail($TxnTaxDetail);						
						
					}
					
					//$this->_p($refund_data);
					//$this->_p($RefundReceipt);
					//return false;
					
					$log_title = "";
					$log_details = "";
					$log_status = 0;
					
					if ($resp = $RefundReceiptService->add($Context, $realm, $RefundReceipt)){
						$qbo_rfnd_id = $this->qbo_clear_braces($resp);
						$log_title.="Export Refund #$wc_rfnd_id Order #$ord_id_num\n";
						$log_details.="Refund #$wc_rfnd_id has been exported, QuickBooks Refund ID is #$qbo_rfnd_id";
						$log_status = 1;						
						$this->save_log($log_title,$log_details,'Refund',$log_status,true);
						$this->add_qbo_item_obj_into_log_file('Refund Add',$refund_data,$RefundReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
						return $qbo_rfnd_id;
						
					}else{
						$res_err = $RefundReceiptService->lastError($Context);
						$log_title.="Export Refund Error #$wc_rfnd_id Order #$ord_id_num\n";
						$log_details.="Error:$res_err";						
						$this->save_log($log_title,$log_details,'Refund',$log_status,true,true);
						$this->add_qbo_item_obj_into_log_file('Refund Add',$refund_data,$RefundReceipt,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());					
						return false;
					}
				}
			}
		}
	}
	
	public function if_refund_exists($refund_data){
		if($this->is_connected()){
			$wc_inv_id = (int) $this->get_array_isset($refund_data,'wc_inv_id',0);
			$wc_rfnd_id = (int) $this->get_array_isset($refund_data,'refund_id',0);
			$Context = $this->Context;
			$realm = $this->realm;
			
			$RefundReceiptService = new QuickBooks_IPP_Service_RefundReceipt();
			$refund_obj = $RefundReceiptService->query($Context, $realm, "SELECT Id FROM RefundReceipt WHERE DocNumber = '{$wc_inv_id}-{$wc_rfnd_id}' ");
			if($refund_obj && count($refund_obj)){
				return true;
			}
		}
		return false;
	}
	
	public function if_sync_refund($refund_data){
		if($this->is_connected()){
			//$this->add_txt_to_log_file(print_r($refund_data,true));
			$_payment_method = $this->get_array_isset($refund_data,'_payment_method','',true);
			$_order_currency = $this->get_array_isset($refund_data,'_order_currency','',true);
			$pm_map_data = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
			//$this->add_txt_to_log_file(print_r($pm_map_data,true));
			$enable_refund = (int) $this->get_array_isset($pm_map_data,'enable_refund',0);
			if($enable_refund){
				return true;
			}
		}
		return false;
	}
	
	//11-05-2017
	public function get_wc_deposit_payment_list($date_whr,$gateway,$currency,$single_pmnt_id=0){		
		$date_whr = str_replace('`{date}`','pm9.meta_value',$date_whr);
		
		$single_pmnt_id = (int) $single_pmnt_id;
		$single_whr = ($single_pmnt_id)?" AND pm8.meta_id = '{$single_pmnt_id}' ":'';
		
		if($single_pmnt_id){
			$date_whr = '';
		}
		
		global $wpdb;
		$sql = "
		SELECT DISTINCT(p.ID) as order_id, p.post_status as order_status, p.post_date as order_date, pm3.meta_value as order_total, pm5.meta_value as customer_user, pm6.meta_value as order_currency, 
		pm8.meta_id as payment_id, pm8.meta_value as transaction_id, pm9.meta_value as paid_date, pm10.meta_value as payment_method, pim.qbo_payment_id, pm12.meta_value as stripe_txn_fee, pm11.meta_value as order_number_formatted
		FROM
		{$wpdb->prefix}posts as p		
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
		
		INNER JOIN ".$wpdb->postmeta." pm8 
		ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_transaction_id' )
		INNER JOIN ".$wpdb->postmeta." pm9 
		ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_paid_date' )
		INNER JOIN ".$wpdb->postmeta." pm10 
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_payment_method' )		
		
		LEFT JOIN ".$wpdb->postmeta." pm12
		ON ( pm12.post_id = p.ID AND pm10.meta_value = 'stripe' AND pm12.meta_key =  'Stripe Fee' )
		
		LEFT JOIN ".$wpdb->postmeta." pm11
		ON ( pm11.post_id = p.ID AND pm11.meta_key =  '_order_number_formatted' )
		
		LEFT JOIN {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map pim ON ( pm8.meta_id = pim.wc_payment_id AND pim.is_wc_order = 0)
		WHERE
		p.post_type = 'shop_order'
		{$single_whr}
		AND pm8.meta_id > 0
		AND pm9.meta_value!=''		
		AND pm10.meta_value!=''
		AND pm10.meta_value = '{$gateway}'
		AND pm6.meta_value!=''
		AND pm6.meta_value = '{$currency}'
		{$date_whr}
		";
		//AND pm8.meta_value!=''
		
		$sql .='GROUP BY pm8.meta_id';	
		
		$orderby = 'pm9.meta_value DESC';
		$sql .= ' ORDER BY  '.$orderby;
		
		
		//echo $sql;
		return $this->get_data($sql);
	}
	
	public function get_wc_deposit_os_payment_list($date_whr,$gateway,$currency,$order_status,$single_ord_id=0){
		$date_whr = str_replace('`{date}`','p.post_date',$date_whr);
		
		$single_ord_id = (int) $single_ord_id;
		$single_whr = ($single_ord_id)?" AND p.ID={$single_ord_id} ":'';
		
		if($single_ord_id){
			$date_whr = '';
		}
		
		global $wpdb;
		$sql = "
		SELECT DISTINCT(p.ID) as order_id, p.post_status as order_status, p.post_date as order_date, pm3.meta_value as order_total, pm5.meta_value as customer_user, pm6.meta_value as order_currency, 
		pm10.meta_value as payment_method, pim.qbo_payment_id, pm11.meta_value as order_number_formatted
		FROM
		{$wpdb->prefix}posts as p		
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )		
		
		INNER JOIN ".$wpdb->postmeta." pm10 
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_payment_method' )
		
		LEFT JOIN ".$wpdb->postmeta." pm11
		ON ( pm11.post_id = p.ID AND pm11.meta_key =  '_order_number_formatted' )
		
		LEFT JOIN {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map pim ON ( p.ID = pim.wc_payment_id AND pim.is_wc_order = 1)
		WHERE
		p.post_type = 'shop_order'
		{$single_whr}
		AND p.post_status = '{$order_status}'
		AND pm10.meta_value!=''
		AND pm10.meta_value = '{$gateway}'
		AND pm6.meta_value!=''
		AND pm6.meta_value = '{$currency}'
		{$date_whr}
		";
		
		$sql .='GROUP BY pm8.meta_id';	
		
		$orderby = 'pm9.meta_value DESC';
		$sql .= ' ORDER BY  '.$orderby;
		
		
		//echo $sql;
		return $this->get_data($sql);
	}
	
	public function get_dps_utc_time_arr(){
		$utc_arr = array();
		for($hours=0; $hours<24; $hours++){
			for($mins=0; $mins<60; $mins+=30){
				$ts = str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($mins,2,'0',STR_PAD_LEFT);
				$utc_arr[$ts] = $ts;
			}
		}
		return $utc_arr;
	}
	
	public function get_dps_cron_ser_str(){
		$dps = '';
		if($this->is_connected()){
			global $wpdb;
			$p_maps_q = "SELECT * FROM `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` WHERE `id` >0 AND `enable_payment` = 1 AND `qbo_account_id` > 0 AND `enable_batch` = 1 AND `individual_batch_support` = 0 AND `wc_paymentmethod` !='' AND `deposit_cron_utc` !='' ";
			
			$p_maps_data = $this->get_data($p_maps_q);
			if(is_array($p_maps_data) && count($p_maps_data)){
				//$this->_p($p_maps_data);
				$dps_arr = array();
				$t_arr = array();
				foreach($p_maps_data as $pmd){
					$t_arr[$pmd['deposit_cron_utc']][$pmd['wc_paymentmethod']][] = $pmd['currency'];
				}
				$dps_arr['deposit_cron_url'] = base64_encode(site_url('index.php?mw_qbo_sync_public_deposit_cron=1'));
				$dps_arr['c_items'] = $t_arr;
				//$this->_p($dps_arr);
				$dps = serialize($dps_arr);
				$dps = base64_encode($dps);
			}
		}
		return $dps;
	}
	
	public function Cron_Deposit($gateways=array(),$single_pmnt_id=0,$currency=array()){
		if($this->is_connected()){
			if(!$this->get_qbo_company_setting('is_deposit_allowed')){
				$this->save_log('Export Deposit Error','QuickBooks Deposit Not Allowed','Deposit',0);
				return false;
			}
			global $wpdb;
			
			$single_pmnt_id = (int) $single_pmnt_id;
			
			$p_map_whr = '';
			if(is_array($gateways) && count($gateways)){
				$pgm_str = '';
				foreach($gateways as $gt){
					if($gt!=''){
						$gt = esc_sql($gt);
						$pgm_str.="'{$gt}',";
					}
				}
				if($pgm_str!=''){
					$pgm_str = substr($pgm_str,0,-1);					
					$p_map_whr = " AND `wc_paymentmethod` IN ($pgm_str)";
				}
				
			}
			$ibs_whr = ' AND `individual_batch_support` = 0 ';
			if($single_pmnt_id){
				$ibs_whr = " AND `individual_batch_support` = 1 ";
			}
			
			if(is_array($currency) && count($currency)){
				$pgm_str = '';
				foreach($currency as $gt){
					if($gt!=''){
						$gt = esc_sql($gt);
						$pgm_str.="'{$gt}',";
					}
				}
				if($pgm_str!=''){
					$pgm_str = substr($pgm_str,0,-1);					
					$p_map_whr.= " AND `currency` IN ($pgm_str)";
				}
			}
			
			$p_maps_q = "SELECT * FROM `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` WHERE `id` >0 AND `enable_payment` = 1 AND `qbo_account_id` > 0 AND `enable_batch` = 1 {$ibs_whr} AND `wc_paymentmethod` !='' {$p_map_whr}";
			
			$p_maps_data = $this->get_data($p_maps_q);
			
			if(is_array($p_maps_data) && count($p_maps_data)){
				foreach($p_maps_data as $pmd){
					$total_deposit_amnt = 0;
					$total_pmnt_amnt = 0;
					$total_txn_fee = 0;
					
					$today = date('Y-m-d');
					$day_name = strtolower(date('l'));
					
					$wc_paymentmethod = $pmd['wc_paymentmethod'];
					
					$date_whr = " AND `{date}` >= now() - INTERVAL 1 DAY ";
					
					$lump_weekend_batches = (int) $pmd['lump_weekend_batches'];
					
					if($lump_weekend_batches){
						if($day_name=='saturday'){
							continue;
						}
						if($day_name=='sunday'){
							$yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));							
							$date_whr = " AND `{date}` >= now() - INTERVAL 2 DAY ";
						}
					}
					
					$pmap_currency = $pmd['currency'];					
					$payment_cur = $pmap_currency;
					$cur_rate = 1;
					
					$wc_inv_ids = array();
					$wc_pmnt_ids = array();
					
					$wc_inv_ids_int = array();
					$wc_pmnt_ids_int = array();
					
					$ps_order_status = trim($pmd['ps_order_status']);
					$p_list_arr = array();
					if($ps_order_status!=''){
						continue;
						//$p_list_arr = $this->get_wc_deposit_os_payment_list($date_whr,$wc_paymentmethod,$payment_cur,$ps_order_status,$single_pmnt_id);
					}else{
						$p_list_arr = $this->get_wc_deposit_payment_list($date_whr,$wc_paymentmethod,$payment_cur,$single_pmnt_id);
					}			
					//$this->_p($p_list_arr);die;
					if(is_array($p_list_arr) && count($p_list_arr)){
						foreach($p_list_arr as $p_list){
							$wc_payment_id = (int) $p_list['payment_id'];
							$qbo_payment_id = (int) $p_list['qbo_payment_id'];
							
							if($qbo_payment_id>0){
								$total_pmnt_amnt+=$p_list['order_total'];
								$total_txn_fee+=($p_list['payment_method']=='stripe' && isset($p_list[$wc_paymentmethod.'_txn_fee']))?(float) $p_list[$wc_paymentmethod.'_txn_fee']:0;
								//$total_txn_fee = 0;
								
								//07-06-2017
								if($this->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $this->option_checked('mw_wc_qbo_sync_compt_p_wsnop') && isset($p_list['order_number_formatted']) && $p_list['order_number_formatted'] !=''){
									$wc_inv_ids[] = '#'.$p_list['order_number_formatted'];								
								}else{
									$wc_inv_ids[] = '#'.$p_list['order_id'];
								}
								
								$wc_pmnt_ids[] = '#'.$p_list['payment_id'];
								
								$wc_inv_ids_int[] = $p_list['order_id'];
								$wc_pmnt_ids_int[] = $p_list['payment_id'];
								
							}
						}
					}
					
					if($pmd['enable_transaction'] && $pmd['txn_expense_acc_id']){
						$total_deposit_amnt = $total_pmnt_amnt-$total_txn_fee;
						//$total_deposit_amnt = $total_pmnt_amnt;
					}else{
						$total_deposit_amnt = $total_pmnt_amnt;
					}
					
					$batch_support_rf = true;
					$b_rf_arr  = array();
					$total_b_rf_amnt = 0;
					
					if($batch_support_rf && count($wc_pmnt_ids_int)){
						$total_deposit_amnt = $total_deposit_amnt-$total_b_rf_amnt;
					}
					
					if($total_deposit_amnt>0){
						$Context = $this->Context;
						$realm = $this->realm;
						
						$DepositService = new QuickBooks_IPP_Service_Deposit();
						$Deposit = new QuickBooks_IPP_Object_Deposit();
						
						$Deposit->setDepositToAccountRef($pmd['qbo_account_id']);
						$Deposit->setTotalAmt($total_deposit_amnt);
						$Deposit->setTxnDate(date('Y-m-d'));
						
						$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
						$_order_currency = $payment_cur;
						if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
							
							$currency_rate_date = date('Y-m-d');
							$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
							
							$Deposit->setCurrencyRef($_order_currency);
							$Deposit->setExchangeRate($currency_rate);
						}
						
						if(is_array($p_list_arr) && count($p_list_arr)){
							foreach($p_list_arr as $p_list){							
								$qbo_payment_id = (int) $p_list['qbo_payment_id'];
								$amount = $p_list['order_total'];
								if($qbo_payment_id>0){
									$Line = new QuickBooks_IPP_Object_Line();
									$Line->setAmount($amount);

									$LinkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
									$LinkedTxn->setTxnId($qbo_payment_id);
									$LinkedTxn->setTxnType('Payment');
									$LinkedTxn->setTxnLineId(0);

									$Line->setLinkedTxn($LinkedTxn);

									$Deposit->addLine($Line);
								}
							}
						}
						
						if($pmd['enable_transaction'] && $pmd['txn_expense_acc_id']){
							$Line = new QuickBooks_IPP_Object_Line();
							$dp_amnt = -1 * abs($total_txn_fee);
							$Line->setAmount($dp_amnt);
							
							$dp_line_desc = "Transaction Fees for $wc_paymentmethod ".'('.date('m/d').')';					
							$Line->setDescription($dp_line_desc);
							
							$Line->setDetailType('DepositLineDetail');
							$DepositLineDetail = new QuickBooks_IPP_Object_DepositLineDetail();
							
							$dp_vendor_id = $pmd['vendor_id'];				
							$qb_p_method_id = $pmd['qb_p_method_id'];
							
							if($dp_vendor_id>0){
								$DepositLineDetail->setEntity($dp_vendor_id);
							}
							
							$DepositLineDetail->setAccountRef($pmd['txn_expense_acc_id']);
							if($qb_p_method_id>0){
								$DepositLineDetail->setPaymentMethodRef($qb_p_method_id);
							}
							
							$Line->addDepositLineDetail($DepositLineDetail);						
							$Deposit->addLine($Line);
						}
						
						if($total_b_rf_amnt>0){
							$Line = new QuickBooks_IPP_Object_Line();
							$rf_amnt = -1 * abs($total_b_rf_amnt);
							
							$Line->setAmount($rf_amnt);
							$dp_line_desc = "Refund for $wc_paymentmethod ".'('.date('m/d').')';
							
							$Line->setDescription($dp_line_desc);
							$Line->setDetailType('DepositLineDetail');
							$DepositLineDetail = new QuickBooks_IPP_Object_DepositLineDetail();
							
							$dp_vendor_id = $pmd['vendor_id'];				
							$qb_p_method_id = $pmd['qb_p_method_id'];
							
							if($dp_vendor_id>0){
								$DepositLineDetail->setEntity($dp_vendor_id);
							}
							
							$DepositLineDetail->setAccountRef($pmd['txn_expense_acc_id']);
							if($qb_p_method_id>0){
								$DepositLineDetail->setPaymentMethodRef($qb_p_method_id);
							}
							
							$Line->addDepositLineDetail($DepositLineDetail);						
							//$Deposit->addLine($Line);
						}
						
						//$this->_p($Deposit);
						//return false;
						
						$log_title = "";
						$log_details = "";
						$log_status = 0;
						
						if ($resp = $DepositService->add($Context, $realm, $Deposit)){
							$qbo_dpst_id = $this->qbo_clear_braces($resp);
							$log_title.="Export Deposit\n";
							if(count($wc_pmnt_ids)>1){
								$log_details.="Created Deposit with ".count($wc_pmnt_ids)." Payments\n";
							}else{
								$log_details.="Created Deposit with ".count($wc_pmnt_ids)." Payment\n";
							}
							$log_details.="Gateway: {$wc_paymentmethod} , Currency: {$pmap_currency}, QuickBooks Deposit ID #{$qbo_dpst_id}";
							
							if(count($wc_inv_ids)){
								$log_details.="\nWooCommerce Orders Included: ".implode(', ',$wc_inv_ids)."\n";
							}
							if(count($wc_pmnt_ids)){
								$log_details.='WooCommerce Payments Included: '.implode(', ',$wc_pmnt_ids)."\n";
							}
							
							$log_status = 1;							
							$this->save_log($log_title,$log_details,'Deposit',$log_status,true);
							$this->add_qbo_item_obj_into_log_file('Deposit Add',$gateways,$Deposit,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
							return $qbo_dpst_id;
							
						}else{
							$res_err = $DepositService->lastError($Context);
							$log_title.="Export Deposit Error\n";
							$log_details.="Gateway: {$wc_paymentmethod} , Currency: {$pmap_currency}\n";
							
							if(count($wc_inv_ids)){
								$log_details.="WooCommerce Orders Included: ".implode(', ',$wc_inv_ids)."\n";
							}
							if(count($wc_pmnt_ids)){
								$log_details.='WooCommerce Payments Included: '.implode(', ',$wc_pmnt_ids)."\n";
							}
							
							$log_details.="Error:{$res_err}";					
							$this->save_log($log_title,$log_details,'Deposit',$log_status,true,true);
							$this->add_qbo_item_obj_into_log_file('Deposit Add',$gateways,$Deposit,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
							return false;
						}
					}else{
						if(!$single_pmnt_id){
							$this->save_log('Deposit cron run - no payments to process.',"Gateway: {$wc_paymentmethod} , Currency: {$pmap_currency}",'Deposit',2);
						}else{
							$idi = ($ps_order_status!='')?'Order':'Payment';
							$this->save_log('Export Deposit Error (Individual)',"Incorrect deposit amount\n{$idi} #{$single_pmnt_id} Gateway: {$wc_paymentmethod} , Currency: {$pmap_currency}",'Deposit',2);
						}						
					}
					
				}
			}
		}
	}
	
	//30-05-2017
	public function AddJournalEntry($je_data,$je_refund=false){
		if($this->is_connected()){
			$manual = $this->get_array_isset($je_data,'manual',false);
			
			$wc_payment_id = (int) $this->get_array_isset($je_data,'wc_payment_id',0);
			$qbo_payment_id = (int) $this->get_array_isset($je_data,'qbo_payment_id',0);
			
			$wc_inv_id = (int) $this->get_array_isset($je_data,'wc_inv_id',0);
			$qbo_invoice_id = (int) $this->get_array_isset($je_data,'qbo_invoice_id',0);
			
			$qbo_customer_id = (int) $this->get_array_isset($je_data,'qbo_customer_id',0);
			
			$txn_fee_amount = $this->get_array_isset($je_data,'txn_fee_amount',0);
			
			$date = $this->get_array_isset($je_data,'date','');
			$date = $this->view_date($date);
			
			$qbo_account_id = (int) $this->get_array_isset($je_data,'qbo_account_id',0);
			$txn_expense_acc_id = (int) $this->get_array_isset($je_data,'txn_expense_acc_id',0);
			
			$_order_currency = $this->get_array_isset($je_data,'order_currency','',true);
			
			if(!$txn_expense_acc_id){
				$this->save_log('Export Journal Entry Error #'.$qbo_payment_id,'QuickBooks Expense Account ID Not Found','Journal Entry',0);
				return false;
			}
			
			if($qbo_payment_id && $txn_fee_amount>0){
				$Context = $this->Context;
				$realm = $this->realm;
				
				$JournalEntryService = new QuickBooks_IPP_Service_JournalEntry();
				if($je_refund){
					$chk_Je = $JournalEntryService->query($Context, $realm, "SELECT * FROM JournalEntry WHERE DocNumber = 'R-{$qbo_payment_id}' ");
				}else{
					$chk_Je = $JournalEntryService->query($Context, $realm, "SELECT * FROM JournalEntry WHERE DocNumber = '{$qbo_payment_id}' ");
				}
				
				if($chk_Je && count($chk_Je)){
					return false;
				}
				
				$JournalEntry = new QuickBooks_IPP_Object_JournalEntry();
				if($je_refund){
					$JournalEntry->setDocNumber('R-'.$qbo_payment_id);
				}else{
					$JournalEntry->setDocNumber($qbo_payment_id);
				}
				
				$JournalEntry->setTxnDate($date);
				
				// Debit line
				$Line1 = new QuickBooks_IPP_Object_Line();
				$Line1->setDescription('Transactions Fee Debit');
				$Line1->setAmount($txn_fee_amount);
				$Line1->setDetailType('JournalEntryLineDetail');

				$Detail1 = new QuickBooks_IPP_Object_JournalEntryLineDetail();
				$Detail1->setPostingType('Debit');
				
				if($je_refund){
					$Detail1->setAccountRef("{-$qbo_account_id}");
				}else{
					$Detail1->setAccountRef("{-$txn_expense_acc_id}");
				}
				
				//Customer				
				$Entity = new QuickBooks_IPP_Object_Entity();					
				$Entity->setType('Customer');
				$Entity->setEntityRef("{-$qbo_customer_id}");				
				$Detail1->setLinkedTxn($Entity);
				
				$Line1->addJournalEntryLineDetail($Detail1);
				
				//LinkedTxn				
				$LinkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
				$LinkedTxn->setTxnId("{-$qbo_payment_id}");
				$LinkedTxn->setTxnType('ReceivePayment');
				$Line1->setLinkedTxn($LinkedTxn);			
				
				$JournalEntry->addLine($Line1);
				
				// Credit line
				$Line2 = new QuickBooks_IPP_Object_Line();
				$Line2->setDescription('Transactions Fee Credit');
				$Line2->setAmount($txn_fee_amount);
				$Line2->setDetailType('JournalEntryLineDetail');

				$Detail2 = new QuickBooks_IPP_Object_JournalEntryLineDetail();
				$Detail2->setPostingType('Credit');
				
				if($je_refund){
					$Detail2->setAccountRef("{-$txn_expense_acc_id}");
				}else{
					$Detail2->setAccountRef("{-$qbo_account_id}");
				}
				
				$Line2->addJournalEntryLineDetail($Detail2);
				$JournalEntry->addLine($Line2);
				
				//JE Currency
				$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
				if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
					
					$currency_rate_date = $date;
					$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
					
					$JournalEntry->setCurrencyRef($_order_currency);
					$JournalEntry->setExchangeRate($currency_rate);
				}
				
				$log_title = "";
				$log_details = "";
				$log_status = 0;
				
				//$this->_p($je_data);
				//$this->_p($JournalEntry);
				//return false;
				if ($resp = $JournalEntryService->add($Context, $realm, $JournalEntry)){
					$qbo_je_id = $this->qbo_clear_braces($resp);
					$log_title.="Export Journal Entry #$qbo_payment_id\n";
					$log_details.="QuickBooks Journal Entry ID is #$qbo_je_id\n";
					$log_details.="WooCommerce Order #{$wc_inv_id}";
					$log_status = 1;
					
					$this->save_log($log_title,$log_details,'Journal Entry',$log_status,true);
					$this->add_qbo_item_obj_into_log_file('Journal Entry Add',$je_data,$JournalEntry,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
					
					return $qbo_je_id;			
				}else{
					$res_err = $JournalEntryService->lastError($Context);
					$log_title.="Export Journal Entry Error #$qbo_payment_id\n";
					$log_details.="WooCommerce Order #{$wc_inv_id}\n";
					$log_details.="Error:$res_err";				
					$this->save_log($log_title,$log_details,'Journal Entry',$log_status,true,true);
					$this->add_qbo_item_obj_into_log_file('Journal Entry Add',$je_data,$JournalEntry,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
					return false;
				}
			}
		}
	}
	
	public function get_je_data_from_pmnt_data($payment_data,$pm_map_data,$je_ed=array()){
		$return = array();		
		if(is_array($payment_data) && count($payment_data) && is_array($pm_map_data) && count($pm_map_data) && is_array($je_ed) && count($je_ed)){
			$is_txn_fee_sync = false;
			
			if($payment_data['payment_method']=='stripe' && (float) $payment_data['stripe_txn_fee']>0){
				$is_txn_fee_sync = true;		
			}
			
			if($is_txn_fee_sync){
				//Currently only home currency fee sync enabled
				$wc_currency = get_woocommerce_currency();
				if($wc_currency!=$payment_data['order_currency']){
					$is_txn_fee_sync = false;
				}
			}
			
			if($is_txn_fee_sync){
				$return['wc_inv_id'] = $payment_data['order_id'];
				$return['wc_inv_no'] = $je_ed['wc_inv_no'];				
				$return['qbo_invoice_id'] = $je_ed['qbo_invoice_id'];
				
				$return['qbo_customer_id'] = $je_ed['qbo_customer_id'];
				
				$return['qbo_payment_id'] = $je_ed['qbo_payment_id'];
				$return['wc_payment_id'] = $payment_data['payment_id'];
				
				$return['payment_method'] = $payment_data['payment_method'];
				$return['txn_fee_amount'] = (float) $payment_data['stripe_txn_fee'];
				
				$return['qbo_account_id'] = $pm_map_data['qbo_account_id'];
				$return['txn_expense_acc_id'] = $pm_map_data['txn_expense_acc_id'];
				
				$return['date'] = $payment_data['paid_date'];
				$return['order_currency'] = $payment_data['order_currency'];
			}
		}
		return $return;
	}
	
	/**
	 * Add Payment Into Quickbooks Online.
	 *
	 * @since    1.0.0
	 * Last Updated: 2017-01-04
	 */
	
	public function AddPayment($payment_data,$customer_data=array()){
		if($this->is_connected()){
		//$this->_p($customer_data);
		if($this->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
			return false;
		}
		
		$manual = $this->get_array_isset($payment_data,'manual',false);
		$payment_id = (int) $this->get_array_isset($payment_data,'payment_id',0);
		$wc_inv_id = (int) $this->get_array_isset($payment_data,'order_id',0);
		$wc_cus_id = (int) $this->get_array_isset($payment_data,'customer_user',0);		
		
		if(!$payment_id){
			$this->save_log('Export Payment Error #'.$payment_id,'Woocommerce payment id not found!','Payment',0);
			return false;
		}
		
		/*
		if(!$wc_cus_id){
			$this->save_log('Export Payment Error #'.$payment_id,'Woocommerce customer not found!','Payment',0);
			return false;
		}
		*/
		
		//25-05-2017
		$wc_inv_no = '';
		if($this->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $this->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
			$wc_inv_no = get_post_meta( $wc_inv_id, '_order_number_formatted', true );
		}
		
		$qbo_invoice_id = (int) $this->get_qbo_invoice_id($wc_inv_id,$wc_inv_no);
		
		if(!$qbo_invoice_id){
			$this->save_log('Export Payment Error #'.$payment_id,'QuickBooks invoice not found!','Payment',0);
			return false;
		}
		
		//10-04-2017
		$qbo_customer_id = 0;
		if($this->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
			$qbo_customer_id = (int) $this->get_option('mw_wc_qbo_sync_orders_to_specific_cust');
		}else{
			//21-03-2017
			if($wc_cus_id){
				$qbo_customer_id = (int) $this->check_save_get_qbo_customer_id($customer_data);
			}else{
				$qbo_customer_id = (int) $this->check_save_get_qbo_guest_id($customer_data);
			}
		}	
		
		if(!$qbo_customer_id){
			$this->save_log('Export Payment Error #'.$payment_id,'QuickBooks customer not found!','Payment',0);
			return false;
		}
		
		if($this->if_sync_payment($payment_data)){
			if(!$this->check_payment_get_obj($payment_data,$qbo_invoice_id,$qbo_customer_id)){
					$Context = $this->Context;
					$realm = $this->realm;
					
					$PaymentService = new QuickBooks_IPP_Service_Payment();
					$Payment = new QuickBooks_IPP_Object_Payment();
					
					$_payment_method = $this->get_array_isset($payment_data,'payment_method','',true);
					$_payment_method_title = $this->get_array_isset($payment_data,'payment_method_title','',true);
					
					$_order_currency = $this->get_array_isset($payment_data,'order_currency','',true);
					$pm_map_data = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					$enable_payment = (int) $this->get_array_isset($pm_map_data,'enable_payment',0);
					$_paid_date = $this->get_array_isset($payment_data,'paid_date','',true);
					
					$payment_amount = $this->get_array_isset($payment_data,'order_total',0);
					$payment_amount = floatval($payment_amount);
					
					if($enable_payment && $payment_amount>0){
						if($_paid_date==''){
							$this->save_log('Export Payment Error #'.$payment_id,'Payment date not found!','Payment',0);
							return false;
						}
						
						$_transaction_id = $this->get_array_isset($payment_data,'transaction_id','',true);
						
						$_paid_date = $this->view_date($_paid_date);
						
						$qb_p_method_id = (int) $this->get_array_isset($pm_map_data,'qb_p_method_id',0);
						$qbo_account_id = (int) $this->get_array_isset($pm_map_data,'qbo_account_id',0);
						
						$enable_batch = (int) $this->get_array_isset($pm_map_data,'enable_batch',0);
						$udf_account_id = (int) $this->get_array_isset($pm_map_data,'udf_account_id',0);
						
						//$Payment->setPaymentRefNum($_transaction_id);
						$Payment->setPaymentRefNum($payment_id);
						$Payment->setTxnDate($_paid_date);								
						
						//Payment Currency
						$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
						if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
							
							$currency_rate_date = $_paid_date;
							$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
							
							$Payment->setCurrencyRef($_order_currency);
							$Payment->setExchangeRate($currency_rate);
						}
						
						if($qb_p_method_id>0){
							 $Payment->setPaymentMethodRef($qb_p_method_id);
						}
						
						$Payment->setTotalAmt($payment_amount);
						
						$Line = new QuickBooks_IPP_Object_Line();
						$Line->setAmount($payment_amount);
						
						$LinkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
						$LinkedTxn->setTxnId($qbo_invoice_id);
						$LinkedTxn->setTxnType('Invoice');
						$Line->setLinkedTxn($LinkedTxn);
						$Payment->addLine($Line);
						
						$Payment->setCustomerRef("{-$qbo_customer_id}");
						
						
						if($enable_batch){
							 $Payment->setDepositToAccountRef("{-$udf_account_id}");
						}else{
							 $Payment->setDepositToAccountRef("{-$qbo_account_id}");
						}
						
						//$Payment->setDepositToAccountRef("{-$qbo_account_id}");
						
						//Add payment log
						$log_title = "";
						$log_details = "";
						$log_status = 0;
						
						//$this->_p($payment_data);
						//$this->_p($Payment);
						//return false;
						
						if ($resp = $PaymentService->add($Context, $realm, $Payment)){
							$qbo_pmnt_id = $this->qbo_clear_braces($resp);
							$log_title.="Export Payment #$payment_id\n";
							$log_details.="Payment #$payment_id has been exported, QuickBooks Payment ID is #$qbo_pmnt_id";
							$log_status = 1;
							$this->save_payment_id_map($payment_id,$qbo_pmnt_id);
							$this->save_log($log_title,$log_details,'Payment',$log_status,true);
							$this->add_qbo_item_obj_into_log_file('Payment Add',$payment_data,$Payment,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
							
							//30-05-2017
							if(!$enable_batch){
								$enable_transaction = (int) $this->get_array_isset($pm_map_data,'enable_transaction',0);
								if($enable_transaction){
									$je_extra_data = array();
									$je_extra_data['wc_inv_no'] = $wc_inv_no;
									$je_extra_data['qbo_customer_id'] = $qbo_customer_id;
									$je_extra_data['qbo_invoice_id'] = $qbo_invoice_id;
									$je_extra_data['qbo_payment_id'] = $qbo_pmnt_id;
									
									$je_data = $this->get_je_data_from_pmnt_data($payment_data,$pm_map_data,$je_extra_data);
									//$this->_p($je_data);
									if(is_array($je_data) && count($je_data)){
										$this->AddJournalEntry($je_data);
									}
								}								
							}						
							
							//17-05-2017
							$individual_batch_support = (int) $this->get_array_isset($pm_map_data,'individual_batch_support',0);
							if($individual_batch_support){
								$this->Cron_Deposit(array($_payment_method),$payment_id,array($_order_currency));
							}
							
							return $qbo_pmnt_id;
							
						}else{
							$res_err = $PaymentService->lastError($Context);
							$log_title.="Export Payment Error #$payment_id\n";
							$log_details.="Error:$res_err";						
							$this->save_log($log_title,$log_details,'Payment',$log_status,true,true);
							$this->add_qbo_item_obj_into_log_file('Payment Add',$payment_data,$Payment,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
							return false;
						}
					}else{
						$this->save_log('Export Payment Error #'.$payment_id,'Payment sync not enabled for the gateway or invalid payment amount.','Payment',0);						
						return false;
					}
					
				}
				
			}				
		}
		
	}
	
	public function save_payment_id_map($payment_id,$qbo_pmnt_id,$is_wc_order=0){
		$payment_id = intval($payment_id);
		$qbo_pmnt_id = intval($qbo_pmnt_id);
		
		$is_wc_order = intval($is_wc_order);
		
		if($qbo_pmnt_id && $qbo_pmnt_id){
			global $wpdb;
			$save_data = array();			
			$save_data['qbo_payment_id'] = $qbo_pmnt_id;			
			$table = $wpdb->prefix.'mw_wc_qbo_sync_payment_id_map';
			
			//$this->get_field_by_val($table,'id','wc_payment_id',$payment_id)
			$pim_row = $this->get_row("SELECT `id` FROM {$table} WHERE `wc_payment_id` = {$payment_id} AND `is_wc_order` = {$is_wc_order} LIMIT 0,1 ");
			
			if(is_array($pim_row) && count($pim_row)){
				$pim_id = $pim_row['id'];
				$wpdb->update($table,$save_data,array('id'=>$pim_id),array('%d'),array('%d'));
			}else{
				$save_data['is_wc_order'] = $is_wc_order;
				$save_data['wc_payment_id'] = $payment_id;
				$wpdb->insert($table, $save_data);
			}
		}
	}
	
	public function check_payment_get_obj($payment_data,$qbo_invoice_id=0,$qbo_customer_id=0){		
		if($this->is_connected()){
			$is_payment_exists = false;
			$payment_id = (int) $this->get_array_isset($payment_data,'payment_id',0);
			global $wpdb;
			$Context = $this->Context;
			$realm = $this->realm;
			
			if($payment_id){				
				$payment_id_map_row = $this->get_row("SELECT * FROM {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map WHERE `wc_payment_id` = {$payment_id} AND `qbo_payment_id` > 0 AND `is_wc_order` = 0 ");			
				if(is_array($payment_id_map_row) && count($payment_id_map_row)){
					$qbo_payment_id = (int) $payment_id_map_row['qbo_payment_id'];
					$PaymentService = new QuickBooks_IPP_Service_Payment();					
					$Payment_Row = $PaymentService->query($Context, $realm, "SELECT * FROM Payment WHERE Id = '$qbo_payment_id' ");
					//$this->_p($Payment_Row,true);			
					if($Payment_Row && count($Payment_Row)){
						$is_payment_exists = true;
					}
				}
			}	
			return $is_payment_exists;
		}		
	}
	
	//04-05-2017
	public function if_sync_os_payment($invoice_data){
		$_order_currency = $this->get_array_isset($invoice_data,'_order_currency','',true);
		$_payment_method = $this->get_array_isset($invoice_data,'_payment_method','',true);
		$order_status = $this->get_array_isset($invoice_data,'order_status','',true);
		
		$payment_method_map_data  = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
		$ps_order_status = $this->get_array_isset($payment_method_map_data,'ps_order_status','',true);
		if($order_status!='' && $order_status==$ps_order_status){
			return true;
		}
		return false;
	}
	
	public function check_os_payment_get_obj($payment_data,$qbo_invoice_id=0,$qbo_customer_id=0){	
		if($this->is_connected()){
			$is_payment_exists = false;
			$payment_id = (int) $this->get_array_isset($payment_data,'wc_inv_id',0); // order_id
			global $wpdb;
			$Context = $this->Context;
			$realm = $this->realm;
			
			if($payment_id){				
				$payment_id_map_row = $this->get_row("SELECT * FROM {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map WHERE `wc_payment_id` = {$payment_id} AND `qbo_payment_id` > 0 AND `is_wc_order` = 1 ");			
				if(is_array($payment_id_map_row) && count($payment_id_map_row)){
					$qbo_payment_id = (int) $payment_id_map_row['qbo_payment_id'];
					$PaymentService = new QuickBooks_IPP_Service_Payment();					
					$Payment_Row = $PaymentService->query($Context, $realm, "SELECT * FROM Payment WHERE Id = '$qbo_payment_id' ");
					//$this->_p($Payment_Row,true);			
					if($Payment_Row && count($Payment_Row)){
						$is_payment_exists = true;
					}
				}
			}	
			return $is_payment_exists;
		}		
	}
	
	public function PushOsPayment($payment_data){
		if($this->is_connected()){
		//$this->_p($invoice_data);
			if($this->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
				return false;
			}
			
			$manual = $this->get_array_isset($payment_data,'manual',false);
			$payment_id = (int) $this->get_array_isset($payment_data,'wc_inv_id',0);
			
			$wc_inv_num = $this->get_array_isset($payment_data,'wc_inv_num','');
			
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$payment_id;
			
			$qbo_customer_id = (int) $this->get_array_isset($payment_data,'qbo_customerid',0);
			
			$qbo_invoice_id = (int) $this->get_qbo_invoice_id($payment_id,$wc_inv_num);
	
			if(!$qbo_invoice_id){
				$this->save_log('Export Payment Error Order #'.$ord_id_num,'QuickBooks invoice not found!','Payment',0);
				return false;
			}
			
			if($this->if_sync_os_payment($payment_data)){
				if(!$this->check_os_payment_get_obj($payment_data,$qbo_invoice_id,$qbo_customer_id)){
					$Context = $this->Context;
					$realm = $this->realm;
					
					$PaymentService = new QuickBooks_IPP_Service_Payment();
					$Payment = new QuickBooks_IPP_Object_Payment();
					
					$_payment_method = $this->get_array_isset($payment_data,'_payment_method','',true);
					$_payment_method_title = $this->get_array_isset($payment_data,'_payment_method_title','',true);
					
					$_order_currency = $this->get_array_isset($payment_data,'_order_currency','',true);
					$pm_map_data = $this->get_mapped_payment_method_data($_payment_method,$_order_currency);
					
					$enable_payment = (int) $this->get_array_isset($pm_map_data,'enable_payment',0);
					$_paid_date = $this->get_array_isset($payment_data,'wc_inv_date','',true);
					$wc_inv_date = $this->get_array_isset($payment_data,'wc_inv_date','',true);
					
					$payment_amount = $this->get_array_isset($payment_data,'_order_total',0);
					$payment_amount = floatval($payment_amount);
					
					if($enable_payment && $payment_amount>0){
						if($_paid_date==''){
							$this->save_log('Export Payment Error Order #'.$ord_id_num,'Payment date not found!','Payment',0);
							return false;
						}
						
						//$_transaction_id = $this->get_array_isset($payment_data,'transaction_id','',true);
						
						$_paid_date = $this->view_date($_paid_date);
						
						$qb_p_method_id = (int) $this->get_array_isset($pm_map_data,'qb_p_method_id',0);
						$qbo_account_id = (int) $this->get_array_isset($pm_map_data,'qbo_account_id',0);
						
						$enable_batch = (int) $this->get_array_isset($pm_map_data,'enable_batch',0);
						
						//$Payment->setPaymentRefNum($_transaction_id);
						$Payment->setPaymentRefNum('Order-'.$payment_id);
						$Payment->setTxnDate($_paid_date);								
						
						//Payment Currency
						$qbo_home_currency = $this->get_qbo_company_setting('h_currency');
						if($_order_currency!='' && $qbo_home_currency!='' && $_order_currency!=$qbo_home_currency){
							
							$currency_rate_date = $_paid_date;
							$currency_rate = $this->get_qbo_cur_rate($_order_currency,$currency_rate_date,$qbo_home_currency);
							
							$Payment->setCurrencyRef($_order_currency);
							$Payment->setExchangeRate($currency_rate);
						}
						
						if($qb_p_method_id>0){
							 $Payment->setPaymentMethodRef($qb_p_method_id);
						}
						
						$Payment->setTotalAmt($payment_amount);
						
						$Line = new QuickBooks_IPP_Object_Line();
						$Line->setAmount($payment_amount);
						
						$LinkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
						$LinkedTxn->setTxnId($qbo_invoice_id);
						$LinkedTxn->setTxnType('Invoice');
						$Line->setLinkedTxn($LinkedTxn);
						$Payment->addLine($Line);
						
						$Payment->setCustomerRef("{-$qbo_customer_id}");
						
						$Payment->setDepositToAccountRef("{-$qbo_account_id}");
						
						//Add payment log
						$log_title = "";
						$log_details = "";
						$log_status = 0;
						
						//$this->_p($payment_data);
						//$this->_p($Payment);
						//return false;
						
						if ($resp = $PaymentService->add($Context, $realm, $Payment)){
							$qbo_pmnt_id = $this->qbo_clear_braces($resp);
							$log_title.="Export Payment Order #$ord_id_num\n";
							$log_details.="Payment for Order #$ord_id_num has been exported, QuickBooks Payment ID is #$qbo_pmnt_id";
							$log_status = 1;
							$this->save_payment_id_map($payment_id,$qbo_pmnt_id,1);
							$this->save_log($log_title,$log_details,'Payment',$log_status,true);
							$this->add_qbo_item_obj_into_log_file('Order Payment Add',$payment_data,$Payment,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse(),true);
							return $qbo_pmnt_id;
							
						}else{
							$res_err = $PaymentService->lastError($Context);
							$log_title.="Export Payment Error Order #$ord_id_num\n";
							$log_details.="Error:$res_err";						
							$this->save_log($log_title,$log_details,'Payment',$log_status,true,true);
							$this->add_qbo_item_obj_into_log_file('Order Payment Add',$payment_data,$Payment,$this->get_IPP()->lastRequest(),$this->get_IPP()->lastResponse());
							return false;
						}
					}else{
						$this->save_log('Export Payment Error Order #'.$ord_id_num,'Payment sync not enabled for the gateway or invalid payment amount.','Payment',0);						
						return false;
					}
				}
			}
		}
	}
	
	public function if_sync_payment($payment_data,$block_realtime_gateways=true){
		return true;
		$_payment_method = $this->get_array_isset($payment_data,'_payment_method','',true);
		$manual = $this->get_array_isset($payment_data,'manual',false);
		if($block_realtime_gateways && !$manual && ($_payment_method=='bacs' || $_payment_method=='cheque' || $_payment_method=='cod')){
			//return false;
		}
		return true;
	}
	
	public function get_domain(){
		return $_SERVER['SERVER_NAME'];
	}
	
	public function get_plugin_ip(){
		$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		if(empty($usersip) && isset($_SERVER['SERVER_NAME'])){
			$usersip = gethostbyname($_SERVER['SERVER_NAME']);
		}		
		return $usersip;
	}
	
	public function get_plugin_connection_dir(){
		$dirpath = dirname(__FILE__);
		return $dirpath;
	}
	
	//07-03-2017
	public function loggly_api_add_log($log_data){
		if(!empty($log_data)){
			if(is_array($log_data) && count($log_data)){
				$log_data = json_encode($log_data);
			}
			$client = new MyWorks_WC_QBO_Sync_SimpleHTTPClient();
			$requestHeader = array(
				"content-type:text/plain"		
			);
			$api_key = 'cbb22de2-5cca-4f43-a028-da5f00a2cebd';
			$api_url = "http://logs-01.loggly.com/inputs/".$api_key."/tag/http/";
			
			$response = $client->makeRequest($api_url, 'POST', $log_data,$requestHeader);
			//$this->_p($response);
		}
	}
	
	public function creds(){
		$creds = array();
		$server_name = $this->get_domain();
		$wc_qbo_plugin_dirpath = $this->get_plugin_connection_dir();
		$wc_qbo_plugin_usersip = $this->get_plugin_ip();
		
		$client = new MyWorks_WC_QBO_Sync_SimpleHTTPClient();
		
		$requestHeader = array(
			'Accept: application/json',
			'Licensekey: '.$this->get_option('mw_wc_qbo_sync_license',''),
			'Accesstoken: '.$this->get_option('mw_wc_qbo_sync_access_token',''),
			'Servername: '.$server_name,
			'Connectionnumber: '.$this->get_option('mw_wc_qbo_sync_connection_number',1),
			'Sandboxmode: '.$this->get_option('mw_wc_qbo_sync_sandbox_mode','no'),
			'Dirpath: '.$wc_qbo_plugin_dirpath,
			'Userip: '.$wc_qbo_plugin_usersip,
		);
		$response = $client->makeRequest($this->quickbooks_connection_dashboard_url.'/wc-qbo-get-connection-creds.php', 'GET' ,null ,$requestHeader);
		//$this->_p($response);
		if($response['status']['statusCode']==200 && $response['body']!=''){
			$creds = json_decode($response['body']);			
			$creds = (array) $creds;
			if(isset($creds['oauth_access_token']) && isset($creds['oauth_access_token_secret']) && isset($creds['encryption_key'])){
				$AES = QuickBooks_Encryption_Factory::create('aes');				
				$creds['oauth_access_token'] = $AES->decrypt($creds['encryption_key'], $creds['oauth_access_token']);
				$creds['oauth_access_token_secret'] = $AES->decrypt($creds['encryption_key'], $creds['oauth_access_token_secret']);				
			}
			
		}		
		//$this->_p($creds);
		$this->creds = $creds;
	}
	
	public function check()
	{
		if ($arr = $this->load())
		{
			return true;
		}
		
		return false;
	}
	
	public function load()
	{
		$arr = $this->creds;		
		if ( is_array($arr) && count($arr) && isset($arr['oauth_access_token']) &&  isset($arr['oauth_access_token']) && strlen($arr['oauth_access_token']) > 0 && strlen($arr['oauth_access_token_secret']) > 0)
		{
			return $arr;
		}
			
		return false;
	}
	
	
	public function test()
	{		
		if ($creds = $this->load())
		{
			
			$IPP = new QuickBooks_IPP();
			
			$IPP->authMode(
				QuickBooks_IPP::AUTHMODE_OAUTH, 
				$creds['app_username'], 
				$creds);
				
			if ($Context = $IPP->context())
			{				
				// Set the DBID
				$IPP->dbid($Context, 'something');
				
				// Set the IPP flavor
				$IPP->flavor($creds['qb_flavor']);
				
				// Get the base URL if it's QBO
				if ($creds['qb_flavor'] == QuickBooks_IPP_IDS::FLAVOR_ONLINE)
				{					
					$cur_version = $IPP->version();

					$IPP->version(QuickBooks_IPP_IDS::VERSION_3);		// Need v3 for this 

					$CustomerService = new QuickBooks_IPP_Service_Customer();
					$customers = $CustomerService->query($Context, $creds['qb_realm'], "SELECT * FROM Customer MAXRESULTS 1");

					$IPP->version($cur_version);		// Revert back to whatever they set 

					//$IPP->baseURL($IPP->getBaseURL($Context, $creds['qb_realm']));
				}
				else
				{					
					$companies = $IPP->getAvailableCompanies($Context);
				}
				
				//print('[[' . $IPP->lastRequest() . ']]' . "\n\n");
				//print('[[' . $IPP->lastResponse() . ']]' . "\n\n");
				//print('here we are! [' . $IPP->errorCode() . ']');
				
				// Check the last error code now...
				
				if ($IPP->errorCode() == 401 or 			// most calls return this
					$IPP->errorCode() == 3200 or				// but for some stupid reason the getAvailableCompanies call returns this
					$IPP->errorCode() == 3100)					//Internal Server 500
				{					
					$this->is_connected =false;
					update_option('mw_wc_qbo_sync_qbo_is_connected',0);
					return false;
					
				}
				$this->is_connected =true;
				update_option('mw_wc_qbo_sync_qbo_is_connected',1);
				return true;
			}
		}
		$this->is_connected =false;
		update_option('mw_wc_qbo_sync_qbo_is_connected',0);
		return false;
	}
	
	public function check_invalid_chars_in_db_conn_info(){
		return false;
		//$invalid_chars = array('@',':','/','\\','\'','+','?','%','#');
		$invalid_chars = array('+','/','#','%','\'','?');
		foreach($invalid_chars as $char){
			if( strpos( DB_USER, $char ) !== false || strpos( DB_PASSWORD, $char ) !== false || strpos( DB_HOST, $char ) !== false || strpos( DB_NAME, $char ) !== false) {
				return true;
			}
		}
		return false;
	}
	
	protected function get_IPP(){
		return $this->IPP;
	}
	
	public function connect(){		
		if ($this->check() && 	$this->test()){
			if($this->check_invalid_chars_in_db_conn_info()){
				$this->is_connected =false;
				update_option('mw_wc_qbo_sync_qbo_is_connected',0);
				return false;
			}
			$creds = $this->creds;
			//$sandbox = true;
			$mw_wc_qbo_sync_sandbox_mode = $this->get_option('mw_wc_qbo_sync_sandbox_mode','');
			$sandbox = ($mw_wc_qbo_sync_sandbox_mode=='yes')?true:false;			
			//$dsn = 'mysqli://'.DB_USER.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME;
			$dsn = null;
			$IPP = new QuickBooks_IPP($dsn);
			
			$the_username = (isset($creds['app_username']))?$creds['app_username']:'';
			$IPP->authMode(
			QuickBooks_IPP::AUTHMODE_OAUTH, 
			$the_username, 
			$this->creds
			);
			
			if ($sandbox)
			{
				// Turn on sandbox mode/URLs 
				$IPP->sandbox(true);
			}
			
			$Context = $IPP->context();
			
			$realm = (isset($creds['qb_realm']))?$creds['qb_realm']:'';
			
			$this->Context = $Context;
			$this->realm = $realm;
			
			$this->IPP = $IPP;
			
		}else{
			update_option('mw_wc_qbo_sync_qbo_is_connected',0);
			$this->Context = '';
			$this->realm = '';
		}
	}
	
	public function is_connected(){		
		return ($this->is_connected)?true:false;
	}
	public function getContext(){
		return $this->Context;
	}
	public function getRealm(){
		return $this->realm;
	}
	
	/***************************************************************Other Variables******************************************************/
	var $yes_no = array(
    	'no'=>'No',
    	'yes'=>'Yes',
    );
	
	var $no_yes = array(
    	'no'=>'No',
    	'yes'=>'Yes',
    );
	
	var $show_per_page = array(
		'10'=>'10',
		'20'=>'20',
		'50'=>'50',
		'100'=>'100',
		'200'=>'200',
		'500'=>'500',
	);
	
	var $log_save_days = array(
		'30'=>'30',
		'60'=>'60',
		'90'=>'90',
		'120'=>'120',	
	);
	
	var $tax_format = array(
		'TaxExclusive'=>'Exclusive of Tax',
		'TaxInclusive'=>'Inclusive of Tax'
	);
	
	var $product_pull_status = array(
		'Pending'=>'Pending Review',		
		'publish'=>'Published',
		'draft'=>'Draft',
	);
	
	var $product_pull_desc_fields = array(
		'description'=>'Description',		
		'short_description'=>'Short Description',
	);
	
	var $qbo_webhook_items = array(
		//'Customer'=>'Customer',
		//'Invoice'=>'Order',
		'Product'=>'Product',
		'Inventory'=>'Inventory',
		'Category'=>'Category',
		'Payment'=>'Payment',
	);
	
	var $qbo_rt_push_items = array(
		'Customer'=>'Customer',
		'Invoice'=>'Order',
		'Product'=>'Product',
		'Inventory'=>'Inventory',
		'Category'=>'Category',
		'Payment'=>'Payment',
		'Refund'=>'Refund',
	);
	
	var $client_dropdown_sort_order = array(
		'dname'=>'Display name',
		'first'=>'First name',
		'last'=>'Last name',
		'company'=>'Company name',
	);
	
	var $default_show_per_page;
	
	
	/***************************************************************Other Functions******************************************************/
	public function sanitize($txt=''){
		$txt = trim($txt);
		$txt   = esc_html( $txt );
		$txt   = esc_sql( $txt );
		$txt   = sanitize_text_field( $txt );
		return $txt;
	}
	public function wc_connection_num(){
		return array_combine(range(1,5), range(1,5));
	}
	
	public function _p($item='',$dump=false){
		echo '<pre>';
		if(is_object($item) || is_array($item)){
			if($dump){
				var_dump($item);
			}else{
				print_r($item);
			}			
		}else{
			if($dump){
				var_dump($item);
			}else{
				echo $item;
			}
			
		}
		echo '</pre>';
	}
	public function ipr_p($item='',$dump=false){		
		if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == ''){
			$this->_p($item,$dump);
		}
	}
	public function var_p($key=''){
		if($key!=''){
			if(isset($_POST[$key])){
				if(!is_array($_POST[$key])){
					return trim($_POST[$key]);
				}
				else{
					return $_POST[$key];
				}
			}
		}
	}

	public function var_g($key=''){
		if($key!=''){
			if(isset($_GET[$key])){
				return trim($_GET[$key]);
			}
		}
	}
	public function is_post($key=''){
		$return = false;
		$key = trim($key);
		if($key!='' && isset($_POST[$key])){
			$return = true;
		}
		return $return;
	}
	public function get_file_extention($filename=''){
		if(trim($filename!='')){
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
			return  $ext;
		}        

	}
	public function now(){
		return date('Y-m-d H:i:s');
	}
	
	
	public function set_session_msg($key='',$msg=''){
		if(!isset($_SESSION[$this->session_prefix.'mwqs_session_msg'])){
			$_SESSION[$this->session_prefix.'mwqs_session_msg'] = array();     
		}

		$_SESSION[$this->session_prefix.'mwqs_session_msg'][$key] = $msg;   
	}

	public function show_session_msg($key='',$div_class="",$unset=true){
		if(isset($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){
			if(!empty($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){            
			echo '<div class="mwqs_session_msg_div '.$div_class.'">';
			if(is_array($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){
				echo implode('<br />', $_SESSION[$this->session_prefix.'mwqs_session_msg'][$key]);
			}
			else{
				echo $_SESSION[$this->session_prefix.'mwqs_session_msg'][$key];
			}
			echo '</div>';
			}
			
			if($unset){
				unset($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key]);
			}			
		}
	}

	
	public function get_session_msg($key='',$div_class="",$unset=true){
		$return="";
		if(isset($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){
			if(!empty($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){
				$return.='<div class="mwqs_session_msg_div '.$div_class.'">';
				if(is_array($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key])){
					$return.= implode('<br />', $_SESSION[$this->session_prefix.'mwqs_session_msg'][$key]);
				}
				else{
					$return.= $_SESSION[$this->session_prefix.'mwqs_session_msg'][$key];
				}
				$return.= '</div>';
			}
			if($unset){
			   unset($_SESSION[$this->session_prefix.'mwqs_session_msg'][$key]); 
			}
		}
		return $return;    
	}
	
	public function window_redirect($url='',$session_msg=false,$key='',$msg=''){
		if($url!=''){		
		?>
		<script type="text/javascript">
			window.location='<?php echo $url;?>';
		</script>
		<?php
		}
	}
	
	public function redirect($url=''){
		if(trim($url)!=''){
			header("location:$url");
			exit(0);
		}
	}
	
	public function view_date($date,$format="Y-m-d"){
		if($date!='' && $date!=NULL && $date!='0000-00-00 00:00:00'){
			$date = strtotime($date);
			return date($format,$date);
		}
	}

	public function view_date_time($date,$format="d-m-Y h:i A"){
		if($date!='' && $date!=NULL && $date!='0000-00-00 00:00:00'){
			$date = strtotime($date);
			return date($format,$date);
		}
	}


	public  function get_paginate_links($total_records=0,$items_per_page=20,$show_total=true,$page=''){    
		if($page==''){		
			$page = $this->get_page_var();
		}
		
		if($total_records>0){
			$pagination_data = '<div class="mwqs_paginate_div">';

			$i_text = ($total_records>1)?'items':'item';
			
			if($show_total){
				//$pagination_data.='<div class="tot_div">Total <span>'.$total_records.'</span> '.$i_text.'</div>';
				//
				$total_pages = ceil($total_records / $items_per_page);
				$pgn_txt = $this->get_pagination_count_txt($page,$total_pages,$total_records,$items_per_page);
				
				$pagination_data.= '<div>'.$pgn_txt.'</div>';
			}
			
			if($total_records>$items_per_page){
				
			$pagination_data.='<div class="pagination">';			
			
			$pagination_data.=paginate_links( array(
								'base' => add_query_arg( 'paged', '%#%' ),
								'format' => '',
								'prev_text' => __('&laquo;'),
								'next_text' => __('&raquo;'),
								'total' => ceil($total_records / $items_per_page),
								'current' => $page,
								'end_size' =>2,
								'mid_size' =>3

								));
			
			$pagination_data.='</div>';			
			}

			$pagination_data.='</div>';

			return $pagination_data;

		}
	}
	
	public function get_pagination_count_txt($page,$total_pages,$count,$itemPerPage){
		$cur_page = ($page==0)?1:$page;
		if ($page != 0) $page--;
		
		$txt = '';
		if($cur_page<=$total_pages){
			$e_text = ($count>1)?'entries':'entry';
			$txt = 'Showing '.($page*$itemPerPage+1).' to '.(($total_pages==$cur_page || $itemPerPage>=$count)?$count:($page+1)*$itemPerPage).' of '.$count.' '.$e_text;		
		}
		return $txt;
	}
	
	
	//18-01-2017
	public function get_log_chart_data(){
		global $wpdb;
		$today = date("Y-m-d").' 00:00:00';
        $month = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 30, date("Y")));
        $year = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m") - 12, 1, date("Y")));
		
		$invoiceData = array();
		$result_inv_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `log_type`='Invoice' AND `success`=1 AND `details` NOT LIKE '%Draft Invoice not allowed%' GROUP BY date_format(added_date, '%k')");
		if(count($result_inv_today)){
			foreach($result_inv_today as $data){
				$invoiceData['today'][$data['date']] = $data['count'];
			}
		}
		$result_inv_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `log_type`='Invoice' AND `success`=1 AND `details` NOT LIKE '%Draft Invoice not allowed%' GROUP BY date_format(added_date, '%e')");
		if(count($result_inv_month)){
			foreach($result_inv_month as $data){
				$invoiceData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_inv_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `log_type`='Invoice' AND `success`=1 AND `details` NOT LIKE '%Draft Invoice not allowed%' GROUP BY date_format(added_date, '%M')");
		if(count($result_inv_year)){
			foreach($result_inv_year as $data){
				$invoiceData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		$paymentData = array();
		$result_pmnt_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `log_type`='Payment' AND `success`=1 GROUP BY date_format(added_date, '%k')");
		if(count($result_pmnt_today)){
			foreach($result_pmnt_today as $data){
				$paymentData['today'][$data['date']] = $data['count'];
			}
		}
		$result_pmnt_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `log_type`='Payment' AND `success`=1 GROUP BY date_format(added_date, '%e')");
		if(count($result_pmnt_month)){
			foreach($result_pmnt_month as $data){
				$paymentData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_pmnt_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `log_type`='Payment' AND `success`=1 GROUP BY date_format(added_date, '%M')");
		if(count($result_pmnt_year)){
			foreach($result_pmnt_year as $data){
				$paymentData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		$clientData = array();
		$result_cl_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `log_type`='Customer' AND `success`=1 GROUP BY date_format(added_date, '%k')");
		if(count($result_cl_today)){
			foreach($result_cl_today as $data){
				$clientData['today'][$data['date']] = $data['count'];
			}
		}
		$result_cl_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `log_type`='Customer' AND `success`=1 GROUP BY date_format(added_date, '%e')");
		if(count($result_cl_month)){
			foreach($result_cl_month as $data){
				$clientData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_cl_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `log_type`='Customer' AND `success`=1 GROUP BY date_format(added_date, '%M')");
		if(count($result_cl_year)){
			foreach($result_cl_year as $data){
				$clientData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		$errorData = array();
		$result_er_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `success`=0 GROUP BY date_format(added_date, '%k')");
		if(count($result_er_today)){
			foreach($result_er_today as $data){
				$errorData['today'][$data['date']] = $data['count'];
			}
		}
		$result_er_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `success`=0 GROUP BY date_format(added_date, '%e')");
		if(count($result_er_month)){
			foreach($result_er_month as $data){
				$errorData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_er_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `success`=0 GROUP BY date_format(added_date, '%M')");
		if(count($result_er_year)){
			foreach($result_er_year as $data){
				$errorData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		$depositData = array();
		$result_dp_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `log_type`='Deposit' AND `success`=1 GROUP BY date_format(added_date, '%k')");
		if(count($result_dp_today)){
			foreach($result_dp_today as $data){
				$depositData['today'][$data['date']] = $data['count'];
			}
		}
		
		$result_dp_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `log_type`='Deposit' AND `success`=1 GROUP BY date_format(added_date, '%e')");
		if(count($result_dp_month)){
			foreach($result_dp_month as $data){
				$depositData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_dp_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `log_type`='Deposit' AND `success`=1 GROUP BY date_format(added_date, '%M')");
		if(count($result_dp_year)){
			foreach($result_dp_year as $data){
				$depositData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		$productData = array();
		$result_prd_today = $this->get_data("SELECT date_format(added_date, '%k') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$today' AND `log_type`='Product' AND `success`=1 GROUP BY date_format(added_date, '%k')");
		if(count($result_prd_today)){
			foreach($result_prd_today as $data){
				$productData['today'][$data['date']] = $data['count'];
			}
		}
		
		$result_prd_month = $this->get_data("SELECT date_format(added_date, '%e %M') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$month' AND `log_type`='Product' AND `success`=1 GROUP BY date_format(added_date, '%e')");
		if(count($result_prd_month)){
			foreach($result_prd_month as $data){
				$productData['month'][$data['date']] = $data['count'];
			}
		}
		
		$result_prd_year = $this->get_data("SELECT date_format(added_date, '%M %Y') AS date, COUNT(id) AS count FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE added_date>'$year' AND `log_type`='Product' AND `success`=1 GROUP BY date_format(added_date, '%M')");
		if(count($result_prd_year)){
			foreach($result_prd_year as $data){
				$productData['year'][$data['date']] = $data['count'];
			}
		}
		
		//
		return array(
            'invoices' => array(
                'total' => $invoiceData,
            ),
            'clients' => array(
                'total' => $clientData,
            ),
			 'errors' => array(
                'total' => $errorData,
            ),
			'payments' => array(
                'total' => $paymentData,
            ),							
			'deposits' => array(
                'total' => $depositData,
            ),
			'products' => array(
                'total' => $productData,
            ),
			
        );
	}
	
	public function get_log_chart_output($viewPeriod=''){
		$data = $this->get_log_chart_data();
		if (!in_array($viewPeriod, array('today', 'month', 'year'))) {
            $viewPeriod = 'today';
        }
		
		$invoiceData = (isset($data['invoices']['total'][$viewPeriod]))?$data['invoices']['total'][$viewPeriod]:array();
		$clientData = (isset($data['clients']['total'][$viewPeriod]))?$data['clients']['total'][$viewPeriod]:array();
		$errorData = (isset($data['errors']['total'][$viewPeriod]))?$data['errors']['total'][$viewPeriod]:array();

		$paymentData = (isset($data['payments']['total'][$viewPeriod]))?$data['payments']['total'][$viewPeriod]:array();

		$productData = (isset($data['products']['total'][$viewPeriod]))?$data['products']['total'][$viewPeriod]:array();
		$depositData = (isset($data['deposits']['total'][$viewPeriod]))?$data['deposits']['total'][$viewPeriod]:array();
		
		if ($viewPeriod == 'today') {

            $graphLabels = array();
			
            $graphDataInv = array();
            $graphDataCus = array();
			$graphDataErr = array();
			
			$graphDataPmnt = array();
			
			$graphDataPrdt = array();
			$graphDataDpst = array();
			
			
            for ($i = 0; $i <= date("H"); $i++) {
                $graphLabels[] = date("ga", mktime($i, date("i"), date("s"), date("m"), date("d"), date("Y")));
                $graphDataInv[] = isset($invoiceData[$i]) ? $invoiceData[$i] : 0;
                $graphDataCus[] = isset($clientData[$i]) ? $clientData[$i] : 0;
				$graphDataErr[] = isset($errorData[$i]) ? $errorData[$i] : 0;
				
				$graphDataPmnt[] = isset($paymentData[$i]) ? $paymentData[$i] : 0;
				
				$graphDataPrdt[] = isset($productData[$i]) ? $productData[$i] : 0;
				$graphDataDpst[] = isset($depositData[$i]) ? $depositData[$i] : 0;
            }

        } elseif ($viewPeriod == 'month') {

            $graphLabels = array();
           
		    $graphDataInv = array();
            $graphDataCus = array();
			$graphDataErr = array();
			
			$graphDataPmnt = array();
			$graphDataPrdt = array();
			$graphDataDpst = array();
			
            for ($i = 0; $i < 30; $i++) {
                $time = mktime(0, 0, 0, date("m"), date("d") - $i, date("Y"));
                $graphLabels[] = date("jS", $time);
                $graphDataInv[] = isset($invoiceData[date("j F", $time)]) ? $invoiceData[date("j F", $time)] : 0;
                $graphDataCus[] = isset($clientData[date("j F", $time)]) ? $clientData[date("j F", $time)] : 0;
				$graphDataErr[] = isset($errorData[date("j F", $time)]) ? $errorData[date("j F", $time)] : 0;
				
				$graphDataPmnt[] = isset($paymentData[date("j F", $time)]) ? $paymentData[date("j F", $time)] : 0;
				
				$graphDataPrdt[] = isset($productData[date("j F", $time)]) ? $productData[date("j F", $time)] : 0;
				$graphDataDpst[] = isset($depositData[date("j F", $time)]) ? $depositData[date("j F", $time)] : 0;
            }

            $graphLabels = array_reverse($graphLabels);
			
            $graphDataInv = array_reverse($graphDataInv);
            $graphDataCus = array_reverse($graphDataCus);
			$graphDataErr = array_reverse($graphDataErr);
			
			$graphDataPmnt = array_reverse($graphDataPmnt);
			
			$graphDataPrdt = array_reverse($graphDataPrdt);
			$graphDataDpst = array_reverse($graphDataDpst);

        } elseif ($viewPeriod == 'year') {

            $graphLabels = array();
            
			$graphDataInv = array();
            $graphDataCus = array();
			$graphDataErr = array();
			
			$graphDataPmnt = array();
			$graphDataPrdt = array();
			$graphDataDpst = array();
			
            for ($i = 0; $i < 12; $i++) {
                $time = mktime(0, 0, 0, date("m") - $i, 1, date("Y"));
                $graphLabels[] = date("F y", $time);
                $graphDataInv[] = isset($invoiceData[date("F Y", $time)]) ? $invoiceData[date("F Y", $time)] : 0;
                $graphDataCus[] = isset($clientData[date("F Y", $time)]) ? $clientData[date("F Y", $time)] : 0;
				$graphDataErr[] = isset($errorData[date("F Y", $time)]) ? $errorData[date("F Y", $time)] : 0;
				
				$graphDataPmnt[] = isset($paymentData[date("F Y", $time)]) ? $paymentData[date("F Y", $time)] : 0;
				$graphDataPrdt[] = isset($productData[date("F Y", $time)]) ? $productData[date("F Y", $time)] : 0;
				$graphDataDpst[] = isset($depositData[date("F Y", $time)]) ? $depositData[date("F Y", $time)] : 0;
            }

            $graphLabels = array_reverse($graphLabels);
			
            $graphDataInv = array_reverse($graphDataInv);
            $graphDataCus = array_reverse($graphDataCus);
			$graphDataErr = array_reverse($graphDataErr);
			
			$graphDataPmnt = array_reverse($graphDataPmnt);
			$graphDataPrdt = array_reverse($graphDataPrdt);
			$graphDataDpst = array_reverse($graphDataDpst);

        }

        $graphLabels = '"' . implode('","', $graphLabels) . '"';
		
        $graphDataInv = implode(',', $graphDataInv);
        
        $graphDataCus = implode(',', $graphDataCus);
		$graphDataErr = implode(',', $graphDataErr);
		
		$graphDataPmnt = implode(',', $graphDataPmnt);
		$graphDataPrdt = implode(',', $graphDataPrdt);
		$graphDataDpst = implode(',', $graphDataDpst);

        $activeToday = ($viewPeriod == 'today') ? ' active' : '';
        $activeThisMonth = ($viewPeriod == 'month') ? ' active' : '';
        $activeThisYear = ($viewPeriod == 'year') ? ' active' : '';
		
		//colors
		$client_bg_color_rgb = '220,220,220,0.5';
		$client_border_color_rgb = '220,220,220,1';
		$client_point_bg_color_rgb = '220,220,220,1';
		$client_point_border_color = '#fff';
		
		
		
		
		$payment_bg_color_rgb = '66, 134, 244, 0.5';
		$payment_border_color_rgb = '66, 134, 244, 1';
		$payment_point_bg_color_rgb = '66, 134, 244, 1';
		$payment_point_border_color = '#fff';
		
		$deposit_bg_color_rgb = '66, 238, 244, 0.5';
		$deposit_border_color_rgb = '66, 238, 244, 1';
		$deposit_point_bg_color_rgb = '66, 238, 244, 1';
		$deposit_point_border_color = '#fff';
		
		$product_bg_color_rgb = '232, 163, 2, 0.5';
		$product_border_color_rgb = '232, 163, 2,1';
		$product_point_bg_color_rgb = '232, 163, 2, 1';
		$product_point_border_color = '#fff';
		
		$help_txt = __('Click on colors or labels for enable/disable','mw_wc_qbo_sync');
		
		//
		return <<<EOF
    <div style="padding:20px;">
    <div class="btn-group btn-group-sm btn-period-chooser" role="group" aria-label="...">
        <button type="button" class="btn btn-default{$activeToday}" data-period="today">Today</button>
        <button type="button" class="btn btn-default{$activeThisMonth}" data-period="month">This Month</button>
        <button type="button" class="btn btn-default{$activeThisYear}" data-period="year">This Year</button>
    </div>
	<p>{$help_txt}</p>
</div>

<div style="width:100%;height:450px;">
    <div id="ChartParent_MWQS">
        <canvas id="Chart_MWQS" height="400"></canvas>
    </div>
</div>

<script>

jQuery(document).ready(function($) {

    $('.btn-period-chooser button').click(function() {
        $('.btn-period-chooser button').removeClass('active');
        $(this).addClass('active');
		var period = $(this).data('period');
		mw_wc_qbo_sync_refresh_log_chart(period);
    });

    var lineData = {
        labels: [{$graphLabels}],
        datasets: [
            {
                label: "Customer",
                backgroundColor: "rgba({$client_bg_color_rgb})",
                borderColor: "rgba({$client_border_color_rgb})",
                pointBackgroundColor: "rgba({$client_point_bg_color_rgb})",
                pointBorderColor: "{$client_point_border_color}",            
                data: [{$graphDataCus}]
            },
            {
                label: "Invoice",
                backgroundColor: "rgba(93,197,96,0.5)",
                borderColor: "rgba(93,197,96,1)",
                pointBackgroundColor: "rgba(93,197,96,1)",
                pointBorderColor: "#fff",                
                data: [{$graphDataInv}]
            },
			{
                label: "Payment",
                backgroundColor: "rgba({$payment_bg_color_rgb})",
                borderColor: "rgba({$payment_border_color_rgb})",
                pointBackgroundColor: "rgba({$payment_point_bg_color_rgb})",
                pointBorderColor: "{$payment_point_border_color}",            
                data: [{$graphDataPmnt}]
            },
			{
                label: "Deposit",
                backgroundColor: "rgba({$deposit_bg_color_rgb})",
                borderColor: "rgba({$deposit_border_color_rgb})",
                pointBackgroundColor: "rgba({$deposit_point_bg_color_rgb})",
                pointBorderColor: "{$deposit_point_border_color}",            
                data: [{$graphDataDpst}]
            },
			{
                label: "Product",
                backgroundColor: "rgba({$product_bg_color_rgb})",
                borderColor: "rgba({$product_border_color_rgb})",
                pointBackgroundColor: "rgba({$product_point_bg_color_rgb})",
                pointBorderColor: "{$product_point_border_color}",            
                data: [{$graphDataPrdt}]
            },
			{
                label: "Error",
                backgroundColor: "rgba(255, 0, 0, 0.4)",
                borderColor: "rgba(255, 0, 0, 0.8)",
                pointBackgroundColor: "rgba(255, 0, 0, 0.6)",
                pointBorderColor: "#fff",                
                data: [{$graphDataErr}]
            },
        ]
    };

    var canvas = document.getElementById("Chart_MWQS");
    var parent = document.getElementById('ChartParent_MWQS');

    canvas.width = parent.offsetWidth;
    canvas.height = parent.offsetHeight;

    var ctx = $("#Chart_MWQS");
	//var ctx = $("#Chart_MWQS").get(0).getContext("2d");	
	//var chartDisplay = new Chart(document.getElementById("Chart_MWQS").getContext("2d")).Line(lineData);
	//var ctx = document.getElementById("Chart_MWQS").getContext("2d");
	var options = {
	 responsive: true,
		maintainAspectRatio: false,
		scales: {
			 yAxes: [{
				 ticks: {
					 beginAtZero: true,
					 userCallback: function(label, index, labels) {
						 // when the floored value is the same as the value we have a whole number
						 if (Math.floor(label) === label) {
							 return label;
						 }

					 },
				 }
			 }],
		},
	}
	var Chart_MWQS = Chart.Line(ctx, {
		data: lineData,
		options: options
	});
	
	/*
    new Chart(ctx, {
        type: 'line',
        data: lineData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
			scales: {
				 yAxes: [{
					 ticks: {
						 beginAtZero: true,
						 userCallback: function(label, index, labels) {
							 // when the floored value is the same as the value we have a whole number
							 if (Math.floor(label) === label) {
								 return label;
							 }

						 },
					 }
				 }],
			},
        }
    });
	*/
});
</script>
EOF;
		
	}
	/************************************************************---------------------------------******************************************************/
	
	public function get_data($query){
		global $wpdb;
		$query = trim($query);
		if($query!=''){
			return $wpdb->get_results($query,ARRAY_A);
		}			
	}
	
	public function get_row($query){
		global $wpdb;
		$query = trim($query);
		if($query!=''){
			return $wpdb->get_row($query,ARRAY_A);
		}
	}
	
	public function get_row_by_val($tbl,$field,$field_val){
        global $wpdb;
        if($tbl!='' && $field!='' && $field_val!=''){
            $tbl_q = "SELECT * FROM $tbl WHERE $field= '%s'";            
            $tbl_data = $this->get_row($wpdb->prepare($tbl_q,$field_val));
            return $tbl_data;
        }
        else{
            return array();
        }
    }

    public function get_field_by_val($tbl,$get_field,$field,$field_val){
        global $wpdb;
        if($tbl!='' && $get_field!='' && $field!='' && $field_val!=''){
            $tbl_q = "SELECT $get_field FROM $tbl WHERE $field= '%s'";            
            $tbl_data = $this->get_row($wpdb->prepare($tbl_q,$field_val));
            return (isset($tbl_data[$get_field]))?$tbl_data[$get_field]:'';
        }
        else{
            return '';
        }
    }


    public function get_tbl($tbl='',$fields='*',$whr='',$orderby='',$limit='',$group_by='',$having=''){
		if($tbl!=''){

			if(trim($fields)==''){$fields='*';}

			$tl_q = "SELECT $fields FROM $tbl ";

			if($whr!=''){
				$tl_q.="WHERE $whr ";
			}
			
			if($group_by!=''){
				$tl_q.="GROUP BY $group_by ";
			}
			
			if($having!=''){
				$tl_q.="HAVING $having ";
			}

			if($orderby!=''){
				$tl_q.="ORDER BY $orderby ";
			}

			if($limit!=''){
				$tl_q.="LIMIT $limit ";
			}
			
			
			return $this->get_data($tl_q);
		}
	}
	
	public function only_option($selected='',$opt_arr = array(),$s_key='',$s_val='',$return=false){
		$options='';
		if(is_array($opt_arr) && count($opt_arr)>0){			
			foreach ($opt_arr as $key => $value) {
				$sel_text = '';
				
				if($s_key!='' && $s_val!=''){
                    //change for multi
                    if(is_array($selected) && count($selected)){
                        if(in_array($value[$s_key],$selected)){$sel_text = 'selected="selected"';}
                    }else{
                        if($value[$s_key] == $selected){$sel_text = 'selected="selected"';}
                    }
					if($return){
						$options.='<option value="'.$value[$s_key].'" '.$sel_text.'>'.$value[$s_val].'</option>';
					}else{
						echo '<option value="'.$value[$s_key].'" '.$sel_text.'>'.$value[$s_val].'</option>';
					}			

				}else{
                    //change for multi
                    if(is_array($selected) && count($selected)){
                        if(in_array($key,$selected)){$sel_text = 'selected="selected"';}
                    }else{
                        if($key == $selected){$sel_text = 'selected="selected"';}
                    }
					if($return){
						$options.='<option value="'.$key.'" '.$sel_text.'>'.$value.'</option>';
					}else{
						echo '<option value="'.$key.'" '.$sel_text.'>'.$value.'</option>';
					}
					
				}
			}
		}
		if($return){
			return $options;
		}
	}

	public function option_html($selected='',$t_name='',$key_field='',$val_field='',$whr='',$orderby='',$limit='',$return=false){
		if($t_name!='' && $key_field!='' && $val_field!=''){
			$op_fields = "$key_field,$val_field";
			$op_data = $this->get_tbl($t_name,$op_fields,$whr,$orderby,$limit);
			if($return){
				return $this->only_option($selected,$op_data,$key_field,$val_field,$return);
			}
			$this->only_option($selected,$op_data,$key_field,$val_field,$return);
		}
	}
	/************************************************************************--*********************************************************************/
	var $per_page_keyword = 'mwqs_per_page';
	public function set_per_page_from_url($unique=''){
		if(isset($_GET[$this->per_page_keyword]) && (int) $_GET[$this->per_page_keyword]>0){
			$pp = (int) $_GET[$this->per_page_keyword];
			if(!$pp){$pp=$this->default_show_per_page;}
			$_SESSION[$this->session_prefix.'item_per_page'.$unique] = $pp;
		}
		
	}
	public function get_item_per_page($unique='',$default=20){
		$default = (!(int) $default)?(int) $this->default_show_per_page:$default;
		$itemPerPage = (isset($_SESSION[$this->session_prefix.'item_per_page'.$unique]))?$_SESSION[$this->session_prefix.'item_per_page'.$unique]:$default;
		return $itemPerPage;
	}
	
	public function get_page_var(){
		//$page = (get_query_var('paged')) ? (int) get_query_var('paged') : 1;
		$page = isset($_GET['paged']) ? (int) $_GET['paged'] : 1;
		if(!$page){$page=1;}
		return $page;
	}
	var $session_prefix = 'mw_wc_qbo_sync_';
	public function set_and_get($keyword){
		if(isset($_GET[$keyword])){
		  $_SESSION[$this->session_prefix.$keyword] = $_GET[$keyword];
		}
	}
	
	public function set_and_post($keyword){
		if(isset($_POST[$keyword])){
		  $_SESSION[$this->session_prefix.$keyword] = $_POST[$keyword];
		}
	}
	
	public function set_session_val($keyword,$value){
		$_SESSION[$this->session_prefix.$keyword] = $value;
	}
	
	public function get_session_val($keyword,$default='',$reset=false){
		$val = $default;
		if(isset($_SESSION[$this->session_prefix.$keyword])){
			$val = $_SESSION[$this->session_prefix.$keyword];
			/*
			if(!is_array($_SESSION[$this->session_prefix.$keyword])){
				$val = sanitize_text_field(esc_sql($_SESSION[$this->session_prefix.$keyword]));
			}
			*/
			if($reset){
				unset($_SESSION[$this->session_prefix.$keyword]);
			}
		}
		
		return $val;
	}
	
	//29-03-2017
	public function get_push_all_wc_customer_count(){
		$roles = 'customer';
		global $wpdb;
		if ( ! is_array( $roles ) )			
			$roles = array_map('trim',explode( ",", $roles ));
		$sql = '
			SELECT  COUNT(DISTINCT(' . $wpdb->users . '.ID))
			FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
			ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id			
			WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
			AND     (
		';
		$i = 1;
		foreach ( $roles as $role ) {
			$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%"' . $role . '"%\' ';
			if ( $i < count( $roles ) ) $sql .= ' OR ';
			$i++;
		}
		$sql .= ' ) ';
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	public function get_push_all_wc_customer_ids($count){
		$count = (int) $count;		
		if($count>0){
			global $wpdb;
			//$gc_length = 1024;
			$gc_length = $count*10;
			
			//SET GLOBAL
			$wpdb->query("SET group_concat_max_len = {$gc_length}");
			
			$roles = 'customer';			
			if ( ! is_array( $roles ) )			
				$roles = array_map('trim',explode( ",", $roles ));
			$sql = '
				SELECT  GROUP_CONCAT(DISTINCT(' . $wpdb->users . '.ID)) AS  `ids`
				FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
				ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id			
				WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
				AND     (
			';
			$i = 1;
			foreach ( $roles as $role ) {
				$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%"' . $role . '"%\' ';
				if ( $i < count( $roles ) ) $sql .= ' OR ';
				$i++;
			}
			$sql .= ' ) ';
			//echo $sql;			
			return (string) $wpdb->get_var($sql);
		}
	}
	
	/*19-01-2017*/
	
	public function count_customers($search_txt='',$list_page=false) {
		$roles = 'customer'; // we can use multiple role comma separeted
		global $wpdb;
		if ( ! is_array( $roles ) )			
			$roles = array_map('trim',explode( ",", $roles ));
		$sql = '
			SELECT  COUNT(DISTINCT(' . $wpdb->users . '.ID))
			FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
			ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id
			LEFT JOIN ' . $wpdb->usermeta . ' um1 ON ( um1.user_id = ' . $wpdb->users . '.ID
			AND um1.meta_key =  \'first_name\' )
			LEFT JOIN ' . $wpdb->usermeta . ' um2 ON ( um2.user_id = ' . $wpdb->users . '.ID
			AND um2.meta_key =  \'last_name\' )
			LEFT JOIN ' . $wpdb->usermeta . ' um3 ON ( um3.user_id = ' . $wpdb->users . '.ID
			AND um3.meta_key =  \'billing_company\' )
			LEFT JOIN ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs
			ON          ' . $wpdb->users . '.ID             =       ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs.wc_customerid
			LEFT JOIN ' . $wpdb->prefix . 'mw_wc_qbo_sync_qbo_customers qc ON ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs.qbo_customerid = qc.qbo_customerid
			WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
			AND     (
		';
		$i = 1;
		foreach ( $roles as $role ) {
			$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%"' . $role . '"%\' ';
			if ( $i < count( $roles ) ) $sql .= ' OR ';
			$i++;
		}
		$sql .= ' ) ';

		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND (".$wpdb->users.".display_name LIKE '%%%s%%' OR ".$wpdb->users.".user_email LIKE '%%%s%%' OR um3.meta_value LIKE '%%%s%%' ) ";
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt);
		}
		
		return $wpdb->get_var($sql);
	}
	
	public function get_customers($search_txt='',$limit='',$list_page=false) {
		$roles = 'customer'; // we can use multiple role comma separeted
		global $wpdb;
		if ( ! is_array( $roles ) )
			//$roles = array_walk( explode( ",", $roles ), 'trim' );
			$roles = array_map('trim',explode( ",", $roles ));
		$sql = '
			SELECT  DISTINCT(' . $wpdb->users . '.ID), ' . $wpdb->users . '.display_name, ' . $wpdb->users . '.user_email, ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs.qbo_customerid, um1.meta_value AS first_name, um2.meta_value AS last_name, um3.meta_value AS billing_company, qc.dname as `qbo_dname`, qc.email as `qbo_email`
			FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
			ON          ' . $wpdb->users . '.ID = ' . $wpdb->usermeta . '.user_id
			LEFT JOIN ' . $wpdb->usermeta . ' um1 ON ( um1.user_id = ' . $wpdb->users . '.ID
			AND um1.meta_key =  \'first_name\' )
			LEFT JOIN ' . $wpdb->usermeta . ' um2 ON ( um2.user_id = ' . $wpdb->users . '.ID
			AND um2.meta_key =  \'last_name\' )
			LEFT JOIN ' . $wpdb->usermeta . ' um3 ON ( um3.user_id = ' . $wpdb->users . '.ID
			AND um3.meta_key =  \'billing_company\' )
			LEFT JOIN ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs
			ON          ' . $wpdb->users . '.ID             =       ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs.wc_customerid
			LEFT JOIN ' . $wpdb->prefix . 'mw_wc_qbo_sync_qbo_customers qc ON ' . $wpdb->prefix . 'mw_wc_qbo_sync_customer_pairs.qbo_customerid = qc.qbo_customerid
			WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
			AND     (
		';
		$i = 1;
		foreach ( $roles as $role ) {
			$sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%%"' . $role . '"%%\' ';
			if ( $i < count( $roles ) ) $sql .= ' OR ';
			$i++;
		}
		$sql .= ' ) ';
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			//$sql .=" AND ".$wpdb->users.".display_name LIKE '%".$search_txt."%' ";
			$sql .=" AND (".$wpdb->users.".display_name LIKE '%%%s%%' OR ".$wpdb->users.".user_email LIKE '%%%s%%' OR um3.meta_value LIKE '%%%s%%' ) ";
		}
		
		//
		$sql.=' GROUP BY '. $wpdb->users . '.ID';
		
		$orderby = $wpdb->users.'.display_name ASC';
		$sql .= ' ORDER BY  '.$orderby;
		
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt);
		}
		//echo $sql;
		
		return $this->get_data($sql);
	}
	
	public function start_with($haystack, $needle){
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}
	
	/*28-01-2017*/
	public function show_sync_window_message($id, $message, $progress=0, $tot=0) {
		$d = array('message' => $message , 'progress' => $progress,'total' => $tot,'cur' => $id);
		echo json_encode($d);		  
		ob_flush();
		flush();
		die();
	}
	
	public function real_time_hooks_add_queue($item_type,$item_id,$item_action='',$wc_hook=''){
		global $wpdb;
		if($this->option_checked('mw_wc_qbo_sync_disable_realtime_sync')){
			$queue_table = $wpdb->prefix.'mw_wc_qbo_sync_real_time_sync_queue';
			$check_queue_query = $wpdb->prepare("SELECT * FROM `$queue_table` WHERE `item_type` = %s AND `item_action` = %s AND `item_id` = %d ",$item_type,$item_action,$item_id);
			if(!count($this->get_row($check_queue_query))){
				$save_queue_data = array();
				$save_queue_data['item_type'] = $item_type;
				$save_queue_data['item_action'] = $item_action;
				$save_queue_data['item_id'] = $item_id;
				$save_queue_data['woocommerce_hook'] = $wc_hook;
				$save_queue_data['run'] = 0;
				$save_queue_data['success'] = 0;
				$save_queue_data['added_date'] = $this->now();
				$wpdb->insert($queue_table, $save_queue_data);
			}
			return true;
		}
		return false;
	}
	public function get_offset($page, $items_per_page){
		return ( $page * $items_per_page ) - $items_per_page;
	}
	
	//29-03-2017
	public function get_push_all_wc_product_count(){
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(p.ID))
		FROM ".$wpdb->posts." p		
		WHERE p.post_type =  'product'
		AND p.post_status NOT IN('trash','auto-draft','inherit')						
		";
		
		//echo $sql;
		return (int) $wpdb->get_var($sql);
	}
	
	public function get_push_all_wc_product_ids($count){
		$count = (int) $count;
		if($count>0){
			global $wpdb;
			
			$gc_length = $count*10;
			$wpdb->query("SET group_concat_max_len = {$gc_length}");
			
			$sql = "
			SELECT GROUP_CONCAT(DISTINCT(p.ID)) AS `ids`
			FROM ".$wpdb->posts." p		
			WHERE p.post_type =  'product'
			AND p.post_status NOT IN('trash','auto-draft','inherit')						
			";
			
			//echo $sql;
			return (string) $wpdb->get_var($sql);
		}
	}
	
	//30-03-2017
	public function get_pull_inventory_map_data($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_ids = implode(',',$item_arr);
			global $wpdb;
			$sql = "
			SELECT DISTINCT(p.ID) AS wc_product_id, pm5.meta_value AS stock, pmap.quickbook_product_id
			FROM ".$wpdb->posts." p			
			LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
			AND pm5.meta_key =  '_stock' )						
			INNER JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_product_pairs pmap ON p.ID = pmap.wc_product_id
			INNER JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
			WHERE p.post_type =  'product'
			AND qp.itemid IN ({$item_ids})
			AND p.post_status NOT IN('trash','auto-draft','inherit')						
			";
			
			//echo $sql;
			return $this->get_data($sql);
		}
	}
	
	//19-05-2017
	public function get_pull_inventory_map_data_variation($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_ids = implode(',',$item_arr);
			global $wpdb;
			$sql = "
			SELECT DISTINCT(p.ID) AS wc_variation_id, pm5.meta_value AS stock, pmap.quickbook_product_id
			FROM ".$wpdb->posts." p			
			LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
			AND pm5.meta_key =  '_stock' )						
			INNER JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_variation_pairs pmap ON p.ID = pmap.wc_variation_id
			INNER JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
			WHERE p.post_type =  'product_variation'
			AND qp.itemid IN ({$item_ids})
			AND p.post_status NOT IN('trash','auto-draft','inherit')						
			";
			
			//echo $sql;
			return $this->get_data($sql);
		}
	}
	
	//02-05-2017
	public function get_pull_category_map_data($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_names = implode(',',$item_arr);			
			if($this->is_connected() && $item_names!=''){				
				global $wpdb;
				$cat_map_sql = "
				SELECT t.term_id AS id, t.name
				FROM   {$wpdb->terms} t
				LEFT JOIN {$wpdb->term_taxonomy} tt
				ON t.term_id = tt.term_id				
				WHERE  tt.taxonomy = 'product_cat'
				AND (t.name IN ({$item_names}) OR REPLACE(t.name,':','') IN ({$item_names}))
				";
				//echo $cat_map_sql;
				$categories = $this->get_data($cat_map_sql);
				//$this->_p($categories);
				$cat_map_arr = array();
				if($categories && count($categories)){
					foreach($categories as $category){
						$tmp_cat_arr = array();
						$tmp_cat_arr['Id'] = $this->qbo_clear_braces($category['id']);
						
						$name_replace_chars = array(':');					
						$cat_name = $this->get_array_isset(array('cat_name'=>$category['name']),'cat_name','',true,100,false,$name_replace_chars);
						//echo $cat_name.'<br />';
						$tmp_cat_arr['Name'] = md5(base64_encode($cat_name));
						$cat_map_arr[] = $tmp_cat_arr;
					}
				}
				return $cat_map_arr;
			}
		}
	}
	
	//25-04-2017
	public function get_push_category_map_data($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_names = implode(',',$item_arr);			
			if($this->is_connected() && $item_names!=''){
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ItemService = new QuickBooks_IPP_Service_Term();				
				$categories = $ItemService->query($Context, $realm, "SELECT Id,Name  FROM Item WHERE Type = 'Category' AND Name IN({$item_names}) ");
				
				//$this->_p($categories);
				$cat_map_arr = array();
				if($categories && count($categories)){
					foreach($categories as $category){
						$tmp_cat_arr = array();
						$tmp_cat_arr['Id'] = $this->qbo_clear_braces($category->getId());
						$tmp_cat_arr['Name'] = md5(base64_encode($category->getName()));
						$cat_map_arr[] = $tmp_cat_arr;
					}
				}
				return $cat_map_arr;
			}
		}
	}
	
	//09-05-2017
	public function get_push_inventory_map_data($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_ids = implode(',',$item_arr);
			if($this->is_connected() && $item_ids!=''){
				$Context = $this->Context;
				$realm = $this->realm;
				
				$ItemService = new QuickBooks_IPP_Service_Term();				
				$items = $ItemService->query($Context, $realm, "SELECT Id,QtyOnHand  FROM Item WHERE Type = 'Inventory' AND Id IN({$item_ids}) ");
				
				//$this->_p($items);
				$invnt_map_arr = array();
				if($items && count($items)){
					foreach($items as $item){
						$tmp_invnt_arr = array();
						$tmp_invnt_arr['quickbook_product_id'] = $this->qbo_clear_braces($item->getId());
						$tmp_invnt_arr['QtyOnHand'] = $item->getQtyOnHand();
						$invnt_map_arr[] = $tmp_invnt_arr;
					}
				}
				return $invnt_map_arr;
			}
		}
	}
	
	//21-06-2017
	public function get_push_payment_map_data($item_arr){
		$pmnt_map_arr = array();
		if(is_array($item_arr) && count($item_arr)){
			$item_ids = implode(',',$item_arr);
			if($this->is_connected()){
				$Context = $this->Context;
				$realm = $this->realm;
				$PaymentService = new QuickBooks_IPP_Service_Payment();
				$payments = $PaymentService->query($Context,$realm ,"SELECT Id FROM Payment WHERE Id IN({$item_ids}) ");				
				if($payments && count($payments)){
					foreach($payments as $payment){
						$pmnt_map_arr[] = $this->qbo_clear_braces($payment->getId());
					}
				}
			}
		}
		return $pmnt_map_arr;
	}
	
	//31-03-2017
	public function get_push_invoice_map_data($item_arr){
		if(is_array($item_arr) && count($item_arr)){
			$item_ids = implode(',',$item_arr);			
			if($this->is_connected()){
				$Context = $this->Context;
				$realm = $this->realm;
				
				if($this->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
					$SalesReceiptService = new QuickBooks_IPP_Service_SalesReceipt();
					$invoices = $SalesReceiptService->query($Context,$realm ,"SELECT Id,DocNumber FROM SalesReceipt WHERE DocNumber IN({$item_ids}) ");
				}else{
					$invoiceService = new QuickBooks_IPP_Service_Invoice();
					$invoices = $invoiceService->query($Context,$realm ,"SELECT Id,DocNumber FROM Invoice WHERE DocNumber IN({$item_ids}) ");
				}
				
				//$this->_p($invoices);
				$inv_map_arr = array();
				if($invoices && count($invoices)){
					foreach($invoices as $invoice){
						$tmp_inv_arr = array();
						$tmp_inv_arr['Id'] = $this->qbo_clear_braces($invoice->getId());
						$tmp_inv_arr['DocNumber'] = $invoice->getDocNumber();
						$inv_map_arr[] = $tmp_inv_arr;
					}
				}
				return $inv_map_arr;
			}
		}
	}
	
	//24-04-2017
	public function count_woocommerce_category_list($search_txt='') {
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(t.term_id))
		FROM   {$wpdb->terms} t
		LEFT JOIN {$wpdb->term_taxonomy} tt
		ON t.term_id = tt.term_id
		LEFT JOIN ".$wpdb->termmeta." tm1 ON ( tm1.term_id = t.term_id
		AND tm1.meta_key =  'product_count_product_cat' )
		WHERE  tt.taxonomy = 'product_cat'		
		";
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( t.name LIKE '%%%s%%' OR tt.description LIKE '%%%s%%' ) ";
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	public function get_woocommerce_category_list($search_txt='',$limit='') {
		global $wpdb;
		$sql = "
		SELECT t.term_id AS id, t.name, t.slug, tm1.meta_value AS product_count,tt.description,tt.parent
		FROM   {$wpdb->terms} t
		LEFT JOIN {$wpdb->term_taxonomy} tt
		ON t.term_id = tt.term_id
		LEFT JOIN ".$wpdb->termmeta." tm1 ON ( tm1.term_id = t.term_id
		AND tm1.meta_key =  'product_count_product_cat' )
		WHERE  tt.taxonomy = 'product_cat'		
		";
		
		//tt.count
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( t.name LIKE '%%%s%%' OR tt.description LIKE '%%%s%%' ) ";
		}
		
		$sql.=" ORDER  BY t.name ASC ";
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		//echo $sql;
		return $this->get_data($sql);
	}
	
	//
	public function count_woocommerce_product_list($search_txt='',$is_inventory=false) {
		$status = 'publish';
		global $wpdb;
		$ext_sql = ($is_inventory)?" AND pm8.meta_value='yes' ":''; // AND pm1.meta_value!=''
		$sql = "
		SELECT COUNT(DISTINCT(p.ID))
		FROM ".$wpdb->posts." p
		LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
		AND pm1.meta_key =  '_sku' ) 
		LEFT JOIN ".$wpdb->postmeta." pm2 ON ( pm2.post_id = p.ID
		AND pm2.meta_key =  '_regular_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 ON ( pm3.post_id = p.ID
		AND pm3.meta_key =  '_sale_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 ON ( pm4.post_id = p.ID
		AND pm4.meta_key =  '_price' )
		LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
		AND pm5.meta_key =  '_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm6 ON ( pm6.post_id = p.ID
		AND pm6.meta_key =  '_backorders' )
		LEFT JOIN ".$wpdb->postmeta." pm7 ON ( pm7.post_id = p.ID
		AND pm7.meta_key =  '_stock_status' )
		LEFT JOIN ".$wpdb->postmeta." pm8 ON ( pm8.post_id = p.ID
		AND pm8.meta_key =  '_manage_stock')
		LEFT JOIN ".$wpdb->postmeta." pm9 ON ( pm9.post_id = p.ID
		AND pm9.meta_key =  'total_sales' )
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_product_pairs pmap ON p.ID = pmap.wc_product_id
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
		WHERE p.post_type =  'product'
		AND p.post_status NOT IN('trash','auto-draft','inherit')
		{$ext_sql}
		";
		//AND pm1.meta_value != ''
		//AND p.post_status = '".$status."'
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( p.post_title LIKE '%%%s%%' OR pm1.meta_value LIKE '%%%s%%' ) ";
		}
		
		//$sql .='GROUP BY p.ID';		
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	public function get_woocommerce_product_list($search_txt='',$limit='',$is_inventory=false) {
		$status = 'publish';
		global $wpdb;
		$ext_sql = ($is_inventory)?" AND pm8.meta_value='yes' ":''; // AND pm1.meta_value!=''
		$sql = "
		SELECT DISTINCT(p.ID), p.post_title AS name, pmap.quickbook_product_id, pmap.class_id, pm1.meta_value AS sku, pm2.meta_value AS regular_price, pm3.meta_value AS sale_price, pm4.meta_value AS price, pm5.meta_value AS stock, pm6.meta_value AS backorders, pm7.meta_value AS stock_status, pm8.meta_value AS manage_stock, pm9.meta_value AS total_sales, qp.name as qp_name, qp.sku as qp_sku, qp.product_type as qp_product_type
		FROM ".$wpdb->posts." p
		LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
		AND pm1.meta_key =  '_sku' ) 
		LEFT JOIN ".$wpdb->postmeta." pm2 ON ( pm2.post_id = p.ID
		AND pm2.meta_key =  '_regular_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 ON ( pm3.post_id = p.ID
		AND pm3.meta_key =  '_sale_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 ON ( pm4.post_id = p.ID
		AND pm4.meta_key =  '_price' )
		LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
		AND pm5.meta_key =  '_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm6 ON ( pm6.post_id = p.ID
		AND pm6.meta_key =  '_backorders' )
		LEFT JOIN ".$wpdb->postmeta." pm7 ON ( pm7.post_id = p.ID
		AND pm7.meta_key =  '_stock_status' )
		LEFT JOIN ".$wpdb->postmeta." pm8 ON ( pm8.post_id = p.ID
		AND pm8.meta_key =  '_manage_stock')
		LEFT JOIN ".$wpdb->postmeta." pm9 ON ( pm9.post_id = p.ID
		AND pm9.meta_key =  'total_sales' )
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_product_pairs pmap ON p.ID = pmap.wc_product_id
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
		WHERE p.post_type =  'product'
		AND p.post_status NOT IN('trash','auto-draft','inherit')
		{$ext_sql}
		";
		//AND pm1.meta_value != ''
		//AND p.post_status = '".$status."'
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( p.post_title LIKE '%%%s%%' OR pm1.meta_value LIKE '%%%s%%' ) ";
		}
		
		$sql .='GROUP BY p.ID';
		
		$orderby = 'p.post_title ASC';
		$sql .= ' ORDER BY  '.$orderby;
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		
		//echo $sql;
		return $this->get_data($sql);		
	}
	
	//07-03-2017
	public function count_woocommerce_variation_list($search_txt='',$status='publish') {
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(p.ID))
		FROM ".$wpdb->posts." p
		LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
		AND pm1.meta_key =  '_sku' ) 
		LEFT JOIN ".$wpdb->postmeta." pm2 ON ( pm2.post_id = p.ID
		AND pm2.meta_key =  '_regular_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 ON ( pm3.post_id = p.ID
		AND pm3.meta_key =  '_sale_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 ON ( pm4.post_id = p.ID
		AND pm4.meta_key =  '_price' )
		LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
		AND pm5.meta_key =  '_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm6 ON ( pm6.post_id = p.ID
		AND pm6.meta_key =  '_backorders' )
		LEFT JOIN ".$wpdb->postmeta." pm7 ON ( pm7.post_id = p.ID
		AND pm7.meta_key =  '_stock_status' )
		LEFT JOIN ".$wpdb->postmeta." pm8 ON ( pm8.post_id = p.ID
		AND pm8.meta_key =  '_manage_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm9 ON ( pm9.post_id = p.ID
		AND pm9.meta_key =  'total_sales' )
		LEFT JOIN ".$wpdb->postmeta." pm_attr ON ( pm_attr.post_id = p.ID
		AND pm_attr.meta_key LIKE 'attribute_%' )
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_variation_pairs pmap ON p.ID = pmap.wc_variation_id
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
		LEFT JOIN " . $wpdb->posts . " p1 ON p.post_parent = p1.ID
		WHERE p.post_type =  'product_variation'
		AND p.post_status NOT IN('trash','auto-draft','inherit')						
		";
		//AND pm1.meta_value != ''
		//AND p.post_status = '".$status."'
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( p.post_title LIKE '%%%s%%' OR p1.post_title LIKE '%%%s%%' OR pm1.meta_value LIKE '%%%s%%' ) ";
		}
		
		//$sql .='GROUP BY p.ID';		
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	
	public function get_woocommerce_variation_list($search_txt='',$limit='',$status='publish') {
		
		global $wpdb;
		$sql = "
		SELECT DISTINCT(p.ID), p.post_title AS name, pmap.quickbook_product_id, pmap.class_id, pm1.meta_value AS sku, pm2.meta_value AS regular_price, pm3.meta_value AS sale_price, pm4.meta_value AS price, pm5.meta_value AS stock, pm6.meta_value AS backorders, pm7.meta_value AS stock_status, pm8.meta_value AS manage_stock, pm9.meta_value AS total_sales, p.post_parent as parent_id, p.post_name, p1.post_title AS parent_name, GROUP_CONCAT(pm_attr.meta_key SEPARATOR ',') as attribute_names, GROUP_CONCAT(pm_attr.meta_value  SEPARATOR ',') as attribute_values,
		qp.name as qp_name, qp.sku as qp_sku, qp.product_type as qp_product_type
		FROM ".$wpdb->posts." p
		LEFT JOIN ".$wpdb->postmeta." pm1 ON ( pm1.post_id = p.ID
		AND pm1.meta_key =  '_sku' ) 
		LEFT JOIN ".$wpdb->postmeta." pm2 ON ( pm2.post_id = p.ID
		AND pm2.meta_key =  '_regular_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 ON ( pm3.post_id = p.ID
		AND pm3.meta_key =  '_sale_price' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 ON ( pm4.post_id = p.ID
		AND pm4.meta_key =  '_price' )
		LEFT JOIN ".$wpdb->postmeta." pm5 ON ( pm5.post_id = p.ID
		AND pm5.meta_key =  '_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm6 ON ( pm6.post_id = p.ID
		AND pm6.meta_key =  '_backorders' )
		LEFT JOIN ".$wpdb->postmeta." pm7 ON ( pm7.post_id = p.ID
		AND pm7.meta_key =  '_stock_status' )
		LEFT JOIN ".$wpdb->postmeta." pm8 ON ( pm8.post_id = p.ID
		AND pm8.meta_key =  '_manage_stock' )
		LEFT JOIN ".$wpdb->postmeta." pm9 ON ( pm9.post_id = p.ID
		AND pm9.meta_key =  'total_sales' )
		LEFT JOIN ".$wpdb->postmeta." pm_attr ON ( pm_attr.post_id = p.ID
		AND pm_attr.meta_key LIKE 'attribute_%' )
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_variation_pairs pmap ON p.ID = pmap.wc_variation_id
		LEFT JOIN " . $wpdb->prefix . "mw_wc_qbo_sync_qbo_items qp ON pmap.quickbook_product_id = qp.itemid
		LEFT JOIN " . $wpdb->posts . " p1 ON p.post_parent = p1.ID
		WHERE p.post_type =  'product_variation'
		AND p.post_status NOT IN('trash','auto-draft','inherit')						
		";
		//AND pm1.meta_value != ''
		//AND p.post_status = '".$status."'
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( p.post_title LIKE '%%%s%%' OR p1.post_title LIKE '%%%s%%' OR pm1.meta_value LIKE '%%%s%%' ) ";
		}
		
		$sql .='GROUP BY p.ID';
		
		//$orderby = 'p.post_title ASC';
		$orderby = 'p.ID DESC, p1.post_parent ASC';
		
		$sql .= ' ORDER BY  '.$orderby;
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt);
		}
		
		//echo $sql;
		return $this->get_data($sql);		
	}
	
	//29-03-2017
	public function get_push_all_wc_order_count(){
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(p.ID))
		FROM
		{$wpdb->prefix}posts as p		
		WHERE
		p.post_type = 'shop_order'		
		";
		
		//echo $sql;
		return (int) $wpdb->get_var($sql);
	}
	
	public function get_push_all_wc_order_ids($count){
		$count = (int) $count;
		if($count>0){
			global $wpdb;
			
			$gc_length = $count*10;
			$wpdb->query("SET group_concat_max_len = {$gc_length}");
			
			$sql = "
			SELECT GROUP_CONCAT(DISTINCT(p.ID)) AS `ids`
			FROM
			{$wpdb->prefix}posts as p		
			WHERE
			p.post_type = 'shop_order'		
			";
			
			//echo $sql;
			return (string) $wpdb->get_var($sql);
		}		
	}
	
	public function count_order_list($search_txt='',$date_from='',$date_to='',$status=''){
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(p.ID))
		FROM
		{$wpdb->prefix}posts as p
		LEFT JOIN ".$wpdb->postmeta." pm1 
		ON ( pm1.post_id = p.ID AND pm1.meta_key =  '_billing_first_name' )
		LEFT JOIN ".$wpdb->postmeta." pm2 
		ON ( pm2.post_id = p.ID AND pm2.meta_key =  '_billing_last_name' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 
		ON ( pm4.post_id = p.ID AND pm4.meta_key =  '_order_key' )
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
		LEFT JOIN ".$wpdb->postmeta." pm7 
		ON ( pm7.post_id = p.ID AND pm7.meta_key =  '_billing_company' )
		LEFT JOIN ".$wpdb->postmeta." pm8 
		ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_payment_method' )
		LEFT JOIN ".$wpdb->postmeta." pm9
		ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_payment_method_title' )
		LEFT JOIN ".$wpdb->postmeta." pm10
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_order_number_formatted' )
		WHERE
		p.post_type = 'shop_order'		
		";
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( pm1.meta_value LIKE '%%%s%%' OR pm2.meta_value LIKE '%%%s%%' OR pm7.meta_value LIKE '%%%s%%' OR CONCAT(pm1.meta_value,' ', pm2.meta_value) LIKE '%%%s%%' ) ";
		}
		
		//
		$status = $this->sanitize($status);
		if($status!=''){
			$sql .=$wpdb->prepare(" AND p.post_status = %s",$status);
		}
		
		$date_from = $this->sanitize($date_from);
		if($date_from!=''){
			$sql .=" AND p.post_date>='".$date_from." 00:00:00'";
		}
		
		$date_to = $this->sanitize($date_to);
		if($date_to!=''){
			$sql .=" AND p.post_date<='".$date_to." 23:59:59'";
		}
		
		//$sql .='GROUP BY p.ID';
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt,$search_txt);
		}
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	public function get_order_list($search_txt='',$limit='',$date_from='',$date_to='',$status=''){
		global $wpdb;
		$sql = "
		SELECT DISTINCT(p.ID), p.post_status, p.post_date, pm1.meta_value as billing_first_name, pm2.meta_value as billing_last_name, pm3.meta_value as order_total, pm4.meta_value as order_key, pm5.meta_value as customer_user, pm6.meta_value as order_currency, pm8.meta_value as payment_method, pm9.meta_value as payment_method_title, pm10.meta_value as order_number_formatted
		FROM
		{$wpdb->prefix}posts as p
		LEFT JOIN ".$wpdb->postmeta." pm1 
		ON ( pm1.post_id = p.ID AND pm1.meta_key =  '_billing_first_name' )
		LEFT JOIN ".$wpdb->postmeta." pm2 
		ON ( pm2.post_id = p.ID AND pm2.meta_key =  '_billing_last_name' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 
		ON ( pm4.post_id = p.ID AND pm4.meta_key =  '_order_key' )
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
		LEFT JOIN ".$wpdb->postmeta." pm7 
		ON ( pm7.post_id = p.ID AND pm7.meta_key =  '_billing_company' )
		LEFT JOIN ".$wpdb->postmeta." pm8 
		ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_payment_method' )
		LEFT JOIN ".$wpdb->postmeta." pm9
		ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_payment_method_title' )
		LEFT JOIN ".$wpdb->postmeta." pm10
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_order_number_formatted' )
		
		WHERE
		p.post_type = 'shop_order'
		";
		
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( pm1.meta_value LIKE '%%%s%%' OR pm2.meta_value LIKE '%%%s%%' OR pm7.meta_value LIKE '%%%s%%' OR CONCAT(pm1.meta_value,' ', pm2.meta_value) LIKE '%%%s%%' ) ";
		}
		
		//
		$status = $this->sanitize($status);
		if($status!=''){
			$sql .=$wpdb->prepare(" AND p.post_status = %s",$status);
		}
		
		$date_from = $this->sanitize($date_from);
		if($date_from!=''){
			$sql .=" AND p.post_date>='".$date_from." 00:00:00'";
		}
		
		$date_to = $this->sanitize($date_to);
		if($date_to!=''){
			$sql .=" AND p.post_date<='".$date_to." 23:59:59'";
		}
		
		$sql .='GROUP BY p.ID';		
		
		$orderby = 'p.post_date DESC';
		$sql .= ' ORDER BY  '.$orderby;
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt,$search_txt);
		}
		//echo $sql;
		return $this->get_data($sql);
	}
	
	public function count_wc_payment_list($search_txt='',$date_from='',$date_to=''){
		global $wpdb;
		$sql = "
		SELECT COUNT(DISTINCT(pm8.meta_id))
		FROM
		{$wpdb->prefix}posts as p
		LEFT JOIN ".$wpdb->postmeta." pm1 
		ON ( pm1.post_id = p.ID AND pm1.meta_key =  '_billing_first_name' )
		LEFT JOIN ".$wpdb->postmeta." pm2 
		ON ( pm2.post_id = p.ID AND pm2.meta_key =  '_billing_last_name' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 
		ON ( pm4.post_id = p.ID AND pm4.meta_key =  '_order_key' )
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
		LEFT JOIN ".$wpdb->postmeta." pm7 
		ON ( pm7.post_id = p.ID AND pm7.meta_key =  '_billing_company' )
		INNER JOIN ".$wpdb->postmeta." pm8 
		ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_transaction_id' )
		INNER JOIN ".$wpdb->postmeta." pm9 
		ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_paid_date' )
		INNER JOIN ".$wpdb->postmeta." pm10 
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_payment_method' )
		INNER JOIN ".$wpdb->postmeta." pm11
		ON ( pm11.post_id = p.ID AND pm11.meta_key =  '_payment_method_title' )
		
		LEFT JOIN ".$wpdb->postmeta." pm12
		ON ( pm12.post_id = p.ID AND pm10.meta_value = 'stripe' AND pm12.meta_key =  'Stripe Fee' )
		
		LEFT JOIN ".$wpdb->postmeta." pm13
		ON ( pm13.post_id = p.ID AND pm13.meta_key =  '_order_number_formatted' )
		
		LEFT JOIN {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map pim ON ( pm8.meta_id = pim.wc_payment_id AND pim.is_wc_order = 0)
		WHERE
		p.post_type = 'shop_order'
		AND pm8.meta_id > 0		
		AND pm9.meta_value!=''		
		AND pm10.meta_value!=''
		";
		//AND pm8.meta_value!=''
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( pm1.meta_value LIKE '%%%s%%' OR pm2.meta_value LIKE '%%%s%%' OR pm7.meta_value LIKE '%%%s%%' OR CONCAT(pm1.meta_value,' ', pm2.meta_value) LIKE '%%%s%%' ) ";
		}
		
		$date_from = $this->sanitize($date_from);
		if($date_from!=''){
			$sql .=" AND pm9.meta_value>='".$date_from." 00:00:00'";
		}
		
		$date_to = $this->sanitize($date_to);
		if($date_to!=''){
			$sql .=" AND pm9.meta_value<='".$date_to." 23:59:59'";
		}
		
		//$sql .='GROUP BY pm8.meta_id';
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt,$search_txt);
		}
		//echo $sql;
		return $wpdb->get_var($sql);
	}
	
	public function get_wc_payment_list($search_txt='',$limit='',$date_from='',$date_to=''){
		global $wpdb;
		$sql = "
		SELECT DISTINCT(p.ID) as order_id, p.post_status as order_status, p.post_date as order_date, pm1.meta_value as billing_first_name, pm2.meta_value as billing_last_name, pm3.meta_value as order_total, pm4.meta_value as order_key, pm5.meta_value as customer_user, pm6.meta_value as order_currency, 
		pm8.meta_id as payment_id, pm8.meta_value as transaction_id, pm9.meta_value as paid_date, pm10.meta_value as payment_method, pm11.meta_value as payment_method_title, pim.qbo_payment_id, pm12.meta_value as stripe_txn_fee , pm13.meta_value as order_number_formatted
		FROM
		{$wpdb->prefix}posts as p
		LEFT JOIN ".$wpdb->postmeta." pm1 
		ON ( pm1.post_id = p.ID AND pm1.meta_key =  '_billing_first_name' )
		LEFT JOIN ".$wpdb->postmeta." pm2 
		ON ( pm2.post_id = p.ID AND pm2.meta_key =  '_billing_last_name' ) 
		LEFT JOIN ".$wpdb->postmeta." pm3 
		ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
		LEFT JOIN ".$wpdb->postmeta." pm4 
		ON ( pm4.post_id = p.ID AND pm4.meta_key =  '_order_key' )
		LEFT JOIN ".$wpdb->postmeta." pm5 
		ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
		LEFT JOIN ".$wpdb->postmeta." pm6 
		ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
		LEFT JOIN ".$wpdb->postmeta." pm7 
		ON ( pm7.post_id = p.ID AND pm7.meta_key =  '_billing_company' )
		INNER JOIN ".$wpdb->postmeta." pm8 
		ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_transaction_id' )
		INNER JOIN ".$wpdb->postmeta." pm9 
		ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_paid_date' )
		INNER JOIN ".$wpdb->postmeta." pm10 
		ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_payment_method' )
		INNER JOIN ".$wpdb->postmeta." pm11
		ON ( pm11.post_id = p.ID AND pm11.meta_key =  '_payment_method_title' )
		
		LEFT JOIN ".$wpdb->postmeta." pm12
		ON ( pm12.post_id = p.ID AND pm10.meta_value = 'stripe' AND pm12.meta_key =  'Stripe Fee' )
		
		LEFT JOIN ".$wpdb->postmeta." pm13
		ON ( pm13.post_id = p.ID AND pm13.meta_key =  '_order_number_formatted' )
		
		LEFT JOIN {$wpdb->prefix}mw_wc_qbo_sync_payment_id_map pim ON ( pm8.meta_id = pim.wc_payment_id AND pim.is_wc_order = 0)
		WHERE
		p.post_type = 'shop_order'
		AND pm8.meta_id > 0
		AND pm9.meta_value!=''		
		AND pm10.meta_value!=''
		";
		//AND pm8.meta_value!=''
		$search_txt = $this->sanitize($search_txt);
		if($search_txt!=''){			
			$sql .=" AND ( pm1.meta_value LIKE '%%%s%%' OR pm2.meta_value LIKE '%%%s%%' OR pm7.meta_value LIKE '%%%s%%' OR CONCAT(pm1.meta_value,' ', pm2.meta_value) LIKE '%%%s%%' ) ";
		}
		
		$date_from = $this->sanitize($date_from);
		if($date_from!=''){
			$sql .=" AND pm9.meta_value>='".$date_from." 00:00:00'";
		}
		
		$date_to = $this->sanitize($date_to);
		if($date_to!=''){
			$sql .=" AND pm9.meta_value<='".$date_to." 23:59:59'";
		}
		
		$sql .='GROUP BY pm8.meta_id';	
		
		$orderby = 'pm9.meta_value DESC';
		$sql .= ' ORDER BY  '.$orderby;
		
		if($limit!=''){
			$sql .= ' LIMIT  '.$limit;
		}
		
		if($search_txt!=''){
			$sql = $wpdb->prepare($sql,$search_txt,$search_txt,$search_txt,$search_txt);
		}
		//echo $sql;
		return $this->get_data($sql);
	}
	
	//Pull QuickBooks Inventory
	public function count_qbo_inventory_list($search_txt='',$date_from='',$date_to='',$show_all_product=false){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			$ItemService = new QuickBooks_IPP_Service_Term();
			
			$whr = '';
			$search_txt = $this->sanitize($search_txt);
			if($search_txt!=''){
				$whr.=" AND Name LIKE '%$search_txt%' ";
			}
			
			$date_from = $this->sanitize($date_from);
			if($date_from!=''){
				$date_from = date('c',strtotime($date_from.' 00:00:00'));
				$whr .=" AND MetaData.CreateTime >='".$date_from."'";
			}
			
			$date_to = $this->sanitize($date_to);
			if($date_to!=''){
				$date_to = date('c',strtotime($date_to.' 23:59:59'));
				$whr .=" AND MetaData.CreateTime <='".$date_to."'";
			}
			
			$type_whr = '';
			if(!$show_all_product){
				$type_whr.=" AND Type = 'Inventory' ";
			}else{
				if((string) $show_all_product=='category'){
					$type_whr.=" AND Type = 'Category' ";
				}else{
					$type_whr.=" AND Type IN ('Inventory','Service','NonInventory','Group') ";
				}
			}
			
			$sql = "SELECT COUNT(*)  FROM Item WHERE Id > '0' $type_whr $whr ";
			//echo $sql;
			$totalCount = $ItemService->query($Context, $realm, $sql);
			return $totalCount;
		}
	}
	
	public function get_qbo_inventory_list($search_txt='',$limit='',$date_from='',$date_to='',$show_all_product=false){
		if($this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			
			$whr = '';
			$search_txt = $this->sanitize($search_txt);
			if($search_txt!=''){
				$whr.=" AND Name LIKE '%$search_txt%' ";
				//OR Not Supported
			}
			
			$date_from = $this->sanitize($date_from);
			if($date_from!=''){
				$date_from = date('c',strtotime($date_from.' 00:00:00'));
				$whr .=" AND MetaData.CreateTime >='".$date_from."'";
			}
			
			$date_to = $this->sanitize($date_to);
			if($date_to!=''){
				$date_to = date('c',strtotime($date_to.' 23:59:59'));
				$whr .=" AND MetaData.CreateTime <='".$date_to."'";
			}
			
			$type_whr = '';
			if(!$show_all_product){
				$type_whr.=" AND Type = 'Inventory' ";
			}else{				
				if((string) $show_all_product == 'category'){				
					$type_whr.=" AND Type = 'Category' ";
				}else{					
					$type_whr.=" AND Type IN ('Inventory','Service','NonInventory','Group') ";
				}				
			}
			
			$sql = "SELECT * FROM Item WHERE Id > '0' $type_whr $whr ORDER BY Id DESC $limit ";
			//echo $sql;
			
			$items = $ItemService->query($Context, $realm, $sql);
			return $items;
		}
	}
	
	public function wc_get_payment_details_by_txn_id($transaction_id='',$order_id=0){
		$order_id = (int) $order_id;
		$payment_row = array();
		$transaction_id = $this->sanitize($transaction_id);
		if($transaction_id!=''){
			global $wpdb;
			$whr = '';
			if($order_id){
				$whr.=" AND p.ID = {$order_id} ";
			}	
			
			$sql = "
			SELECT DISTINCT(p.ID) as order_id, p.post_status as order_status, p.post_date as order_date, pm1.meta_value as billing_first_name, pm2.meta_value as billing_last_name, pm3.meta_value as order_total, pm4.meta_value as order_key, pm5.meta_value as customer_user, pm6.meta_value as order_currency, 
			pm8.meta_id as payment_id, pm8.meta_value as transaction_id, pm9.meta_value as paid_date, pm10.meta_value as payment_method, pm11.meta_value as payment_method_title, pm12.meta_value as stripe_txn_fee
			FROM
			{$wpdb->prefix}posts as p
			LEFT JOIN ".$wpdb->postmeta." pm1 
			ON ( pm1.post_id = p.ID AND pm1.meta_key =  '_billing_first_name' )
			LEFT JOIN ".$wpdb->postmeta." pm2 
			ON ( pm2.post_id = p.ID AND pm2.meta_key =  '_billing_last_name' ) 
			LEFT JOIN ".$wpdb->postmeta." pm3 
			ON ( pm3.post_id = p.ID AND pm3.meta_key =  '_order_total' ) 
			LEFT JOIN ".$wpdb->postmeta." pm4 
			ON ( pm4.post_id = p.ID AND pm4.meta_key =  '_order_key' )
			LEFT JOIN ".$wpdb->postmeta." pm5 
			ON ( pm5.post_id = p.ID AND pm5.meta_key =  '_customer_user' )
			LEFT JOIN ".$wpdb->postmeta." pm6 
			ON ( pm6.post_id = p.ID AND pm6.meta_key =  '_order_currency' )
			LEFT JOIN ".$wpdb->postmeta." pm7 
			ON ( pm7.post_id = p.ID AND pm7.meta_key =  '_billing_company' )
			INNER JOIN ".$wpdb->postmeta." pm8 
			ON ( pm8.post_id = p.ID AND pm8.meta_key =  '_transaction_id' )
			INNER JOIN ".$wpdb->postmeta." pm9 
			ON ( pm9.post_id = p.ID AND pm9.meta_key =  '_paid_date' )
			INNER JOIN ".$wpdb->postmeta." pm10 
			ON ( pm10.post_id = p.ID AND pm10.meta_key =  '_payment_method' )
			INNER JOIN ".$wpdb->postmeta." pm11
			ON ( pm11.post_id = p.ID AND pm11.meta_key =  '_payment_method_title' )
			
			LEFT JOIN ".$wpdb->postmeta." pm12
			ON ( pm12.post_id = p.ID AND pm10.meta_value = 'stripe' AND pm12.meta_key =  'Stripe Fee' )
		
			WHERE
			p.post_type = 'shop_order'			
			AND pm9.meta_value!=''		
			AND pm10.meta_value!=''
			AND pm8.meta_id > 0
			AND pm8.meta_value='{$transaction_id}'
			$whr
			";			
			//AND pm8.meta_value!=''
			
			$payment_row = $this->get_row($sql);
		}
		return $payment_row;
	}
	
	public function get_custom_post_list($post_type='post',$items_per_page,$search_txt='',$orderby='post_date',$order='desc',$post_status='publish',$meta_query=array()){
		
		$offset = $this->get_offset($this->get_page_var(),$items_per_page);
		
		$args = array(
			'posts_per_page'   => $items_per_page,
			'orderby'          => $orderby,
			'order'            => $order,
			'post_type'        => $post_type,
			'post_status'      => $post_status,
			'offset'          => $offset,			
		);
		
		$search_txt = trim($search_txt);
		if($search_txt!=''){
			$args['s'] = $search_txt;
		}
		
		if(is_array($meta_query) && count($meta_query)){
			$args['meta_query'] = $meta_query;
		}
		
		$post_query_obj = new WP_Query( $args );
		$post_array = $post_query_obj->posts;
		
		$total_records = $post_query_obj->found_posts;
		wp_reset_query();
		//$this->_p($post_query_obj);
		$pagination_links = $this->get_paginate_links($total_records,$items_per_page);
		return array('post_array'=>$post_array, 'pagination_links'=>$pagination_links);
	}
	
	public function quick_refresh_qbo_customers(){		
		if($this->is_connected()){
			global $wpdb;
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$CustomerService = new QuickBooks_IPP_Service_Term();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $CustomerService->query($Context, $realm, "SELECT COUNT(*)  FROM Customer");
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			
			$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_customers';
			
			//if not truncate permission
			$wpdb->query("DELETE FROM `".$table."` WHERE `id` > 0 ");
			$wpdb->query("TRUNCATE TABLE `".$table."` ");
			
			$total_customer_added = 0;
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$customers = $CustomerService->query($Context, $realm, "SELECT * FROM Customer STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($customers && count($customers)>0){
					$qrc_val_str = '';
					foreach($customers as $Customer){
						//$this->_p($Customer);
						$customer_id = $this->qbo_clear_braces($Customer->getId());
						
						$save_data = array();
						
						$save_data['first'] = (string) $Customer->getGivenName();
						$save_data['middle'] = (string) $Customer->getMiddleName();
						$save_data['last'] = (string) $Customer->getFamilyName();
						$save_data['company'] = (string) $Customer->getCompanyName();
						$save_data['dname'] = (string) $Customer->getDisplayName();
						
						$save_data['qbo_customerid'] = $customer_id;
						
						$email = ($Customer->countPrimaryEmailAddr())?$Customer->getPrimaryEmailAddr()->getAddress():'';
						
						/*
						if($email==''){
							continue;
						}
						*/
						
						//
						$save_data['email'] = $email;
						
						/*
						if($this->get_field_by_val($table,'id','email',$email)){
							$wpdb->update($table,$save_data,array('email'=>$email),'',array('%s'));
						}else{
							//
							$save_data['email'] = $email;
							$wpdb->insert($table, $save_data);
						}
						*/
						$save_data = array_map('trim', $save_data);
						$save_data = array_map('addslashes', $save_data);
						$save_data = array_map(array($this, 'sanitize'), $save_data);
						
						//$qrc_val_str.="('".$save_data['first']."','".$save_data['middle']."','".$save_data['last']."','".$save_data['company']."','".$save_data['dname']."',".$save_data['qbo_customerid'].",'".$save_data['email']."'),";
						
						$qrc_val_str.=$wpdb->prepare("(%s,%s,%s,%s,%s,%d,%s),",$save_data['first'],$save_data['middle'],$save_data['last'],$save_data['company'],$save_data['dname'],$save_data['qbo_customerid'],$save_data['email']);
						
						//$wpdb->insert($table, $save_data);
						$total_customer_added++;
						
					}
					if($qrc_val_str!=''){
						$qrc_val_str = substr($qrc_val_str,0,-1);
						$qrc_insert_q = "INSERT INTO {$table} (".implode(", ", array_keys($save_data)).") VALUES {$qrc_val_str} ";
						//echo $qrc_insert_q;
						$wpdb->query($qrc_insert_q);
					}
				}
			}
			return $total_customer_added;
		}		
	}
	
	public function quick_refresh_qbo_products(){
		if($this->is_connected()){
			global $wpdb;
			
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Term();
			$qboMaxLimit = $this->qbo_query_limit;
			$totalCount = $ItemService->query($Context, $realm, "SELECT COUNT(*)  FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ");
			
			$batchCount =  ($qboMaxLimit >= $totalCount) ? 1 : ceil($totalCount / $qboMaxLimit);
			
			$table = $wpdb->prefix.'mw_wc_qbo_sync_qbo_items';
			
			//if not truncate permission
			$wpdb->query("DELETE FROM `".$table."` WHERE `id` > 0 ");
			$wpdb->query("TRUNCATE TABLE `".$table."` ");
			$total_product_added = 0;
			for ($i=0; $i<$batchCount; $i++) {
				$startPos = $i*$qboMaxLimit;
				$items = $ItemService->query($Context, $realm, "SELECT * FROM Item WHERE Type IN ('Inventory','Service','NonInventory','Group') ORDER BY Name ASC STARTPOSITION $startPos MaxResults $qboMaxLimit ");			
				if($items && count($items)>0){
					$qrp_val_str = '';
					foreach($items as $Item){
						if($Item->countUnitPrice()){
							//$this->_p($Item);
							$item_id = $this->qbo_clear_braces($Item->getId());
							$item_name = $Item->getName();
							$sku = ($Item->countSku())?$Item->getSku():'';
							
							$product_type = $Item->getType();
							
							$save_data = array();
							$save_data['name'] = $item_name;
							$save_data['sku'] = $sku;
							$save_data['product_type'] = $product_type;
							//
							$save_data['itemid'] = $item_id;
							
							$save_data = array_map('trim', $save_data);
							$save_data = array_map('addslashes', $save_data);
							$save_data = array_map(array($this, 'sanitize'), $save_data);
							
							/*
							if($this->get_field_by_val($table,'id','itemid',$item_id)){
								$wpdb->update($table,$save_data,array('itemid'=>$item_id),'',array('%d'));
							}else{
								$save_data['itemid'] = $item_id;
								$wpdb->insert($table, $save_data);
							}
							*/
							//$qrp_val_str.="('".$save_data['name']."','".$save_data['sku']."','".$save_data['product_type']."',".$save_data['itemid']."),";
							
							$qrp_val_str.=$wpdb->prepare("(%s,%s,%s,%d),",$save_data['name'],$save_data['sku'],$save_data['product_type'],$save_data['itemid']);
							//$wpdb->insert($table, $save_data);
							$total_product_added++;
						}						
					}
					if($qrp_val_str!=''){
						$qrp_val_str = substr($qrp_val_str,0,-1);
						$qrp_insert_q = "INSERT INTO {$table} (".implode(", ", array_keys($save_data)).") VALUES {$qrp_val_str} ";
						//echo $qrp_insert_q;
						$wpdb->query($qrp_insert_q);
					}
				}
			}
			return $total_product_added;
		}
	}
	
	//
	public function get_wc_tax_rate_dropdown($wc_tax_rates,$selected='',$skip_rate_id='',$skip_rate_class='None'){		
		$options='<option value=""></option>';
		if(is_array($wc_tax_rates) && count($wc_tax_rates)){
			foreach($wc_tax_rates as $rates){
				if($skip_rate_id!=$rates['tax_rate_id'] && $skip_rate_class!=$rates['tax_rate_class']){
					//
					$options.='<option  data-tax_rate_country="'.$rates['tax_rate_country'].'"  data-tax_rate_state="'.$rates['tax_rate_state'].'"  data-tax_rate="'.$rates['tax_rate'].'"  data-tax_rate_name="'.$rates['tax_rate_name'].'"  data-tax_rate_priority="'.$rates['tax_rate_priority'].'"  data-tax_rate_compound="'.$rates['tax_rate_compound'].'"  data-tax_rate_shipping="'.$rates['tax_rate_shipping'].'" data-tax_rate_order="'.$rates['tax_rate_order'].'" data-tax_rate_class="'.$rates['tax_rate_class'].'" value="'.$rates['tax_rate_id'].'">'.$rates['tax_rate_name'].'</option>';
				}
			}
		}
		return $options;
	}
	
	public function get_wc_tax_rate_id_array($wc_tax_rates){
		$tx_rate_arr = array();
		if(is_array($wc_tax_rates) && count($wc_tax_rates)){
			foreach($wc_tax_rates as $rates){
				$tx_rate_arr[$rates['tax_rate_id']] = $rates;
			}
		}
		return $tx_rate_arr;
	}
	
	//28-03-2017
	public function wc_get_formated_qbo_display_name($firstname,$lastname,$company,$email,$wc_customerid=0){
		$format = trim($this->get_option('mw_wc_qbo_sync_display_name_pattern'));
		if($format!=''){
			$s_arr = array('{firstname}','{lastname}','{companyname}','{email}');
			$r_arr = array($firstname,$lastname,$company,$email);
			$wc_customerid = (int) $wc_customerid;
			
			if($wc_customerid){
				$s_arr[] = '{id}';
				$r_arr[] = $wc_customerid;
			}
			$display_name = str_replace($s_arr,$r_arr,$format);
		}else{
			$display_name = $firstname." ".$lastname;
		}
		return $display_name;
	}
	
	//07-02-2017
	public function wc_get_display_name($customer_data,$guest=false){
		$display_name = '';
		$wc_customerid = 0;
		
		if($guest){
			$firstname = $this->get_array_isset($customer_data,'billing_first_name','',true);
			$lastname = $this->get_array_isset($customer_data,'billing_last_name','',true);
			$company = $this->get_array_isset($customer_data,'billing_company','',true);
			$email = $this->get_array_isset($customer_data,'billing_email','',true);
		}else{
			$firstname = $this->get_array_isset($customer_data,'firstname','',true);
			$lastname = $this->get_array_isset($customer_data,'lastname','',true);
			$company = $this->get_array_isset($customer_data,'company','',true);
			$email = $this->get_array_isset($customer_data,'email','',true);
			
			$wc_customerid = $this->get_array_isset($customer_data,'wc_customerid',0);
		}
		$shipping_company = $this->get_array_isset($customer_data,'shipping_company','',true);
		
		//$display_name = $firstname." ".$lastname;
		if($shipping_company!='' && $this->option_checked('mw_wc_qbo_sync_customer_qbo_check_ship_addr')){
			$display_name = $shipping_company;
		}else{
			$display_name = $this->wc_get_formated_qbo_display_name($firstname,$lastname,$company,$email,$wc_customerid);
		}
		
		if(trim($display_name)==''){
			$display_name = $this->get_array_isset($customer_data,'display_name','',true);
		}
		if(trim($display_name)==''){
			$display_name = $email;
		}
		return $display_name;
	}
	
	public function get_country_name_from_code($code=''){
		if($code!=''){
			 $countries_obj   = new WC_Countries();
			 $countries   = $countries_obj->__get('countries');
			 if(is_array($countries) && isset($countries[$code])){
				 return $countries[$code];
			 }
		}
		return $code;
	}
	
	//07-03-2017
	public function get_dashboard_status_items(){
		$items = array();
		global $wpdb;
		
		$quickbooks_connection = ($this->get_option('mw_wc_qbo_sync_qbo_is_connected'))?true:false;
		$initial_quickbooks_data_loaded = $this->option_checked('mw_wc_qbo_sync_qbo_is_refreshed');
		$default_setting_saved = $this->option_checked('mw_wc_qbo_sync_qbo_is_default_settings');
		$mapping_active = $this->option_checked('mw_wc_qbo_sync_qbo_is_data_mapped');
		
		$items['quickbooks_connection'] = $quickbooks_connection;
		$items['initial_quickbooks_data_loaded'] = $initial_quickbooks_data_loaded;
		$items['default_setting_saved'] = $default_setting_saved;
		$items['mapping_active'] = $mapping_active;
		
		$customer_mapped = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_customer_pairs WHERE `qbo_customerid` > 0 ");
		$items['customer_mapped'] = $customer_mapped;
		
		$product_mapped = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_product_pairs WHERE `quickbook_product_id` > 0 ");
		$items['product_mapped'] = $product_mapped;
		
		$gateway_mapped = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map WHERE `qbo_account_id` > 0 ");
		$items['gateway_mapped'] = $gateway_mapped;
		
		$tax_mapped = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_tax_map WHERE `qbo_tax_code` !='' ");
		$items['tax_mapped'] = $tax_mapped;
		
		//from log table
		$customer_synced = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_log WHERE `log_type` = 'Customer' AND `success` = 1 ");
		$items['customer_synced'] = $customer_synced;
		
		$order_synced = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_log WHERE `log_type` = 'Invoice' AND `success` = 1 ");
		$items['order_synced'] = $order_synced;
		
		$product_synced = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_log WHERE `log_type` = 'Product' AND `success` = 1 ");
		$items['product_synced'] = $product_synced;
		
		$error = (int) $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}mw_wc_qbo_sync_log WHERE `success` = 0 ");
		$items['error'] = $error;
		
		return $items;
	}
	
	//28-03-2017
	public function send_daily_email_log(){
		if(!$this->option_checked('mw_wc_qbo_sync_email_log')){
			return false;
		}
		$to = '';
		
		$w_admin_id = (int) $this->get_option('mw_wc_qbo_sync_admin_email');
		if($w_admin_id){
			$to = (string) $this->get_admin_email_by_id($w_admin_id);
		}
		
		if($to==''){return false;}
		
		$log_q = "SELECT * FROM `wp_mw_wc_qbo_sync_log` WHERE `added_date` > DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
		$log_data = $this->get_data($log_q);
		$log_email_html = '';
		
		if(is_array($log_data) && count($log_data)){
			$border_color = 'lightgrey';
			$log_email_html = '
			<h1>MyWorks WooCommerce Sync for QuickBooks Online</h1>
			<p>Last 24 hours log list ('.$this->now().')</p>		
			<table style="width:100%;border-top:1px solid '.$border_color.';border-right:1px solid '.$border_color.';font-size:14px;" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%" align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">#</td>
				<td width="10%" align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">Type</td>
				<td width="25%" align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">Title</td>
				<td width="44%" align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">Message</td>
				<td width="16%" align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">Date</td>
			</tr>
			';
			foreach($log_data as $ld){
				$log_color = ($ld['success']==0)?'color:red;':'';
				$log_email_html.='
				<tr>
					<td align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">'.$ld['id'].'</td>
					<td align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">'.$ld['log_type'].'</td>
					<td align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;'.$log_color.'">'.$ld['log_title'].'</td>
					<td align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;'.$log_color.'">'.$ld['details'].'</td>
					<td align="left" style="border-bottom:1px solid '.$border_color.';border-left:1px solid '.$border_color.';padding:2px;">'.$ld['added_date'].'</td>
				</tr>
				';
			}
			$log_email_html.='</table>';
			
			$headers = array(
				'MIME-Version: 1.0',
				'Content-type:text/html;charset=UTF-8',
			);
			//echo $log_email_html;return;
			wp_mail($to, 'Daily Email Log', $log_email_html, $headers);
		}
	}
	
	//31-03-2017
	public function get_current_request_protocol(){
		if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO'])){
			 return $_SERVER['HTTP_X_FORWARDED_PROTO'];
		}
		return !empty($_SERVER['HTTPS']) ? "https" : "http";
	}
	
	public function get_sync_window_url(){
		//$this->_p($_SERVER);
		$request_protocol = $this->get_current_request_protocol();
		
		$current_url = $request_protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
		$sync_window_url = site_url('index.php?mw_qbo_sync_public_sync_window=1');
		
		if(strpos($current_url, 's://')===false){
			$sync_window_url = str_replace('s://','://',$sync_window_url);
		}else{	
			if(strpos($sync_window_url, 's://')===false){
				$sync_window_url = str_replace('://','s://',$sync_window_url);
			}
		}
		
		if(strpos($current_url, '://www.')===false){
			$sync_window_url = str_replace('://www.','://',$sync_window_url);
		}else{	
			if(strpos($sync_window_url, '://www.')===false){
				$sync_window_url = str_replace('://','://www.',$sync_window_url);
			}
		}
		
		return $sync_window_url;
	}
	
	//10-04-2017
	public function get_order_currency_list(){
		global $wpdb;
		
		$gc_length = 2048;		
		$wpdb->query("SET group_concat_max_len = {$gc_length}");
		
		$cur_list = $this->get_var("SELECT GROUP_CONCAT(DISTINCT(`meta_value`)) AS currency_list FROM {$wpdb->postmeta} WHERE `meta_key` = '_order_currency' AND `meta_key` != '' ");
		if($cur_list==''){
			$cur_list = get_woocommerce_currency();
		}
		
		if(is_array($cur_list)){
			return $cur_list;
		}
		
		if($cur_list!=''){
			return explode(',',$cur_list);
		}
	}
	
	public function get_world_currency_list($symbol=false){
		if($symbol){
			return array(
				'AED' => '&#1583;.&#1573;', // ?
				'AFN' => '&#65;&#102;',
				'ALL' => '&#76;&#101;&#107;',
				'AMD' => 'AMD',
				'ANG' => '&#402;',
				'AOA' => '&#75;&#122;', // ?
				'ARS' => '&#36;',
				'AUD' => '&#36;',
				'AWG' => '&#402;',
				'AZN' => '&#1084;&#1072;&#1085;',
				'BAM' => '&#75;&#77;',
				'BBD' => '&#36;',
				'BDT' => '&#2547;', // ?
				'BGN' => '&#1083;&#1074;',
				'BHD' => '.&#1583;.&#1576;', // ?
				'BIF' => '&#70;&#66;&#117;', // ?
				'BMD' => '&#36;',
				'BND' => '&#36;',
				'BOB' => '&#36;&#98;',
				'BRL' => '&#82;&#36;',
				'BSD' => '&#36;',
				'BTN' => '&#78;&#117;&#46;', // ?
				'BWP' => '&#80;',
				'BYR' => '&#112;&#46;',
				'BZD' => '&#66;&#90;&#36;',
				'CAD' => '&#36;',
				'CDF' => '&#70;&#67;',
				'CHF' => '&#67;&#72;&#70;',
				'CLF' => 'CLF', // ?
				'CLP' => '&#36;',
				'CNY' => '&#165;',
				'COP' => '&#36;',
				'CRC' => '&#8353;',
				'CUP' => '&#8396;',
				'CVE' => '&#36;', // ?
				'CZK' => '&#75;&#269;',
				'DJF' => '&#70;&#100;&#106;', // ?
				'DKK' => '&#107;&#114;',
				'DOP' => '&#82;&#68;&#36;',
				'DZD' => '&#1583;&#1580;', // ?
				'EGP' => '&#163;',
				'ETB' => '&#66;&#114;',
				'EUR' => '&#8364;',
				'FJD' => '&#36;',
				'FKP' => '&#163;',
				'GBP' => '&#163;',
				'GEL' => '&#4314;', // ?
				'GHS' => '&#162;',
				'GIP' => '&#163;',
				'GMD' => '&#68;', // ?
				'GNF' => '&#70;&#71;', // ?
				'GTQ' => '&#81;',
				'GYD' => '&#36;',
				'HKD' => '&#36;',
				'HNL' => '&#76;',
				'HRK' => '&#107;&#110;',
				'HTG' => '&#71;', // ?
				'HUF' => '&#70;&#116;',
				'IDR' => '&#82;&#112;',
				'ILS' => '&#8362;',
				'INR' => '&#8377;',
				'IQD' => '&#1593;.&#1583;', // ?
				'IRR' => '&#65020;',
				'ISK' => '&#107;&#114;',
				'JEP' => '&#163;',
				'JMD' => '&#74;&#36;',
				'JOD' => '&#74;&#68;', // ?
				'JPY' => '&#165;',
				'KES' => '&#75;&#83;&#104;', // ?
				'KGS' => '&#1083;&#1074;',
				'KHR' => '&#6107;',
				'KMF' => '&#67;&#70;', // ?
				'KPW' => '&#8361;',
				'KRW' => '&#8361;',
				'KWD' => '&#1583;.&#1603;', // ?
				'KYD' => '&#36;',
				'KZT' => '&#1083;&#1074;',
				'LAK' => '&#8365;',
				'LBP' => '&#163;',
				'LKR' => '&#8360;',
				'LRD' => '&#36;',
				'LSL' => '&#76;', // ?
				'LTL' => '&#76;&#116;',
				'LVL' => '&#76;&#115;',
				'LYD' => '&#1604;.&#1583;', // ?
				'MAD' => '&#1583;.&#1605;.', //?
				'MDL' => '&#76;',
				'MGA' => '&#65;&#114;', // ?
				'MKD' => '&#1076;&#1077;&#1085;',
				'MMK' => '&#75;',
				'MNT' => '&#8366;',
				'MOP' => '&#77;&#79;&#80;&#36;', // ?
				'MRO' => '&#85;&#77;', // ?
				'MUR' => '&#8360;', // ?
				'MVR' => '.&#1923;', // ?
				'MWK' => '&#77;&#75;',
				'MXN' => '&#36;',
				'MYR' => '&#82;&#77;',
				'MZN' => '&#77;&#84;',
				'NAD' => '&#36;',
				'NGN' => '&#8358;',
				'NIO' => '&#67;&#36;',
				'NOK' => '&#107;&#114;',
				'NPR' => '&#8360;',
				'NZD' => '&#36;',
				'OMR' => '&#65020;',
				'PAB' => '&#66;&#47;&#46;',
				'PEN' => '&#83;&#47;&#46;',
				'PGK' => '&#75;', // ?
				'PHP' => '&#8369;',
				'PKR' => '&#8360;',
				'PLN' => '&#122;&#322;',
				'PYG' => '&#71;&#115;',
				'QAR' => '&#65020;',
				'RON' => '&#108;&#101;&#105;',
				'RSD' => '&#1044;&#1080;&#1085;&#46;',
				'RUB' => '&#1088;&#1091;&#1073;',
				'RWF' => '&#1585;.&#1587;',
				'SAR' => '&#65020;',
				'SBD' => '&#36;',
				'SCR' => '&#8360;',
				'SDG' => '&#163;', // ?
				'SEK' => '&#107;&#114;',
				'SGD' => '&#36;',
				'SHP' => '&#163;',
				'SLL' => '&#76;&#101;', // ?
				'SOS' => '&#83;',
				'SRD' => '&#36;',
				'STD' => '&#68;&#98;', // ?
				'SVC' => '&#36;',
				'SYP' => '&#163;',
				'SZL' => '&#76;', // ?
				'THB' => '&#3647;',
				'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
				'TMT' => '&#109;',
				'TND' => '&#1583;.&#1578;',
				'TOP' => '&#84;&#36;',
				'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
				'TTD' => '&#36;',
				'TWD' => '&#78;&#84;&#36;',
				'TZS' => 'TZS',
				'UAH' => '&#8372;',
				'UGX' => '&#85;&#83;&#104;',
				'USD' => '&#36;',
				'UYU' => '&#36;&#85;',
				'UZS' => '&#1083;&#1074;',
				'VEF' => '&#66;&#115;',
				'VND' => '&#8363;',
				'VUV' => '&#86;&#84;',
				'WST' => '&#87;&#83;&#36;',
				'XAF' => '&#70;&#67;&#70;&#65;',
				'XCD' => '&#36;',
				'XDR' => 'XDR',
				'XOF' => 'XOF',
				'XPF' => '&#70;',
				'YER' => '&#65020;',
				'ZAR' => '&#82;',
				'ZMK' => '&#90;&#75;', // ?
				'ZWL' => '&#90;&#36;',
			);
		}
		$cur_arr = array (
			'ALL' => 'Albania Lek',
			'AFN' => 'Afghanistan Afghani',
			'ARS' => 'Argentina Peso',
			'AWG' => 'Aruba Guilder',
			'AUD' => 'Australia Dollar',
			'AZN' => 'Azerbaijan New Manat',
			'BSD' => 'Bahamas Dollar',
			'BBD' => 'Barbados Dollar',
			'BDT' => 'Bangladeshi taka',
			'BYR' => 'Belarus Ruble',
			'BZD' => 'Belize Dollar',
			'BMD' => 'Bermuda Dollar',
			'BOB' => 'Bolivia Boliviano',
			'BAM' => 'Bosnia and Herzegovina Convertible Marka',
			'BWP' => 'Botswana Pula',
			'BGN' => 'Bulgaria Lev',
			'BRL' => 'Brazil Real',
			'BND' => 'Brunei Darussalam Dollar',
			'KHR' => 'Cambodia Riel',
			'CAD' => 'Canada Dollar',
			'KYD' => 'Cayman Islands Dollar',
			'CLP' => 'Chile Peso',
			'CNY' => 'China Yuan Renminbi',
			'COP' => 'Colombia Peso',
			'CRC' => 'Costa Rica Colon',
			'HRK' => 'Croatia Kuna',
			'CUP' => 'Cuba Peso',
			'CZK' => 'Czech Republic Koruna',
			'DKK' => 'Denmark Krone',
			'DOP' => 'Dominican Republic Peso',
			'XCD' => 'East Caribbean Dollar',
			'EGP' => 'Egypt Pound',
			'SVC' => 'El Salvador Colon',
			'EEK' => 'Estonia Kroon',
			'EUR' => 'Euro Member Countries',
			'FKP' => 'Falkland Islands (Malvinas) Pound',
			'FJD' => 'Fiji Dollar',
			'GHC' => 'Ghana Cedis',
			'GIP' => 'Gibraltar Pound',
			'GTQ' => 'Guatemala Quetzal',
			'GGP' => 'Guernsey Pound',
			'GYD' => 'Guyana Dollar',
			'HNL' => 'Honduras Lempira',
			'HKD' => 'Hong Kong Dollar',
			'HUF' => 'Hungary Forint',
			'ISK' => 'Iceland Krona',
			'INR' => 'India Rupee',
			'IDR' => 'Indonesia Rupiah',
			'IRR' => 'Iran Rial',
			'IMP' => 'Isle of Man Pound',
			'ILS' => 'Israel Shekel',
			'JMD' => 'Jamaica Dollar',
			'JPY' => 'Japan Yen',
			'JEP' => 'Jersey Pound',
			'KZT' => 'Kazakhstan Tenge',
			'KPW' => 'Korea (North) Won',
			'KRW' => 'Korea (South) Won',
			'KGS' => 'Kyrgyzstan Som',
			'LAK' => 'Laos Kip',
			'LVL' => 'Latvia Lat',
			'LBP' => 'Lebanon Pound',
			'LRD' => 'Liberia Dollar',
			'LTL' => 'Lithuania Litas',
			'MKD' => 'Macedonia Denar',
			'MYR' => 'Malaysia Ringgit',
			'MUR' => 'Mauritius Rupee',
			'MXN' => 'Mexico Peso',
			'MNT' => 'Mongolia Tughrik',
			'MZN' => 'Mozambique Metical',
			'NAD' => 'Namibia Dollar',
			'NPR' => 'Nepal Rupee',
			'ANG' => 'Netherlands Antilles Guilder',
			'NZD' => 'New Zealand Dollar',
			'NIO' => 'Nicaragua Cordoba',
			'NGN' => 'Nigeria Naira',
			'NOK' => 'Norway Krone',
			'OMR' => 'Oman Rial',
			'PKR' => 'Pakistan Rupee',
			'PAB' => 'Panama Balboa',
			'PYG' => 'Paraguay Guarani',
			'PEN' => 'Peru Nuevo Sol',
			'PHP' => 'Philippines Peso',
			'PLN' => 'Poland Zloty',
			'QAR' => 'Qatar Riyal',
			'RON' => 'Romania New Leu',
			'RUB' => 'Russia Ruble',
			'SHP' => 'Saint Helena Pound',
			'SAR' => 'Saudi Arabia Riyal',
			'RSD' => 'Serbia Dinar',
			'SCR' => 'Seychelles Rupee',
			'SGD' => 'Singapore Dollar',
			'SBD' => 'Solomon Islands Dollar',
			'SOS' => 'Somalia Shilling',
			'ZAR' => 'South Africa Rand',
			'LKR' => 'Sri Lanka Rupee',
			'SEK' => 'Sweden Krona',
			'CHF' => 'Switzerland Franc',
			'SRD' => 'Suriname Dollar',
			'SYP' => 'Syria Pound',
			'TWD' => 'Taiwan New Dollar',
			'THB' => 'Thailand Baht',
			'TTD' => 'Trinidad and Tobago Dollar',
			'TRY' => 'Turkey Lira',
			'TRL' => 'Turkey Lira',
			'TVD' => 'Tuvalu Dollar',
			'UAH' => 'Ukraine Hryvna',
			'GBP' => 'United Kingdom Pound',
			'UGX' => 'Uganda Shilling',
			'USD' => 'United States Dollar',
			'UYU' => 'Uruguay Peso',
			'UZS' => 'Uzbekistan Som',
			'VEF' => 'Venezuela Bolivar',
			'VND' => 'Viet Nam Dong',
			'YER' => 'Yemen Rial',
			'ZWD' => 'Zimbabwe Dollar'
		);
		if($symbol=='name'){
			return $cur_arr;
		}
		return array_combine(array_keys($cur_arr),array_keys($cur_arr));
	}
	
	//18-05-2017
	public function get_compt_checkout_fields($s_f_name = ''){
		global $wpdb;
		$s_whr = '';
		if($s_f_name!=''){
			$s_whr = " AND pm1.meta_value = %s ";
		}
		
		$sql="
		SELECT p.ID, pm1.meta_value as cf_label, pm2.meta_value as cf_key
		FROM `{$wpdb->posts}` p
		LEFT JOIN `{$wpdb->postmeta}` pm1
		ON(p.ID = pm1.post_id AND pm1.meta_key='label')
		
		LEFT JOIN `{$wpdb->postmeta}` pm2
		ON(p.ID = pm2.post_id AND pm2.meta_key='key')
		
		WHERE p.`post_type` = 'wccf_checkout_field'
		{$s_whr}
		AND p.post_status NOT IN('trash','auto-draft','inherit')
		";
		
		if($s_f_name!=''){
			$sql.=' LIMIT 0,1 ';
			$sql = $wpdb->prepare($sql,$s_f_name);
		}
		
		//echo $sql;
		if($s_f_name!=''){
			return $this->get_row($sql);
		}
		return $this->get_data($sql);
	}
	
	public function get_wc_fee_qbo_product($dfn='',$efd=''){	
		$fee_qp = 0;
		$isdf = false;
		if($this->is_plugin_active('woocommerce-gateways-discounts-and-fees') && $this->option_checked('mw_wc_qbo_sync_compt_gf_qbo_is')){
			$isdf = true;
		}
		
		if($this->is_plugin_active('woocommerce-additional-fees','woocommerce_additional_fees_plugin') && $this->option_checked('mw_wc_qbo_sync_compt_gf_qbo_is_gbf')){
			$isdf = true;
		}
		if($isdf){
			$fee_qp = (int) $this->get_option('mw_wc_qbo_sync_compt_gf_qbo_item');
		}
		if($this->is_plugin_active('woocommerce-custom-fields') && $this->option_checked('mw_wc_qbo_sync_compt_wccf_fee')){
			$fee_qp = 0;
			if($dfn!=''){
				$ccf_data = $this->get_compt_checkout_fields($dfn);
				if(is_array($ccf_data) && count($ccf_data)){
					$ccf_id = (int) $ccf_data['ID'];
					if($ccf_id){
						$mw_wc_qbo_sync_compt_wccf_fee_wf_qi_map = $this->get_option('mw_wc_qbo_sync_compt_wccf_fee_wf_qi_map');
						if($mw_wc_qbo_sync_compt_wccf_fee_wf_qi_map!=''){
							$ccf_map_arr = unserialize($mw_wc_qbo_sync_compt_wccf_fee_wf_qi_map);
							if(is_array($ccf_map_arr) && count($ccf_map_arr)){
								if(isset($ccf_map_arr[$ccf_id]) && (int) $ccf_map_arr[$ccf_id]){
									$fee_qp = (int) $ccf_map_arr[$ccf_id];
								}
							}
						}
					}					
				}				
			}
			
		}
		
		if(!$fee_qp){
			$fee_qp = (int) $this->get_option('mw_wc_qbo_sync_default_qbo_item');
		}
		return $fee_qp;
	}
	
	public function get_wc_fee_plugin_check(){		
		$enabled = false;
		if($this->is_plugin_active('woocommerce-gateways-discounts-and-fees') && $this->option_checked('mw_wc_qbo_sync_compt_gf_qbo_is')){
			$enabled = true;
		}
		
		if($this->is_plugin_active('woocommerce-additional-fees','woocommerce_additional_fees_plugin') && $this->option_checked('mw_wc_qbo_sync_compt_gf_qbo_is_gbf')){
			$enabled = true;
		}
		
		if($this->is_plugin_active('woocommerce-custom-fields') && $this->option_checked('mw_wc_qbo_sync_compt_wccf_fee')){
			$enabled = true;
		}
		return $enabled;
	}
	
	public function is_plugin_active($plugin='',$diff_filename=''){
		$active = false;
		$plugin = trim($plugin);
		$diff_filename = trim($diff_filename);
		$plugin_file = ($diff_filename!='')?$diff_filename:$plugin;
		
		//13-06-2017
		$compt_addon_active = false;
		if($plugin!=''){
			//require_once(ABSPATH.'wp-admin/includes/plugin.php');
			if(function_exists('is_plugin_active')){
				if( is_plugin_active( $plugin.'/'.$plugin_file.'.php' ) ) {
					$active = true;
				}
				if( is_plugin_active( 'compatibility-settings-for-myworks-qbo-sync/compatibility-settings-for-myworks-qbo-sync.php' ) ) {
					$compt_addon_active = true;
				}
			}else{
				$active = in_array( $plugin, (array) get_option( 'active_plugins', array() ) );
				$compt_addon_active = in_array( 'compatibility-settings-for-myworks-qbo-sync', (array) get_option( 'active_plugins', array() ) );
			}		
		}
		if($plugin=='woocommerce-measurement-price-calculator' && !class_exists('WC_Measurement_Price_Calculator')){
			$active = false;
		}
		//13-06-2017
		$compt_p_arr = array();
		$compt_p_arr[] = 'woocommerce-measurement-price-calculator';
		$compt_p_arr[] = 'woocommerce-product-bundles';
		$compt_p_arr[] = 'woocommerce-gateways-discounts-and-fees';
		$compt_p_arr[] = 'woocommerce-additional-fees';
		$compt_p_arr[] = 'woocommerce-order-delivery';
		$compt_p_arr[] = 'woocommerce-sequential-order-numbers-pro';
		$compt_p_arr[] = 'woocommerce-custom-fields';
		$compt_p_arr[] = 'woocommerce-hear-about-us';
		$compt_p_arr[] = 'woocommerce-admin-custom-order-fields';
		if(in_array($plugin,$compt_p_arr) && !$compt_addon_active){
			$active = false;
		}
		
		return $active;
	}
	
	//26-05-2017
	public function get_qbo_group_product_details($p_id){
		$return = array();
		$p_id = (int) $p_id;
		if($p_id && $this->is_connected()){
			$Context = $this->Context;
			$realm = $this->realm;
			
			$ItemService = new QuickBooks_IPP_Service_Item();
			$sql = "SELECT * FROM Item WHERE Id = '{$p_id}' AND Type = 'Group' ";
			$items = $ItemService->query($Context, $realm, $sql);
			if($items && count($items)){
				$item = $items[0];
				$return['Id'] = $this->qbo_clear_braces($item->getId());
				$return['Name'] = $item->getName();
				$return['Sku'] = $item->getSku();
				
				$buldle_items = array();
				$b_tp = 0;
				
				if($item->countItemGroupDetail()){
					$line_count = $item->getItemGroupDetail()->countItemGroupLine();
					
					$gi_id_arr = array();
					$gi_tp_arr = array();
					
					for($i=0;$i<$line_count;$i++){
						$GI = $item->getItemGroupDetail()->getItemGroupLine($i);
						$gi_id = (int) $this->qbo_clear_braces($GI->getItemRef());
						if($gi_id){
							$gi_id_arr[] = $gi_id;
							$gi_tp_arr[] = $GI->getItemRef_type();
						}
					}
					
					$gi_price_arr = array();
					if(count($gi_id_arr) && count($gi_tp_arr)){
						$gi_tp_arr = array_unique($gi_tp_arr);						
						$gi_tp_str = implode("','", $gi_tp_arr);
						$gi_tp_str = "'".$gi_tp_str."'";						
						
						$gi_id_str = implode("','", $gi_id_arr);
						$gi_id_str = "'".$gi_id_str."'";
						
						$sql = "SELECT Id,UnitPrice,Type  FROM Item WHERE Id IN({$gi_id_str}) AND Type IN({$gi_tp_str}) ";
						$g_items = $ItemService->query($Context, $realm, $sql);
						//$this->_p($g_items);
						if($g_items && count($g_items)){
							foreach($g_items as $gi){
								if($gi->countUnitPrice()){
									$gi_price_arr[(int) $this->qbo_clear_braces($gi->getId())] = $gi->getUnitPrice();
								}
							}
						}
					}
					
					//$this->_p($gi_price_arr);
					
					for($i=0;$i<$line_count;$i++){
						$GI = $item->getItemGroupDetail()->getItemGroupLine($i);
						$gi_id = (int) $this->qbo_clear_braces($GI->getItemRef());
						if($gi_id){
							$tbi = array();
							$tbi['ItemRef'] = $gi_id;
							
							$up = (isset($gi_price_arr[$gi_id]))?$gi_price_arr[$gi_id]:0;
							$tbi['UnitPrice'] = $up;
							$b_tp+=($up*$GI->getQty());
							
							$tbi['Qty'] = $GI->getQty();							
							$tbi['ItemRef_name'] = $GI->getItemRef_name();
							$tbi['ItemRef_type'] = $GI->getItemRef_type();
							$buldle_items[] = $tbi;
						}
					}
					
				}

				$return['buldle_items'] = $buldle_items;
				$return['b_tp'] = $b_tp;
			}
		}
		return $return;
	}
	
	public function get_woo_version_number(){
		// If get_plugins() isn't available, require it
		if ( ! function_exists( 'get_plugins' ) )
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		
			// Create the plugins folder and file variables
		$plugin_folder = get_plugins( '/' . 'woocommerce' );
		$plugin_file = 'woocommerce.php';
		
		// If the plugin version number is set, return it 
		if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
			return $plugin_folder[$plugin_file]['Version'];

		} else {
		// Otherwise return null
			return NULL;
		}
	}
}
/*Class End*/