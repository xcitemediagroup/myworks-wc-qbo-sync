<?php

/**
 * Fired during plugin activation
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 * @author     My Works <support@myworks.design>
 */
class MyWorks_WC_QBO_Sync_Oth_Funcs {	
	
	/**
	 * Method description.
	 *
	 * @since    1.0.0
	 */
	protected $quickbooks_connection_dashboard_url='http://myworks.design/account';
	protected $is_valid_license = false;
	protected $plugin_license_status = '';
	
	public function __construct(){
		//Not using for now.		
	}
	
	public function is_valid_license($licensekey,$localkey=""){
		if(!$this->is_valid_license){
			$license_data = $this->myworks_wc_qbo_sync_check_license($licensekey,$localkey);
			$this->plugin_license_status = (isset($license_data['status']))?$license_data['status']:'';
			if(isset($license_data['status']) && $license_data['status']=='Active' && !isset($license_data['trial_expired'])){
				$this->is_valid_license = true;
			}else{
				if(isset($license_data['trial_expired'])){
					$this->plugin_license_status = 'Invalid';
				}				
			}
		}
		return $this->is_valid_license;
	}
	
	public function get_license_status(){
		return $this->plugin_license_status;
	}
	
	protected function myworks_wc_qbo_sync_check_license($licensekey,$localkey="") {	
		
		$licensing_secret_key = 'QM8S20LSKJ03H3J'; #ALL
		
			
		// Enter the url to your WHMCS installation here
		$whmcsurl = $this->quickbooks_connection_dashboard_url.'/';
		// Must match what is specified in the MD5 Hash Verification field
		// of the licensing product that will be used with this check.  
		
		// The number of days to wait between performing remote license checks
		$localkeydays = 5;
		// The number of days to allow failover for after local key expiry
		$allowcheckfaildays = 5;

		// -----------------------------------
		//  -- Do not edit below this line --
		// -----------------------------------
		
		
		$check_token = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
		$checkdate = date("Ymd");
		$domain = $_SERVER['SERVER_NAME'];
	   //$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
	    $usersip = $this->get_plugin_ip();
		
		$dirpath = dirname(__FILE__);
		$verifyfilepath = 'modules/servers/licensing/verify.php';
		$localkeyvalid = false;
		if ($localkey) {
			$localkey = str_replace("\n", '', $localkey); # Remove the line breaks
			$localdata = substr($localkey, 0, strlen($localkey) - 32); # Extract License Data
			$md5hash = substr($localkey, strlen($localkey) - 32); # Extract MD5 Hash
			if ($md5hash == md5($localdata . $licensing_secret_key)) {
				$localdata = strrev($localdata); # Reverse the string
				$md5hash = substr($localdata, 0, 32); # Extract MD5 Hash
				$localdata = substr($localdata, 32); # Extract License Data
				$localdata = base64_decode($localdata);
				$localkeyresults = unserialize($localdata);
				$originalcheckdate = $localkeyresults['checkdate'];
				if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
					$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $localkeydays, date("Y")));
					if ($originalcheckdate > $localexpiry) {
						$localkeyvalid = true;
						$results = $localkeyresults;
						if(is_array($results)){
							if(isset($results['validdomain'])){
								$validdomains = explode(',', $results['validdomain']);
								if (!in_array($_SERVER['SERVER_NAME'], $validdomains)) {
									$localkeyvalid = false;
									$localkeyresults['status'] = "Invalid";
									$results = array();
								}
							}
							
							if(isset($results['validip'])){
								$validips = explode(',', $results['validip']);
								if (!in_array($usersip, $validips)) {
									$localkeyvalid = false;
									$localkeyresults['status'] = "Invalid";
									$results = array();
								}
							}
							
							if(isset($results['validdirectory'])){
								$validdirs = explode(',', $results['validdirectory']);
								if (!in_array($dirpath, $validdirs)) {
									$localkeyvalid = false;
									$localkeyresults['status'] = "Invalid";
									$results = array();
								}
							}							
						}						
					}
				}
			}
		}
		
		if (!$localkeyvalid) {
			$responseCode = 0;
			$postfields = array(
				'licensekey' => $licensekey,
				'domain' => $domain,
				'ip' => $usersip,
				'dir' => $dirpath,
			);
			if ($check_token) $postfields['check_token'] = $check_token;
			$query_string = '';
			foreach ($postfields AS $k=>$v) {
				$query_string .= $k.'='.urlencode($v).'&';
			}
			if (function_exists('curl_exec')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $whmcsurl . $verifyfilepath);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
			} else {
				$responseCodePattern = '/^HTTP\/\d+\.\d+\s+(\d+)/';
				$fp = @fsockopen($whmcsurl, 80, $errno, $errstr, 5);
				if ($fp) {
					$newlinefeed = "\r\n";
					$header = "POST ".$whmcsurl . $verifyfilepath . " HTTP/1.0" . $newlinefeed;
					$header .= "Host: ".$whmcsurl . $newlinefeed;
					$header .= "Content-type: application/x-www-form-urlencoded" . $newlinefeed;
					$header .= "Content-length: ".@strlen($query_string) . $newlinefeed;
					$header .= "Connection: close" . $newlinefeed . $newlinefeed;
					$header .= $query_string;
					$data = $line = '';
					@stream_set_timeout($fp, 20);
					@fputs($fp, $header);
					$status = @socket_get_status($fp);
					while (!@feof($fp)&&$status) {
						$line = @fgets($fp, 1024);
						$patternMatches = array();
						if (!$responseCode
							&& preg_match($responseCodePattern, trim($line), $patternMatches)
						) {
							$responseCode = (empty($patternMatches[1])) ? 0 : $patternMatches[1];
						}
						$data .= $line;
						$status = @socket_get_status($fp);
					}
					@fclose ($fp);
				}
			}
			if ($responseCode != 200) {
				$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - ($localkeydays + $allowcheckfaildays), date("Y")));
				if (isset($originalcheckdate) && $originalcheckdate > $localexpiry) {
					$results = $localkeyresults;
				} else {
					$results = array();
					$results['status'] = "Invalid";
					$results['description'] = "Remote Check Failed";
					return $results;
				}
			} else {
				preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
				$results = array();
				foreach ($matches[1] AS $k=>$v) {
					$results[$v] = $matches[2][$k];
				}
			}
			if (!is_array($results)) {
				die("Invalid License Server Response");
			}
			if (isset($results['md5hash'])) {
				if ($results['md5hash'] != md5($licensing_secret_key . $check_token)) {
					$results['status'] = "Invalid";
					$results['description'] = "MD5 Checksum Verification Failed";
					return $results;
				}
			}
			if ($results['status'] == "Active") {
				$results['checkdate'] = $checkdate;
				$data_encoded = serialize($results);
				$data_encoded = base64_encode($data_encoded);
				$data_encoded = md5($checkdate . $licensing_secret_key) . $data_encoded;
				$data_encoded = strrev($data_encoded);
				$data_encoded = $data_encoded . md5($data_encoded . $licensing_secret_key);
				$data_encoded = wordwrap($data_encoded, 80, "\n", true);
				$results['localkey'] = $data_encoded;
			}
			$results['remotecheck'] = true;
		}
		//$this->_p($results);
		
		//
		if($licensekey!=''){
			update_option('mw_wc_qbo_sync_license',$licensekey);
		}
		if ($results["status"]=="Active" && isset($results["localkey"]) && $results["localkey"]!='') {			
			update_option('mw_wc_qbo_sync_localkey',$results["localkey"]);
		}
		if ($results["status"]=="Active"){			
			//24-03-2017
			$productname = $results["productname"];
			if(strpos($productname,'Free Trial')!==false){
				if(strpos($productname,'30 Day')!==false){
					$trialdaysleft = (int) 30-((strtotime(date("Y-m-d")) - strtotime($results["regdate"]))/86400);
				}else{
					$trialdaysleft = (int) 14-((strtotime(date("Y-m-d")) - strtotime($results["regdate"]))/86400);
				}								
				if($trialdaysleft<0){
					//					
					delete_option('mw_wc_qbo_sync_localkey');
					delete_option('mw_wc_qbo_sync_access_token');
					$results['trial_expired'] = true;
					$trialdaysleft = 0;
				}
				$serviceid = $results["serviceid"];
				update_option('mw_wc_qbo_sync_trial_license','true');
				update_option('mw_wc_qbo_sync_trial_days_left',$trialdaysleft);
				update_option('mw_wc_qbo_sync_trial_license_serviceid',$serviceid);
			}else{
				delete_option('mw_wc_qbo_sync_trial_license');
				delete_option('mw_wc_qbo_sync_trial_days_left');
				delete_option('mw_wc_qbo_sync_trial_license_serviceid');
			}		
		}
		unset($postfields,$data,$matches,$whmcsurl,$licensing_secret_key,$checkdate,$usersip,$localkeydays,$allowcheckfaildays,$md5hash);
		return $results;
	}
	
	public function get_connection_iframe_extra_params(){
		$extra_param = '';
		$dirpath = $this->get_plugin_connection_dir();
		$usersip = $this->get_plugin_ip();		
		$dirpath = base64_encode($dirpath);
		$extra_param.='&wc_qbo_plugin_dirpath='.$dirpath;
		$usersip = base64_encode($usersip);
		$extra_param.='&wc_qbo_plugin_usersip='.$usersip;
		return $extra_param;
	}
	
	public function get_plugin_domain(){
		$domain = $_SERVER['SERVER_NAME'];
		return $domain;
	}
	
	public function get_plugin_ip(){
		$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		if(empty($usersip) && isset($_SERVER['SERVER_NAME'])){
			$usersip = gethostbyname($_SERVER['SERVER_NAME']);
		}		
		return $usersip;
	}
	
	public function get_plugin_connection_dir(){
		$dirpath = dirname(__FILE__);
		return $dirpath;
	}
	
	public function myworks_wc_qbo_sync_set_session_msg($key='',$msg=''){
		if(!isset($_SESSION['myworks_wc_qbo_sync_session_msg'])){
			$_SESSION['myworks_wc_qbo_sync_session_msg'] = array();     
		}

		$_SESSION['myworks_wc_qbo_sync_session_msg'][$key] = $msg;        
	}

	public function myworks_wc_qbo_sync_show_session_msg($key='',$div_class=""){
		if(isset($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){
			if(!empty($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){            
			echo '<div class="myworks_wc_qbo_sync_session_msg_div '.$div_class.'">';
			if(is_array($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){
				echo implode('<br />', $_SESSION['myworks_wc_qbo_sync_session_msg'][$key]);
			}
			else{
				echo $_SESSION['myworks_wc_qbo_sync_session_msg'][$key];
			}
			echo '</div>';
			}

			unset($_SESSION['myworks_wc_qbo_sync_session_msg'][$key]);
		}
	}


	public function myworks_wc_qbo_sync_get_session_msg($key='',$div_class="",$unset=true){
		$return="";
		if(isset($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){
			if(!empty($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){
				$return.='<div class="myworks_wc_qbo_sync_session_msg_div '.$div_class.'">';
				if(is_array($_SESSION['myworks_wc_qbo_sync_session_msg'][$key])){
					$return.= implode('<br />', $_SESSION['myworks_wc_qbo_sync_session_msg'][$key]);
				}
				else{
					$return.= $_SESSION['myworks_wc_qbo_sync_session_msg'][$key];
				}
				$return.= '</div>';
			}
			if($unset){
			   unset($_SESSION['myworks_wc_qbo_sync_session_msg'][$key]); 
			}
		}
		return $return;    
	}
	
	public function myworks_wc_qbo_sync_now(){
		return date('Y-m-d H:i:s');
	}
	
	public function var_p($key=''){
		if($key!=''){
			if(isset($_POST[$key])){
				if(!is_array($_POST[$key])){
					return trim($_POST[$key]);
				}
				else{
					return $_POST[$key];
				}
			}
		}
	}

	public function var_g($key=''){
		if($key!=''){
			if(isset($_GET[$key])){
				return trim($_GET[$key]);
			}
		}
	}
	
	public function _p($item=''){
		if(is_array($item) || is_object($item)){
			echo '<pre>'; print_r($item); echo '</pre>';
		}
		else{
			echo $item;
		}
	}
	
	public function get_select2_js($item='select',$d_item=''){
		if(get_option('mw_wc_qbo_sync_select2_status')!='true'){
			return '';
		}
		
		$is_ajax_dd = 0;
		if(get_option('mw_wc_qbo_sync_select2_ajax')=='true'){
			$is_ajax_dd = 1;
		}
		
		$json_data_url = '';
		if($d_item=='qbo_product'){
			$json_data_url = site_url('index.php?mw_qbo_sync_public_get_json_item_list=1&item=qbo_product');
		}
		
		if($d_item=='qbo_customer'){
			$json_data_url = site_url('index.php?mw_qbo_sync_public_get_json_item_list=1&item=qbo_customer');
		}
		
		return <<<EOF
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
		<script type="text/javascript">
		  jQuery(document).ready(function(){
			 jQuery('{$item}').addClass('mwqs_s2');
		  });
		
		  jQuery(function($){			 
			  //jQuery('{$item}').select2();
			   jQuery('{$item}').each(function(){
				   if(jQuery(this).prop('multiple')){
						jQuery(this).select2();
						 jQuery(this).removeClass('mwqs_s2');
				   }
			   });
			  
			  jQuery('{$item}').hover(function(){
				  var is_ajax_dd = {$is_ajax_dd};
				  if(jQuery(this).hasClass('mwqs_dynamic_select') && is_ajax_dd==1){					   
					   jQuery(this).select2({
						   ajax: {
							url: "{$json_data_url}",
							dataType: 'json',
							delay: 250,
							data: function (params) {
								return {
									q: params.term // search term
								};
							},
							processResults: function (data) {								
								return {
									results: data
								};
							},
							cache: true
						},
						minimumInputLength: 3
					   });
				  }else{
					  jQuery(this).select2();
				  }
				  
				  jQuery(this).removeClass('mwqs_s2');
				  
			  });
			  var head = $("head");
			  var headlinklast = head.find("link[rel='stylesheet']:last");
			  var linkElement = "<style type='text/css'>ul.select2-results__options li:first-child{padding:12px 0;}</style>";
			  if (headlinklast.length){
			    headlinklast.after(linkElement);
			  }
			  else {
			   head.append(linkElement);
			  }
		  });
		</script>
EOF;
	}
	
	public function get_tablesorter_js($item='table'){
		return <<<EOF
		<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.5/css/theme.blue.css" rel="stylesheet" />-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.5/js/jquery.tablesorter.js"></script>
		<script type="text/javascript">
		  jQuery(function($){
			  //jQuery('{$item}').addClass('tablesorter-blue');
			  jQuery('{$item} th').css('cursor','pointer');
			  jQuery('{$item} th').each(function(){
				  var sort_th_title = jQuery(this).attr('title');
				  if (sort_th_title == null){
					  sort_th_title = '';
				  }
				  if(sort_th_title==''){
					  sort_th_title = jQuery(this).text();
				  }				  
				  sort_th_title = jQuery.trim(sort_th_title);				  
				  if(sort_th_title!=''){
					  sort_th_title = 'Sort By '+sort_th_title;
					jQuery(this).attr('title',sort_th_title);
				  }else{
					  //jQuery(this).addClass('{sorter: false}');
					  jQuery(this).attr('data-sorter','false');
					  jQuery(this).attr('data-filter','false');
				  }				  
			  });
			  jQuery('{$item}').tablesorter();
		  });
		</script>
EOF;
	}
	
	public function get_bootstrap_switch_lib(){
		return <<<EOF
		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' type='text/css' media='all' />
	   <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
	   
	   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.css' type='text/css' media='all' />
	   <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js'></script>
EOF;
	}
	
	public function get_html_msg($title='',$body=''){
		$display = '
		<html>
			<head>
			<title>'.$title.'</title>
			</head>
			
			<body>
			'.$body.'
			</body>
		</html>';
		return $display;
	}
	
	public function get_plugin_settings_post_data(){
		//$this->_p($_POST);die;
		return array(
		//'mw_wc_qbo_sync_sandbox_mode' => isset($_POST['mw_wc_qbo_sync_sandbox_mode'])?$_POST['mw_wc_qbo_sync_sandbox_mode']:'',
		
		'mw_wc_qbo_sync_default_qbo_item' => isset($_POST['mw_wc_qbo_sync_default_qbo_item'])?(int) $_POST['mw_wc_qbo_sync_default_qbo_item']:0,

		'mw_wc_qbo_sync_default_qbo_product_account' => isset($_POST['mw_wc_qbo_sync_default_qbo_product_account'])?(int) $_POST['mw_wc_qbo_sync_default_qbo_product_account']:0,

		'mw_wc_qbo_sync_default_qbo_asset_account' => isset($_POST['mw_wc_qbo_sync_default_qbo_asset_account'])?(int) $_POST['mw_wc_qbo_sync_default_qbo_asset_account']:0,

		'mw_wc_qbo_sync_default_qbo_expense_account' => isset($_POST['mw_wc_qbo_sync_default_qbo_expense_account'])?(int) $_POST['mw_wc_qbo_sync_default_qbo_expense_account']:0,
		
		'mw_wc_qbo_sync_default_qbo_discount_account' => isset($_POST['mw_wc_qbo_sync_default_qbo_discount_account'])?(int) $_POST['mw_wc_qbo_sync_default_qbo_discount_account']:0,

		'mw_wc_qbo_sync_default_coupon_code' => isset($_POST['mw_wc_qbo_sync_default_coupon_code'])?(int) $_POST['mw_wc_qbo_sync_default_coupon_code']:0,
		
		'mw_wc_qbo_sync_default_shipping_product' => isset($_POST['mw_wc_qbo_sync_default_shipping_product'])?(int) $_POST['mw_wc_qbo_sync_default_shipping_product']:0,
		
		'mw_wc_qbo_sync_order_as_sales_receipt' => isset($_POST['mw_wc_qbo_sync_order_as_sales_receipt'])?$_POST['mw_wc_qbo_sync_order_as_sales_receipt']:'',
		
		'mw_wc_qbo_sync_invoice_min_id' => isset($_POST['mw_wc_qbo_sync_invoice_min_id'])?(int) $_POST['mw_wc_qbo_sync_invoice_min_id']:0,
		
		'mw_wc_qbo_sync_null_invoice' => isset($_POST['mw_wc_qbo_sync_null_invoice'])?$_POST['mw_wc_qbo_sync_null_invoice']:'',
		
		'mw_wc_qbo_sync_invoice_notes' => isset($_POST['mw_wc_qbo_sync_invoice_notes'])?$_POST['mw_wc_qbo_sync_invoice_notes']:'',
		
		'mw_wc_qbo_sync_invoice_note_id' => isset($_POST['mw_wc_qbo_sync_invoice_note_id'])?(int) $_POST['mw_wc_qbo_sync_invoice_note_id']:0,
		
		'mw_wc_qbo_sync_invoice_note_name' => isset($_POST['mw_wc_qbo_sync_invoice_note_name'])?$_POST['mw_wc_qbo_sync_invoice_note_name']:'',
		
		'mw_wc_qbo_sync_invoice_cancelled' => isset($_POST['mw_wc_qbo_sync_invoice_cancelled'])?$_POST['mw_wc_qbo_sync_invoice_cancelled']:'',
		
		'mw_wc_qbo_sync_invoice_memo' => isset($_POST['mw_wc_qbo_sync_invoice_memo'])?$_POST['mw_wc_qbo_sync_invoice_memo']:'',
		
		'mw_wc_qbo_sync_invoice_memo_statement' => isset($_POST['mw_wc_qbo_sync_invoice_memo_statement'])?$_POST['mw_wc_qbo_sync_invoice_memo_statement']:'',
		
		'mw_wc_qbo_sync_invoice_date' => isset($_POST['mw_wc_qbo_sync_invoice_date'])?$_POST['mw_wc_qbo_sync_invoice_date']:'',
		
		'mw_wc_qbo_sync_tax_rule' => isset($_POST['mw_wc_qbo_sync_tax_rule'])?(int) $_POST['mw_wc_qbo_sync_tax_rule']:0,
		
		'mw_wc_qbo_sync_tax_format' => isset($_POST['mw_wc_qbo_sync_tax_format'])?$_POST['mw_wc_qbo_sync_tax_format']:'',
		
		'mw_wc_qbo_sync_append_client' => isset($_POST['mw_wc_qbo_sync_append_client'])?$_POST['mw_wc_qbo_sync_append_client']:'',
		
		'mw_wc_qbo_sync_display_name_pattern' => isset($_POST['mw_wc_qbo_sync_display_name_pattern'])?$_POST['mw_wc_qbo_sync_display_name_pattern']:'',
		
		'mw_wc_qbo_sync_client_sort_order' => isset($_POST['mw_wc_qbo_sync_client_sort_order'])?$_POST['mw_wc_qbo_sync_client_sort_order']:'',
		
		'mw_wc_qbo_sync_client_check_email' => isset($_POST['mw_wc_qbo_sync_client_check_email'])?$_POST['mw_wc_qbo_sync_client_check_email']:'',
		
		'mw_wc_qbo_sync_pull_enable' => isset($_POST['mw_wc_qbo_sync_pull_enable'])?$_POST['mw_wc_qbo_sync_pull_enable']:'',
		'mw_wc_qbo_sync_product_pull_wc_status' => isset($_POST['mw_wc_qbo_sync_product_pull_wc_status'])?$_POST['mw_wc_qbo_sync_product_pull_wc_status']:'',
		'mw_wc_qbo_sync_product_pull_desc_field' => isset($_POST['mw_wc_qbo_sync_product_pull_desc_field'])?$_POST['mw_wc_qbo_sync_product_pull_desc_field']:'',
		'mw_wc_qbo_sync_auto_pull_client' => isset($_POST['mw_wc_qbo_sync_auto_pull_client'])?$_POST['mw_wc_qbo_sync_auto_pull_client']:'',
		
		'mw_wc_qbo_sync_auto_pull_invoice' => isset($_POST['mw_wc_qbo_sync_auto_pull_invoice'])?$_POST['mw_wc_qbo_sync_auto_pull_invoice']:'',
		
		'mw_wc_qbo_sync_auto_pull_payment' => isset($_POST['mw_wc_qbo_sync_auto_pull_payment'])?$_POST['mw_wc_qbo_sync_auto_pull_payment']:'',
		
		'mw_wc_qbo_sync_auto_pull_limit' => isset($_POST['mw_wc_qbo_sync_auto_pull_limit'])?$_POST['mw_wc_qbo_sync_auto_pull_limit']:'',
		
		'mw_wc_qbo_sync_auto_pull_interval' => isset($_POST['mw_wc_qbo_sync_auto_pull_interval'])?(int) $_POST['mw_wc_qbo_sync_auto_pull_interval']:0,
		'mw_wc_qbo_sync_webhook_enable' => isset($_POST['mw_wc_qbo_sync_webhook_enable'])?$_POST['mw_wc_qbo_sync_webhook_enable']:'',
		'mw_wc_qbo_sync_webhook_items' => (isset($_POST['mw_wc_qbo_sync_webhook_items']) && is_array($_POST['mw_wc_qbo_sync_webhook_items']) && count($_POST['mw_wc_qbo_sync_webhook_items']))?implode(',',$_POST['mw_wc_qbo_sync_webhook_items']):'',
		
		
		'mw_wc_qbo_sync_rt_push_enable' => isset($_POST['mw_wc_qbo_sync_rt_push_enable'])?$_POST['mw_wc_qbo_sync_rt_push_enable']:'',
		'mw_wc_qbo_sync_rt_push_items' => (isset($_POST['mw_wc_qbo_sync_rt_push_items']) && is_array($_POST['mw_wc_qbo_sync_rt_push_items']) && count($_POST['mw_wc_qbo_sync_rt_push_items']))?implode(',',$_POST['mw_wc_qbo_sync_rt_push_items']):'',
		
		'mw_wc_qbo_sync_disable_realtime_sync' => isset($_POST['mw_wc_qbo_sync_disable_realtime_sync'])?$_POST['mw_wc_qbo_sync_disable_realtime_sync']:'',
		
		'mw_wc_qbo_sync_disable_sync_status' => isset($_POST['mw_wc_qbo_sync_disable_sync_status'])?$_POST['mw_wc_qbo_sync_disable_sync_status']:'',
		
		'mw_wc_qbo_sync_disable_realtime_client_update' => isset($_POST['mw_wc_qbo_sync_disable_realtime_client_update'])?$_POST['mw_wc_qbo_sync_disable_realtime_client_update']:'',
		
		'mw_wc_qbo_sync_enable_invoice_prefix' => isset($_POST['mw_wc_qbo_sync_enable_invoice_prefix'])?$_POST['mw_wc_qbo_sync_enable_invoice_prefix']:'',
		'mw_wc_qbo_sync_qbo_invoice' => isset($_POST['mw_wc_qbo_sync_qbo_invoice'])?$_POST['mw_wc_qbo_sync_qbo_invoice']:'',
		
		'mw_wc_qbo_sync_email_log' => isset($_POST['mw_wc_qbo_sync_email_log'])?$_POST['mw_wc_qbo_sync_email_log']:'',
		'mw_wc_qbo_sync_err_add_item_obj_into_log_file' => isset($_POST['mw_wc_qbo_sync_err_add_item_obj_into_log_file'])?$_POST['mw_wc_qbo_sync_err_add_item_obj_into_log_file']:'',
		
		'mw_wc_qbo_sync_success_add_item_obj_into_log_file' => isset($_POST['mw_wc_qbo_sync_success_add_item_obj_into_log_file'])?$_POST['mw_wc_qbo_sync_success_add_item_obj_into_log_file']:'',
		'mw_wc_qbo_sync_save_log_for' => isset($_POST['mw_wc_qbo_sync_save_log_for'])?(int) $_POST['mw_wc_qbo_sync_save_log_for']:'',
		'mw_wc_qbo_sync_update_option' => isset($_POST['mw_wc_qbo_sync_update_option'])?$_POST['mw_wc_qbo_sync_update_option']:'',
		'mw_wc_qbo_sync_auto_refresh' => isset($_POST['mw_wc_qbo_sync_auto_refresh'])?$_POST['mw_wc_qbo_sync_auto_refresh']:'',
		'mw_wc_qbo_sync_admin_email' => isset($_POST['mw_wc_qbo_sync_admin_email'])?$_POST['mw_wc_qbo_sync_admin_email']:'',
		'mw_wc_qbo_sync_customer_qbo_check' => isset($_POST['mw_wc_qbo_sync_customer_qbo_check'])?$_POST['mw_wc_qbo_sync_customer_qbo_check']:'',
		'mw_wc_qbo_sync_customer_qbo_check_ship_addr' => isset($_POST['mw_wc_qbo_sync_customer_qbo_check_ship_addr'])?$_POST['mw_wc_qbo_sync_customer_qbo_check_ship_addr']:'',
		'mw_wc_qbo_sync_select2_status' => isset($_POST['mw_wc_qbo_sync_select2_status'])?$_POST['mw_wc_qbo_sync_select2_status']:'',
		'mw_wc_qbo_sync_select2_ajax' => isset($_POST['mw_wc_qbo_sync_select2_ajax'])?$_POST['mw_wc_qbo_sync_select2_ajax']:'',
		'mw_wc_qbo_sync_orders_to_specific_cust' => intval(isset($_POST['mw_wc_qbo_sync_orders_to_specific_cust'])?$_POST['mw_wc_qbo_sync_orders_to_specific_cust']:''),
		'mw_wc_qbo_sync_orders_to_specific_cust_opt' => isset($_POST['mw_wc_qbo_sync_orders_to_specific_cust_opt'])?$_POST['mw_wc_qbo_sync_orders_to_specific_cust_opt']:'',
		'mw_wc_qbo_sync_store_currency' => (isset($_POST['mw_wc_qbo_sync_store_currency']) && is_array($_POST['mw_wc_qbo_sync_store_currency']) && count($_POST['mw_wc_qbo_sync_store_currency']))?implode(',',$_POST['mw_wc_qbo_sync_store_currency']):'',
		
		'mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses' => (isset($_POST['mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses']) && is_array($_POST['mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses']) && count($_POST['mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses']))?implode(',',$_POST['mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses']):'',
		
		'mw_wc_qbo_sync_pmnt_pull_order_status' => isset($_POST['mw_wc_qbo_sync_pmnt_pull_order_status'])?$_POST['mw_wc_qbo_sync_pmnt_pull_order_status']:'',
		'mw_wc_qbo_sync_specific_order_status' => isset($_POST['mw_wc_qbo_sync_specific_order_status'])?$_POST['mw_wc_qbo_sync_specific_order_status']:'',
		);
	}
	
	public function get_plugin_option_keys(){
		$option_keys = array(
		'mw_wc_qbo_sync_sandbox_mode',
		'mw_wc_qbo_sync_default_qbo_item',
		'mw_wc_qbo_sync_default_qbo_product_account',
		'mw_wc_qbo_sync_default_qbo_asset_account',
		'mw_wc_qbo_sync_default_qbo_expense_account',
		'mw_wc_qbo_sync_default_qbo_discount_account',
		'mw_wc_qbo_sync_default_coupon_code',
		'mw_wc_qbo_sync_default_shipping_product',
		'mw_wc_qbo_sync_order_as_sales_receipt',
		'mw_wc_qbo_sync_invoice_min_id',
		'mw_wc_qbo_sync_null_invoice',
		'mw_wc_qbo_sync_invoice_notes',
		'mw_wc_qbo_sync_invoice_note_id',
		'mw_wc_qbo_sync_invoice_note_name',
		'mw_wc_qbo_sync_invoice_cancelled',
		'mw_wc_qbo_sync_invoice_memo',
		'mw_wc_qbo_sync_invoice_memo_statement',
		'mw_wc_qbo_sync_invoice_date',
		'mw_wc_qbo_sync_tax_rule',
		'mw_wc_qbo_sync_tax_format',
		'mw_wc_qbo_sync_append_client',
		'mw_wc_qbo_sync_display_name_pattern',
		'mw_wc_qbo_sync_client_sort_order',
		'mw_wc_qbo_sync_client_check_email',
		'mw_wc_qbo_sync_pull_enable',
		'mw_wc_qbo_sync_product_pull_wc_status',
		'mw_wc_qbo_sync_product_pull_desc_field',
		'mw_wc_qbo_sync_auto_pull_client',
		'mw_wc_qbo_sync_auto_pull_invoice',
		'mw_wc_qbo_sync_auto_pull_payment',
		'mw_wc_qbo_sync_auto_pull_limit',
		'mw_wc_qbo_sync_auto_pull_interval',
		'mw_wc_qbo_sync_webhook_enable',
		'mw_wc_qbo_sync_webhook_items',
		'mw_wc_qbo_sync_rt_push_enable',
		'mw_wc_qbo_sync_rt_push_items',
		'mw_wc_qbo_sync_disable_realtime_sync',
		'mw_wc_qbo_sync_disable_sync_status',
		'mw_wc_qbo_sync_disable_realtime_client_update',
		'mw_wc_qbo_sync_enable_invoice_prefix',
		'mw_wc_qbo_sync_qbo_invoice',
		'mw_wc_qbo_sync_email_log',
		'mw_wc_qbo_sync_err_add_item_obj_into_log_file',
		'mw_wc_qbo_sync_success_add_item_obj_into_log_file',
		'mw_wc_qbo_sync_save_log_for',
		'mw_wc_qbo_sync_update_option',
		'mw_wc_qbo_sync_auto_refresh',
		'mw_wc_qbo_sync_admin_email',
		'mw_wc_qbo_sync_customer_qbo_check',
		'mw_wc_qbo_sync_select2_status',
		'mw_wc_qbo_sync_select2_ajax',
		'mw_wc_qbo_sync_orders_to_specific_cust',
		'mw_wc_qbo_sync_orders_to_specific_cust_opt',
		'mw_wc_qbo_sync_store_currency',
		'mw_wc_qbo_sync_specific_order_status',
		'mw_wc_qbo_sync_measurement_qty',
		'mw_wc_qbo_sync_compt_gf_qbo_is',
		'mw_wc_qbo_sync_compt_gf_qbo_item',
		'mw_wc_qbo_sync_compt_gf_qbo_is_gbf',
		'mw_wc_qbo_sync_compt_wccf_fee',
		'mw_wc_qbo_sync_compt_wccf_fee_wf_qi_map',
		'mw_wc_qbo_sync_compt_p_wod',
		'mw_wc_qbo_sync_compt_p_wsnop',
		'mw_wc_qbo_sync_compt_wpbs',
		'mw_wc_qbo_sync_compt_wpbs_ap_item',
		'mw_wc_qbo_sync_customer_qbo_check_ship_addr',
		'mw_wc_qbo_sync_compt_wchau_enable',
		'mw_wc_qbo_sync_compt_wchau_wf_qi_map',
		'mw_wc_qbo_sync_compt_p_wacof',
		'mw_wc_qbo_sync_compt_p_wacof_m_field',
		'mw_wc_qbo_sync_compt_acof_wf_qi_map',
		'mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses',
		'mw_wc_qbo_sync_pmnt_pull_order_status',
		);
		return $option_keys;
	}
	
	
}
