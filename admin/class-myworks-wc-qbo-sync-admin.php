<?php
if ( ! defined( 'ABSPATH' ) )
exit;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/admin
 * @author     My Works <support@myworks.design>
 */
class MyWorks_WC_QBO_Sync_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		
		global $MWQS_OF,$MSQS_QL,$MSQS_AD;
		$MWQS_OF = new MyWorks_WC_QBO_Sync_Oth_Funcs();
		$MSQS_QL = new MyWorks_WC_QBO_Sync_QBO_Lib();
		
		$MSQS_AD = $this;
		
		/**
		 * The class responsible for defining all AJAX actions that occur in the site
		 * side of the site.
		 */
		require_once plugin_dir_path( __FILE__ ).'myworks-wc-qbo-sync-ajax-actions.php';
	}
	
	public function mw_wc_qbo_admin_init(){

		function qbo_init_check_admin_notice() {

			echo '<div title="MyWorks QuickBooks Sync Setup Error" class="notice notice-error mwqs-setup-notice">'.__('MyWorks Woo Sync for QuickBooks Online has been deactivated! This plugin requires <a target="_blank" href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> to be active!', 'mw_wc_qbo_sync').'</div>';
		}

		$active_plugins = get_option('active_plugins');
		if(!in_array('woocommerce/woocommerce.php',$active_plugins)){
			deactivate_plugins( WP_PLUGIN_DIR . '/myworks-wc-qbo-sync/myworks-wc-qbo-sync.php' );
		  	add_action( 'admin_notices', 'qbo_init_check_admin_notice' );
		}
		
		//
		MyWorks_WC_QBO_Sync_Admin::mw_wc_qbo_sync_qbo_is_init();
		
		MyWorks_WC_QBO_Sync_Admin::admin_settings_save();
		MyWorks_WC_QBO_Sync_Admin::admin_settings_get();
		
		MyWorks_WC_QBO_Sync_Admin::myworks_wc_qbo_sync_activation_redirect();
		MyWorks_WC_QBO_Sync_Admin::mw_qbo_sync_version_control();
		MyWorks_WC_QBO_Sync_Admin::plugin_version_updated();
		MyWorks_WC_QBO_Sync_Admin::plugin_help_init();
		
		$this->health_checker();
		$this->mw_qbo_sync_add_qbo_status_column();
		$this->mw_qbo_sync_add_qbo_status_widget();
		
		$this->wc_order_footer_script();
		
		$server_env = explode('/',$_SERVER['SERVER_SOFTWARE']);
		if(isset($server_env[0]) && $server_env[0] == 'nginx'){
			MyWorks_WC_QBO_Sync_Admin::nginx_register_apache_request_headers();
		}
	}
	
	public function mw_wc_qbo_init(){
		$this->mw_wc_qbo_enable_big_select_join();
		$this->mw_qbo_sync_logging();
		
		$server_env = explode('/',$_SERVER['SERVER_SOFTWARE']);
		if(isset($server_env[0]) && $server_env[0] == 'nginx'){
			MyWorks_WC_QBO_Sync_Admin::nginx_register_apache_request_headers();
		}
	}
	
	public function wc_order_footer_script() {
		add_action('admin_footer', 'wc_order_list_custom_footer_js');
		function wc_order_list_custom_footer_js(){
			global $MSQS_QL;
			$wco_ids = $MSQS_QL->get_session_val('wc_order_id_list',array(),true);
			if(is_array($wco_ids) && count($wco_ids)){
				echo '<!--MWQS Script-->'.PHP_EOL;
				$wco_map_data_arr = $MSQS_QL->get_push_invoice_map_data($wco_ids);				
				//$MSQS_QL->_p($wco_map_data_arr);				
				
				echo '
				<script type="text/javascript">
				jQuery(document).ready(function($) {
				var list_ids = [];
				';
				if(is_array($wco_map_data_arr) && count($wco_map_data_arr)){
					foreach($wco_map_data_arr as $pmd){
						echo 'list_ids.push("'.md5($pmd['DocNumber']).'");';
						if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
							$sync_status_html = '<span title="QuickBooks SalesReceipt Id #'.$pmd['Id'].'">Synced</span>';
						}else{
							$sync_status_html = '<span title="QuickBooks Invoice Id #'.$pmd['Id'].'">Synced</span>';
						}
						echo "jQuery('#ph_inv_ss_".md5($pmd['DocNumber'])."').html('{$sync_status_html}').addClass('mw_qbo_sync_status_paid');";
					}
				}				
				echo '
				jQuery(\'.ph_inv_ss\').each(function(){
					 //console.log($(this).attr(\'id\').replace("ph_inv_ss_", ""));
					 if($.inArray($(this).attr(\'id\').replace("ph_inv_ss_", ""), list_ids) == -1){
						$(this).html(\'<span>Not Synced</span>\').addClass(\'mw_qbo_sync_status_due\');					
					 }
				 });
				';
				echo '
				});
				</script>';
			}
		}		
	}	

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'widget', plugin_dir_url( __FILE__ ) . 'css/wc-widget-css.css', array(), $this->version, 'all' );

		$query_string = explode('=',$_SERVER['QUERY_STRING']);
		if(isset($query_string[1])){
			if( strpos( $query_string[1], "myworks-wc-qbo" ) !== false ) {
			    wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/myworks-wc-qbo-sync-admin.css', array(), $this->version, 'all' );
			}
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/myworks-wc-qbo-sync-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register required menus for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function create_admin_menus() {
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		/**
		 * Register the admin menu pages for plugin.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		add_menu_page( 
			__( 'MyWorks Sync', 'mw_wc_qbo_sync' ),
			__( 'MyWorks Sync', 'mw_wc_qbo_sync' ), 
			'manage_options', 
			'myworks-wc-qbo-sync', 
			array($this, 'qbo_admin_menu_sync'),
			plugin_dir_url( __FILE__ ) . 'image/menu-icon-sync.png', 
			3
		);	
		
		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Dashboard', 'mw_wc_qbo_sync' ),
			__( 'Dashboard', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync',
			array($this, 'qbo_admin_menu_sync')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));

		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Connection', 'mw_wc_qbo_sync' ),
			__( 'Connection', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync-connection',
			array($this, 'qbo_admin_sync_connection_submenu')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));

		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Settings', 'mw_wc_qbo_sync' ),
			__( 'Settings', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync-settings',
			array($this, 'qbo_admin_sync_settings_submenu')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));

		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Log', 'mw_wc_qbo_sync' ),
			__( 'Log', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync-log',
			array($this, 'qbo_admin_sync_log_submenu')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));

		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Map', 'mw_wc_qbo_sync' ),
			__( 'Map', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-map',
			array($this, 'qbo_admin_menu_map')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));

		$sub_page = add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Push', 'mw_wc_qbo_sync' ),
			__( 'Push', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-push',
			array($this, 'qbo_admin_menu_push')
		);
		add_action('load-'.$sub_page, array($this, 'add_help_tabs'));
		
		//21-02-2017
		if($MSQS_QL->option_checked('mw_wc_qbo_sync_pull_enable')){	
			$sub_page = add_submenu_page( 
				'myworks-wc-qbo-sync', 
				__( 'Pull', 'mw_wc_qbo_sync' ),
				__( 'Pull', 'mw_wc_qbo_sync' ),
				'manage_options',
				'myworks-wc-qbo-pull',
				array($this, 'qbo_admin_menu_pull')
			);			
			add_action('load-'.$sub_page, array($this, 'add_help_tabs'));
		}
		
		/*
		add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'Compatibility', 'mw_wc_qbo_sync' ),
			__( 'Compatibility', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync-compt',
			array($this, 'qbo_admin_menu_sync_compt')
		);
		*/

		add_submenu_page( 
			'myworks-wc-qbo-sync', 
			__( 'System Info', 'mw_wc_qbo_sync' ),
			__( 'System Info', 'mw_wc_qbo_sync' ),
			'manage_options',
			'myworks-wc-qbo-sync-sys',
			array($this, 'qbo_admin_menu_sync_sys')
		);

	}
	
	public function mw_qbo_sync_add_qbo_status_column(){

		add_filter( 'manage_edit-shop_order_columns', 'custom_shop_order_column',11);
		function custom_shop_order_column($columns){
		    $columns['mw_qbo_sync_inv_status'] = __( 'QBO Status','mw_wc_qbo_sync');
		    return $columns;
		}

		add_action( 'manage_shop_order_posts_custom_column' , 'custom_orders_list_column_content', 10, 2 );
		function custom_orders_list_column_content( $column ){
			global $MSQS_QL;			
		    global $post, $woocommerce, $the_order;
			//21-06-2017			
			$woo_version = $MSQS_QL->get_woo_version_number();
			if ( $woo_version >= 3.0 ) {
				$order_id = $the_order->get_id();
			}else{
				$order_id = $the_order->id;
			}
			
			//25-05-2017
			if($MSQS_QL->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $MSQS_QL->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
				$_order_number_formatted = get_post_meta( $order_id, '_order_number_formatted', true );
				if($_order_number_formatted!=''){
					$order_id = $_order_number_formatted;
				}
			}
			
		    switch ( $column ){
		        case 'mw_qbo_sync_inv_status' :
					$_SESSION[$MSQS_QL->session_prefix.'wc_order_id_list'][] = "'".$order_id."'";
		    		echo '<span id="ph_inv_ss_'.md5($order_id).'" class="ph_inv_ss mw_qbo_sync_status_span"></span>';
		            break;
		    }
		}
	}

	public function mw_qbo_sync_add_qbo_status_widget(){

		add_action( 'add_meta_boxes', 'mw_add_meta_boxes' );
		if ( ! function_exists( 'mw_add_meta_boxes' ) ){
		    function mw_add_meta_boxes(){
		        global $woocommerce, $order, $post;
		        add_meta_box( 'qbo_invoice_info', __('QuickBooks Invoice Information','mw_wc_qbo_sync'), 'qbo_invoice_info_fn', 'shop_order', 'side', 'core' );
		    }
		}

		if ( ! function_exists( 'qbo_invoice_info_fn' ) ){
		    function qbo_invoice_info_fn(){
		        global $woocommerce,$post;
				global $MSQS_QL;
				//30-05-2017
				$order_id = $post->ID;
				if($MSQS_QL->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $MSQS_QL->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
					$_order_number_formatted = get_post_meta( $order_id, '_order_number_formatted', true );
					if($_order_number_formatted!=''){
						$order_id = $_order_number_formatted;
					}
				}
				
				$wco_map_data_arr = $MSQS_QL->get_push_invoice_map_data(array("'{$order_id}'"));
				if(is_array($wco_map_data_arr) && count($wco_map_data_arr)){
					echo '<p class="mw_qbo_sync_status_p">
					Status: &nbsp;&nbsp;
					<span class="mw_qbo_sync_status_span mw_qbo_sync_status_paid">Synced</span>
					</p>
		            <p class="mw_qbo_sync_number_p">
					Number: <span class="mw_qbo_sync_status_span mw_qbo_sync_status_info">'.$wco_map_data_arr[0]['DocNumber'].'</span>
					</p>';
				}else{
					echo '
					<p class="mw_qbo_sync_status_p">
					Status: &nbsp;&nbsp;
					<span class="mw_qbo_sync_status_span mw_qbo_sync_status_due">Not Synced</span>
					</p>
		            <p class="mw_qbo_sync_number_p">
					Number: <span class="mw_qbo_sync_status_span mw_qbo_sync_status_info">Not Applicable</span>
					</p>';
				}
		    }
		}
	}
	
	public function add_help_tabs(){
		return false; //Not Using For Now
		
		$help_tabs = array(
			'mwqs_help' => array(
				'title'	=> __('QuickBooks Sync'),
				'content'	=> '<p>' . __( 'QuickBooks Sync.','mw_wc_qbo_sync' ) . '</p>'
			),
			'mwqs_support' => array(
				'title'	=> __('Help & Support'),
				'content'	=> '<p>' . __( 'Help & Support.','mw_wc_qbo_sync' ) . '</p>'
			),
		);
		
		foreach($help_tabs as $key=>$val){
			$screen = get_current_screen();
			$screen->add_help_tab( array(
				'id'	=> $key,
				'title'	=> $val['title'],
				'content'	=> $val['content']
			) );
			$screen->set_help_sidebar(__('Sidebar' ,'mw_wc_qbo_sync' ));
		}
	}

	/**
	 * Register main menu page for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_menu_sync(){

		//
		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-dashboard.php';
	}

	/**
	 * Register main menu page for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_menu_sync_sys(){
		//
		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-sys.php';
	}
	
	/*
	public static function qbo_admin_menu_sync_compt(){
		//
		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-compt.php';
	}
	*/
	
	/**
	 * Register connection submenu for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_sync_connection_submenu(){

		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-connection.php';
	}

	/**
	 * Register settings submenu for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_sync_settings_submenu(){

		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-settings.php';
	}

	/**
	 * Register log submenu for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_sync_log_submenu(){
		
		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-log.php';
	}

	/**
	 * Register invoice submenu for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_menu_map(){

		require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-map.php';
	}

	/**
	 * Register invoice submenu for admin area.
	 *
	 * @since    1.0.0
	 */
	public static function qbo_admin_menu_push(){
		global $MSQS_QL;		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-push-notice.php';
		}else{
			require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-push.php';
		}
		
	}
	
	public static function qbo_admin_menu_pull(){
		global $MSQS_QL;		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true' || !$MSQS_QL->option_checked('mw_wc_qbo_sync_pull_enable')) {
			require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-pull-notice.php';
		}else{
			require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-admin-pull.php';
		}
		
	}


	/**
	 * Save admin connection settings.
	 *
	 * @since    1.0.0
	 */
	public function admin_settings_save($data=array(),$trigger=''){
		if($trigger){
			foreach($data as $key=>$value){
				$value   = sanitize_text_field( $value );
				update_option($key,$value);
			}
		}
	}

	public static function set_setting_alert($save_status=''){
		
		if($save_status){
			if($save_status=='admin-success-green'){
				echo '<script>swal("Rock On!", "Your settings have been saved.", "success")</script>';
			}elseif($save_status=='red lighten-2'){
				echo '<script>swal("Oops!", "Hmmmm something went wrong.", "error")</script>';
			}elseif($save_status!='admin-success-green' && $save_status!='red lighten-2' && $save_status!='error'){
				echo '<script>swal("Rock On!", "'.$save_status.'", "success")</script>';
			}else{
				echo '<script>swal("Oops!", "Hmmmm something went wrong.", "error")</script>';
			}
			echo '<script type="text/javascript">
			jQuery(document).ready(function(e){
				jQuery(".confirm").on("click",function(e){
					jQuery(".sweet-overlay").hide();
					jQuery(".showSweetAlert").hide();
					jQuery("body").removeClass("stop-scrolling");
				});
			});
			</script>';
		}
	}

	public static function get_settings_assets($trigger=''){
		
		echo '<link href="'.esc_url( plugins_url( "admin/css/sweetalert.css", dirname(__FILE__) ) ).'" rel="stylesheet" type="text/css">';
		
		echo '<link href="'.esc_url( plugins_url( "admin/css/woocommerce-custom.css", dirname(__FILE__) ) ).'" rel="stylesheet" type="text/css">';
		
		echo '<link href="'.esc_url( plugins_url( "admin/css/font-awesome.css", dirname(__FILE__) ) ).'" rel="stylesheet" type="text/css">';
		
		echo '<script type="text/javascript" src="'.esc_url( plugins_url( "admin/js/sweetalert-dev.js", dirname(__FILE__) ) ).'"></script>';
		
		//Checkbox Switch
		echo MyWorks_WC_QBO_Sync_Admin::get_checkbox_switch_assets();
	}
	
	public static function get_checkbox_switch_assets(){
		//		
		return <<<EOF
		
		<!--<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' type='text/css' media='all' />-->
	   <!--<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>-->
	   
	   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.css' type='text/css' media='all' />
	   <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js'></script>
EOF;
	
	}
	
	//
	public function mw_qbo_sync_logging(){

		if(defined('DISABLE_WP_CRON')){
			if( DISABLE_WP_CRON ){
				function qbo_setup_admin_notice_wp_cron(){
					echo '<div title="MyWorks QuickBooks Sync Setup Error" class="notice notice-error mwqs-setup-notice">'.__("MyWorks Woo Sync for QuickBooks Online needs to enable WP CRON for email logging. Please remove code like<b>define('DISABLE_WP_CRON', true)</b> from <b>wp-config.php (if found)</b> and add <b>define('DISABLE_WP_CRON', false)</b>.", 'mw_wc_qbo_sync').'</div>';
				}
				add_action( 'admin_notices', 'qbo_setup_admin_notice_wp_cron' );
			}
		}
		
		global $MSQS_QL;
		if ($MSQS_QL->option_checked('mw_wc_qbo_sync_email_log') || $MSQS_QL->option_checked('mw_wc_qbo_sync_auto_refresh')){

			if (! wp_next_scheduled ( 'mw_qbo_sync_logging_hook' )){
				wp_schedule_event(time(), 'daily', 'mw_qbo_sync_logging_hook');
			}
			add_action('mw_qbo_sync_logging_hook', 'mw_qbo_sync_logging_callback');

			function mw_qbo_sync_logging_callback() {
				// do something daily
				if($MSQS_QL->option_checked('mw_wc_qbo_sync_email_log')){
					$MSQS_QL->save_log('Log Email Cron Run.','Cron',2);
					$MSQS_QL->send_daily_email_log();
				}
				
				
				//if($MSQS_QL->option_checked('mw_wc_qbo_sync_auto_refresh')){}
				$MSQS_QL->save_log('Quick Refresh Cron Started.','Cron',2);
				$MSQS_QL->quick_refresh_qbo_customers();
				$MSQS_QL->quick_refresh_qbo_products();
				$MSQS_QL->save_log('Quick Refresh Cron Ended.','Cron',2);
			}
		}else{
			wp_clear_scheduled_hook('mw_qbo_sync_logging_hook');
		}
		
	}

	public function mw_wc_qbo_enable_big_select_join(){
		global $wpdb;
		$wpdb->query('SET SESSION SQL_BIG_SELECTS = 1');
	}

	public static function is_trial_version_check(){
		global $MSQS_QL;
		if($MSQS_QL->option_checked('mw_wc_qbo_sync_trial_license')){
			$query_string = explode('=',$_SERVER['QUERY_STRING']);
			if(isset($query_string[1])){
				if( strpos( $query_string[1], "myworks-wc-qbo" ) !== false ) {
					$image = plugin_dir_url( __FILE__ ) . 'image/Favicon-square.png';
				    echo '<div class="text-btn">
				    		<img width="25"  alt="MyWorks Sync" title="MyWorks Sync" src="'.$image.'">&nbsp;
								<h3><b>'.$MSQS_QL->get_option('mw_wc_qbo_sync_trial_days_left').'</b> &nbsp;DAYS LEFT ON YOUR FREE TRIAL</h3><a target="_blank" href="https://myworks.design/account/clientarea.php?action=productdetails&id='.$MSQS_QL->get_option('mw_wc_qbo_sync_trial_license_serviceid').'" class="btn btn-info" role="button">UPGRADE NOW!</a>
							</div>';
				}
			}
		}
	}

	/**
	 * Get admin connection settings.
	 *
	 * @since    1.0.0
	 */
	public function admin_settings_get($data=array(),$trigger=''){

		if($trigger){
			foreach($data as $value){
				$admin_connection_settings_data[$value] = get_option($value);
			}
			return $admin_connection_settings_data;
		}
	}
	
	
	public function get_customer_meta_from_order_meta($order_id,$manual=false){
		$customer_data = array();
		global $MSQS_QL;
		
		$order_meta = get_post_meta((int) $order_id);
		//$MSQS_QL->_p($order_meta);
		$_customer_user = (isset($order_meta['_customer_user'][0]))?(int) $order_meta['_customer_user'][0]:0;
		$customer_data['wc_customerid'] = $_customer_user;
		$customer_data['wc_cus_id '] = $_customer_user;		
		
		if(is_array($order_meta) && count($order_meta)){
			foreach ($order_meta as $key => $value){				
				if($MSQS_QL->start_with($key,'_billing_') || $MSQS_QL->start_with($key,'_shipping_')){
					if($MSQS_QL->start_with($key,'_billing_')){
						$key = str_replace('_billing_','billing_',$key);
					}else{
						$key = str_replace('_shipping_','shipping_',$key);
					}					
					$customer_data[$key] = ($value[0])?$value[0]:'';
				}				
			}
		}
		
		$_order_currency = (isset($order_meta['_order_currency'][0]))?$order_meta['_order_currency'][0]:'';
		$customer_data['currency'] = $_order_currency;
		
		$customer_data['note'] = '';
		
		$mw_wc_display_name = $MSQS_QL->wc_get_display_name($customer_data,true);
		$customer_data['display_name'] = $mw_wc_display_name;
		
		$customer_data['manual'] = $manual;
		return $customer_data;
	}

	public function mw_qbo_sync_customer_meta($customer_id='',$manual=false){
			
		if($customer_id){
			global $MSQS_QL;
			$user_info = get_userdata($customer_id);
			$user_id = $user_info->ID;
			$mw_wc_first_name = $user_info->first_name?$user_info->first_name:'';
		    $mw_wc_last_name = $user_info->last_name?$user_info->last_name:'';
		    $mw_wc_display_name = $user_info->display_name?$user_info->display_name:'';
		    $mw_wc_email = $user_info->user_email?$user_info->user_email:'';
			//
			$user_meta = get_user_meta($user_id);
			
			$customer_data = array();
			$customer_data['wc_customerid'] = $user_id;
			$customer_data['wc_cus_id '] = $user_id;
			
			$customer_data['firstname'] = $mw_wc_first_name;
			$customer_data['lastname'] = $mw_wc_last_name;
			
			$customer_data['email'] = $mw_wc_email;
			
			$mw_wc_billing_company = (isset($user_meta['billing_company'][0]))?$user_meta['billing_company'][0]:'';
			$customer_data['company'] = $mw_wc_billing_company;
			
			if(is_array($user_meta) && count($user_meta)){
				foreach ($user_meta as $key => $value){
					if($MSQS_QL->start_with($key,'billing_') || $MSQS_QL->start_with($key,'shipping_')){					
						$customer_data[$key] = ($value[0])?$value[0]:'';
					}				
				}
			}
			
			$customer_data['display_name'] = $mw_wc_display_name;
			$mw_wc_display_name = $MSQS_QL->wc_get_display_name($customer_data);
			$customer_data['display_name'] = $mw_wc_display_name;
			
			//
			
			$customer_data['currency'] = (string) $MSQS_QL->get_wc_customer_currency($user_id);
			$customer_data['note'] = '';
			$customer_data['manual'] = $manual;

			return $customer_data;
		}
	}

	
	
	public static function myworks_wc_qbo_sync_activation_redirect(){
		global $MSQS_QL;
		MyWorks_WC_QBO_Sync_Admin::check_setup_status();
	}
	
	public static function check_setup_status(){
		global $MSQS_QL;		
		$is_setup_ok = true;
		$error_msg = array();
		global $mwqs_admin_msg;
		$widget_msg = array();
		
		if (!(int) $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_connected')){
			$error_msg[] = __('QuickBooks not connected.','mw_wc_qbo_sync');
			$mwqs_admin_msg[] = __('QuickBooks not connected <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-connection').'">click here</a> to connect.','mw_wc_qbo_sync');
			$is_setup_ok = false;
		}
		
		if($is_setup_ok){
			global $wpdb;
			$qbo_customer_remote_data = array();
			$qbo_product_remote_data = array();
			
			if($MSQS_QL->is_connected()){
				$Context = $MSQS_QL->getContext();
				$realm = $MSQS_QL->getRealm();
				$CustomerService = new QuickBooks_IPP_Service_Customer();
				$qbo_customer_remote_data = $CustomerService->query($Context,$realm,"SELECT Id FROM Customer STARTPOSITION 1 MaxResults 1 ");
				
				$ItemService = new QuickBooks_IPP_Service_Term();
				$qbo_product_remote_data = $ItemService->query($Context, $realm, "SELECT Id , Name FROM Item ORDER BY Name ASC STARTPOSITION 1 MaxResults 1 ");
			}
			
			$quick_refresh_error = false;
			$customer_map_error = false;
			$product_map_error = false;
			
			if($qbo_customer_remote_data && count($qbo_customer_remote_data)){
				$qbo_customer_local_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_qbo_customers', 'id', '', '', ' 0,1' );
				if(empty($qbo_customer_local_data)){
					$is_setup_ok = false;
					$error_msg[] = __('QuickBooks customers not found in local table.','mw_wc_qbo_sync');
					$quick_refresh_error = true;
				}else{
					$customer_map_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_customer_pairs', 'id', '', '', ' 0,1' );
					if(empty($customer_map_data)){
						$is_setup_ok = false;
						$error_msg[] = __('Customers not mapped.','mw_wc_qbo_sync');
						$customer_map_error = true;
						delete_option('mw_wc_qbo_sync_qbo_is_data_mapped_customer');
					}else{
						update_option('mw_wc_qbo_sync_qbo_is_data_mapped_customer','true');
					}
				}
				
			}
			
			if($qbo_product_remote_data && count($qbo_product_remote_data)){
				$qbo_product_local_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_qbo_items', 'id', '', '', ' 0,1' );
				if(empty($qbo_product_local_data)){
					$is_setup_ok = false;
					$error_msg[] = __('QuickBooks products not found in local table.','mw_wc_qbo_sync');
					$quick_refresh_error = true;
				}else{
					$product_map_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_product_pairs', 'id', '', '', ' 0,1' );
					if(empty($product_map_data)){
						$is_setup_ok = false;
						$error_msg[] = __('Products not mapped.','mw_wc_qbo_sync');
						$product_map_error = true;
						delete_option('mw_wc_qbo_sync_qbo_is_data_mapped_product');
					}else{
						update_option('mw_wc_qbo_sync_qbo_is_data_mapped_product','true');
					}
				}
				
			}
			
			if($quick_refresh_error){
				$error_msg[] = __('Please click on Quick refresh data button in plugin <a href="admin.php?page=myworks-wc-qbo-sync">dashboard</a>.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('Please <a href="'.site_url('index.php?mw_qbo_sync_public_quick_refresh=1').'">click here</a> to add customers and products from QuickBooks.','mw_wc_qbo_sync');
				delete_option('mw_wc_qbo_sync_qbo_is_refreshed');
			}else{
				update_option('mw_wc_qbo_sync_qbo_is_refreshed','true');
			}
			
			if($customer_map_error){
				$mwqs_admin_msg[] = __('Customers not mapped <a href="'.admin_url('admin.php?page=myworks-wc-qbo-map').'">click here</a> to map.','mw_wc_qbo_sync');
			}
			
			if($product_map_error){
				$mwqs_admin_msg[] = __('Products not mapped <a href="'.admin_url('admin.php?page=myworks-wc-qbo-map&tab=product').'">click here</a> to map.','mw_wc_qbo_sync');
			}
			
			//28-04-2017
			$is_default_settings_ok = true;
			
			$mw_wc_qbo_sync_default_qbo_item = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_qbo_item');
			if(!$mw_wc_qbo_sync_default_qbo_item){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default product not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}

			$mw_wc_qbo_sync_default_qbo_product_account = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_qbo_product_account');
			if(!$mw_wc_qbo_sync_default_qbo_product_account){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default product account not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}
			
			$mw_wc_qbo_sync_default_qbo_asset_account = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_qbo_asset_account');
			if(!$mw_wc_qbo_sync_default_qbo_asset_account){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default product inventory asset account not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product inventory asset account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}
			
			$mw_wc_qbo_sync_default_qbo_expense_account = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_qbo_expense_account');
			if(!$mw_wc_qbo_sync_default_qbo_expense_account){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default product expense account not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product expense account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}
			
			$mw_wc_qbo_sync_default_qbo_discount_account = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_qbo_discount_account');
			if(!$mw_wc_qbo_sync_default_qbo_discount_account){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default product discount account not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product expense account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}
			
			$mw_wc_qbo_sync_default_coupon_code = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_coupon_code');
			if(!$mw_wc_qbo_sync_default_coupon_code){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default coupon product not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product expense account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}
			
			$mw_wc_qbo_sync_default_shipping_product = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_default_shipping_product');
			if(!$mw_wc_qbo_sync_default_shipping_product && !$MSQS_QL->get_qbo_company_setting('is_shipping_allowed')){
				$is_setup_ok = false;
				$error_msg[] = __('QuickBooks default shipping product not set in settings.','mw_wc_qbo_sync');
				$mwqs_admin_msg[] = __('QuickBooks default product expense account not set in <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-settings').'">settings</a>.','mw_wc_qbo_sync');
				$is_default_settings_ok = false;
			}

			$p_map_whr = " `wc_paymentmethod` !='' AND `qbo_account_id` >0 AND `enable_payment` = 1 ";
			$qbo_payment_map_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_paymentmethod_map', 'id', $p_map_whr, '', ' 0,1' );
			
			if(empty($qbo_payment_map_data)){
				$error_msg[] = __('Payment methods not mapped.','mw_wc_qbo_sync');
				$is_setup_ok = false;
				$mwqs_admin_msg[] = __('Payment methods not mapped <a href="'.admin_url('admin.php?page=myworks-wc-qbo-map&tab=payment-method').'">click here</a> to map.','mw_wc_qbo_sync');
				delete_option('mw_wc_qbo_sync_qbo_is_data_mapped_payment');
			}else{
				update_option('mw_wc_qbo_sync_qbo_is_data_mapped_payment','true');
			}

			if($MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_payment') && $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_product') && $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_customer')){
				update_option('mw_wc_qbo_sync_qbo_is_data_mapped','true');
			}else{
				delete_option('mw_wc_qbo_sync_qbo_is_data_mapped');
			}

			if($is_default_settings_ok){
				update_option('mw_wc_qbo_sync_qbo_is_default_settings','true');
			}else{
				delete_option('mw_wc_qbo_sync_qbo_is_default_settings');
			}
			
		}
		if(count($error_msg)){
			$MSQS_QL->set_session_msg('mw_wc_qbo_sync_activation_session_msg',$error_msg);
		}
		
		if($is_setup_ok){
			update_option('mw_qbo_sync_activation_redirect', 'true');			
		}else{
			delete_option('mw_qbo_sync_activation_redirect');
			
			function qbo_setup_admin_notice() {
				/*
				echo '<div class="notice notice-error mwqs-setup-notice"><p>'.__( 'MyWorks QuickBooks Sync Initial Setup Required <a href="'.admin_url('admin.php?page=myworks-wc-qbo-sync-init').'">view</a>', 'mw_wc_qbo_sync' ).'</p></div>';
				*/
				global $mwqs_admin_msg;
				if(isset($mwqs_admin_msg) && is_array($mwqs_admin_msg) && count($mwqs_admin_msg)){
					echo '<div title="MyWorks QuickBooks Sync Setup Error" class="notice notice-error mwqs-setup-notice">';
					foreach($mwqs_admin_msg as $msg){											
						echo '<p>'.$msg.'</p>';						
					}
					echo '</div>';
				}
				
			}
			//add_action( 'admin_notices', 'qbo_setup_admin_notice' );
		}
		return $is_setup_ok;
	}

	public static function plugin_help_init(){

		function qbo_help_admin_notice() {

			$query_string = explode('=',$_SERVER['QUERY_STRING']);
			if(isset($query_string[1])){
				if( strpos( $query_string[1], "myworks-wc-qbo" ) !== false ) {
					global $MSQS_QL;
					if(!(int) $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_connected') || !$MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_refreshed') || !$MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_default_settings') || !$MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_data_mapped')){
							require_once plugin_dir_path( __FILE__ ) . 'partials/myworks-wc-qbo-sync-init.php';
					}
				}
			}
		}
		add_action( 'admin_notices', 'qbo_help_admin_notice' );
	}

	public static function mw_qbo_sync_version_control(){
		global $MSQS_QL;
		global $MWQS_OF;
		$mw_wc_qbo_sync_license = $MSQS_QL->get_option('mw_wc_qbo_sync_license');
		$mw_wc_qbo_sync_localkey = $MSQS_QL->get_option('mw_wc_qbo_sync_localkey');		
		$mw_wc_qbo_sync_update_option = $MSQS_QL->get_option('mw_wc_qbo_sync_update_option');

		if($MWQS_OF->is_valid_license($mw_wc_qbo_sync_license,$mw_wc_qbo_sync_localkey)){
			require_once( WP_PLUGIN_DIR . '/myworks-wc-qbo-sync/wp-updates-plugin.php' );
			new WPUpdatesPluginUpdater_1570( 'http://wp-updates.com/api/2/plugin', 'myworks-wc-qbo-sync/myworks-wc-qbo-sync.php' );

			if($mw_wc_qbo_sync_update_option == 'true'){
				require_once( WP_PLUGIN_DIR . '/myworks-wc-qbo-sync/wp-updates-plugin-beta.php' );
				new WPUpdatesPluginUpdater_1604( 'http://wp-updates.com/api/2/plugin', 'myworks-wc-qbo-sync/myworks-wc-qbo-sync.php' );
			}
		}	
	}

	public static function nginx_register_apache_request_headers(){

		if( !function_exists('apache_request_headers') ) {
		    function apache_request_headers() {
		        $arh = array();
		        $rx_http = '/\AHTTP_/';

		        foreach($_SERVER as $key => $val) {
		            if( preg_match($rx_http, $key) ) {
		                $arh_key = preg_replace($rx_http, '', $key);
		                $rx_matches = array();
			           // do some nasty string manipulations to restore the original letter case
			           // this should work in most cases
		                $rx_matches = explode('_', $arh_key);

		                if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
		                    foreach($rx_matches as $ak_key => $ak_val) {
		                        $rx_matches[$ak_key] = ucfirst($ak_val);
		                    }

		                    $arh_key = implode('-', $rx_matches);
		                }

		                $arh[$arh_key] = $val;
		            }
		        }

		        return( $arh );
		    }
		}
	}

	public static function return_plugin_version(){
		$plugin_data = get_plugin_data( WP_PLUGIN_DIR . '/myworks-wc-qbo-sync/myworks-wc-qbo-sync.php', false, false );
		return $plugin_data['Version'];
	}

	public static function mw_wc_qbo_sync_qbo_is_init(){
		if(isset($_GET['mw_wc_qbo_sync_qbo_is_init']) && $_GET['mw_wc_qbo_sync_qbo_is_init']=='true'){
			update_option('mw_wc_qbo_sync_qbo_is_init','true');
		}
	}

	// 07-03-2017
	public static function db_checker($file_version,$db_version){
		global $wpdb;
		$db_delta = MyWorks_WC_QBO_Sync_Admin::determine($file_version,$db_version);		
		if(count($db_delta)){
			foreach($db_delta as $query){
				if($query)
				$wpdb->query($query);				
			}
		}
	}

	public function health_checker($operation=''){

		global $wpdb;
		global $MSQS_QL;
		$tables = array(
			'mw_wc_qbo_sync_customer_pairs',
			'mw_wc_qbo_sync_log',
			'mw_wc_qbo_sync_paymentmethod_map',
			'mw_wc_qbo_sync_payment_id_map',
			'mw_wc_qbo_sync_product_pairs',
			'mw_wc_qbo_sync_promo_code_product_map',
			'mw_wc_qbo_sync_qbo_customers',
			'mw_wc_qbo_sync_qbo_items',
			'mw_wc_qbo_sync_real_time_sync_history',
			'mw_wc_qbo_sync_real_time_sync_queue',
			'mw_wc_qbo_sync_shipping_product_map',
			'mw_wc_qbo_sync_tax_map',
			'mw_wc_qbo_sync_variation_pairs',
			'mw_wc_qbo_sync_wq_cf_map'
		);
		
		if($operation=='tables'){
			return $tables;
		}else{

			$missing_db = array();
			foreach ( $tables as $table ) {
				if($wpdb->get_var( $wpdb->prepare( "SHOW TABLES LIKE %s;", $wpdb->prefix . $table ) ) !== $wpdb->prefix . $table){
					$missing_db[] = $table;
				}
			}
			
			if(count($missing_db)){
				$MSQS_QL->set_session_val('missing_db',$missing_db);
			}			
			
			function qbo_setup_admin_notice_health_check() {
				global $MSQS_QL;				
				if(is_array($MSQS_QL->get_session_val('missing_db')) && count($MSQS_QL->get_session_val('missing_db'))){
					echo '<div class="notice notice-error mwqs-setup-notice">
					MyWorks Sync Database Table Missing: ';
					echo implode(',',$MSQS_QL->get_session_val('missing_db',array(),true));
					echo '</div>';
				}
				
				if($MSQS_QL->check_invalid_chars_in_db_conn_info()){

					echo '<div title="MyWorks QuickBooks Sync Setup Error" class="notice notice-error mwqs-setup-notice">'.__("MyWorks QuickBooks Online Sync for WooCommerce does not support these special characters in your database password in your wp-config.php file:  (+ / # % ‘ ?)  Please update your database password to not include these characters.", 'mw_wc_qbo_sync').'</div>';
				}
			}
			
			add_action( 'admin_notices', 'qbo_setup_admin_notice_health_check' );
		}
	}
	
	public static function determine($base,$peak){
		
		global $wpdb;
		$db_array = $versions = array();
		
		$versions['1.0.8'] = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'mw_wc_qbo_sync_variation_pairs` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `wc_variation_id` int(11) NOT NULL,
								  `quickbook_product_id` int(11) NOT NULL,
								  `class_id` varchar(255) NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
		$versions['1.0.19'][] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_payment_id_map` ADD `is_wc_order` INT(1) NOT NULL DEFAULT '0' AFTER `qbo_payment_id`;";
		$versions['1.0.19'][] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `ps_order_status` VARCHAR(255) NOT NULL AFTER `term_id`;";
		$versions['1.0.19'][] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `individual_batch_support` INT(1) NOT NULL AFTER `ps_order_status`;
		";
		$versions['1.0.20'] = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'mw_wc_qbo_sync_wq_cf_map` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,								  
								  `wc_field` varchar(255) NOT NULL,
								  `qb_field` varchar(255) NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
		
		$versions['1.0.21'][] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `deposit_cron_utc` VARCHAR(255) NOT NULL AFTER `individual_batch_support`;";

		foreach($versions as $key=>$value){
			if(version_compare($key, $peak, '>') && version_compare($key, $base, '<=')){
				if($value!=''){
				    if(is_array($value)){
				        foreach($value as $sub_val)
					    $db_array[] = $sub_val;
				    }else{
				        $db_array[] = $value;
				    }
				}				
			}
		}

		return $db_array;
	}
	// 07-03-2017

	public static function plugin_version_updated(){
		global $MSQS_QL;
		$current_version = MyWorks_WC_QBO_Sync_Admin::return_plugin_version();
		$old_version = $MSQS_QL->get_option('mw_qbo_sync_last_updated_version');
		if(!$old_version){
			update_option('mw_qbo_sync_last_updated_version',$current_version);
		}else{
			if (version_compare($current_version, $old_version, '<=')) {
			    return true;
			}else{

				// 07-03-2017
				MyWorks_WC_QBO_Sync_Admin::db_checker($current_version,$old_version);				

				$url = get_bloginfo('url');
				$company = get_bloginfo('name');
				$email = get_bloginfo('admin_email');
				$license_key = $MSQS_QL->get_option('mw_wc_qbo_sync_license');
				
				// Peter disabled 06/26/17
				/*
				$message = "<b>WooCommerce Sync for QuickBooks Online</b></br>";
			    $message .= "</br>";
			    $message .= "<b>License Key:</b> " . $license_key ."</br>";
			    $message .= "</br>";
			    $message .= "<b>Old Version:</b> " . $old_version ."</br>";
			    $message .= "<b>New Version:</b> " . $current_version . "</br>";
			    $message .= "</br>";
			    $message .= "<b>Company:</b> " .$company ."</br>";
				$message .= "<b>Email:</b> " .$email ."</br>";
				$message .= "<b>WooCommerce URL:</b> " .$url ."</br>";
				
				$headers = array(
					'MIME-Version: 1.0',
					'Content-type:text/html;charset=UTF-8',
				);		
				
				$to = 'notifications@myworks.design';

				wp_mail($to, 'Upgrade - WooCommerce Sync', $message, $headers);
				*/
				
				$post_url = 'https://myworks.design/dashboard/api/dashboard/product/saveModule';
				
				$params = array(
					'api_version'=>'0.1',
					'result_type'=>'json',
					'process'=>'upgrade',
					'licensekey'=>$license_key,
					'version'=>$current_version,	
					'company'=>$company,
					'email'=>$email,
					'system_url'=>$url
				);

			    $ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL, $post_url); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch, CURLOPT_PROXYPORT, 3128); curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); curl_setopt($ch, CURLOPT_POST, true); curl_setopt($ch, CURLOPT_POSTFIELDS, $params); $response = curl_exec($ch);
			    curl_close($ch);

				update_option('mw_qbo_sync_last_updated_version',$current_version);
			}
		}
	}
	
	
	public function mw_qbo_wc_product_delete($product_id=''){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		if($product_id){
			global $post_type;
			if ( $post_type != 'product' ) return false;
			//Delete Product
		}
	}
	
	public function mw_qbo_wc_variation_save($post_id='', $post=''){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		
		if(is_array($post_id)){
			$variation_id = (int) $post_id['variation_id'];
			$manual = true;
		}else{
			$variation_id = (int) $post_id;
			$manual = false;
		}
		
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Variation')){
			return false;
		}
		
		if($variation_id){
			$variation = get_post($variation_id);
			if(!is_object($variation) || !count($variation)){
				if($manual){
					$MSQS_QL->save_log('Export Variation Error #'.$variation_id,'Woocommerce variation not found!','Invoice',0);
				}
				return false;
			}
			if($variation->post_type!='product_variation'){
				if($manual){
					$MSQS_QL->save_log('Export Variation Error #'.$variation_id,'Woocommerce variation is not valid.','Invoice',0);
				}
				return false;
			}			
			
			if($variation->post_status=='auto-draft'){
				return false;
			}
			
			if(!$manual && $variation->post_status=='draft'){
				return false;
			}
			
			$variation_meta = get_post_meta($variation_id);
			
			$variation_data = array();
			$variation_data['wc_product_id'] = $variation->ID;
			
			$variation_data['name'] = $variation->post_title;
			
			$variation_data['description'] = $variation->post_content;
			$variation_data['short_description'] = $variation->post_excerpt;
			
			$variation_data['is_variation'] = true;
			
			$variation_data['manual'] = $manual;
			
			if(is_array($variation_meta) && count($variation_meta)){
				foreach ($variation_meta as $key => $value){
					$variation_data[$key] = ($value[0])?$value[0]:'';
				}
			}
			//$MSQS_QL->_p($variation_data);
			if(!$MSQS_QL->check_product_exists($variation_data)){
				return $mw_qbo_product_id = $MSQS_QL->AddProduct($variation_data);
			}else{
				/*
				if($manual){
					$MSQS_QL->save_log('Export Variation Error #'.$variation_data['wc_product_id'],'Variation already exists!','Product',0);
				}
				*/
				return $mw_qbo_product_id = $MSQS_QL->UpdateProduct($variation_data);
			}
		}
		
	}

	public function mw_qbo_wc_product_save($post_id='', $post=''){

		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;

		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}

		if(is_array($post_id)){
			$product_id = (int) $post_id['product_id'];
			$manual = true;
		}else{
			$product_id = (int) $post_id;
			$manual = false;
		}
		
		//$MSQS_QL->save_log('Export Product Test #'.$product_id,'Hook Testing','Product',2);return false;
		
		//if(!$manual){return false;}
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Product')){
			return false;
		}
		
		if($product_id){								
			$_product = wc_get_product( $product_id );
			if(!count($_product)){
				$MSQS_QL->save_log('Export Product Error #'.$product_id,'Woocommerce product not found. ','Product',0);
				return false;
			}
			
			//12-04-2017
			if($_product->post->post_status=='auto-draft'){
				return false;
			}
			//$MSQS_QL->_p($_product);
			
			if(!$manual && $_product->post->post_status=='draft'){
				return false;
			}
			
			$product_meta = get_post_meta($product_id);
			
			$product_data = array();
			$product_data['wc_product_id'] = $_product->id;
			
			$product_data['product_type'] = $_product->product_type;
			$product_data['total_stock'] = $_product->total_stock;
			
			$product_data['name'] = $_product->post->post_title;
			
			//01-06-2017
			$product_data['description'] = $_product->post->post_content;
			$product_data['short_description'] = $_product->post->post_excerpt;
			
			if(is_array($product_meta) && count($product_meta)){
				foreach ($product_meta as $key => $value){
					$product_data[$key] = ($value[0])?$value[0]:'';
				}
			}
			
			$product_data['manual'] = $manual;
			//$MSQS_QL->_p($product_data);
			if(!$MSQS_QL->check_product_exists($product_data)){
				return $mw_qbo_product_id = $MSQS_QL->AddProduct($product_data);
			}else{
				/*
				if($manual){
					$MSQS_QL->save_log('Export Product Error #'.$product_data['wc_product_id'],'Product already exists!','Product',0);
				}
				*/
				return $mw_qbo_product_id = $MSQS_QL->UpdateProduct($product_data);
			}			
		}
	}
	
	public function mw_qbo_wc_order_payment($pmnt_sync_info=''){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		//$MSQS_QL->_p($pmnt_sync_info,true);die;
		global $wpdb;
		$_transaction_id = '';
		$payment_id = '';
		if(is_array($pmnt_sync_info)){
			$payment_id = (int) $pmnt_sync_info['payment_id'];
			$_transaction_id = $MSQS_QL->get_field_by_val($wpdb->postmeta,'meta_value','meta_id',$payment_id);
			$order_id = (int) $MSQS_QL->get_field_by_val($wpdb->postmeta,'post_id','meta_id',$payment_id);
			$manual = true;
		}else{
			$order_id = (int) $pmnt_sync_info;
			$_transaction_id = get_post_meta($order_id, '_transaction_id', true );
			$manual = false;
		}
		
		if(!$manual){
			//$MSQS_QL->save_log('Payment Sync Debug','Hook Run','Payment',2);
		}		
		
		if($manual && !$payment_id){
			return false;
		}
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Payment')){
			return false;
		}
		
		$wc_inv_no = '';
		if($MSQS_QL->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $MSQS_QL->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
			$wc_inv_no = get_post_meta( $order_id, '_order_number_formatted', true );
		}
		
		$ord_id_num = ($wc_inv_no!='')?$wc_inv_no:$order_id;
		
		//21-06-2017		
		$_transaction_id_tmp = (!$_transaction_id)?'TXN-'.$order_id:$_transaction_id;
		
		if($_transaction_id_tmp){
			$payment_data = $MSQS_QL->wc_get_payment_details_by_txn_id($_transaction_id,$order_id);
			if(!count($payment_data)){
				if($manual){
					$MSQS_QL->save_log('Export Payment Error #'.$payment_id,'Woocommerce payment info not found!','Payment',0);
				}else{				
					$MSQS_QL->save_log('Export Payment Error for Order #'.$ord_id_num,'Woocommerce payment info not found!','Payment',0);					
				}
				return false;
			}
			
			if(!$MSQS_QL->check_payment_get_obj($payment_data)){
				$wc_cus_id = (int) $MSQS_QL->get_array_isset($payment_data,'customer_user',0);
				if($wc_cus_id){
					$customer_data = $this->mw_qbo_sync_customer_meta($wc_cus_id,$manual);					
				}else{					
					$customer_data = $this->get_customer_meta_from_order_meta($order_id,$manual);
				}
				//$MSQS_QL->_p($payment_data);$MSQS_QL->_p($customer_data);die;
				return $mw_qbo_payment_id = $MSQS_QL->AddPayment($payment_data,$customer_data);
			}else{
				if($manual){
					$MSQS_QL->save_log('Export Payment Error #'.$payment_data['payment_id'],'Payment already exists!','Payment',0);
				}
			}
		}else{			
			$MSQS_QL->save_log('Export Payment Error for Order #'.$ord_id_num,'Woocommerce Payment TXN ID Not Found','Payment',0);
		}
	}

	
	public function myworks_wc_qbo_sync_registration_realtime($cust_sync_info=''){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		if(is_array($cust_sync_info)){
			$user_id = (int) $cust_sync_info['user_id'];
			$manual = true;
		}else{
			$user_id = (int) $cust_sync_info;
			$manual = false;
		}
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Customer')){
			return false;
		}
		
		if($MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
			return false;
		}
		
		if($user_id){
						
			$user_info = get_userdata($user_id);
			if(is_array($user_info->roles) && in_array('customer',$user_info->roles)){
				$customer_data = $this->mw_qbo_sync_customer_meta($user_id,$manual);
				if(!$MSQS_QL->if_qbo_customer_exists($customer_data)){
					if($customer_data['firstname']!='' || $manual){
						return $mw_qbo_cust_id = $MSQS_QL->AddCustomer($customer_data);
					}
					if($manual){
						$MSQS_QL->save_log('Export Customer Error #'.$customer_data['wc_customerid'],'Customer name can\'t be empty','Customer',0);
					}
					
				}else{
					if($manual){
						//$MSQS_QL->save_log('Export Customer Error #'.$customer_data['wc_customerid'],'Customer already exists!','Customer',0);
						return $mw_qbo_cust_id = $MSQS_QL->UpdateCustomer($customer_data);
					}					
				}				
			}
		}
	}
	
	//11-04-2017
	public function mw_wc_qbo_sync_woocommerce_order_refunded($order_sync_info=0,$refund_id=0){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		
		if(!(int) $refund_id && !is_array($order_sync_info)){
			global $wpdb;
			$ID = (int) $order_sync_info;
			$rf_data = $wpdb->get_row("SELECT ID FROM `{$wpdb->posts}` WHERE `post_type` = 'shop_order_refund' AND `post_parent` = {$ID} ORDER BY ID DESC LIMIT 0,1 ");
			if(is_object($rf_data) && count($rf_data)){
				$refund_id = $rf_data->ID;
			}
		}
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		
		if(is_array($order_sync_info)){
			$order_id = (int) $order_sync_info['order_id'];
			$manual = true;
		}else{
			$order_id = (int) $order_sync_info;
			$manual = false;
		}
		$refund_id = (int) $refund_id;
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Refund')){
			return false;
		}
		//$MSQS_QL->_p($order_id);$MSQS_QL->_p($refund_id);
		if($order_id && $refund_id){
			$order = get_post($order_id);
			
			//13-06-2017
			$_payment_method = get_post_meta( $order_id, '_payment_method', true );
			$_order_currency = get_post_meta( $order_id, '_order_currency', true );
			if(!$MSQS_QL->if_sync_refund(array('_payment_method'=>$_payment_method,'_order_currency'=>$_order_currency))){				
				return false;
			}
			
			if(!is_object($order) || !count($order)){
				if($manual){
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$order_id,'Woocommerce order not found!','Refund',0);
				}
				return false;
			}
			if($order->post_type!='shop_order'){
				if($manual){					
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$order_id,'Woocommerce order is not valid.','Refund',0);
				}
				return false;
			}
			$refund_data = $MSQS_QL->get_wc_order_details_from_order($order_id,$order);
			if(!is_array($refund_data) || !count($refund_data)){
				if($manual){					
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$order_id,'Woocommerce order details not found.','Refund',0);
				}
				return false;
			}
			
			//07-06-2017
			$wc_inv_num = $MSQS_QL->get_array_isset($refund_data,'wc_inv_num','');
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$order_id;
			
			$refund_post = get_post($refund_id);
			if(!count($refund_post)){
				if($manual){
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'Woocommerce refund not found!','Refund',0);
				}
				return false;
			}
			
			$refund_meta = get_post_meta($refund_id);
			
			if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
				$qbo_salesreceipt_id = (int) $MSQS_QL->get_qbo_salesreceipt_id($order_id,$wc_inv_num);
		
				if(!$qbo_salesreceipt_id){
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'QuickBooks salesreceipt not found!','Refund',0);
					return false;
				}
			}else{
				$qbo_invoice_id = (int) $MSQS_QL->get_qbo_invoice_id($order_id,$wc_inv_num);
		
				if(!$qbo_invoice_id){
					$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'QuickBooks invoice not found!','Refund',0);
					return false;
				}
			}

			$refund_data['refund_id'] = $refund_id;
			
			$refund_data['refund_date'] = $refund_post->post_date;
			$refund_data['refund_post_parent'] = $refund_post->post_parent;
			$refund_data['refund_note'] = $refund_post->post_excerpt;
			
			$_refund_amount = isset($refund_meta['_refund_amount'][0])?$refund_meta['_refund_amount'][0]:0;
			if($_refund_amount<= 0){
				return false;
			}
			$refund_data['_refund_amount'] = $_refund_amount;
			
			$wc_cus_id = (int) $refund_data['wc_cus_id'];
			$qbo_customerid = 0;
			if($MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
				$qbo_customerid = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_orders_to_specific_cust');
			}else{
				$customer_data = $this->mw_qbo_sync_customer_meta($wc_cus_id,$manual);				
				$get_cd_from_om = true;
				if(is_array($customer_data) && isset($customer_data['firstname']) && $customer_data['firstname']!=''){
					$get_cd_from_om = false;
				}
				
				if($get_cd_from_om){
					$customer_data = $this->get_customer_meta_from_order_meta($order_id,$manual);
				}
				
				if(!$wc_cus_id){
					$customer_data['wc_inv_id'] = (int) $order_id;
					$qbo_customerid = (int) $MSQS_QL->check_save_get_qbo_guest_id($customer_data);
				}else{
					$qbo_customerid = (int) $MSQS_QL->check_save_get_qbo_customer_id($customer_data);
				}
			}
			
			if(!$qbo_customerid){
				$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'Quickbooks customer not found!','Refund',0);
				return false;
			}
			$refund_data['qbo_customerid'] = $qbo_customerid;
			
			//
			if(is_array($refund_data['qbo_inv_items'])){
				$refund_data['qbo_inv_items'] = array_filter($refund_data['qbo_inv_items']);
			}
			if(empty($refund_data['qbo_inv_items'])){
				$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'Order QBO mapped items not found!','Refund',0);
				return false;
			}
			
			$refund_data['manual'] = $manual;
			//$MSQS_QL->_p($refund_data);return false;
			
			if(!$MSQS_QL->if_refund_exists($refund_data)){
				return $mw_qbo_inv_id = $MSQS_QL->AddRefund($refund_data);
			}else{
				if($manual){
					//$MSQS_QL->save_log('Export Refund Error #'.$refund_id.' Order #'.$ord_id_num,'Refund already exists in QuickBooks.','Refund',0);		
				}
				return false;
			}
		}
	}
	
	//25-04-2017
	public function myworks_wc_qbo_sync_product_category_realtime($category_sync_info='',$tt_id=0){	
		//die("I'm here!");
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		//27-04-2017
		if(!$MSQS_QL->get_qbo_company_info('is_category_enabled')){
			return false;
		}
		
		if(is_array($category_sync_info)){
			$category_id = (int) $category_sync_info['category_id'];
			$manual = true;
		}else{
			$category_id = (int) $category_sync_info;
			$manual = false;
		}
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Category')){
			return false;
		}
		
		if($category_id){
			$cat_data = (array) get_term($category_id,'product_cat');
			//$MSQS_QL->_p($cat_data);
			if(is_array($cat_data) && count($cat_data)){
				$cat_data['manual'] = $manual;
				if(!$MSQS_QL->check_category_exists($cat_data)){
					return $mw_qbo_category_id = $MSQS_QL->AddCategory($cat_data);
				}else{
					if($manual){						
						//$MSQS_QL->save_log('Export Category Error #'.$category_id,'Category already exists!','Category',0);
						return $mw_qbo_category_id = $MSQS_QL->UpdateCategory($cat_data);
					}
				}
			}
		}
	}
	
	//11-04-2017
	public function myworks_wc_qbo_sync_order_realtime($order_sync_info=''){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {
			return false;
		}
		if(is_array($order_sync_info)){
			$order_id = (int) $order_sync_info['order_id'];
			$manual = true;
		}else{
			$order_id = (int) $order_sync_info;
			$manual = false;
		}
		
		/*
		if(!$manual){
			$MSQS_QL->save_log('Hook Call','Order Sync Hook #'.$order_id,'Test',2);
			return false;
		}
		*/
		
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Invoice')){
			return false;
		}
		
		if($order_id){
			$order = get_post($order_id);
			if(!is_object($order) || !count($order)){
				if($manual){
					$MSQS_QL->save_log('Export Order Error #'.$order_id,'Woocommerce order not found!','Invoice',0);
				}
				return false;
			}
			if($order->post_type!='shop_order'){
				if($manual){
					$MSQS_QL->save_log('Export Order Error #'.$order_id,'Woocommerce order is not valid.','Invoice',0);
				}
				return false;
			}
			
			//25-04-2017
			if($order->post_status=='auto-draft'){
				return false;
			}
			
			if(!$manual && $order->post_status=='draft'){
				return false;
			}
			
			$is_os_err = false; $is_os_p_sync = false;
			//24-04-2017
			$only_sync_status = $MSQS_QL->get_option('mw_wc_qbo_sync_specific_order_status');
			if(!$manual && $only_sync_status!='' && $only_sync_status!=$order->post_status){				
				//return false;
				$is_os_err = true;
			}
			
			$invoice_data = $MSQS_QL->get_wc_order_details_from_order($order_id,$order);
			
			$wc_inv_num = $MSQS_QL->get_array_isset($invoice_data,'wc_inv_num','');
			$ord_id_num = ($wc_inv_num!='')?$wc_inv_num:$order_id;
			
			//04-05-2017 : 14-06-2017
			if(!is_array($invoice_data) || !isset($invoice_data['_order_key']) || !isset($invoice_data['_order_total'])){
				if($manual && !$is_os_err){
					$MSQS_QL->save_log('Export Order Error #'.$ord_id_num,'Woocommerce order details not found.','Invoice',0);
				}
				if($is_os_p_sync){
					$MSQS_QL->save_log('Export Payment Error Order #'.$ord_id_num,'Woocommerce order details not found.','Payment',0);
				}
				return false;
			}
			
			//19-05-2017 //Prevent Multiple Realtime call
			if(!$manual && !is_admin()){
				if((int) $MSQS_QL->get_session_val('current_rt_order_id_'.$order_id,0)==$order_id){
					return false;
				}
				$MSQS_QL->set_session_val('current_rt_order_id_'.$order_id,$order_id);
			}
			//$MSQS_QL->save_log('Hook Call','Order Sync Hook #'.$ord_id_num,'Test',2);
			
			$is_os_p_sync = $MSQS_QL->if_sync_os_payment($invoice_data);
			
			$wc_cus_id = (int) $invoice_data['wc_cus_id'];
			
			$qbo_customerid = 0;
			if($MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
				$qbo_customerid = (int) $MSQS_QL->get_option('mw_wc_qbo_sync_orders_to_specific_cust');
			}else{
				$customer_data = $this->mw_qbo_sync_customer_meta($wc_cus_id,$manual);
				//$MSQS_QL->_p($customer_data);die;
				$get_cd_from_om = true;
				if(is_array($customer_data) && isset($customer_data['firstname']) && $customer_data['firstname']!=''){
					$get_cd_from_om = false;
				}
				
				if($get_cd_from_om){							
					$customer_data = $this->get_customer_meta_from_order_meta($order_id,$manual);
				}
				//$MSQS_QL->_p($customer_data);die;
				if(!$wc_cus_id){					
					$customer_data['wc_inv_id'] = (int) $order_id;
					$qbo_customerid = (int) $MSQS_QL->check_save_get_qbo_guest_id($customer_data);
				}else{
					$qbo_customerid = (int) $MSQS_QL->check_save_get_qbo_customer_id($customer_data);
				}
			}			
			
			if(!$qbo_customerid){		
				if(!$is_os_err){					
					$MSQS_QL->save_log('Export Order Error #'.$ord_id_num,'Quickbooks customer not found!','Invoice',0);
				}else{
					if($is_os_p_sync){
						$MSQS_QL->save_log('Export Payment Error Order #'.$ord_id_num,'Quickbooks customer not found!','Payment',0);
					}					
				}			
				return false;
			}
			$invoice_data['qbo_customerid'] = $qbo_customerid;
			//
			if(is_array($invoice_data['qbo_inv_items'])){
				$invoice_data['qbo_inv_items'] = array_filter($invoice_data['qbo_inv_items']);
			}
			
			if(empty($invoice_data['qbo_inv_items'])){
				if(!$is_os_err){
					$MSQS_QL->save_log('Export Order Error #'.$ord_id_num,'Order QBO mapped items not found!','Invoice',0);
				}else{
					if($is_os_p_sync){
						$MSQS_QL->save_log('Export Payment Error Order #'.$ord_id_num,'Order QBO mapped items not found!','Payment',0);
					}					
				}				
				return false;
			}
			
			$invoice_data['manual'] = $manual;
			
			
			//$MSQS_QL->_p($invoice_data);return false;
			if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
				if($is_os_err){return false;}
				if(!$MSQS_QL->check_quickbooks_salesreceipt_get_obj($invoice_data['wc_inv_id'],$invoice_data['wc_inv_num'])){
					$mw_qbo_sr_id = $MSQS_QL->AddSalesReceipt($invoice_data);
					//Action - 13-06-2017
					$ext_acd = array('order_sync_as'=>'salesreceipt','qbo_salesreceipt_id'=>$mw_qbo_sr_id);
					do_action('mw_wc_qbo_sync_order_sync_after_action',$ext_acd,$invoice_data);
					return $mw_qbo_sr_id;
				}else{
					if($manual){
						$mw_qbo_sr_id = $MSQS_QL->UpdateSalesReceipt($invoice_data);
						return $mw_qbo_sr_id;
					}					
				}
				
			}else{
				if(!$MSQS_QL->check_quickbooks_invoice_get_obj($invoice_data['wc_inv_id'],$invoice_data['wc_inv_num'])){
					if($is_os_err){return false;}
					
					$mw_qbo_inv_id = $MSQS_QL->AddInvoice($invoice_data);
					//Add Payment Based On Order Status
					if($is_os_p_sync){
						$MSQS_QL->PushOsPayment($invoice_data);
					}
					//Action
					$ext_acd = array('order_sync_as'=>'invoice','qbo_invoice_id'=>$mw_qbo_inv_id);
					do_action('mw_wc_qbo_sync_order_sync_after_action',$ext_acd,$invoice_data);
					return $mw_qbo_inv_id;
				}else{				
					if($manual){
						if(!$is_os_err){
							$mw_qbo_inv_id = $MSQS_QL->UpdateInvoice($invoice_data);
						}						
						if($is_os_p_sync){
							$MSQS_QL->PushOsPayment($invoice_data);
						}
						if(!$is_os_err){
							return $mw_qbo_inv_id;
						}						
					}else{
						if($is_os_p_sync){
							$MSQS_QL->PushOsPayment($invoice_data);
						}
					}					
				}
				
			}
			
		}
	}
	//10-05-2017
	public function myworks_wc_qbo_sync_update_stock($instance){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		//$MSQS_QL->_p($instance);
		if(!is_admin()){
			return false;
		}
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {			
			return false;
		}
		$manual = false;
		//Check If Real Time Enabled
		if(!$manual && !$MSQS_QL->check_if_real_time_push_enable_for_item('Inventory')){			
			return false;
		}
		
		if(!is_object($instance) || !count($instance)){			
			return false;
		}		
		
		$product_id = (int) $instance->get_id();
		//$MSQS_QL->_p($product_id);die;
		if($product_id){
			$inventory_data = array();
			$inventory_data['wc_inventory_id'] = $product_id;			
			$MSQS_QL->UpdateQboInventory($inventory_data);
		}
	}
	
	//06-06-2017
	public function myworks_wc_qbo_sync_variation_update_stock($instance){
		if(!class_exists('WooCommerce')) return;
		global $MSQS_QL;
		
		if(!is_admin()){
			return false;
		}
		
		if ($MSQS_QL->get_option('mw_qbo_sync_activation_redirect')!='true') {			
			return false;
		}
		$manual = false;
		
		if(!is_object($instance) || !count($instance)){			
			return false;
		}		
		
		$variation_id = (int) $instance->get_id();
		if($variation_id){
			$inventory_data = array();
			$inventory_data['wc_inventory_id'] = $variation_id;	
			$inventory_data['is_variation'] = true;
			$MSQS_QL->VariationUpdateQboInventory($inventory_data);
		}
		
	}
}
/*Class End*/