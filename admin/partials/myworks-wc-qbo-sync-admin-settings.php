<?php
if ( ! defined( 'ABSPATH' ) )
     exit;

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/admin/partials
 */
?>
<?php
global $MSQS_QL;
global $MWQS_OF;

global $wpdb;

$page_url = 'admin.php?page=myworks-wc-qbo-sync-settings';
$selected_tab = (isset($_GET['selected_tab']))?$MSQS_QL->sanitize($_GET['selected_tab']):'';

$save_status = '';
if(isset($_POST['mw_wc_qbo_sync_settings']) && check_admin_referer( 'myworks_wc_qbo_sync_save_settings', 'map_wc_qbo_update_settings' )){
	MyWorks_WC_QBO_Sync_Admin::admin_settings_save($MWQS_OF->get_plugin_settings_post_data(),1);
	$save_status = 'admin-success-green';
	$MSQS_QL->set_session_val('settings_save_class',$save_status);
	$MSQS_QL->set_session_val('settings_current_tab',isset($_POST['mw_qbo_sybc_settings_current_tab'])?$_POST['mw_qbo_sybc_settings_current_tab']:'mw_qbo_sybc_settings_tab_one');
	
	$MSQS_QL->redirect($page_url);
}
$save_status = $MSQS_QL->get_session_val('settings_save_class','',true);
$settings_current_tab = $MSQS_QL->get_session_val('settings_current_tab','mw_qbo_sybc_settings_tab_one',true);

$option_keys = $MWQS_OF->get_plugin_option_keys();

$admin_settings_data = $MSQS_QL->get_all_options($option_keys);

/*
$admin_settings_data = MyWorks_WC_QBO_Sync_Admin::admin_settings_get($option_keys,1);
*/
$mw_qbo_product_list = '';
if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
	$mw_qbo_product_list = $MSQS_QL->get_product_dropdown_list('');
}

//
$qbo_customer_options = '';
if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){	
	$cdd_sb = 'dname';
	$mw_wc_qbo_sync_client_sort_order = $MSQS_QL->sanitize($MSQS_QL->get_option('mw_wc_qbo_sync_client_sort_order'));
	if($mw_wc_qbo_sync_client_sort_order!=''){
		$cdd_sb = $mw_wc_qbo_sync_client_sort_order;
		if($cdd_sb!='dname' && $cdd_sb!='first' && $cdd_sb!='last' && $cdd_sb!='company'){
			$cdd_sb = 'dname';
		}
	}
	$qbo_customer_options = $MSQS_QL->option_html('', $wpdb->prefix.'mw_wc_qbo_sync_qbo_customers','qbo_customerid','dname','',$cdd_sb.' ASC','',true);
}

$get_account_dropdown_list = $MSQS_QL->get_account_dropdown_list('',true);

$list_selected = '';
if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
	$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_qbo_item\').val('.$admin_settings_data['mw_wc_qbo_sync_default_qbo_item'].');';
	$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_coupon_code\').val('.$admin_settings_data['mw_wc_qbo_sync_default_coupon_code'].');';
	$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_shipping_product\').val('.$admin_settings_data['mw_wc_qbo_sync_default_shipping_product'].');';
	$list_selected.='jQuery(\'#mw_wc_qbo_sync_orders_to_specific_cust\').val('.$admin_settings_data['mw_wc_qbo_sync_orders_to_specific_cust'].');';
}

$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_qbo_product_account\').val('.$admin_settings_data['mw_wc_qbo_sync_default_qbo_product_account'].');';
$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_qbo_asset_account\').val('.$admin_settings_data['mw_wc_qbo_sync_default_qbo_asset_account'].');';
$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_qbo_expense_account\').val('.$admin_settings_data['mw_wc_qbo_sync_default_qbo_expense_account'].');';
$list_selected.='jQuery(\'#mw_wc_qbo_sync_default_qbo_discount_account\').val('.$admin_settings_data['mw_wc_qbo_sync_default_qbo_discount_account'].');';

// PAGE SCRIPTS AND STYLES
MyWorks_WC_QBO_Sync_Admin::get_settings_assets(1);
MyWorks_WC_QBO_Sync_Admin::is_trial_version_check();

//19-06-2017
$order_statuses = wc_get_order_statuses();
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="mw_wc_qbo_sync_container">
<form method="post">
<?php wp_nonce_field( 'myworks_wc_qbo_sync_save_settings', 'map_wc_qbo_update_settings' ); ?>
<input type="hidden" name="mw_qbo_sybc_settings_current_tab" id="mw_qbo_sybc_settings_current_tab" value="<?php echo $settings_current_tab; ?>">
<nav class="mw-qbo-sync-grey">
	<div class="nav-wrapper">
		<a class="brand-logo left" href="javascript:void(0)">
			<img src="<?php echo plugins_url( 'myworks-wc-qbo-sync/admin/image/mwd-logo.png' ) ?>">
		</a>
		<ul class="hide-on-med-and-down right">
			<li class="default-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_one"><?php echo __('Default','mw_wc_qbo_sync') ?></a></li>
			<li class="invoice-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_two"><?php echo __('Order','mw_wc_qbo_sync') ?></a></li>
			<?php /* <li class="payment-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_three"><?php echo __('Payment','mw_wc_qbo_sync') ?></a></li> */ ?>
			<li class="tax-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_four"><?php echo __('Tax','mw_wc_qbo_sync') ?></a></li>
			<li class="mapping-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_five"><?php echo __('Mapping','mw_wc_qbo_sync') ?></a></li>
			<li class="pull-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_six"><?php echo __('Pull','mw_wc_qbo_sync') ?></a></li>
			
			<li class="webhook-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_wh"><?php echo __('Real Time Sync','mw_wc_qbo_sync') ?></a></li>
			
			<li style="display:none; class="dis-icon mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_seven"><?php echo __('Disable','mw_wc_qbo_sync') ?></a></li>
			<li style="display:none;" class="adv-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_eight"><?php echo __('Advanced','mw_wc_qbo_sync') ?></a></li>
			<li class="misc-menu mwqs_stb"><a href="javascript:void(0)" id="mw_qbo_sybc_settings_tab_nine"><?php echo __('Miscellaneous','mw_wc_qbo_sync') ?></a></li>			
		</ul>
	</div>
</nav>

<div class="container" id="mw_qbo_sybc_settings_tables">
	<div class="card">
		<div class="card-content">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="row">
						<div class="col s12 m12 l12">
                          	<div id="mw_qbo_sybc_settings_tab_one_body" style="display: none;">
							<h6><?php echo __('Default Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body_body">
							<tbody>                				
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO Product','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<?php
														$dd_options = '<option value=""></option>';
														$dd_ext_class = '';
														if($MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
															$dd_ext_class = 'mwqs_dynamic_select';
															if((int) $admin_settings_data['mw_wc_qbo_sync_default_qbo_item']){
																$itemid = (int) $admin_settings_data['mw_wc_qbo_sync_default_qbo_item'];
																$qb_item_name = $MSQS_QL->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','name','itemid',$itemid);
																if($qb_item_name!=''){
																	$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
																}
															}
														}else{
															$dd_options.=$mw_qbo_product_list;
														}
													?>
													<select name="mw_wc_qbo_sync_default_qbo_item" id="mw_wc_qbo_sync_default_qbo_item" class="filled-in production-option mw_wc_qbo_sync_select <?php echo $dd_ext_class;?>">
														<?php echo $dd_options;?>
													</select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
									  <span class="tooltiptext"><?php echo __('Default QuickBooks Online Product assigned to line items not mapped to a product.','mw_wc_qbo_sync') ?></span>
									</div>
                                    </td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO Sales Account for New Products ','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_default_qbo_product_account" id="mw_wc_qbo_sync_default_qbo_product_account" class="filled-in production-option mw_wc_qbo_sync_select">
													<option value=""></option>
										            <?php echo $get_account_dropdown_list ?>
										            </select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Default account assigned to your WooCommerce products when pushing them over to QBO. This should be an income or expense account.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO Inventory Asset Account for New Products','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_default_qbo_asset_account" id="mw_wc_qbo_sync_default_qbo_asset_account" class="filled-in production-option mw_wc_qbo_sync_select">
													<option value=""></option>
										            <?php echo $get_account_dropdown_list ?>
										            </select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Default inventory asset account assigned to your WooCommerce products when pushing them over to QBO.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO COGS Account for New Products','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_default_qbo_expense_account" id="mw_wc_qbo_sync_default_qbo_expense_account" class="filled-in production-option mw_wc_qbo_sync_select">
													<option value=""></option>
										            <?php echo $get_account_dropdown_list ?>
										            </select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Default Cost of Goods Sold account assigned to your WooCommerce products when pushing them over to QBO.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO Discount Account for New Products','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_default_qbo_discount_account" id="mw_wc_qbo_sync_default_qbo_discount_account" class="filled-in production-option mw_wc_qbo_sync_select">
													<option value=""></option>
										            <?php echo $get_account_dropdown_list ?>
										            </select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Default Income Account in QuickBooks Online for unmapped Discounts in WooCommerce.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
								
								<tr <?php //if($MSQS_QL->get_qbo_company_setting('is_discount_allowed')){echo 'style="display:none;"';}?>>
									<th class="title-description">
								    	<?php echo __('Default QBO Coupon Code Product','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<?php
														$dd_options = '<option value=""></option>';
														$dd_ext_class = '';
														if($MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
															$dd_ext_class = 'mwqs_dynamic_select';
															if((int) $admin_settings_data['mw_wc_qbo_sync_default_coupon_code']){
																$itemid = (int) $admin_settings_data['mw_wc_qbo_sync_default_coupon_code'];
																$qb_item_name = $MSQS_QL->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','name','itemid',$itemid);
																if($qb_item_name!=''){
																	$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
																}
															}
														}else{
															$dd_options.=$mw_qbo_product_list;
														}
													?>
													
													<select name="mw_wc_qbo_sync_default_coupon_code" id="mw_wc_qbo_sync_default_coupon_code" class="filled-in production-option mw_wc_qbo_sync_select <?php echo $dd_ext_class;?>">
														<?php echo $dd_options;?>
													</select>
													
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose a QuickBooks Online Product to fallback to in invoice line items for unmapped Coupon Codes.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
								<tr <?php if($MSQS_QL->get_qbo_company_setting('is_shipping_allowed')){echo 'style="display:none;"';}?>>
									<th class="title-description">
								    	<?php echo __('Default QBO Shipping Product','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<?php
														$dd_options = '<option value=""></option>';
														$dd_ext_class = '';
														if($MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
															$dd_ext_class = 'mwqs_dynamic_select';
															if((int) $admin_settings_data['mw_wc_qbo_sync_default_shipping_product']){
																$itemid = (int) $admin_settings_data['mw_wc_qbo_sync_default_shipping_product'];
																$qb_item_name = $MSQS_QL->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_items','name','itemid',$itemid);
																if($qb_item_name!=''){
																	$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
																}
															}
														}else{
															$dd_options.=$mw_qbo_product_list;
														}
													?>													
													
													<select name="mw_wc_qbo_sync_default_shipping_product" id="mw_wc_qbo_sync_default_shipping_product" class="filled-in production-option mw_wc_qbo_sync_select <?php echo $dd_ext_class;?>">
														<?php echo $dd_options;?>
													</select>
													
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose a QuickBooks Online Product to fallback to for unmapped Shipping Methods.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>

								<tr>
									<th class="title-description">
								    	<?php echo __('Enable currencies for your WooCommerce store','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_store_currency[]" id="mw_wc_qbo_sync_store_currency" class="filled-in production-option mw_wc_qbo_sync_select mqs_multi" multiple="multiple">
													<option value=""></option>
													<?php 
														$sel_cur_list = $admin_settings_data['mw_wc_qbo_sync_store_currency'];
														if($sel_cur_list!=''){
															$sel_cur_list = explode(',',$sel_cur_list);
														}
													?>
										            <?php $MSQS_QL->only_option($sel_cur_list,$MSQS_QL->get_world_currency_list()) ?>
										            </select>
												</p>
											</div>
										</div>
									</td>
                                    <td>
                                        <div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Select currencies for your WooCommerce store. You can select multiple currency as per requirement.','mw_wc_qbo_sync') ?></span>
										</div>
                                    </td>
								</tr>
								
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_two_body" style="display: none;">
							<h6><?php echo __('Order Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body_body">
							<tbody>
								<tr>
									<th class="title-description">
								    	<?php echo __('Sync WooCommerce Orders as Sales Receipts','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_order_as_sales_receipt" id="mw_wc_qbo_sync_order_as_sales_receipt" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_order_as_sales_receipt']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Turn on to sync WooCommerce orders as Sales Receipts into QuickBooks Online. Otherwise, they will be synced as an Invoice + Payment.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Do not sync Orders before ID','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="text" name="mw_wc_qbo_sync_invoice_min_id" id="mw_wc_qbo_sync_invoice_min_id" value="<?php echo $admin_settings_data['mw_wc_qbo_sync_invoice_min_id'] ?>">
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Disable syncing WooCommerce orders before this Order ID to QuickBooks Online. Default is 0 as previous orders will not be synced anyways unless edited and saved.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Do not Sync $0 Invoices','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_null_invoice" id="mw_wc_qbo_sync_null_invoice" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_null_invoice']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Select to disable the real-time syncing of invoices with a $0 total to QuickBooks Online.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Sync Notes Into Custom Field','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_invoice_notes" id="mw_wc_qbo_sync_invoice_notes" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_invoice_notes']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Sync WooCommerce Invoice Note into QuickBooks Online custom field.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('QBO Custom Field ID for Note','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="text" name="mw_wc_qbo_sync_invoice_note_id" id="mw_wc_qbo_sync_invoice_note_id" value="<?php echo $admin_settings_data['mw_wc_qbo_sync_invoice_note_id'] ?>">
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Select the ID of your QuickBooks Custom Invoice Field for WooCommerce Note above.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('QBO Custom Field Name for Note','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="text" name="mw_wc_qbo_sync_invoice_note_name" id="mw_wc_qbo_sync_invoice_note_name" value="<?php echo $admin_settings_data['mw_wc_qbo_sync_invoice_note_name'] ?>">
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Enter the Name of your QuickBooks Custom Invoice Field for WooCommerce Note','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
                				<tr>
									<th class="title-description">
								    	<?php echo __('Mark Cancelled Orders in QBO','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_invoice_cancelled" id="mw_wc_qbo_sync_invoice_cancelled" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_invoice_cancelled']=='true') echo 'checked' ?>>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to mark orders as void in QBO when cancelled in WooCommerce. Works in real-time, not applicable to historical orders.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Sync Order Notes to Statement Memo','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_invoice_memo" id="mw_wc_qbo_sync_invoice_memo" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_invoice_memo']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable the syncing of the WooCommerce Order Note contents to the QBO Statement Memo field.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Copy First Line Desc to Statement Memo','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_invoice_memo_statement" id="mw_wc_qbo_sync_invoice_memo_statement" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_invoice_memo_statement']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable the syncing of the WooCommerce first order line item description contents to the QBO Statement Memo field.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr>
									<th class="title-description">
								    	<?php echo __('Sync WooCommerce Order Date to QBO Service Date','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_invoice_date" id="mw_wc_qbo_sync_invoice_date" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_invoice_date']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable the syncing of the WooCommerce Order Date or Due Date to the QuickBooks Online service date field in the invoice.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr>
									<th class="title-description">
								    	<?php _e('Only sync orders if they are a specific status','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<select name="mw_wc_qbo_sync_specific_order_status" id="mw_wc_qbo_sync_specific_order_status" class="filled-in production-option mw_wc_qbo_sync_select">
														<option value="">All Status</option>
														<?php echo  $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_specific_order_status'],$order_statuses);?>
													</select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Only sync orders (real time) if they are a specific status (Processing, On Hold, Completed, Cancelled etc). This is applicable if above option is enabled.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_three_body" style="display: none;">
							<h6><?php echo __('Mapping Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_four_body" style="display: none;">
							<h6><?php echo __('Tax Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
                				<tr>
									<th class="title-description">
								    	<?php echo __('QuickBooks Tax 0% Rule','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<select name="mw_wc_qbo_sync_tax_rule" id="mw_wc_qbo_sync_tax_rule" class=" mw_wc_qbo_sync_select">
									            <option value=""></option>
												<?php echo $MSQS_QL->get_tax_code_dropdown_list($admin_settings_data['mw_wc_qbo_sync_tax_rule']);?>
									            </select>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose your QBO Tax rule with 0% tax for non-taxable items.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('QuickBooks Tax/Price Format','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_tax_format" id="mw_wc_qbo_sync_tax_format" class="filled-in production-option mw_wc_qbo_sync_select">
										            <option value=""></option>
													<?php $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_tax_format'],$MSQS_QL->tax_format)?>
										            </select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose whether your tax setup is Inclusive - prices already include the tax, or Exclusive - taxes are additionally added on.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_five_body" style="display: none;">
							<h6><?php echo __('Mapping Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
                				<tr>
									<th class="title-description">
								    	<?php echo __('Append Client ID If Duplicate Client','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_append_client" id="mw_wc_qbo_sync_append_client" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_append_client']=='true') echo 'checked' ?>>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Append the WooCommerce Client ID to the QuickBooks Online Display Name if the Client already exists in QuickBooks Online. Prevents errors from occuring when a duplicate client is being synced.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Default QBO Display Name','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<textarea name="mw_wc_qbo_sync_display_name_pattern" id="mw_wc_qbo_sync_display_name_pattern"><?php if(isset($admin_settings_data['mw_wc_qbo_sync_display_name_pattern'])) echo $admin_settings_data['mw_wc_qbo_sync_display_name_pattern']; else '{firstname} {lastname} - {id}'; ?></textarea>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose the WooCommerce client name values you would like to be assigned to the QBO "Display Name As" client field. This setting will determine the value in the QuickBooks Online Display Name for clients synced over. Choose either first/last name OR Company name - not both.<br><b>Available Tags: {firstname} , {lastname} , {companyname} , {id} ,{email}</b>','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Client Dropdown Sort Order','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_client_sort_order" id="mw_wc_qbo_sync_client_sort_order" class="filled-in production-option mw_wc_qbo_sync_select">
										            <option value=""></option>
													<?php $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_client_sort_order'],$MSQS_QL->client_dropdown_sort_order)?>
										            </select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose the sort order for QuickBooks Online clients names. It will be applied in the QuickBooks Online client dropdown in the client mapping page.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Use Shipping Company For Mapping and Syncing Customer','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_customer_qbo_check_ship_addr" id="mw_wc_qbo_sync_customer_qbo_check_ship_addr" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_customer_qbo_check_ship_addr']=='true') echo 'checked' ?>>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Use shipping company for customer sync','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Check Mapped Customer Directly From QuickBooks Online','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_customer_qbo_check" id="mw_wc_qbo_sync_customer_qbo_check" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_customer_qbo_check']=='true') echo 'checked' ?>>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check customer on QuickBooks by email if no record in local server.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr>
									<th class="title-description">
								    	<?php _e('Enable Map and sync all orders to a specific QuickBooks Online customer?','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_orders_to_specific_cust_opt" id="mw_wc_qbo_sync_orders_to_specific_cust_opt" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_orders_to_specific_cust_opt']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check if you want to select a specific customer on QuickBooks to map and sync all orders.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr>
									<th class="title-description">
								    	<?php _e('Choose Sync All QuickBooks Customer','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<?php
														$dd_options = '<option value=""></option>';
														$dd_ext_class = '';
														if($MSQS_QL->option_checked('mw_wc_qbo_sync_select2_ajax')){
															$dd_ext_class = 'mwqs_dynamic_select';
															if((int) $admin_settings_data['mw_wc_qbo_sync_orders_to_specific_cust']){
																$itemid = (int) $admin_settings_data['mw_wc_qbo_sync_orders_to_specific_cust'];
																$qb_item_name = $MSQS_QL->get_field_by_val($wpdb->prefix.'mw_wc_qbo_sync_qbo_customers','dname','qbo_customerid',$itemid);
																if($qb_item_name!=''){
																	$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
																}
															}
														}else{
															$dd_options.=$qbo_customer_options;
														}
													?>
													
													<select name="mw_wc_qbo_sync_orders_to_specific_cust" id="mw_wc_qbo_sync_orders_to_specific_cust" class="filled-in production-option mw_wc_qbo_sync_select <?php echo $dd_ext_class;?>">
														<?php echo $dd_options;?>
													</select>
													
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Select a specific customer on QuickBooks to map and sync all orders. Only applicable if above setting is enabled.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr style="display:none;">
									<th class="title-description">
								    	<?php echo __('Use Email For Client Check','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_client_check_email" id="mw_wc_qbo_sync_client_check_email" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_client_check_email']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to use email along with other fields for check if client exists or for automap client.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_six_body" style="display: none;">
							<h6><?php echo __('Pull Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								<tr>
									<th class="title-description">
								    	<?php echo __('Show Pull Page Tab','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_pull_enable" id="mw_wc_qbo_sync_pull_enable" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_pull_enable']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable the Customer, Order, Product & Payment Pull pages. This will enable you to use the manual pull pages to manually pull data into WooCommerce from QuickBooks Online.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Pulled WooCommerce Product Status','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_product_pull_wc_status" id="mw_wc_qbo_sync_product_pull_wc_status" class="filled-in production-option mw_wc_qbo_sync_select">
										            <option value=""></option>
													<?php $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_product_pull_wc_status'],$MSQS_QL->product_pull_status)?>
										            </select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose the product status that products inherit when they are first pulled in WooCommerce.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Pull Product Sales Description Into','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_product_pull_desc_field" id="mw_wc_qbo_sync_product_pull_desc_field" class="filled-in production-option mw_wc_qbo_sync_select">
										            <option value=""></option>
													<?php $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_product_pull_desc_field'],$MSQS_QL->product_pull_desc_fields)?>
										            </select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose field for QuickBooks products sales description pulled into WooCommerce.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php _e('Prevent Payment Pull For These Order Statues','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<select name="mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses[]" id="mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses" class="filled-in production-option mw_wc_qbo_sync_select mqs_multi" multiple="multiple">
														<?php 
															$mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses = $admin_settings_data['mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses'];
															if($mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses!=''){
																$mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses = explode(',',$mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses);
															}
														?>
														<?php echo  $MSQS_QL->only_option($mw_wc_qbo_sync_pmnt_pull_prevent_order_statuses,$order_statuses);?>
													</select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Prevent Payment Pull For These Order Statues','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php _e('Payment Pull Order Status','mw_wc_qbo_sync') ?>
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>													
													<select name="mw_wc_qbo_sync_pmnt_pull_order_status" id="mw_wc_qbo_sync_pmnt_pull_order_status" class="filled-in production-option mw_wc_qbo_sync_select">
														<option value=""></option>
														<?php echo  $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_pmnt_pull_order_status'],$order_statuses);?>
													</select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Payment Pull Order Status','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<?php /*
								<tr>
									<th class="title-description">
								    	<?php echo __('Client Auto-Pull','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_auto_pull_client" id="mw_wc_qbo_sync_auto_pull_client" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_auto_pull_client']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable automatically pulling in new QuickBooks Online clients in the auto-pull cron job.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Invoice Auto-Pull','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_auto_pull_invoice" id="mw_wc_qbo_sync_auto_pull_invoice" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_auto_pull_invoice']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable automatically pulling in new QuickBooks Online invoices in the auto-pull cron job.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Payment Auto-Pull','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_auto_pull_payment" id="mw_wc_qbo_sync_auto_pull_payment" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_auto_pull_payment']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable automatically pulling in new QuickBooks Online invoice payments in the auto-pull cron job.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Limit AutoPull Data to Existing WooCommerce Clients','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_auto_pull_limit" id="mw_wc_qbo_sync_auto_pull_limit" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_auto_pull_limit']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check this box to only AutoPull in new data from QuickBooks Online for clients that already exist in WooCommerce. For this setting to correctly work, the "Client Auto-Pull" setting above must be OFF.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('AutoPull Interval','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<input type="text" name="mw_wc_qbo_sync_auto_pull_interval" id="mw_wc_qbo_sync_auto_pull_interval" value="<?php echo $admin_settings_data['mw_wc_qbo_sync_auto_pull_interval'] ?>">
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Please enter auto pull interval in minutes','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								*/ ?>
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_wh_body" style="display: none;">
							<h6><?php echo __('RealTime Sync Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								<tr class="wc_qb_tr">
									<td colspan="3" height="50">
										<b><?php echo __('WooCommerce -> QuickBooks Online','mw_wc_qbo_sync') ?></b>
										
									</td>									
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Enable RealTime Sync','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_rt_push_enable" id="mw_wc_qbo_sync_rt_push_enable" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_rt_push_enable']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable realtime changes into QuickBooks Online from WooCommerce.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Data Types','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<!--
													<select multiple="multiple" name="mw_wc_qbo_sync_rt_push_items[]" id="mw_wc_qbo_sync_rt_push_items" class="filled-in production-option mw_wc_qbo_sync_select mqs_multi">
										            <option value=""></option>
													<?php //$MSQS_QL->only_option(explode(',',$admin_settings_data['mw_wc_qbo_sync_rt_push_items']),$MSQS_QL->qbo_rt_push_items)?>
													</select>
													-->
													
													<?php if(is_array($MSQS_QL->qbo_rt_push_items) && count($MSQS_QL->qbo_rt_push_items)):?>
													<?php $rpi_val_arr = explode(',',$admin_settings_data['mw_wc_qbo_sync_rt_push_items']);?>
													<?php foreach($MSQS_QL->qbo_rt_push_items as $rpi_key => $rpi_val):?>
													<?php
														$rpi_checked = '';
														if(is_array($rpi_val_arr) && in_array($rpi_key,$rpi_val_arr)){
															$rpi_checked = ' checked="checked"';
														}
													?>
													
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_rt_push_items[]" id="mw_wc_qbo_sync_rt_push_items" value="<?php echo $rpi_key;?>" <?php echo $rpi_checked;?>>
													&nbsp;<span class="rt_item_hd"><?php echo $rpi_val;?></span>
													<br /><br />
													<?php endforeach;?>
													<?php endif;?>										            
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose sync items (you can choose multiple items).','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr class="qb_wc_tr" height="50">
									<td colspan="3">
										<b><?php echo __('QuickBooks Online -> WooCommerce ','mw_wc_qbo_sync') ?></b>										
									</td>									
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Enable RealTime Sync','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_webhook_enable" id="mw_wc_qbo_sync_webhook_enable" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_webhook_enable']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to enable realtime changes into WooCommerce from QuickBooks Online.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('Data Types','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<!--
													<select multiple="multiple" name="mw_wc_qbo_sync_webhook_items[]" id="mw_wc_qbo_sync_webhook_items" class="filled-in production-option mw_wc_qbo_sync_select mqs_multi">
										            <option value=""></option>
													<?php //$MSQS_QL->only_option(explode(',',$admin_settings_data['mw_wc_qbo_sync_webhook_items']),$MSQS_QL->qbo_webhook_items)?>
										            </select>
													-->
													
													<?php if(is_array($MSQS_QL->qbo_webhook_items) && count($MSQS_QL->qbo_webhook_items)):?>
													<?php $qwi_val_arr = explode(',',$admin_settings_data['mw_wc_qbo_sync_webhook_items']);?>
													<?php foreach($MSQS_QL->qbo_webhook_items as $qwi_key => $qwi_val):?>
													<?php
														$qwi_checked = '';
														if(is_array($qwi_val_arr) && in_array($qwi_key,$qwi_val_arr)){
															$qwi_checked = ' checked="checked"';
														}
													?>
													
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_webhook_items[]" id="mw_wc_qbo_sync_webhook_items" value="<?php echo $qwi_key;?>" <?php echo $qwi_checked;?>>
													&nbsp;<span class="rt_item_hd"><?php echo $qwi_val;?></span>
													<br /><br />
													<?php endforeach;?>
													<?php endif;?>
													
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose sync items (you can choose multiple items).','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_seven_body" style="display: none;">
							<h6><?php echo __('Disable Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								<tr style="display:none;">
									<th class="title-description">
								    	<?php echo __('Disable Real-Time Push (Queue)','mw_wc_qbo_sync') ?>
								    	 
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_disable_realtime_sync" id="mw_wc_qbo_sync_disable_realtime_sync" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_disable_realtime_sync']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><font color="red">OFF: Default</font> <?php echo __('Check to disable real time data syncing. This will speed up WooCommerce operations slightly and sync data using a cron job you need to set up on the Cron Setup page.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Disable Sync Status Icons','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_disable_sync_status" id="mw_wc_qbo_sync_disable_sync_status" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_disable_sync_status']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to disable Sync Statuses in Push Pages (invoice and payment). This will speed up the loading of these pages.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Block Real Time Client Update','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_disable_realtime_client_update" id="mw_wc_qbo_sync_disable_realtime_client_update" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_disable_realtime_client_update']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to block the update of client profile information in QuickBooks Online when it is updated in WooCommerce.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_eight_body" style="display: none;">
							<h6><?php echo __('Advanced Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								<tr>
									<th class="title-description">
								    	<?php echo __('Enable Invoice Prefix','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_enable_invoice_prefix" id="mw_wc_qbo_sync_enable_invoice_prefix" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_enable_invoice_prefix']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wclp_qbo_sync') ?>
										  <span class="tooltiptext"><font color="red">OFF: Default</font> <?php echo __('Check to enable support for invoice prefixes. Only check this box if your WooCommerce invoices have custom prefixes.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Use QBO Invoice #s','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_qbo_invoice" id="mw_wc_qbo_sync_qbo_invoice" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_qbo_invoice']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><font color="red">OFF: Default</font> <?php echo __('Check to create WooCommerce Invoices with the next Invoice Number from QBO. Only check this box if you do not wish to use the WooCommerce numbering system.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
            				</tbody>
							</table>
							</div>

							<div id="mw_qbo_sybc_settings_tab_nine_body" style="display: none;">
							<h6><?php echo __('Miscellaneous Settings','mw_wc_qbo_sync') ?></h6>
							<table class="mw-qbo-sync-settings-table mwqs_setting_tab_body">
							<tbody>
								<tr>
									<th class="title-description">
								    	<?php echo __('WooCommerce Admin User','mw_wc_qbo_sync') ?>				    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<select name="mw_wc_qbo_sync_admin_email" id="mw_wc_qbo_sync_admin_email">
													<option value=""></option>
													<?php $MSQS_QL->get_admin_user_dropdown_list($admin_settings_data['mw_wc_qbo_sync_admin_email']); ?>
													</select>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Select admin email for daily report of syncing activity. This report is emailed when the module cron is run.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Email Log Daily','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_email_log" id="mw_wc_qbo_sync_email_log" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_email_log']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to email a daily report of syncing activity to the admin selected above. This report is emailed when the module cron is run.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr style="display: none;">
									<th class="title-description">
								    	<?php echo __('Auto Quick Refresh Daily','mw_wc_qbo_sync') ?>				    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_auto_refresh" id="mw_wc_qbo_sync_auto_refresh" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_auto_refresh']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to automatic quick refresh data on regular basis. This is done when the module cron is run.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
                				<tr>
									<th class="title-description">
								    	<?php echo __('Save Logs for Days','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<select name="mw_wc_qbo_sync_save_log_for" id="mw_wc_qbo_sync_save_log_for" class=" mw_wc_qbo_sync_select">
									            <?php $MSQS_QL->only_option($admin_settings_data['mw_wc_qbo_sync_save_log_for'],$MSQS_QL->log_save_days)?>
									            </select>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Choose how many days log entry you want to save','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr height="50">
									<td colspan="3">
										<b><?php echo __('Plugin Debug Settings','mw_wc_qbo_sync') ?></b>										
									</td>									
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('QuickBooks Error Add/Update Item Object, Request/Response Into Log File ','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_err_add_item_obj_into_log_file" id="mw_wc_qbo_sync_err_add_item_obj_into_log_file" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_err_add_item_obj_into_log_file']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Only for debug, Add QuickBooks Item Object into log file (last 24 hours).','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								
								<tr>
									<th class="title-description">
								    	<?php echo __('QuickBooks Success Add/Update Item Object, Request/Response Into Log File ','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_success_add_item_obj_into_log_file" id="mw_wc_qbo_sync_success_add_item_obj_into_log_file" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_success_add_item_obj_into_log_file']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Only for debug, Add QuickBooks Item Object into log file (last 24 hours).','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr height="50">
									<td colspan="3">
										<b><?php echo __('Update preference','mw_wc_qbo_sync') ?></b>										
									</td>									
								</tr>

								<tr>
									<th class="title-description">
								    	<?php echo __('Accept Beta Update?','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_update_option" id="mw_wc_qbo_sync_update_option" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_update_option']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Check to get beta version updates too. If not, it will only update stable versions.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>

								<tr height="50">
									<td colspan="3">
										<b><?php echo __('Plugin Dropdown Settings','mw_wc_qbo_sync') ?></b>										
									</td>									
								</tr>

								<tr>
									<th class="title-description">
								    	<?php echo __('Enable our Select2 searchable dropdown style for plugin dropdowns','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_select2_status" id="mw_wc_qbo_sync_select2_status" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_select2_status']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('This setting is on by default - to enable the Select2 dropdown style. Turn this off to display a normal dropdown for the plugin.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="title-description">
								    	<?php echo __('Enable AJAX search for Select2 Dropdowsn (customer and product)','mw_wc_qbo_sync') ?>
								    	
								    </th>
									<td>
										<div class="row">
											<div class="input-field col s12 m12 l12">
												<p>
													<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_sync_select2_ajax" id="mw_wc_qbo_sync_select2_ajax" value="true" <?php if($admin_settings_data['mw_wc_qbo_sync_select2_ajax']=='true') echo 'checked' ?>>
												</p>
											</div>
										</div>
									</td>
									<td>
										<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_sync') ?>
										  <span class="tooltiptext"><?php echo __('Enable AJAX search for our Select2 dropdown styles. This option is applicable if Select2 is enabled on above setting. This is efficient if your install has huge customer and product data lists.','mw_wc_qbo_sync') ?></span>
										</div>
									</td>
								</tr>
            				</tbody>
							</table>
							</div>
							
							
							<div class="mw_wc_qbo_sync_clear"></div>
							<div class="row">
								<div class="input-field col s12 m6 l4">
									<input type="submit" name="mw_wc_qbo_sync_settings" id="mw_wc_qbo_sync_settings" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save All">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</form>
</div>

<?php MyWorks_WC_QBO_Sync_Admin::set_setting_alert($save_status) ?>

<script type="text/javascript">
jQuery(document).ready(function($){
	<?php echo $list_selected;?>
	<?php if($selected_tab!=''):?>
	if(jQuery('#mw_qbo_sybc_settings_tab_<?php echo $selected_tab;?>').length >0) {
		jQuery('#mw_qbo_sybc_settings_tab_<?php echo $selected_tab;?>').trigger( "click" );
	}	
	<?php endif;?>
		
	jQuery('input.mwqs_st_chk').attr('data-size','small');
	jQuery('input.mwqs_st_chk').bootstrapSwitch();
});
</script>
<?php echo $MWQS_OF->get_select2_js('select','qbo_product');?>