<?php
if ( ! defined( 'ABSPATH' ) )
exit;

global $MSQS_QL;
global $MWQS_OF;
$tab = isset($_GET['tab'])?$_GET['tab']:'';
?>
<nav class="mw-qbo-sync-grey">
	<div class="nav-wrapper">
		<a class="brand-logo left" href="javascript:void(0)">
			<img src="<?php echo plugins_url( 'myworks-wc-qbo-sync/admin/image/mwd-logo.png' ) ?>">
		</a>
		<ul class="hide-on-med-and-down right">
			<?php if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')):?>
			<li class="cust-icon <?php if($tab=='customer' || !isset($_GET['tab'])) echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=customer') ?>">Customer</a>
			</li>
			<li class="ord-icon <?php if($tab=='invoice') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=invoice') ?>">Order</a>
			</li>
			<?php else:?>
			<li class="ord-icon <?php if($tab=='invoice' || !isset($_GET['tab'])) echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=invoice') ?>">Order</a>
			</li>
			<?php endif;?>
			
			<li class="pro-icon <?php if($tab=='product') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=product') ?>">Product</a>
			</li>
			
			<li class="var-icon <?php if($tab=='variation') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=variation') ?>">Variation</a>
			</li>
			
			<?php if($MSQS_QL->get_qbo_company_info('is_sku_enabled')):?>
			
			<li class="pro-icon <?php if($tab=='inventory') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=inventory') ?>">Inventory</a>
			</li>
			
			<?php endif;?>
			
			<?php if($MSQS_QL->get_qbo_company_info('is_category_enabled')):?>
			<li class="cat-icon <?php if($tab=='category') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=category') ?>">Category</a>
			</li>
			<?php endif;?>
			
			<?php if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')):?>
			<li class="pay-icon <?php if($tab=='payment') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-push&tab=payment') ?>">Payment</a>
			</li>
			<?php endif;?>
		</ul>
	</div>
</nav>