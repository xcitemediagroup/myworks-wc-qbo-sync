<?php
if ( ! defined( 'ABSPATH' ) )
     exit;
$tab = isset($_GET['tab'])?$_GET['tab']:'';
?>
<nav class="mw-qbo-sync-grey">
	<div class="nav-wrapper">
		<a class="brand-logo left" href="javascript:void(0)">
			<img src="<?php echo plugins_url( 'myworks-wc-qbo-sync/admin/image/mwd-logo.png' ) ?>">
		</a>
		<ul class="hide-on-med-and-down right">
			<?php if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')):?>
			<li class="cust-icon <?php if($tab=='customer' || !isset($_GET['tab'])) echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=customer') ?>">Customer</a>
			</li>			
			<li class="pay-icon <?php if($tab=='payment-method') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=payment-method') ?>">Payment Method</a>
			</li>			
			<?php else:?>			
			<li class="pay-icon <?php if($tab=='payment-method' | !isset($_GET['tab'])) echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=payment-method') ?>">Payment Method</a>
			</li>			
			<?php endif;?>
			
			<li class="pro-icon <?php if($tab=='product') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=product') ?>">Product</a>
			</li>
			<li class="tax-icon <?php if($tab=='tax-class') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=tax-class') ?>">Tax Class</a>
			</li>
			
			<?php if(!$MSQS_QL->get_qbo_company_setting('is_discount_allowed')):?>
			<li class="cou-icon <?php if($tab=='coupon-code') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=coupon-code') ?>">Coupon Code</a>
			</li>
			<?php endif;?>
			
			<?php if(!$MSQS_QL->get_qbo_company_setting('is_shipping_allowed')):?>
			<li class="ship-icon <?php if($tab=='shipping-method') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=shipping-method') ?>">Shipping Method</a>
			</li>
			<?php endif;?>
			
			<li class="cf-icon <?php if($tab=='custom-fields') echo 'active' ?>">
				<a href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-map&tab=custom-fields') ?>">Custom Fields</a>
			</li>
			
		</ul>
	</div>
</nav>