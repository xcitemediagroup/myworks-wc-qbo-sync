<?php
if ( ! defined( 'ABSPATH' ) )
exit;

$page_url = 'admin.php?page=myworks-wc-qbo-push&tab=product';
 
global $MWQS_OF;
global $MSQS_QL;
global $wpdb;

$MSQS_QL->set_per_page_from_url();
$items_per_page = $MSQS_QL->get_item_per_page();

$MSQS_QL->set_and_get('product_push_search');
$product_push_search = $MSQS_QL->get_session_val('product_push_search');

$total_records = $MSQS_QL->count_woocommerce_product_list($product_push_search);

$offset = $MSQS_QL->get_offset($MSQS_QL->get_page_var(),$items_per_page);
$pagination_links = $MSQS_QL->get_paginate_links($total_records,$items_per_page);

$wc_product_list = $MSQS_QL->get_woocommerce_product_list($product_push_search," $offset , $items_per_page");
$wc_currency_symbol = get_woocommerce_currency_symbol();
//$MSQS_QL->_p($wc_product_list);
?>
<div class="container">
	<div class="page_title"><h4><?php _e( 'Product Push', 'mw_wc_qbo_sync' );?></h4></div>
	<div class="card">
		<div class="card-content">

						<div class="col s12 m12 l12">

						        <div class="panel panel-primary">
						             <div class="mw_wc_filter">
									 <span class="search_text">Search</span>
									  &nbsp;
									  <input type="text" id="product_push_search" value="<?php echo $product_push_search;?>">
									  &nbsp;		
									  <button onclick="search_item();" class="btn btn-info">Filter</button>
									  &nbsp;
									  <button onclick="reset_item();" class="btn btn-info">Reset</button>
									  &nbsp;
									  <span class="filter-right-sec">
										  <span class="entries">Show entries</span>
										  &nbsp;
										  <select style="width:50px;" onchange="window.location='<?php echo $page_url;?>&<?php echo $MSQS_QL->per_page_keyword;?>='+this.value;">
											<?php echo  $MSQS_QL->only_option($items_per_page,$MSQS_QL->show_per_page);?>
										 </select>
									 </span>
									 </div>
									 <br />
									 <div class="row">
										<div class="input-field col s12 m12 14">
											<button id="push_selected_product_btn" class="waves-effect waves-light btn save-btn mw-qbo-sync-green"><?php echo __('Push Selected Products','mw_wc_qbo_sync')?></button>
											<button id="push_all_product_btn" class="waves-effect waves-light btn save-btn mw-qbo-sync-green"><?php echo __('Push All Products','mw_wc_qbo_sync')?></button>
											<button disabled="disabled" id="push_all_unsynced_product_btn" class="waves-effect waves-light btn save-btn mw-qbo-sync-green"><?php echo __('Push Un-synced Products','mw_wc_qbo_sync')?></button>
										</div>
									</div>
									 <br />

									<?php if(is_array($wc_product_list) && count($wc_product_list)):?>
									<div class="table-m">
										<table class="table" id="mwqs_product_push_table">
											<thead>
												<tr>
													<th width="2%">
													<input type="checkbox" onclick="mw_qbo_sync_check_all(this,'product_push_')">
													</th>
													<th width="5%">#</th>
													<th width="30%">Name</th>
													<th width="14%">SKU</th>
													<th width="8%">Price</th>
													<th width="9%">Manage Stock</th>
													<th width="5%">Stock</th>
													<th width="7%">Backorders</th>
													<th width="8%">Stock Status</th>
													<th width="8%">Total Sales</th>
													<th width="5%">&nbsp;</th>										
												</tr>
											</thead>
											<tbody>
											
											<?php foreach($wc_product_list as $p_val):?>
											<?php 
											$sync_status_html = '<i class="fa fa-times-circle" style="color:red"></i>';
											if((int) $p_val['quickbook_product_id']){
												$sync_status_html = '<i title="Mapped to #'.$p_val['quickbook_product_id'].'" class="fa fa-check-circle" style="color:green"></i>';
											}
											?>
											<tr>
												<td><input type="checkbox" id="product_push_<?php echo $p_val['ID']?>"></td>
												<td><?php echo $p_val['ID']?></td>
												<td><?php _e( $p_val['name'], 'mw_wc_qbo_sync' );?></td>
												<td><?php echo $p_val['sku'];?></td>
												<td>
												<?php
												echo $wc_currency_symbol;
												echo (isset($p_val['price']))?floatval($p_val['price']):'0.00';
												?>
												</td>
												
												<td><?php echo $p_val['manage_stock'];?></td>
												<td><?php echo number_format(floatval($p_val['stock']),2);?></td>
												<td><?php echo $p_val['backorders'];?></td>
												<td><?php echo $p_val['stock_status'];?></td>
												<td><?php echo $p_val['total_sales'];?></td>
												<td><?php echo $sync_status_html;?></td>
											</tr>
											<?php endforeach;?>									
											</tbody>
										</table>
									</div>
									<?php echo $pagination_links?>
									<?php else:?>
									<p><?php _e( 'No products found.', 'mw_wc_qbo_sync' );?></p>
									<?php endif;?>						           
						        </div>

						</div>
		</div>
	</div>
</div>
<?php $sync_window_url = $MSQS_QL->get_sync_window_url();?>
 <script type="text/javascript">
	function search_item(){		
		var product_push_search = jQuery('#product_push_search').val();
		if(product_push_search!=''){			
			window.location = '<?php echo $page_url;?>&product_push_search='+product_push_search;
		}else{
			alert('<?php echo __('Please enter search keyword.','mw_wc_qbo_sync')?>');
		}
	}

	function reset_item(){		
		window.location = '<?php echo $page_url;?>&product_push_search=';
	}
	
	jQuery(document).ready(function($) {
		var item_type = 'product';
		$('#push_selected_product_btn').click(function(){
			var item_ids = '';
			var item_checked = 0;
			
			jQuery( "input[id^='product_push_']" ).each(function(){
				if(jQuery(this).is(":checked")){
					item_checked = 1;
					var only_id = jQuery(this).attr('id').replace('product_push_','');
					only_id = parseInt(only_id);
					if(only_id>0){
						item_ids+=only_id+',';
					}					
				}
			});
			
			if(item_ids!=''){
				item_ids = item_ids.substring(0, item_ids.length - 1);
			}
			
			if(item_checked==0){
				alert('<?php echo __('Please select at least one item.','mw_wc_qbo_sync');?>');
				return false;
			}
			
			popUpWindow('<?php echo $sync_window_url;?>&sync_type=push&item_ids='+item_ids+'&item_type='+item_type,'mw_qs_product_push',0,0,650,350);
			return false;
		});
		
		$('#push_all_product_btn').click(function(){
			popUpWindow('<?php echo $sync_window_url;?>&sync_type=push&sync_all=1&item_type='+item_type,'mw_qs_product_push',0,0,650,350);
			return false;
		});
		
		$('#push_all_unsynced_product_btn').click(function(){
			popUpWindow('<?php echo $sync_window_url;?>&sync_type=push&sync_unsynced=1&item_type='+item_type,'mw_qs_product_push',0,0,650,350);
			return false;
		});
	});
 </script>
 <?php echo $MWQS_OF->get_tablesorter_js('#mwqs_product_push_table');?>