<?php
if ( ! defined( 'ABSPATH' ) )
     exit;
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/admin/partials
 */

 global $MWQS_OF;
 global $MSQS_QL;
 
 $page_url = 'admin.php?page=myworks-wc-qbo-sync-connection';
 
 if (! empty( $_POST['update_connection_data'] ) && check_admin_referer( 'myworks_wc_qbo_save_connection_data', 'update_connection_data' ) ) {

	 $mw_wc_qbo_sync_license_update = $MSQS_QL->var_p('mw_wc_qbo_sync_license_update');
	 $mw_wc_qbo_sync_license_update = $MSQS_QL->sanitize($mw_wc_qbo_sync_license_update);
	 
	 $mw_wc_qbo_sync_connection_number_update = $MSQS_QL->var_p('mw_wc_qbo_sync_connection_number');
	 //$mw_wc_qbo_sync_connection_number_update = $MSQS_QL->sanitize($mw_wc_qbo_sync_connection_number_update);
	 $mw_wc_qbo_sync_connection_number_update = (int) $mw_wc_qbo_sync_connection_number_update;
	 if(!$mw_wc_qbo_sync_connection_number_update){ $mw_wc_qbo_sync_connection_number_update=1;}
	 if($mw_wc_qbo_sync_connection_number_update>5){$mw_wc_qbo_sync_connection_number_update=5;}
	 
	 
	$mw_wc_qbo_sync_sandbox_mode_update = $MSQS_QL->var_p('mw_wc_qbo_sync_sandbox_mode');
	if($mw_wc_qbo_sync_sandbox_mode_update!='yes' && $mw_wc_qbo_sync_sandbox_mode_update!='no'){
		$mw_wc_qbo_sync_sandbox_mode_update = 'no';
	}
	
	
	
	update_option( 'mw_wc_qbo_sync_license', $mw_wc_qbo_sync_license_update );
	if($MSQS_QL->get_option('mw_wc_qbo_sync_license','')!=$mw_wc_qbo_sync_license_update){
		update_option( 'mw_wc_qbo_sync_localkey', '' );
	}
	update_option( 'mw_wc_qbo_sync_connection_number', $mw_wc_qbo_sync_connection_number_update );	
	update_option( 'mw_wc_qbo_sync_sandbox_mode', $mw_wc_qbo_sync_sandbox_mode_update );
	
	
	 $MSQS_QL->redirect($page_url);
 }
 
 if ( ! empty( $_POST['update_access_token'] ) && check_admin_referer( 'myworks_wc_qbo_save_access_token', 'update_access_token' ) ) {
	$mw_wc_qbo_sync_access_token_update = $MSQS_QL->var_p('mw_wc_qbo_sync_access_token');
	$mw_wc_qbo_sync_access_token_update = $MSQS_QL->sanitize($mw_wc_qbo_sync_access_token_update);
	
	update_option( 'mw_wc_qbo_sync_access_token', $mw_wc_qbo_sync_access_token_update );
	 $MSQS_QL->redirect($page_url);
 }
 

 $mw_wc_qbo_sync_license = $MSQS_QL->get_option('mw_wc_qbo_sync_license','');
 $mw_wc_qbo_sync_localkey = $MSQS_QL->get_option('mw_wc_qbo_sync_localkey','');
 
 $mw_wc_qbo_sync_sandbox_mode = $MSQS_QL->get_option('mw_wc_qbo_sync_sandbox_mode','');
 $mw_wc_qbo_sync_access_token = $MSQS_QL->get_option('mw_wc_qbo_sync_access_token','');
 
 $mw_wc_qbo_sync_connection_number = $MSQS_QL->get_option('mw_wc_qbo_sync_connection_number','');
  
 $sandbox = ($mw_wc_qbo_sync_sandbox_mode=='yes')?'&sandbox=1':'&sandbox=0';
 
 $wp_plugin_api_url = site_url('index.php?mw_qbo_sync_public_api=1');
 $wp_plugin_api_url = base64_encode($wp_plugin_api_url);
 
 $wp_plugin_qbo_webhook_url = site_url('index.php?mw_qbo_sync_public_qbo_webhooks=1');
 $wp_plugin_qbo_webhook_url = base64_encode($wp_plugin_qbo_webhook_url);
 
 $enable_webhook = ($MSQS_QL->option_checked('mw_wc_qbo_sync_webhook_enable'))?1:0;
 
 $extra_connection_params = $MWQS_OF->get_connection_iframe_extra_params();
 
 $local_connection_status_txt = '';
 
 //
 $deposit_ser_cron_data = '';
 if($MSQS_QL->is_connected()){
	 //	
	 $deposit_ser_cron_data = $MSQS_QL->get_dps_cron_ser_str();
	 
	 $Context = $MSQS_QL->getContext();
	 $realm = $MSQS_QL->getRealm();
	 
	 $CompanyInfoService = new QuickBooks_IPP_Service_CompanyInfo();
	 $quickbooks_CompanyInfo = $CompanyInfoService->get($Context, $realm);
	 //$MSQS_QL->_p($quickbooks_CompanyInfo);
	 $local_connection_status_txt = '<h5 style="color:green;">Connected</h5>';	
 }else{
	 $local_connection_status_txt = '<h5 style="color:red;">Not Connected</h5>';
 }
 
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="mw_wc_qbo_sync_container container conect-outer">
<?php if($MWQS_OF->is_valid_license($mw_wc_qbo_sync_license,$mw_wc_qbo_sync_localkey)):?>
<div class="mwqs_conection_options">
	<h4>
	Step 1:	<?php echo __('Configure License Key & Connection Settings','mw_wc_qbo_sync');?>
	</h4>
	
	<form method="post" action="<?php echo $page_url;?>">
	<table class="widefat fixed">
		<tr>
			<td width="20%"><label for="mw_wc_qbo_sync_license_update" class="mw_wc_qbo_sync_label">License Key:</label></td>
			<td  width="50%">
			<input class="mw_wc_qbo_sync_input" type="text" name="mw_wc_qbo_sync_license_update" id="mw_wc_qbo_sync_license_update" value="<?php echo $mw_wc_qbo_sync_license; ?>" required="required"/>
			&nbsp;<span class="mw_wc_qbo_sync_span"></span>
			</td>
			<td  width="30%"><p class="mw_wc_qbo_sync_paragraph">&nbsp;</p></td>
		</tr>
		<tr class="alternate">
			<td><label for="mw_wc_qbo_sync_connection_number" class="mw_wc_qbo_sync_label">Connection Number:</label></td>
			<td>
			<select class="mw_wc_qbo_sync_input" name="mw_wc_qbo_sync_connection_number" id="mw_wc_qbo_sync_connection_number">
			<?php echo $MSQS_QL->only_option($mw_wc_qbo_sync_connection_number,$MSQS_QL->wc_connection_num());?>
			</select>
			</td>
			<td>
			<p class="mw_wc_qbo_sync_paragraph">
			<?php echo __('Choose the connection number for this Woocommerce install. Default is 1. ONLY CHANGE if you\'re using this module on multiple Woocommerce installations - one number per install, up to 5.','mw_wc_qbo_sync');?>
			</p>
			</td>
		</tr>

		<tr>
			<td><label for="mw_wc_qbo_sync_sandbox_mode" class="mw_wc_qbo_sync_label">Sandbox Mode:</label></td>
			<td>
			<select class="mw_wc_qbo_sync_input" name="mw_wc_qbo_sync_sandbox_mode" id="mw_wc_qbo_sync_sandbox_mode">
			<?php echo $MSQS_QL->only_option($mw_wc_qbo_sync_sandbox_mode,$MSQS_QL->no_yes);?>
			</select>
			</td>
			<td>
			<p class="mw_wc_qbo_sync_paragraph">
			<?php echo __('We recommend you set this to NO, unless you are using sandbox.qbo.intuit.com','mw_wc_qbo_sync');?>
			</p>
			</td>
		</tr>
		
	</table>
	
	<br />
	
	<div class="mw_wc_qbo_sync_clear"></div>
	
	<?php wp_nonce_field( 'myworks_wc_qbo_save_connection_data', 'update_connection_data' ); ?>
	<input type="submit" name="mwqs_con_btn" class="button button-primary button-large mw_wc_qbo_sync_submit" value="Save" />
	
	</form>
</div>

<div class="mwqs_conection_frame_cont">
	<h4>
	Step 2:	<?php echo __('Login to your MyWorks.Design account - then Connect to QuickBooks Online','mw_wc_qbo_sync');?>
	</h4>
	<iframe src="<?php echo $MSQS_QL->get_quickbooks_connection_dashboard_url();?>/wc-qbo-connection.php?iframe=1&connection=<?php echo (int) $mw_wc_qbo_sync_connection_number;?>&wp_plugin_api_url=<?php echo $wp_plugin_api_url;?>&wp_plugin_qbo_webhook_url=<?php echo $wp_plugin_qbo_webhook_url;?>&enable_webhook=<?php echo $enable_webhook;?><?php echo $sandbox;?><?php echo $extra_connection_params;?>&deposit_data=<?php echo $deposit_ser_cron_data;?>" height="900" width="100%"></iframe>
</div>

<div class="mwqs_rp_cont">
	<h4>
		Step 3:	<?php echo __('Refresh Page','mw_wc_qbo_sync');?>
	</h4>
	<button class="button button-primary button-large" onclick="window.location.reload();">
	<?php echo __('Reload','mw_wc_qbo_sync');?>
	</button>
</div>

<div class="mwqs_at_cont">
	<h4>
	Step 4:	<?php echo __('Copy Access Token from above if blank','mw_wc_qbo_sync');?>
	</h4>
	
	<form method="post" action="<?php echo $page_url;?>">
	<table class="widefat fixed">
		<tr>
			<td><label for="mw_wc_qbo_sync_access_token" class="mw_wc_qbo_sync_label">Access Token:</label></td>
			<td>
			<input class="mw_wc_qbo_sync_input" type="text" name="mw_wc_qbo_sync_access_token" id="mw_wc_qbo_sync_access_token" value="<?php echo $mw_wc_qbo_sync_access_token; ?>" required="required"/>
			&nbsp;<span class="mw_wc_qbo_sync_span"></span>
			</td>
			<td><p class="mw_wc_qbo_sync_paragraph">&nbsp;</p></td>
		</tr>
	</table>
	
	<br />
	
	<div class="mw_wc_qbo_sync_clear"></div>
	
	<?php wp_nonce_field( 'myworks_wc_qbo_save_access_token', 'update_access_token' ); ?>
	<input type="submit" name="mwqs_at_btn" class="button button-primary button-large mw_wc_qbo_sync_submit" value="Save" />
	</form>
</div>

<div class="mwqs_conection_local_info">
	<h4>	
	<?php echo __('Local QuickBooks Online Connection Info','mw_wc_qbo_sync');?>
	</h4>
	
	<div class="cnctin-infrmtionarea">	
	<?php echo $local_connection_status_txt;?>
	<div class="com-add">
	<?php  if($MSQS_QL->is_connected()):?>
	<p>Realm: <?php print($realm); ?></p>
	<?php  if(isset($quickbooks_CompanyInfo) && $quickbooks_CompanyInfo):?>
	<p>Company: 
	<?php
	if($quickbooks_CompanyInfo->countCompanyName()){
		print($quickbooks_CompanyInfo->getCompanyName());
	}	 
	?>
	</p>
	<p>Email: 
	<?php
	if($quickbooks_CompanyInfo->countEmail()){		
		if(is_object($quickbooks_CompanyInfo->getEmail()) && $quickbooks_CompanyInfo->getEmail()->countAddress()){
			print($quickbooks_CompanyInfo->getEmail()->getAddress());
		}
	}	
	?>
	</p>
	<p>Country: 
	<?php
	if($quickbooks_CompanyInfo->countCountry()){
		print($quickbooks_CompanyInfo->getCountry());
	}	 
	?>
	</p>
	<?php else:?>
	<p style="color:red;"><?php echo __('QuickBooks online company info not found.','mw_wc_qbo_sync');?></p>
	<?php endif;?>
	
	<?php endif;?>
	</div>

	<br />
	<b style="font-size:16px;">
	<?php echo __('Reload the connection page after connect, disconect or reconnect to view real time connection status','mw_wc_qbo_sync');?>
	</b>
	
	</div>
	
</div>

<?php elseif($MWQS_OF->get_license_status()=='Invalid'):?>
<p><?php echo __('You have entered an invalid license key. Please enter a valid license key in order to use the plugin.','mw_wc_qbo_sync');?></p>
<?php elseif($MWQS_OF->get_license_status()=='Expired'):?>
<p><?php echo __('Your license key is expired. Please renew your license with us or enter a valid license key in order to continue to use the plugin.','mw_wc_qbo_sync');?></p>
<?php elseif($MWQS_OF->get_license_status()=='Suspended'):?>
<p><?php echo __('Your license key is suspended. Please contact us to unsuspend your license or enter a valid license key in order to continue to use the plugin.','mw_wc_qbo_sync');?></p>
<?php else:?>
<p><?php echo __('Please enter a valid license key in order to use the plugin.','mw_wc_qbo_sync');?></p>
<?php endif;?>

<?php if($MWQS_OF->get_license_status()!='Active'):?>

<div class="mwqs_conection_license_check">
<form method="post" id="myworks_wc_qbo_sync_check_license">
	<label for ="mw_wc_qbo_sync_license">License Key: </label>
	<input type="text" name="mw_wc_qbo_sync_license" id="mw_wc_qbo_sync_license" value="<?php echo $mw_wc_qbo_sync_license;?>">
	 <?php wp_nonce_field( 'myworks_wc_qbo_sync_check_license', 'check_plugin_license' ); ?>
	<input size="30" type="submit" value="Enter" class="button button-primary">
	<span id="mwqs_license_chk_loader" style="visibility:hidden;">
	<img src="<?php echo esc_url( plugins_url( 'image/ajax-loader.gif', dirname(__FILE__) ) );?>" alt="Loading..." />
	</span>
</form>
</div>

<?php endif;?>
</div>