<?php
if ( ! defined( 'ABSPATH' ) )
exit;

MyWorks_WC_QBO_Sync_Admin::is_trial_version_check();
global $MSQS_QL;
global $MWQS_OF;
?>
<?php require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-nav.php' ?>

<?php
$tab = isset($_GET['tab']) ? $_GET['tab'] : '' ;

if($tab=='customer'){
	if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-customer.php';
	}	
}else if($tab=='invoice'){
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-invoice.php';
}else if($tab=='product'){
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-product.php';
}else if($tab=='variation'){
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-variation.php';
}else if($tab=='inventory'){
	if($MSQS_QL->get_qbo_company_info('is_sku_enabled')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-inventory.php';
	}
}else if($tab=='category'){
	if($MSQS_QL->get_qbo_company_info('is_category_enabled')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-category.php';
	}	
}else if($tab=='payment'){
	if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
		echo '<h4>'.__('WooCommerce Orders as Sales Receipts Option Enabled in Settings. ','mw_wc_qbo_sync'). '</h4>';
	}else{
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-payment.php';
	}	
}else{
	if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-customer.php';
	}else{
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-push-invoice.php';
	}
}
?>