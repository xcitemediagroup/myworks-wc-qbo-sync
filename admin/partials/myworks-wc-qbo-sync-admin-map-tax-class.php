<?php
if ( ! defined( 'ABSPATH' ) )
exit;

global $MWQS_OF;
global $MSQS_QL;
global $wpdb;

$page_url = 'admin.php?page=myworks-wc-qbo-map&tab=tax-class';

if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_map_wc_qbo_tax', 'map_wc_qbo_tax' ) ) {
	$item_ids = array();
	$item_ids_combo = array();
	foreach ($_POST as $key=>$value){
		if ($MSQS_QL->start_with($key, "wtax_")){
			$id = (int) str_replace("wtax_", "", $key);			
			if($id && (int) $value){ 
				$item_ids[$id] = (int) $value;
			}
		}
		
		if ($MSQS_QL->start_with($key, "cobmbo_wtax_")){
			$id = (int) str_replace("cobmbo_wtax_", "", $key);			
			if($id && (int) $value){
				$item_ids_combo[$id] = (int) $value;
			}
		}	
	}
	//$MSQS_QL->_p($item_ids);die;
	$is_tax_saved = false;
	$table = $wpdb->prefix.'mw_wc_qbo_sync_tax_map';
	$wpdb->query("DELETE FROM `".$table."` WHERE `id` > 0 ");
	$wpdb->query("TRUNCATE TABLE `".$table."` ");
	if(count($item_ids)){
		foreach ($item_ids as $key=>$value){
			$save_data = array();
			$save_data['wc_tax_id'] = $key;
			$save_data['qbo_tax_code'] = $value;
			$save_data['wc_tax_id_2'] = 0;
						
			$wpdb->insert($table, $save_data);
		}
		$is_tax_saved = true;		
	}
	if(count($item_ids_combo)){		
		foreach ($item_ids_combo as $key=>$value){
			$save_data = array();
			$save_data['wc_tax_id'] = $key;
			$save_data['qbo_tax_code'] = $value;
			$save_data['wc_tax_id_2'] = (isset($_POST['sc_wtax_'.$key]))?$_POST['sc_wtax_'.$key]:0;
			$wpdb->insert($table, $save_data);
		}
		$is_tax_saved = true;
	}
	if($is_tax_saved){
		$MSQS_QL->set_session_val('map_page_update_message',__('Tax rates mapped successfully.','mw_wc_qbo_sync'));
	}
	$MSQS_QL->redirect($page_url);
}

//$wc_tax_classes = WC_Tax::get_tax_classes();
$wc_tax_rates = $MSQS_QL->get_tbl($wpdb->prefix.'woocommerce_tax_rates','','','tax_rate_class ASC');
$qbo_tax_options = '<option value=""></option>';
$qbo_tax_options.=$MSQS_QL->get_tax_code_dropdown_list();

$selected_options_script = '';
$wc_all_tax_rates = $MSQS_QL->get_wc_tax_rate_id_array($wc_tax_rates);
$tm_map_data = $MSQS_QL->get_tbl($wpdb->prefix.'mw_wc_qbo_sync_tax_map');
if(is_array($tm_map_data) && count($tm_map_data)){
	foreach($tm_map_data as $tm_k=>$tm_val){
		if($tm_val['wc_tax_id_2']>0){
			$tl_tax_rate_class = (isset($wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_class']))?$wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_class']:'';
			$tl_tax_rate_class = ($tl_tax_rate_class=='')?'Standard rate':ucfirst(str_replace('-',' ',$tl_tax_rate_class));
			$tl_country = (isset($wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_country']))?$wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_country']:'';
			$tl_state = (isset($wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_state']))?$wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate_state']:'';
			$tl_taxrate = (isset($wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate']))?$wc_all_tax_rates[$tm_val['wc_tax_id']]['tax_rate']:'';
			
			$selected_options_script.='jQuery(\'#sc_wtax_'.$tm_val['wc_tax_id'].'\').val(\''.$tm_val['wc_tax_id_2'].'\');';
			$selected_options_script.='jQuery(\'#cobmbo_wtax_'.$tm_val['wc_tax_id'].'\').val(\''.$tm_val['qbo_tax_code'].'\');';
			
			$selected_options_script.='jQuery(\'#tl_tax_rate_class_'.$tm_val['wc_tax_id'].'\').html(\''.$tl_tax_rate_class.'\');';
			$selected_options_script.='jQuery(\'#tl_country_'.$tm_val['wc_tax_id'].'\').html(\''.$tl_country.'\');';
			$selected_options_script.='jQuery(\'#tl_state_'.$tm_val['wc_tax_id'].'\').html(\''.$tl_state.'\');';
			$selected_options_script.='jQuery(\'#tl_taxrate_'.$tm_val['wc_tax_id'].'\').html(\''.$tl_taxrate.'\');';
		}else{
			$selected_options_script.='jQuery(\'#wtax_'.$tm_val['wc_tax_id'].'\').val(\''.$tm_val['qbo_tax_code'].'\');';
		}		
	}	
}

?>
<?php require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-nav.php' ?>
<div class="container map-tax-class-outer">
	<div class="page_title"><h4><?php _e( 'Tax Mappings', 'mw_wc_qbo_sync' );?></h4></div>
	<div class="card">
		<div class="card-content">
			<div class="row">
				<?php if(is_array($wc_tax_rates) && count($wc_tax_rates)):?>
				<form method="POST" class="col s12 m12 l12" action="<?php echo $page_url;?>">
					<div class="row">
						<div class="col s12 m12 l12">
							<table class="mw-qbo-sync-map-table menu-blue-bg" width="100%">
                            	<thead>
                                	<tr>
                                    	<th width="5%" class="title-description">
											ID							    	
										</th>
                                        <th width="25%" class="title-description">
											Tax	Name							    	
                                        </th>
                                        <th width="20%" class="title-description">
                                            Tax	Class						    	
                                        </th>
                                        <th width="10%" class="title-description">
                                            Country								    	
                                        </th>
                                        <th width="10%" class="title-description">
                                            State								    	
                                        </th>
                                        <th width="10%" class="title-description">
                                            Rate								    	
                                        </th>
                                        <th width="20%" class="title-description">
                                            Quickbooks Tax
                                        </th>
                                    </tr>
                                </thead>

								<?php 
								foreach($wc_tax_rates as $rates):
								$tax_rate_class = ($rates['tax_rate_class']=='')?'Standard rate':ucfirst(str_replace('-',' ',$rates['tax_rate_class']));
								?>
								<tr>
									<td><?php echo $rates['tax_rate_id'];?></td>
									<td><?php echo $rates['tax_rate_name'];?></td>
									<td><?php echo $tax_rate_class;?></td>
									<td><?php echo $rates['tax_rate_country'];?></td>
									<td><?php echo $rates['tax_rate_state'];?></td>
									<td><?php echo $rates['tax_rate'];?></td>
									<td>
									<select class="mw_wc_qbo_sync_select2 qbo_select sc_sel_tx" name="wtax_<?php echo $rates['tax_rate_id'];?>" id="wtax_<?php echo $rates['tax_rate_id'];?>">
									<?php echo $qbo_tax_options;?>
									</select>							
									</td>
								</tr>
								<tr id="sc_tx_row_<?php echo $rates['tax_rate_id'];?>">
									<td>+&nbsp;</td>
									<td>
									<?php echo $rates['tax_rate_name'];?><br />
									<select class="qbo_select mw_wc_qbo_sync_select2 sc_sel_tx" name="sc_wtax_<?php echo $rates['tax_rate_id'];?>" id="sc_wtax_<?php echo $rates['tax_rate_id'];?>">
										<?php echo $MSQS_QL->get_wc_tax_rate_dropdown($wc_tax_rates,'',$rates['tax_rate_id']);?>
									</select>
									</td>
									
									<td id="tl_tax_rate_class_<?php echo $rates['tax_rate_id'];?>"></td>
									<td id="tl_country_<?php echo $rates['tax_rate_id'];?>"></td>
									<td id="tl_state_<?php echo $rates['tax_rate_id'];?>"></td>
									<td id="tl_taxrate_<?php echo $rates['tax_rate_id'];?>"></td>
									<td>
										<select class="qbo_select mw_wc_qbo_sync_select2" name="cobmbo_wtax_<?php echo $rates['tax_rate_id'];?>" id="cobmbo_wtax_<?php echo $rates['tax_rate_id'];?>">
											<?php echo $qbo_tax_options;?>
										</select>
									</td>
								</tr>
								<?php endforeach;?>
							</table>
						</div>
					</div>
					<div class="row">
						<?php wp_nonce_field( 'myworks_wc_qbo_sync_map_wc_qbo_tax', 'map_wc_qbo_tax' ); ?>
						<div class="input-field col s12 m6 l4">
							<button class="waves-effect waves-light btn save-btn mw-qbo-sync-green">Save</button>
						</div>
					</div>
				</form>
				<?php else:?>
				<p><?php _e( 'No tax found.', 'mw_wc_qbo_sync' );?></p>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery('.sc_sel_tx').change(function(){			
			var p_tx = jQuery(this).attr('id');
			p_tx = p_tx.replace('sc_wtax_','');
			
			var tx_val = $('option:selected', this).val();
					
			if(tx_val!=''){				
				var tax_rate_class = $('option:selected', this).attr('data-tax_rate_class');
				var tx_country = $('option:selected', this).attr('data-tax_rate_country');
				var tx_state = $('option:selected', this).attr('data-tax_rate_state');
				var tx_taxrate = $('option:selected', this).attr('data-tax_rate');
				
				jQuery('#tl_tax_rate_class_'+p_tx).html(tax_rate_class);
				jQuery('#tl_country_'+p_tx).html(tx_country);
				jQuery('#tl_state_'+p_tx).html(tx_state);
				jQuery('#tl_taxrate_'+p_tx).html(tx_taxrate);
			}else{
				jQuery('#tl_tax_rate_class_'+p_tx).html('');
				jQuery('#tl_country_'+p_tx).html('');
				jQuery('#tl_state_'+p_tx).html('');
				jQuery('#tl_taxrate_'+p_tx).html('');
			}
		});
		<?php if($selected_options_script!=''):?>		
			<?php echo $selected_options_script;?>		
		<?php endif;?>
	});				
</script>
<?php echo $MWQS_OF->get_select2_js('.mw_wc_qbo_sync_select2');?>