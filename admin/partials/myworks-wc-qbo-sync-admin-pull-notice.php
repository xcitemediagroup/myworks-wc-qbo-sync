<?php
if ( ! defined( 'ABSPATH' ) )
exit;
?>

<h4><?php _e( 'Please complete initial setup or check the enable pull option in module settings to access this page.', 'mw_wc_qbo_sync' );?></h4>