<?php
if ( ! defined( 'ABSPATH' ) )
exit;
global $MSQS_QL;

MyWorks_WC_QBO_Sync_Admin::is_trial_version_check();
MyWorks_WC_QBO_Sync_Admin::get_settings_assets(1);
$tab = isset($_GET['tab']) ? $_GET['tab'] : '';

if($tab=='customer'){
	if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-customer.php';
	}	
}elseif($tab=='payment-method'){ 
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-payment-method.php'; 
}elseif($tab=='product'){
	if(isset($_GET['variation'])){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-variation.php';
	}else{
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-product.php'; 
	}	
}elseif($tab=='tax-class'){ 
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-tax-class.php'; 
}elseif($tab=='custom-fields'){
	require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-custom-fields.php'; 
}elseif($tab=='shipping-method'){
	if(!$MSQS_QL->get_qbo_company_setting('is_shipping_allowed')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-shipping-method.php';
	}	 
}elseif($tab=='coupon-code'){
	if(!$MSQS_QL->get_qbo_company_setting('is_discount_allowed')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-coupon-code.php';
	}	 
}else{	
	if(!$MSQS_QL->option_checked('mw_wc_qbo_sync_orders_to_specific_cust_opt')){
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-customer.php'; 
	}else{
		require_once plugin_dir_path( __FILE__ ) . 'myworks-wc-qbo-sync-admin-map-payment-method.php';
	}	
}

if($save_status = $MSQS_QL->get_session_val('map_page_update_message','',true)){
	$save_status = ($save_status!='')?$save_status:'error';
	MyWorks_WC_QBO_Sync_Admin::set_setting_alert($save_status);
}