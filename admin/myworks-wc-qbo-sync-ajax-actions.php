<?php
if ( ! defined( 'ABSPATH' ) )
     exit;
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
function myworks_wc_qbo_sync_check_license(){	
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_check_license', 'check_plugin_license' ) ) {
		// process form data
		global $MWQS_OF;
		global $MSQS_QL;
		$mw_wc_qbo_sync_localkey = get_option('mw_wc_qbo_sync_localkey','');
		$mw_wc_qbo_sync_localkey = $MSQS_QL->sanitize($mw_wc_qbo_sync_localkey);
		
		$mw_wc_qbo_sync_license =  $MWQS_OF->var_p('mw_wc_qbo_sync_license');
		$mw_wc_qbo_sync_license = $MSQS_QL->sanitize($mw_wc_qbo_sync_license);
		
		if($MWQS_OF->is_valid_license($mw_wc_qbo_sync_license,$mw_wc_qbo_sync_localkey)){
			echo 'License Activated';
		}else{
			echo 'Invalid License key';
		}		
	}
	wp_die();
}

function mw_wc_qbo_sync_refresh_log_chart(){
	global $MSQS_QL;
	$vp = $MSQS_QL->var_p('period');
	$vp  = $MSQS_QL->sanitize($vp);
	$MSQS_QL->set_session_val('dashboard_graph_period',$vp);
	echo $MSQS_QL->get_log_chart_output($vp);
	wp_die();
}

function mw_wc_qbo_sync_window(){
	global $MSQS_QL;
	global $MSQS_AD;
	//
	global $wpdb;
	
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_window', 'window_qbo_sync' ) ) {
		$sync_type = $MSQS_QL->var_p('sync_type');
		$item_type = $MSQS_QL->var_p('item_type');
		$id = (int) $MSQS_QL->var_p('id');
		$cur_item = (int) $MSQS_QL->var_p('cur_item');
		$tot_item = (int ) $MSQS_QL->var_p('tot_item');
		
		$check_sync_valid = true;
		if($sync_type!='push' && $sync_type!='pull'){
			$check_sync_valid = false;
		}
		if($item_type!='customer' && $item_type!='invoice' && $item_type!='payment' && $item_type!='product' && $item_type!='inventory' && $item_type!='category' && $item_type!='variation'){
			$check_sync_valid = false;
		}
		if(!$id || !$cur_item || !$tot_item){
			$check_sync_valid = false;
		}
		
		if($check_sync_valid){
			try{
				$key =  $cur_item;		  
				$per = $key/$tot_item*100;
				$per = ceil($per);			
				$msg = '';
				
				//Push
				if($sync_type=='push'){
					if($item_type=='customer'){
						$return_id = $MSQS_AD->myworks_wc_qbo_sync_registration_realtime(array('user_id'=>$id));
						$manual_push_update = $MSQS_QL->get_session_val('sync_window_push_manual_update',false,true);
						
						//$manual_push_session_err_msg = $MSQS_QL->get_session_msg('manual_push_session_err_msg','error');
						
						if($return_id){
							if($manual_push_update){
								$msg = "<span style='color:green;'>Customer #$id has been updated, QuickBooks customer id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Customer #$id has been pushed, QuickBooks customer id #$return_id</span>";
							}							
						}else{
							if($manual_push_update){
								$msg = "<span style='color:red;'>There was an error updating customer #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error pushing customer #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='invoice'){											
						$return_id = $MSQS_AD->myworks_wc_qbo_sync_order_realtime(array('order_id'=>$id));
						$manual_push_update = $MSQS_QL->get_session_val('sync_window_push_manual_update',false,true);
						//07-06-2017
						$wc_inv_no = '';
						if($MSQS_QL->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers') && $MSQS_QL->option_checked('mw_wc_qbo_sync_compt_p_wsnop')){
							$wc_inv_no = get_post_meta( $id, '_order_number_formatted', true );
						}
						$ord_id_num = ($wc_inv_no!='')?$wc_inv_no:$id;
						
						//23-05-2017
						$rf_data = $MSQS_QL->get_data("SELECT ID FROM `{$wpdb->posts}` WHERE `post_type` = 'shop_order_refund' AND `post_parent` = {$id} ORDER BY ID ASC ");
						//$MSQS_QL->_p($rf_data);
						if(is_array($rf_data) && count($rf_data)){
							foreach($rf_data as $rfd){
								$refund_id = (int) $rfd['ID'];
								if($refund_id){
									$MSQS_AD->mw_wc_qbo_sync_woocommerce_order_refunded(array('order_id'=>$id),$refund_id);
								}								
							}							
						}
						
						//$manual_push_session_err_msg = $MSQS_QL->get_session_msg('manual_push_session_err_msg','error');
						
						if($return_id){
							if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
								if($manual_push_update){
									$msg = "<span style='color:green;'>Order #$ord_id_num has been updated, QuickBooks salesreceipt id #$return_id</span>";
								}else{
									$msg = "<span style='color:green;'>Order #$ord_id_num has been pushed, QuickBooks salesreceipt id #$return_id</span>";
								}								
							}else{
								if($manual_push_update){
									$msg = "<span style='color:green;'>Order #$ord_id_num has been updated, QuickBooks invoice id #$return_id</span>";
								}else{
									$msg = "<span style='color:green;'>Order #$ord_id_num has been pushed, QuickBooks invoice id #$return_id</span>";
								}								
							}						
						}else{
							if($MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
								if($manual_push_update){
									$msg = "<span style='color:red;'>There was an error updating salesreceipt #$ord_id_num ,please check log</span>";
								}else{
									$msg = "<span style='color:red;'>There was an error pushing order #$ord_id_num ,please check log</span>";
								}
							}else{
								if($manual_push_update){
									$msg = "<span style='color:red;'>There was an error updating invoice #$ord_id_num ,please check log</span>";
								}else{
									$msg = "<span style='color:red;'>There was an error pushing order #$ord_id_num ,please check log</span>";
								}
							}
														
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='product'){									
						$return_id = $MSQS_AD->mw_qbo_wc_product_save(array('product_id'=>$id));
						$manual_push_update = $MSQS_QL->get_session_val('sync_window_push_manual_update',false,true);
						if($return_id){
							if($manual_push_update){
								$msg = "<span style='color:green;'>Product #$id has been updated, QuickBooks product id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Product #$id has been pushed, QuickBooks product id #$return_id</span>";
							}							
						}else{
							if($manual_push_update){
								$msg = "<span style='color:red;'>There was an error updating product #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error pushing product #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='variation'){							
						$return_id = $MSQS_AD->mw_qbo_wc_variation_save(array('variation_id'=>$id));
						$manual_push_update = $MSQS_QL->get_session_val('sync_window_push_manual_update',false,true);
						if($return_id){
							if($manual_push_update){
								$msg = "<span style='color:green;'>Variation #$id has been updated, QuickBooks product id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Variation #$id has been pushed, QuickBooks product id #$return_id</span>";
							}							
						}else{
							if($manual_push_update){
								$msg = "<span style='color:red;'>There was an error updating variation #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error pushing variation #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='payment' && !$MSQS_QL->option_checked('mw_wc_qbo_sync_order_as_sales_receipt')){
						$return_id = $MSQS_AD->mw_qbo_wc_order_payment(array('payment_id'=>$id));						
						if($return_id){
							$msg = "<span style='color:green;'>Payment #$id has been pushed, QuickBooks payment id #$return_id</span>";
						}else{
							$msg = "<span style='color:red;'>There was an error pushing payment #$id ,please check log</span>";
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);				
					}
					
					//25-04-2017
					if($item_type=='category'){						
						$return_id = $MSQS_AD->myworks_wc_qbo_sync_product_category_realtime(array('category_id'=>$id));
						$manual_push_update = $MSQS_QL->get_session_val('sync_window_push_manual_update',false,true);
						if($return_id){
							if($manual_push_update){
								$msg = "<span style='color:green;'>Category #$id has been updated, QuickBooks category id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Category #$id has been pushed, QuickBooks category id #$return_id</span>";
							}							
						}else{
							if($manual_push_update){
								$msg = "<span style='color:red;'>There was an error pushing category #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error updating category #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='inventory'){
						$return_id = $MSQS_QL->UpdateQboInventory(array('wc_inventory_id'=>$id,'manual'=>true));						
						
						if($return_id){
							$msg = "<span style='color:green;'>Inventory #$id has been updated, QuickBooks inventory id #$return_id</span>";
						}else{
							$msg = "<span style='color:red;'>There was an error updating inventory #$id ,please check log</span>";
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
				}
				
				//Pull
				if($sync_type=='pull' && $MSQS_QL->option_checked('mw_wc_qbo_sync_pull_enable')){
					if($item_type=='inventory'){
						$return_id = $MSQS_QL->UpdateWooCommerceInventory(array('qbo_inventory_id'=>$id,'manual'=>true));						
						
						if($return_id){
							$msg = "<span style='color:green;'>Inventory #$id has been updated, WooCommerce product id #$return_id</span>";
						}else{
							$msg = "<span style='color:red;'>There was an error updating inventory #$id ,please check log</span>";
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='product'){
						$return_id = $MSQS_QL->Qbo_Pull_Product(array('qbo_product_id'=>$id,'manual'=>true));
						$manual_pull_update = $MSQS_QL->get_session_val('sync_window_pull_manual_update',false,true);
						
						if($return_id){
							if($manual_pull_update){
								$msg = "<span style='color:green;'>Product #$id has been updated, WooCommerce product id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Product #$id has been imported, WooCommerce product id #$return_id</span>";
							}							
						}else{
							if($manual_pull_update){
								$msg = "<span style='color:red;'>There was an error importing product #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error updating product #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
					
					if($item_type=='category'){
						$return_id = $MSQS_QL->Qbo_Pull_Category(array('qbo_category_id'=>$id,'manual'=>true));
						$manual_pull_update = $MSQS_QL->get_session_val('sync_window_pull_manual_update',false,true);
						
						if($return_id){
							if($manual_pull_update){
								$msg = "<span style='color:green;'>Category #$id has been updated, WooCommerce category id #$return_id</span>";
							}else{
								$msg = "<span style='color:green;'>Category #$id has been imported, WooCommerce category id #$return_id</span>";
							}							
						}else{
							if($manual_pull_update){
								$msg = "<span style='color:red;'>There was an error updating category #$id ,please check log</span>";
							}else{
								$msg = "<span style='color:red;'>There was an error importing category #$id ,please check log</span>";
							}							
						}
						$MSQS_QL->show_sync_window_message($key, $msg , $per, $tot_item);
					}
				}
				
			}catch (Exception $e) {
				$Exception = $e->getMessage();
			}
		}
	}
	wp_die();
}

function mw_wc_qbo_sync_clear_all_mappings(){	
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_clear_all_mappings', 'clear_all_mappings' ) ) {		
		global $wpdb;
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_customer_pairs` ");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_product_pairs` ");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_paymentmethod_map` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_paymentmethod_map` ");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_promo_code_product_map` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_promo_code_product_map` ");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_shipping_product_map` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_shipping_product_map` ");
		
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_tax_map` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_tax_map` ");
		
		//23-05-2017
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_wq_cf_map` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_wq_cf_map` ");
		
		//23-06-2017
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_variation_pairs` ");
		
		if(isset($_POST['payment_map_delete'])){
			$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_payment_id_map` WHERE `id` > 0 ");
			$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_payment_id_map` ");
		}
		echo 'Success';
	}
	wp_die();
}

function mw_wc_qbo_sync_automap_customers(){
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_automap_customers', 'automap_customers' ) ) {
		global $MSQS_QL;
		$map_count = (int) $MSQS_QL->AutoMapCustomerNew();
		//echo 'Success';
		echo 'Total Customer Mapped: '.$map_count;
	}	
	wp_die();
}

function mw_wc_qbo_sync_automap_products(){
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_automap_products', 'automap_products' ) ) {
		global $MSQS_QL;
		$map_count = (int) $MSQS_QL->AutoMapProductNew();
		//echo 'Success';
		echo 'Total Product Mapped: '.$map_count;
	}	
	wp_die();
}

//26-04-2017
function mw_wc_qbo_sync_automap_variations(){
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_automap_variations', 'automap_variations' ) ) {
		global $MSQS_QL;
		$map_count = (int) $MSQS_QL->AutoMapVariation();
		//echo 'Success';
		echo 'Total Variation Mapped: '.$map_count;
	}	
	wp_die();
}

function mw_wc_qbo_sync_clear_all_logs(){
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_clear_all_logs', 'mwqs_clear_all_logs' ) ) {
		global $wpdb;	
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE `id` > 0 ");
		$wpdb->query("TRUNCATE TABLE `".$wpdb->prefix."mw_wc_qbo_sync_log` ");
		echo 'Success';
	}	
	wp_die();
}

function mw_wc_qbo_sync_clear_all_log_errors(){
	if ( ! empty( $_POST ) && check_admin_referer( 'myworks_wc_qbo_sync_clear_all_log_errors', 'mwqs_clear_all_log_errors' ) ) {
		global $wpdb;
		$wpdb->query("DELETE FROM `".$wpdb->prefix."mw_wc_qbo_sync_log` WHERE `success` = 0 ");
		echo 'Success';
	}	
	wp_die();
}
